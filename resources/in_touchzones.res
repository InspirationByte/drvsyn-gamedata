// ORIGINAL FILENAME: resources/in_touchzones.res
// It's all about touch controls system
//

/*
How the touchzones are located:

X0,Y0___________________________ X+ 1.0
|
|
|
|
|
|
|
|                      __  ___
|  _______            |HB||   |
| |   |   |           |__|| A |
| | L | R |     _    |B | |___|
| |___|___|    |C|   |__|
Y+ 1.0

Position is mapped to screen size percentage
Size on screen is (SCRWIDTH/1280 * size)

*/

atlas_texture "ui/touch_ctrls";

zones
{
	in_left
	{
		position 0.1 0.85;
		size 0.2 0.28;

		bind "steerleft";
	}

	in_right
	{
		position 0.3 0.85;
		size 0.2 0.28;

		bind "steerright";
	}

	in_accel
	{
		position 0.91 0.84;
		size 0.15 0.28;

		bind "accel";
	}

	in_burnout
	{
		position 0.91 0.62;
		size 0.15 0.15;

		bind "burnout";
	}

	in_brake
	{
		position 0.78 0.9;
		size 0.1 0.2;

		bind "brake";
	}

	in_handbrake
	{
		position 0.78 0.7;
		size 0.07 0.15;

		bind "handbrake";
	}

	in_camera
	{
		position 0.5 0.9;
		size 0.06 0.1;

		bind "changecamera";
	}

	in_signal
	{
		position 0.04 0.6;
		size 0.06 0.1;

		bind "horn";
	}

	in_lookl
	{
		position 0.06 0.12;
		size 0.1 0.2;

		bind "lookleft";
	}

	in_lookr
	{
		position 0.94 0.12;
		size 0.1 0.2;

		bind "lookright";
	}

	in_console
	{
		position 0.5 0.12;
		size 0.1 0.05;

		bind "con_show";
	}
}
