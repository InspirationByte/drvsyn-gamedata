panel "UI_PlayerSettings"
{
	label		"Test window";
	font 		"Roboto" 10;

	window		1;
	visible 	0;

	position 	0 0;
	size 		640 480;
	//maximize 	1;

	child label
	{
		position	10 10;
		size		100 24;
		label		"You can click on button!";
	
		font "Default" 20;
	}
	
	child label
	{
		position	10 25;
		size		100 24;

		label		"Here is another label with different font";
		font 		"Roboto" 20;
		fontScale	20 20;
	}

	child button "testClickButton"
	{
		label 		"Click me pls a very long text";
		position	10 64;
		size		110 40;

		command engine "echo You have clicked the test button";
		// command lua "Msg(\"You have clicked the test button\")";

		font 		"Roboto Condensed" 20;
		fontScale	20 20;
	}
}