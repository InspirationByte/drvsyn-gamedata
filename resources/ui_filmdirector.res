position 		0 0;
size 			640 480;	// map hud size to 640x480

font			Roboto 30;
fontScale		10 10;

// freecam button
child image "freecam"
{
	atlas		"ui/hud_director" "cam_free";
	
	align		left top;
	position	20 20;
	
	size		50 50;
	scaling		uniform;
	
	child button "freecam_btn"
	{
		position	0 0;
		size		50 50;
		scaling		inherit;
		selfvisible 	0;
		command engine "echo You have clicked the test button";
	}

	child label "freecam_label"
	{
		position	0 55;
		visible		1;
	
		size		80 15;
		scaling		uniform;
		align		top hcenter;
	
		textAlign	hcenter;
	
		label		"F";
	}
}

// play button
child image "play_pause"
{
	atlas		"ui/hud_director" "play";
	
	align		right top;
	position	20 20;
	
	size		50 50;
	scaling		uniform;
	
	child button "play_pause_btn"
	{
		position	0 0;
		size		50 50;
		scaling		inherit;
		selfvisible 	0;
		command engine "echo You have clicked the test button";
	}

	child label "play_label"
	{
		position	0 55;
		visible		1;
	
		size		80 15;
		scaling		uniform;
		align		top hcenter;
	
		textAlign	hcenter;
	
		label		"P";
	}
}

// rewind button
child image "rewind_ff"
{
	atlas		"ui/hud_director" "rewind";
	
	align		right top;
	position	120 20;
	
	size		50 50;
	scaling		uniform;
	
	child button "rewind_ff_btn"
	{
		position	0 0;
		size		50 50;
		scaling		inherit;
		selfvisible 	0;
		command engine "echo You have clicked the test button";
	}

	child label "rewind_ff_label"
	{
		position	0 55;
		visible		1;
	
		size		80 20;
		scaling		uniform;
		align		top hcenter;
	
		textAlign	hcenter;
	
		label		"BACKSPACE";
	}
}

// camera button
child image "cammenu"
{
	atlas		"ui/hud_director" "cam_menu";
	
	align		top hcenter;
	position	0 20;

	size		50 50;
	scaling		uniform;
	
	child button "cammenu_btn"
	{
		position	0 0;
		size		50 50;
		scaling		inherit;
		selfvisible 	0;
		command engine "echo You have clicked the test button";
	}

	child label "cammenu_label"
	{
		position	0 55;
		visible		1;
	
		size		80 20;
		scaling		uniform;
		align		top hcenter;
	
		textAlign	hcenter;
	
		label		"C";
	}
}

//-------------------------------------------------------------
// Bottom part

// prev camera button
child image "cam_prev"
{
	atlas		"ui/hud_director" "cam_prev";
	
	align		left bottom;
	position	120 60;
	
	size		40 40;
	scaling		uniform;
	
	child button "cam_prev_btn"
	{
		position	0 0;
		size		40 40;
		scaling		inherit;
		selfvisible 	0;
		command engine "echo You have clicked the test button";
	}

	child label "cam_prev_label"
	{
		position	0 45;
		visible		1;
	
		size		80 15;
		scaling		uniform;
		align		top hcenter;
	
		textAlign	hcenter;
	
		label		"PGDN";
	}
}

// next camera button
child image "cam_next"
{
	atlas		"ui/hud_director" "cam_next";
	
	align		right bottom;
	position	120 60;
	
	size		40 40;
	scaling		uniform;
	
	child button "cam_next_btn"
	{
		position	0 0;
		size		40 40;
		scaling		inherit;
		selfvisible 	0;
		command engine "echo You have clicked the test button";
	}

	child label "cam_next_label"
	{
		position	0 45;
		visible		1;
	
		size		80 15;
		scaling		uniform;
		align		top hcenter;
	
		textAlign	hcenter;
	
		label		"PGUP";
	}
}