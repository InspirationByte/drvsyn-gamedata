﻿DRVSYN_GAME_TITLE	"THE DRIVER SYNDICATE";

MENU_YES		"Да";
MENU_NO			"Нет";

MENU_HINT_BACK		"Назад";
MENU_HINT_NAVIGATE	"Навигация";
MENU_HINT_ACCEPT	"Принять";
MENU_HINT_CLEAR		"Очистить";

MENU_MAIN_STARTGAME	"Начать игру";
MENU_MAIN_EXIT		"К титульному экрану";
MENU_MAIN_OPTIONS	"Настройки";
MENU_MAIN_ADDONS	"Дополнения";
MENU_MAIN_REPLAYS	"Повторы";

MENU_MAIN_EXITTOSYSTEM	"Выйти из игры";

MENU_MAIN_SYNDYCATE	"Синдикат";
MENU_MAIN_SYNDYCATE_DEMO	"Синдикат (Демо)";
MENU_MAIN_SINGLEPLAYER	"Один игрок";
MENU_MAIN_MULTIPLAYER	"Сетевая игра";

MENU_MAIN_MINIGAMES	"Мини-игры";

MENU_SYNDICATE_NEWGAME	"Новая игра";
MENU_SYNDICATE_CONTINUE	"Продолжить";

MENU_GAME_TAKEARIDE		"Прокатиться";
MENU_GAME_INTERVIEW		"\"Собеседование\"";

MENU_GAME_LOSETHETAIL	"Побег";
	MENU_GAME_LOSETHETAIL_01	"Побег 1";
	MENU_GAME_LOSETHETAIL_02	"Побег 2";
	
MENU_GAME_PURSUIT		"Погоня";
	MENU_GAME_PURSUIT_01		"Погоня 1";
	MENU_GAME_PURSUIT_02		"Погоня 2";
	
MENU_GAME_CHECKPOINT	"Гонка по чекпоинтам";
	MENU_GAME_CHECKPOINT_01		"Гонка по чекпоинтам 1";
	MENU_GAME_CHECKPOINT_02		"Гонка по чекпоинтам 2";

MENU_GAME_SURVIVAL		"Выживание";
	MENU_GAME_SURVIVAL_01		"Выживание 1";
	MENU_GAME_SURVIVAL_02		"Выживание 2";
	
MENU_GAME_TRAILBLAZER	"Первопроходец";
	MENU_GAME_TRAILBLAZER_01	"Первопроходец 1";
	MENU_GAME_TRAILBLAZER_02	"Первопроходец 2";
	
MENU_GAME_GATES			"В ворота";
	MENU_GAME_GATES_01			"В ворота 1";
	MENU_GAME_GATES_02			"В ворота 2";

MENU_GAME_MODE 		"Режим: < %s >";
MENU_GAME_PRACTICE 	"Практика";
MENU_GAME_TIMEATTACK	"На время";

MENU_MP_PLAYERSETTINGS	"Настройки игрока";
MENU_MP_JOINGAME	"Присоединиться к игре";
MENU_MP_CREATESERVER	"Создать игру";

MENU_NO_ADDONS		"Дополнения не найдены";

MENU_ON			"Вкл";
MENU_OFF		"Откл";

MENU_SETTING_OFF	"Откл";
MENU_SETTING_LOW	"Низко";
MENU_SETTING_MEDIUM	"Средне";
MENU_SETTING_HIGH	"Высоко";

MENU_SETTING_EASY	"Легко";
MENU_SETTING_NORMAL	"Обычно";
MENU_SETTING_HARD	"Тяжело";

MENU_SETTINGS_GAME		"Игра";
MENU_SETTINGS_GRAPHICS	"Графика";
MENU_SETTINGS_SOUND		"Звук";
MENU_SETTINGS_CONTROLS	"Управление";

MENU_SETTINGS_DIFFICULTY	"Сложность: < %s >";
MENU_SETTINGS_STEERINGHELP	"Помощь при поворотах: < %s >";

MENU_SETTINGS_MASTERVOLUME	"Общая громкость: < %.0f%% >";
MENU_SETTINGS_VOICEVOLUME	"Голос: < %.0f%% >";
MENU_SETTINGS_EFFECTSVOLUME	"Эффекты: < %.0f%% >";
MENU_SETTINGS_MUSICVOLUME	"Музыка: < %.0f%% >";
MENU_SETTINGS_DRAWDISTANCE	"Дистанция отображения: < %.0f >";
MENU_SETTINGS_LIGHTDISTANCE	"Дистанция света: < %.0f >";
MENU_SETTINGS_NIGHTBRIGHTNESS	"Яркость в ночи: < %.0f %% >";
MENU_SETTINGS_TEXTUREQUALITY	"Качество текстур: < %s >";
MENU_SETTINGS_REFLECTIONQUALITY	"Качество отражений: < %s >";

MENU_SETTINGS_SHOWHUD		"Отображать дисплей: < %s >";
MENU_SETTINGS_CAMERAEFFECTS	"Эффект раскачивания камеры: < %s >";
MENU_SETTINGS_CAMERAFOV		"Угол обзора < %.0f >";
MENU_SETTINGS_SHOWFPS		"Отображать счётчик FPS: < %s >";
MENU_SETTINGS_SLIGHT_BRIGHTNESS "Яркость проблесковых маячков: < %.0f %% >";

MENU_SETTINGS_SKIDMARKS		"Следы шин: < %s >";
MENU_SETTINGS_SHADOWS    	"Отображать тени: < %s >";

MENU_SETTINGS_SCREENRESOLUTION 	"Разрешение экрана < %s >";
MENU_SETTINGS_FULLSCREEN 	"Полноэкранный режим < %s >";
MENU_SETTINGS_APPLY 		"Применить";

MENU_SETTINGS_CONTROL_PRESSKEY		"<Нажмите сочетание... %s>";
MENU_SETTINGS_CONTROL_ALREADY_BOUND	"Клавиши уже привязаны к %s!";

MENU_SETTINGS_CONTROL_ACCEL		"Газ";
MENU_SETTINGS_CONTROL_BRAKE		"Тормоз";
MENU_SETTINGS_CONTROL_LEFT		"Повернуть влево";
MENU_SETTINGS_CONTROL_RIGHT		"Повернуть вправо";
MENU_SETTINGS_CONTROL_HANDBRAKE		"Ручной тормоз";
MENU_SETTINGS_CONTROL_BURNOUT		"Отжечь в пол";
MENU_SETTINGS_CONTROL_FASTSTEER		"Быстрый поворот";
MENU_SETTINGS_CONTROL_HORN		"Сигнал";
MENU_SETTINGS_CONTROL_SIREN		"Сирена";

MENU_SETTINGS_CONTROL_HAZARDS		"Вкл/выкл аварийку";
MENU_SETTINGS_CONTROL_BEAMS		"Вкл/выкл свет";

MENU_SETTINGS_CONTROL_CAMERA		"Сменить камеру";

MENU_SETTINGS_CONTROL_LOOKLEFT		"Смотреть влево";
MENU_SETTINGS_CONTROL_LOOKRIGHT		"Смотреть вправо";
MENU_SETTINGS_CONTROL_LOOKBACK		"Смотреть назад";
MENU_SETTINGS_CONTROL_MOUSELOOK		"Осмотр мышью";

MENU_SETTINGS_CONTROL_SHOWMESSAGE	"Показать сообщение";
MENU_SETTINGS_CONTROL_MAPZOOM		"Развернуть карту";

MENU_SETTINGS_JOYPAD_SETTINGS		"Настройка контроллера";

MENU_SETTINGS_JOY_AXIS_STEERING		"Ось руления";
MENU_SETTINGS_JOY_AXIS_ACCELBRAKE	"Ось газа/тормоза";
MENU_SETTINGS_JOY_AXIS_ACCEL		"Ось газа";
MENU_SETTINGS_JOY_AXIS_BRAKE		"Ось тормоза";

MENU_SETTINGS_JOY_DEADZONE		"Мёртвая зона < %.0f%% >";

MENU_SETTINGS_JOY_STEER_SMOOTH		"Руль < %s >";

MENU_SETTINGS_JOY_STEER_LINEAR		"Линейность руля < %.2f >";
MENU_SETTINGS_JOY_ACCEL_LINEAR		"Линейность газа < %.2f >";
MENU_SETTINGS_JOY_BRAKE_LINEAR		"Линейность тормоза < %.2f >";

MENU_UNDERCONSTRUCTION	"В разработке...";

//--------------------------------------------------------

MENU_TAKEARIDE_CITY		"Город: < %s >";
MENU_TAKEARIDE_CAR		"Автомобиль: < %s >";
MENU_TAKEARIDE_TIMEOFDAY 	"Время: < %s >";
MENU_TAKEARIDE_START		"Начать!";

MENU_TAKEARIDE_WEATHER_DAY "День";
MENU_TAKEARIDE_WEATHER_NIGHT "Ночь";

MENU_TAKEARIDE_WEATHER_DAY_STORM "День (шторм)";
MENU_TAKEARIDE_WEATHER_NIGHT_STORM "Ночь (шторм)";

MENU_TAKEARIDE_WEATHER_DAWN "Заря";