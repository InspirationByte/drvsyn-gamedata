IN_MOU_B1				"Mouse 1";
IN_MOU_B2				"Mouse 2";
IN_MOU_B3				"Mouse 3";
IN_MOU_B4				"Mouse 4";
IN_MOU_B5				"Mouse 5";

IN_MOU_WHUP				"Mouse Wheel Up";
IN_MOU_WHDN				"Mouse Wheel Down";

IN_JOY_A				"Joypad A";
IN_JOY_B				"Joypad B";
IN_JOY_X				"Joypad X";
IN_JOY_Y				"Joypad Y";

IN_JOY_BACK 			"Joypad Back";
IN_JOY_GUIDE			"Joypad Guide";
IN_JOY_START			"Joypad Start";

IN_JOY_LSTICK			"Joypad Left Stick";
IN_JOY_RSTICK			"Joypad Right Stick";

IN_JOY_LSHOULDER		"Joypad Left Shoulder";
IN_JOY_RSHOULDER		"Joypad Right Shoulder";

IN_JOY_DPAD_UP 			"Joypad Up";
IN_JOY_DPAD_DOWN		"Joypad Down";
IN_JOY_DPAD_LEFT		"Joypad Left";
IN_JOY_DPAD_RIGHT 		"Joypad Right";

IN_JOYAXIS_LX			"Joy Left X Axis";
IN_JOYAXIS_LY			"Joy Left Y Axis";
IN_JOYAXIS_RX			"Joy Right Axis";
IN_JOYAXIS_RY			"Joy Right Axis";
IN_JOYAXIS_TL			"Joy Trigger Left";
IN_JOYAXIS_TR			"Joy Trigger Right";