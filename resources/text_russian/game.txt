﻿#include "missions";

MENU_TITLE_PRESS_ENTER	"Нажмите &#FFA000;ENTER&; чтобы начать игру";
MENU_TITLE_TAP_SCREEN	"Нажмите на &#FFA000;ЭКРАН&; чтобы начать игру";
MENU_TITLE_PRESS_START	"Нажмите &#FFA000;START&; чтобы начать игру";

GAME_IS_LOADING		"Загрузка...";
PLEASE_WAIT		"Пожалуйста, подождите...";
DEMO			"Демонстрация";

HUD_DAMAGE_TITLE	"ПОВРЕЖДЕНИЯ";
HUD_FELONY_TITLE	"ПРЕСЛЕДОВАНИЕ";

TRAILBLAZER_CONES_LABEL 	"Конусы";
TRAILBLAZER_MISSED_LABEL	"Пропущено";
TRAILBLAZER_TITLE_MESSAGE	"Сбивай &#FF0000;конусы&;";

GATES_GATES_LABEL		"Ворота";
GATES_MISSED_LABEL		"Пропущено";
GATES_BEATEN_LABEL		"Сбито конусов";
GATES_TITLE_MESSAGE	 	"Езжай через &#FF0000;конусы&;";