panel
{
	position 		0 0;
	size 			640 480;	// map hud size to 640x480

	font			"Roboto Condensed" 30;
	fontScale		14 14;

	child image "background"
	{
		path		"ui/ui_menu_bg2";
		position	100 15;
		size		1000  600;
		align		vcenter hcenter;

		scaling		aspecth;
	}
	
	child container "menu_bg_top"
	{
		align			bottom left;
		scaling			uniform;
		position 		-650 0;
		size 			800 600;	// map hud size to 640x480

		transform
		{
			rotate 15;
			translate 0 0;
		}
		
		child panel "items_bg"
		{
			color		0.08 0.08 0.08 1;
			position 	0 0;
			size		800 2000;
			align		left vcenter;
			scaling		aspecth;
		}
		
		child image "lines_bg"
		{
			color		0.08 0.08 0.08 1;
			path		"ui/lines";
			position	790 15;
			size		300 2000;
			align		left vcenter;
		
			scaling		aspecth;
		}
	}

	child label "loadingLabel"
	{
		position	8 20;
		size		200 24;
		label		"#GAME_IS_LOADING";

		font		"Roboto" 30;
		textalign	center;
		
		scaling		aspecth;
		align		left vcenter;
		
		fontScale	18 18;
	}
	
	child PercentageBar "progressBar"
	{
		position	0 120;
		
		align		hcenter vcenter;
		value		0.5;
		color		0.8 0.4 0 1.0;
		
		size		510 17;
		scaling		uniform;
	}
}