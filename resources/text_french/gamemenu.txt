MENU_GAME_CONTINUE		"Retour au jeu";
MENU_GAME_RESTART		"Recommencer";
MENU_GAME_EXIT			"Quitter";
MENU_GAME_NEXTMISSION	"Prochaine mission";

MENU_GAME_DIRECTOR		"Mode réalisateur";
MENU_GAME_QUICKREPLAY		"Replay rapide";
MENU_GAME_SHOWMAP		"Afficher la carte N/A";
MENU_GAME_SETTINGS		"Options";

MENU_GAME_QUEST_SURE		"Vous êtes sûr(e) ?";

MENU_GAME_TITLE_PAUSE		"Pause";
MENU_GAME_TITLE_MENU		"Menu principal";
MENU_GAME_TITLE_END		"Game over";
MENU_GAME_TITLE_MISSION_SUCCESS	"Mission accomplie";
MENU_GAME_TITLE_MISSION_FAILED	"Mission ratée";
MENU_GAME_SHOWSCORETABLE	"Tableau des scores";
MENU_GAME_TITLE_SCORETABLE	"Tableau des scores";
MENU_GAME_ENTERNAME		"Entrez votre nom: %s";

