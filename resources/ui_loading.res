panel
{
	position 		0 0;
	size 			640 480;	// map hud size to 640x480
	
	//font			Roboto 30;

	font			"Roboto Condensed" 30 italic;
	fontScale		16 16;
	
	child Panel "black_bg"
	{
		position 		0 0;
		size 			640 480;	// map hud size to 640x480
		scaling			inherit;
		color			0 0 0 1;
	}
	
	child label "title"
	{
		position	20 20;
		size		640 20;
		//anchors		right bottom;
		scaling		aspecth;

		align		bottom left;
		textAlign	left;

		font		"Roboto Condensed" italic bold;

		label		"#GAME_IS_LOADING";
		fontScale	18 18;
	}
}