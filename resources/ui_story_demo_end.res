panel
{
	position 		0 0;
	size 			640 480;	// map hud size to 640x480
	
	//font			Roboto 30;

	font			"Roboto Condensed" 30 italic;
	fontScale		16 16;
	
	child image "background"
	{
		path		"ui/ui_menu_bg5";
		position	0 15;
		size		1000  500;
		align		vcenter hcenter;
		color		0 0.4 1 1;

		scaling		aspecth;
	}

	child label "message1"
	{
		position	0 100;
		size		640 120;
		//anchors		right bottom;
		scaling		aspecth;

		align		top hcenter;
		textAlign	center;

		font		"Roboto Condensed" italic bold;

		label
"Thanks for playing &#FF8000;The Driver Syndicate&; story demo!
Enjoy the tons of wonderful &#FF0000;Driver Madness&; community mods for the game!";
		
		fontScale	18 18;
	}
	
	child label "message2"
	{
		position	0 220;
		size		640 420;
		//anchors		right bottom;
		scaling		aspecth;

		align		top hcenter;
		textAlign	left;

		font		"Roboto Condensed" italic bold;

		label
"Stay tuned for the future exciting game updates including:
- A complete story set in new &#808080;City of Coalmount&;
- New vehicles ranging from sedans to utility trucks
- New gamemodes
- Online score tables and stats
- Pocket Wheelman Edition - &#FF8000;The Driver Syndicate&; comes to mobile!";
		
		
		fontScale	18 18;
	}

	child panel "fade"
	{
		position 		0 0;
		size 			640 480;	// map hud size to 640x480
		scaling			inherit;
	}
}