panel
{
	position 		0 0;
	size 			640 480;	// map hud size to 640x480

	font			"Roboto Condensed" 30 bold italic;
	fontScale		14 14;

	child Panel "black_bg"
	{
		position 		0 0;
		size 			640 480;	// map hud size to 640x480
		scaling			inherit;
		color			0 0 0 1;
	}

	child image "game_logo"
	{
		path		"ui/logo_h";
		position	0 0;
		size		512 125;

		align		hcenter vcenter;
		
		scaling		aspecth;
	}
	
	child label "loadingLabel"
	{
		position	16 80;
		size		200 24;
		label		"#GAME_IS_LOADING";

		textalign	center;
		
		scaling		aspecth;
		align		hcenter vcenter;
		
		fontScale	18 18;
	}
	
	child PercentageBar "progressBar"
	{
		position	0 120;
		
		align		hcenter vcenter;
		value		0.5;
		color		0.8 0.4 0 1.0;
		
		size		510 17;
		scaling		uniform;
	}
}