//
// Equilibrium Font Description file
//

"LogoNumbers"
{
	"0"
	{
		reg	"logo_numbers";
		// bld 	"sdfb";
		// itl	"sdfi";
	}
}

"LPlate"
{
	"0"
	{
		reg	"lplate";
		// bld 	"sdfb";
		// itl	"sdfi";
	}
}


"Roboto"
{
	"0"
	{
		reg	"roboto";
		bld 	"roboto_b";
		itl	"roboto_i";
		b+i	"robotocon_bi";
	}
}

"Roboto Condensed"
{
	"0"
	{
		reg	"robotocon";
		bld 	"robotocon_b";
		itl	"robotocon_i";
		b+i	"robotocon_bi";
	}
}