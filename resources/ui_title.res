$schema_kv3

panel
{
	position 		int: [0, 0];
	size 			int: [640, 480];	// map hud size to 640x480

	fontScale		float: [14, 14];

	child [image "background"]
	{
		path			"ui/ui_menu_bg1";
		position		int: [0, 0];
		size			int: [960,  540];
		align			[vcenter, hcenter];
			
		scaling			[aspecth];
	}

	child [image "game_logo"]
	{
		path			"ui/logo_h";
		position		int: [0, 45];
		size			int: [512, 125];
	
		align			[hcenter];// vcenter;
			
		scaling			aspecth;
	}

	child [label "title"]
	{
		position		int: [0, 70];
		size			int: [640, 20];
		//anchors		right bottom;
		scaling			aspecth;
	
		align			[bottom hcenter];
		textAlign		center;
		
		font			["Roboto Condensed", italic, bold];
	
		label				"#MENU_TITLE_PRESS_ENTER";
		fontScale			float: [18, 18];
		// fancy stuff
		// textWeight		float: 0.05;
		// textShadowColor		float: [0,1,0,1];
		// textShadowOffset 	float: 5.0;
		// textShadowWeight		float: 0.1;
	}

	child [label "copyright"]
	{
		position		int: [0, 30];
		size			int: [640, 14];
		//anchors			right bottom;
		scaling			aspecth;
		align			[bottom hcenter];
	
		textAlign		center;
	
		font			["Roboto", 30];
	
		label			"#INSCOPYRIGHT";
		fontScale		[12, 12];
	}

	child [panel "fade"]
	{
		position 		int: [0, 0];
		size 			int: [640, 480];	// map hud size to 640x480
		scaling			inherit;
		color			float: [0, 0, 0, 1];
	}
	
	%script
	{

	}
}