base "defaulthud.res";

child PercentageBar "damageBar"
{
	visible 	0;
}

child Label "felonyBar"
{
	visible 	0;
}

child HudElement "map"
{
	visible 	0;
}

// items found by Lua GUI API
child image "timer_gauge"
{
	atlas		"ui/hud_stopwatch" "stopwatch";
	position	25 5;
	size		120 150;
	
	scaling		uniform;
	/*
	child image "timer_arrow"
	{
		atlas		"ui/hud_stopwatch" "stopwatch_arrow";
		position	0 0;
		size		100 100;
	}*/
}

child Timer "timer"
{
	position	40 58;
	size		85 85;
	
	scaling		uniform;

	font		"Roboto Condensed" 30;
	fontScale	30 30;
	textColor	0 0 0 1;
	
	type		1;	// set timer type to have arrow
}

child image "tasks"
{
	atlas		"ui/hud_iview" "tasklist";
	
	align		right top;
	position	25 5;
	
	size		200 200;
	scaling		uniform;
	
	// damage
	child image "marker_hit_1"
	{
		atlas		"ui/hud_iview" "marker_x1";
		position	28 10;
		size		16 16;
		scaling		inherit;
		visible 	0;
	}
	
	child image "marker_hit_2"
	{
		atlas		"ui/hud_iview" "marker_x2";
		position	45 10;
		size		16 16;
		scaling		inherit;
		visible 	0;
	}
	
	child image "marker_hit_3"
	{
		atlas		"ui/hud_iview" "marker_x3";
		position	60 10;
		size		16 16;
		scaling		inherit;
		visible 	0;
	}
	
	child image "marker_hit_4"
	{
		atlas		"ui/hud_iview" "marker_x4";
		position	75 10;
		size		16 16;
		scaling		inherit;
		visible 	0;
	}
	
	// completion
	child image "marker_burnout"
	{
		atlas		"ui/hud_iview" "marker_strike";
		position	80 34;
		size		70 8;
		scaling		inherit;
		visible 	0;
	}
	
	child image "marker_handbrake"
	{
		atlas		"ui/hud_iview" "marker_strike";
		position	84 50;
		size		70 8;
		scaling		inherit;
		visible 	0;
	}
	
	child image "marker_slalom"
	{
		atlas		"ui/hud_iview" "marker_strike";
		position	80 65;
		size		70 8;
		scaling		inherit;
		visible 	0;
	}
	
	child image "marker_180"
	{
		atlas		"ui/hud_iview" "marker_strike";
		position	67 78;
		size		80 8;
		scaling		inherit;
		visible 	0;
	}
	
	child image "marker_360"
	{
		atlas		"ui/hud_iview" "marker_strike";
		position	70 94;
		size		80 8;
		scaling		inherit;
		visible 	0;
	}

	child image "marker_rev180"
	{
		atlas		"ui/hud_iview" "marker_strike";
		position	70 108;
		size		80 8;
		scaling		inherit;
		visible 	0;
	}
	
	child image "marker_speed"
	{
		atlas		"ui/hud_iview" "marker_strike";
		position	68 122;
		size		90 8;
		scaling		inherit;
		visible 	0;
	}
	
	child image "marker_brake"
	{
		atlas		"ui/hud_iview" "marker_strike";
		position	72 137;
		size		88 8;
		scaling		inherit;
		visible 	0;
	}
	
	child image "marker_lap"
	{
		atlas		"ui/hud_iview" "marker_strike";
		position	80 155;
		size		60 8;
		scaling		inherit;
		visible 	0;
	}
}