position 		0 0;
size 			640 480;	// map hud size to 640x480

// for splitscreen each child will be suffixed with "_p<number>"

child PercentageBar "damageBar"
{
	position	20 30;
	
	size		210 17;
	scaling		uniform;

	child label "damageLabel"
	{
		position	5 -5;
		
		size		200 40;
		scaling		uniform;
		
		label		"#HUD_DAMAGE_TITLE";
		font		"Roboto Condensed" 30 bold italic;
		fontScale	16 16;
	}
}

child Label "felonyBar"
{
	position	20 60;
	
	size		200 20;
	scaling		uniform;
	font		"Roboto" 30 bold;
	fontScale	14 14;
	
	label		"#HUD_FELONY_TITLE";
	
	child label "CountLabel"
	{
		position	150 0;
		
		size		90 24;
		scaling		uniform;
		
		label		"(counter)";
	}
}
/*
child PercentageBar "felonyBar"
{
	position	20 60;
	
	size		210 17;
	scaling		uniform;

	child label "felonyLabel"
	{
		position	5 -5;
		
		size		200 40;
		scaling		uniform;
		
		label		"#HUD_FELONY_TITLE";
		font		"Roboto Condensed" 30 bold italic;
		fontScale	16 16;
	}
}*/

child Timer "timer"
{
	position	-20 0;	// -50 because we want center it
	size		220 80;
	
	align		hcenter;
	textAlign	center;
	
	scaling		uniform;

	font		"LogoNumbers" 30;
	fontScale	24 24;
	
	visible		0;
}

child HudElement "map"
{
	position	20 30;
	align		bottom right;
	
	size		160 120;
	scaling		uniform;
}

//---------------------------------------------------------------

child label "WIPText"
{
	position	15 20;
	visible		1;
	
	size		400 14;
	scaling		uniform;
	
	align		bottom left;
	textalign	left;
	
	label		"#GAME_VERSION";
	font		"Roboto" bold italic;
	fontScale	8 8;
}
