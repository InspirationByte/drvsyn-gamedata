base "defaulthud.res";

child Label "felonyBar"
{
	visible 0;
}

child label "CheckStatusText"
{
	position	20 70;
	visible		1;
	
	size		200 24;
	scaling		uniform;
	
	label		"#CHECKPOINT_LABEL";
	font		Roboto 30;
	fontScale	14 14;
	
	child label "CountLabel"
	{
		position	180 0;
		
		size		90 24;
		scaling		uniform;
		
		label		"(counter)";
		font		Roboto 30;
		fontScale	14 14;
	}
}

