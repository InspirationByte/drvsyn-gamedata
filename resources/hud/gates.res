base "defaulthud.res";

child Label "felonyBar"
{
	visible 0;
}

child label "CheckStatusText"
{
	position	20 70;
	visible		1;
	
	size		200 24;
	scaling		uniform;
	
	label		"#GATES_GATES_LABEL";
	font		Roboto 30;
	fontScale	14 14;
	
	child label "CountLabel"
	{
		position	180 0;
		
		size		80 24;
		scaling		uniform;
		
		label		"(counter)";
		font		Roboto 30;
		fontScale	14 14;
	}
}

child label "MissedStatusText"
{
	position	20 90;
	visible		1;
	
	size		200 24;
	scaling		uniform;
	
	label		"#GATES_MISSED_LABEL";
	font		Roboto 30;
	fontScale	10 10;
	
	child label "CountLabel"
	{
		position	180 0;
		
		size		80 24;
		scaling		uniform;
		
		label		"(counter)";
		font		Roboto 30;
		fontScale	10 10;
	}
}

child label "BeatenStatusText"
{
	position	20 108;
	visible		1;
	
	size		200 24;
	scaling		uniform;
	
	label		"#GATES_BEATEN_LABEL";
	font		Roboto 30;
	fontScale	10 10;
	
	child label "CountLabel"
	{
		position	180 0;
		
		size		80 24;
		scaling		uniform;
		
		label		"(counter)";
		font		Roboto 30;
		fontScale	10 10;
	}
}