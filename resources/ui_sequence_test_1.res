panel
{
	position 		0 0;
	size 			640 480;	// map hud size to 640x480
	
	//font			Roboto 30;

	font			"Roboto Condensed" 30 italic;
	fontScale		16 16;
	
	child image "background"
	{
		path		"ui/ui_menu_bg5";
		position	0 15;
		size		1000  500;
		align		vcenter hcenter;

		scaling		aspecth;
	}

	child label "title"
	{
		position	0 70;
		size		640 20;
		//anchors		right bottom;
		scaling		aspecth;

		align		bottom hcenter;
		textAlign	center;

		font		"Roboto Condensed" italic bold;

		label		"Welcome to Lua-based screen";
		fontScale	18 18;
	}

	child panel "fade"
	{
		position 		0 0;
		size 			640 480;	// map hud size to 640x480
		scaling			inherit;
	}
	
	child panel "moving"
	{
		position 		0 0;
		size 			90 90;	// map hud size to 640x480
		color			1 1 0 1;
		align			vcenter hcenter;
	}
}