// $INCLUDE skinning_vs.h
// $INCLUDE vehicle_vs.h

struct PsIn 
{
	float4 pos;
	float2 texCoord;
};

uniform float4x4 	WVP;
uniform float4 		BaseTextureTransform;
uniform sampler2D 	BaseTextureSampler;

PsIn vs_main(
	float4 vPos, 
	float2 texCoord
	// skinning and instancing can't be used both
#ifdef SKIN
	, float4 boneIndex
	, float4 boneWeight
#endif
#ifdef VEHICLE
	, float4 boneIndex
	, float4 boneWeight
	, float4 vPos1	
	, float3 normal1
#endif
#ifdef INST
	, float4x4 World
#endif
	)
{
	PsIn Out;
	
#if defined(SKIN)
	float4 transPosit 	= S_TransformVertexWeights(vPos, boneIndex, boneWeight);
#elif defined(VEHICLE)
	float interpVal 	= S_GetDamageInterpolation(boneIndex, boneWeight);
	float4 transPosit 	= lerp(vPos, vPos1, interpVal);
#else
	float4 transPosit 	= vPos;
#endif
	
#ifdef INST
	transPosit = mul(World, vPos);
#endif

	Out.pos = mul(WVP, transPosit);

	Out.texCoord.x = texCoord.x*BaseTextureTransform.x + BaseTextureTransform.z;
	Out.texCoord.y = texCoord.y*BaseTextureTransform.y + BaseTextureTransform.w;

	return Out;
}

float4 ps_main(PsIn input)
{
	float4 baseTexture = tex2D(BaseTextureSampler, input.texCoord.xy);

	clip(baseTexture.a-0.5);

	return float4(0,0,0,1);
}

//----------------------------------------
// separate entry points
//----------------------------------------

#ifdef VERTEX

// can be generated
layout(location = 0) in vec4 input_vPos;
layout(location = 1) in vec2 input_texCoord;

#if defined(SKIN)
layout(location = 5) in vec4 input_boneIndex;
layout(location = 6) in vec4 input_boneWeight;
#elif defined(VEHICLE)
layout(location = 3) in vec4 input_boneIndex;
layout(location = 4) in vec4 input_boneWeight;
layout(location = 5) in vec4 input_vPos_dam;
layout(location = 6) in vec3 input_normal_dam;
#elif defined(INST)
layout(location = 3) in mat4 input_inst;
#endif

out PsIn output;

void main()
{
#if defined(SKIN)
	output = vs_main(input_vPos, input_texCoord, input_boneIndex, input_boneWeight);
#elif defined(VEHICLE)
	output = vs_main(input_vPos, input_texCoord, input_boneIndex, input_boneWeight, input_vPos_dam, input_normal_dam);
#elif defined(INST)
	output = vs_main(input_vPos, input_texCoord, transpose(input_inst));
#else
	output = vs_main(input_vPos, input_texCoord);
#endif

	gl_Position = output.pos;
}
#endif

#ifdef FRAGMENT

in PsIn output;
out vec4 fragColor;

void main()
{
	fragColor = ps_main(output);
}

#endif