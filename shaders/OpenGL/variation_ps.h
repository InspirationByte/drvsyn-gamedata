#define NOISE_TEXTURE

uniform sampler2D NoiseTexture;

float sum( float3 v ) { return v.x+v.y+v.z; }

float4 texNoTile( sampler2D tex, float2 x, float v )
{
	float k = tex2D( NoiseTexture, 0.085*x ).x; // cheap (cache friendly) lookup

	float2 duvdx = ddx( x );
	float2 duvdy = ddy( x );

	float l = k*8.0;
	float i = floor( l );
	float f = frac( l );

	float2 offa = sin(float2(3.0,7.0)*(i+0.0));
	float2 offb = sin(float2(3.0,7.0)*(i+1.0));

	float4 cola = textureGrad( tex, x + v*offa, duvdx, duvdy );
	float4 colb = textureGrad( tex, x + v*offb, duvdx, duvdy );

	return lerp( cola, colb, smoothstep(0.2,0.8,f-0.1*sum((cola-colb).rgb)) );
}
