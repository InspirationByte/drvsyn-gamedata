#define NOISE_TEXTURE
// $INCLUDE shadowmap_ps.h

struct PsIn 
{
	float4 pos;
	
#ifdef DOFOG
	float3 texCoord;
#else
	float2 texCoord;
#endif // DOFOG

	float4 	color;
	float3	viewVec;
};

uniform float4x4 	World;
uniform float4x4 	WVP;
uniform float3 		ViewPos;

#ifdef DOFOG
uniform float3 		FogParams;
uniform float3 		FogColor;
#endif

uniform sampler2D 	NoiseTexture;
uniform sampler2D 	BaseTextureSampler;
uniform float4 		AmbientColor;

PsIn vs_main(
	float4 vPos,
	float2 texCoord,
	float4 color
	)
{
	PsIn Out;

	float3 vertPos = mul(World,vPos).xyz;
	Out.pos = mul(WVP, vPos);
	
	Out.viewVec = ViewPos-vertPos;

	Out.texCoord.x = texCoord.x;
	Out.texCoord.y = texCoord.y;
	Out.color = color;

#ifdef DOFOG
	Out.texCoord.z = (FogParams.y - Out.pos.z) * FogParams.z;
#endif

	return Out;
}

#define SHADOW_SIZE 		800
#define SHADOWTEXEL_SIZE 	(1.0f / SHADOW_SIZE)

float ShadowPCF(sampler2D ShadowMap, float2 projVector)
{
	float fShadow;
	fShadow = tex2D(ShadowMap, projVector).r;
	
	fShadow += tex2D(ShadowMap, projVector + float2(SHADOWTEXEL_SIZE,0)).r;
	fShadow += tex2D(ShadowMap, projVector + float2(0,SHADOWTEXEL_SIZE)).r;
	fShadow += tex2D(ShadowMap, projVector + float2(SHADOWTEXEL_SIZE,SHADOWTEXEL_SIZE)).r;
	
	fShadow += tex2D(ShadowMap, projVector - float2(SHADOWTEXEL_SIZE,0)).r;
	fShadow += tex2D(ShadowMap, projVector - float2(0,SHADOWTEXEL_SIZE)).r;
	fShadow += tex2D(ShadowMap, projVector - float2(SHADOWTEXEL_SIZE,SHADOWTEXEL_SIZE)).r;

	fShadow += tex2D(ShadowMap, projVector - float2(-SHADOWTEXEL_SIZE,SHADOWTEXEL_SIZE)).r;
	fShadow += tex2D(ShadowMap, projVector - float2(SHADOWTEXEL_SIZE,-SHADOWTEXEL_SIZE)).r;

	float shadowResult = fShadow * 0.111;

	return saturate(shadowResult - (frac(pow(shadowResult + tex2D(NoiseTexture, projVector*4096).r, 0.5))-0.25));
}

float4 ps_main(PsIn input)
{
	float shadowTex = 1.0-ShadowPCF(BaseTextureSampler, input.texCoord.xy);

#ifdef SHADOWMAP	
	// apply sun shadow to diffuse light factor
	// also apply normal bias
	shadowTex = lerp(0.0, shadowTex, pow(GetSunShadow(input.viewVec + ViewPos), 0.5));
#endif

	float4 result = float4(AmbientColor.rgb, shadowTex*AmbientColor.a*input.color.a);

#ifdef DOFOG
	result.a = lerp(result.a, 0.0, saturate(1.0f - input.texCoord.z));
#endif

	return result;
}

//----------------------------------------
// separate entry points
//----------------------------------------

#ifdef VERTEX

// can be generated
layout(location = 0) in vec4 input_vPos;
layout(location = 1) in vec2 input_texCoord;
layout(location = 2) in vec4 input_color;

out PsIn output;

void main()
{
	output = vs_main(input_vPos, input_texCoord, input_color);
	gl_Position = output.pos;
}
#endif

#ifdef FRAGMENT

in PsIn output;
out vec4 fragColor;

void main()
{
	fragColor = ps_main(output);
}

#endif