struct PsIn 
{
	float4 pos;
	float2 texCoord;
};

uniform float4x4 	WVP;

uniform sampler2D 	Texture1;
uniform sampler2D 	Texture2;

PsIn vs_main(
	float4 vPos,
	float2 texCoord
	)
{
	PsIn Out;

	Out.pos = mul(WVP, vPos);
	Out.texCoord = texCoord;
	Out.texCoord.y = 1.0 - Out.texCoord.y;

	return Out;
}

float4 ps_main(PsIn input)
{
	float depth1 = tex2D(Texture1, input.texCoord).r;
	float depth2 = tex2D(Texture2, input.texCoord).r;
	
	return float4( min(depth1, depth2), 0, 0, 1);
}

//----------------------------------------
// separate entry points
//----------------------------------------

#ifdef VERTEX

// can be generated
layout(location = 0) in vec4 input_vPos;
layout(location = 1) in vec2 input_texCoord;

out PsIn output;

void main()
{
	output = vs_main(input_vPos, input_texCoord);
	gl_Position = output.pos;
}
#endif

#ifdef FRAGMENT

in PsIn output;
out vec4 fragColor;

void main()
{
	fragColor = ps_main(output);
}

#endif