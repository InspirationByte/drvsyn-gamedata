uniform float GameTime;

float2 GetTreeWaving(float3 vPos, float windScale, float3 tcVector, float2 texCoord)
{
	float MOVE_SCALE_HEIGHT = saturate(vPos.y);
	
	float MOVE_SCALE1 = 0.05 * windScale * MOVE_SCALE_HEIGHT;
	float MOVE_SCALE2 = 0.01 * windScale * MOVE_SCALE_HEIGHT;

	float wave1 = sin(vPos.x + vPos.y + GameTime * 2.0);
	float wave2 = sin(vPos.z + vPos.y + GameTime * 2.0 + 1.5);
	float wave3 = sin(wave1 * 10.0) + sin(wave2 * 10.0 + 1.5);
	float wave4 = sin(wave2 * 10.0) + sin(wave1 * 10.0 + 1.5);
	
	float2 wave;
	wave.x = tcVector.y*wave1 * MOVE_SCALE1 + (1.0-texCoord.y)*wave3 * MOVE_SCALE2;
	wave.y = tcVector.y*wave2 * MOVE_SCALE1 + (1.0-texCoord.y)*wave4 * MOVE_SCALE2;

	return wave;
}

float3 GetTreeLeavesOffset(float3 vPos, float4x4 view, float4x4 world, float2 texCoord, float windScale)
{
	float LEAF_SCALE = 0.85;	// 0.85 is minimal - less looks worse
	
	float3x3 view3 = float3x3(view);
	float3x3 world3 = float3x3(world);
	
	float3x3 facingMatrix = transpose(mul(view3, world3));
	float3 tcVector = float3(texCoord - 0.5, 0) * LEAF_SCALE;

	float2 waves = GetTreeWaving(vPos, windScale, tcVector, texCoord);
	
	float3 vertPos1 = facingMatrix[0] * (tcVector.x + waves.x);
	float3 vertPos2 = facingMatrix[1] * (tcVector.y + waves.y);
	
	return (vertPos1 - vertPos2);
}