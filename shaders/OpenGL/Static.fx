// $INCLUDE variation_ps.h
// $INCLUDE shadowmap_ps.h
// $INCLUDE phong.h
// $INCLUDE drvsyn_lightning.h
// $INCLUDE quaternion.h

struct PsIn 
{
	float4	pos;
	
#ifdef DOFOG
	float4 texCoord;
#else
	float3 texCoord;
#endif // DOFOG

	float3	viewVec;
	float3	normal;
	
	float3	lights;
	
	float3	screenCord;
	float2	noiseCord;
};

uniform float4x4 	WVP;
uniform float4x4 	World;
uniform float3 		ViewPos;

uniform float4 		AmbientColor;
uniform float4		SunColor;
uniform float3		SunDir;

uniform float4 		BaseTextureTransform;
uniform sampler2D 	BaseTextureSampler;

#if defined(HAS_NIGHTLIGHT) || defined(HAS_TRANSITION)
uniform sampler2D 	BaseTexture2Sampler;
#endif // HAS_NIGHTLIGHT

#ifdef CUBEMAP
uniform sampler2D 		Reflection;
uniform samplerCUBE 	CubemapTexture;
#endif

#ifdef SPECULAR
uniform sampler2D 		SpecularMapSampler;
#endif // SPECULAR

#ifdef DOFOG
uniform samplerCUBE 	FogEnvMap;
uniform float3 			FogColor;
uniform float3 			FogParams;
#endif

uniform float 			PHONG;
uniform float 			WET_SCALE;
uniform float 			SPECULAR_SCALE;
uniform float2 			ENVPARAMS;

PsIn vs_main(
	float4 vPos,
	float2 texCoord,
	float4 normal
#ifdef INST
	, qtransform_t qtr
#endif
	)
{
	PsIn Out;

#ifdef INST
	float3 vertPos 	= qTransformVector(qtr, vPos.xyz);
	Out.pos 		= mul(WVP, float4(vertPos, 1));
	Out.normal 		= qRotateVector(qtr, normal.xyz);
#else
	float3 vertPos 	= mul(World, vPos).xyz;
	Out.pos 		= mul(WVP, vPos);
	Out.normal 		= mul(mat3(World), normal.xyz).xyz;
#endif

	Out.viewVec 	= ViewPos - vertPos;
	Out.noiseCord 	= vertPos.xz*0.025;
	Out.screenCord 	= Out.pos.xyw;

	Out.texCoord.x = texCoord.x*BaseTextureTransform.x + BaseTextureTransform.z;
	Out.texCoord.y = texCoord.y*BaseTextureTransform.y + BaseTextureTransform.w;
	Out.texCoord.z = normal.w; // transition factor
	
#ifdef DOFOG
	Out.texCoord.w = (FogParams.y - length(Out.viewVec)) * FogParams.z;
#endif // DOFOG

	Out.lights.rgb = ProcessWorldLights(Out.viewVec, Out.normal);

	return Out;
}

#define PUDDLE_GREYSCALE 0.2
#define REFL_TEXEL (1.0/512.0)

#ifdef CUBEMAP
float ComputePuddle(float2 noiseCoord, float3 normal)
{
	// get wet depth factor
	float wetDepth = tex2D(NoiseTexture, noiseCoord * WET_SCALE).r;
	wetDepth = saturate(pow(wetDepth,4.0) * 4.0 + saturate(ENVPARAMS.x-0.5) * 0.5);
	wetDepth *= saturate(dot(normal, float3(0,1,0)));

	return wetDepth;
}

#endif // CUBEMAP

//
// Pixel main function
//
float4 	ps_main(PsIn input) 
{
#ifdef VARIATION
	float4 baseTexture = texNoTile(BaseTextureSampler, input.texCoord.xy, 0.5);
#else
	float4 baseTexture = tex2D(BaseTextureSampler, input.texCoord.xy);
#endif

#ifdef HAS_TRANSITION
	float transition = input.texCoord.z;
	
#ifdef VARIATION
	float4 baseTexture2 = texNoTile(BaseTexture2Sampler, input.texCoord.xy, 0.5);
#else
	float4 baseTexture2 = tex2D(BaseTexture2Sampler, input.texCoord.xy);
#endif

	// perform transition
	baseTexture = lerp(baseTexture, baseTexture2, transition);
#endif // HAS_TRANSITION

#ifdef MODULATE
	baseTexture.a = 0.0f;
#endif

#ifdef ALPHATEST
	clip(baseTexture.a-0.5);
#endif

#ifdef USE_BASETEXTUREALPHA_SPECULAR
	float fSpecFactor = baseTexture.a*SPECULAR_SCALE;
#else
	
#	ifdef SPECULAR
	float4 specularMap = tex2D(SpecularMapSampler, input.texCoord.xy);
	float fSpecFactor = specularMap.r*SPECULAR_SCALE;
#	else
	float fSpecFactor = SPECULAR_SCALE;
#	endif

#endif // #ifndef USE_BASETEXTUREALPHA_SPECULAR

	float diffuseLight = saturate(dot(input.normal, SunDir));

#ifdef SHADOWMAP
	// apply sun shadow to diffuse light factor
	// also apply normal bias
	diffuseLight *= GetSunShadow(input.viewVec + ViewPos - input.normal * SHADOW_NORMAL_BIAS);
#endif

	float3 viewVec = normalize(input.viewVec);

#ifdef CUBEMAP
	float3 cubemapSample = texCUBE(CubemapTexture, reflect(-viewVec, input.normal)).rgb;
	float3 wetReflection = float3(0,0,0);
	
	// apply wetness
	if(ENVPARAMS.x > 0.0)
	{
		float fFresnel = 1.0 - saturate(dot(input.normal, viewVec));
		fFresnel = pow(fFresnel, 5);
		
		// read the reflection texture and apply it to cubemap
		float2 screenCoords = input.screenCord.xy / input.screenCord.z * 0.5 + 0.5 + REFL_TEXEL * 0.5;
		float3 reflTexture = tex2D(Reflection, screenCoords).rgb;
		
		// get wet depth factor
		float puddle = ComputePuddle(input.noiseCord, input.normal) * ENVPARAMS.x * WET_SCALE;
		
		// calculate puddle color
		float3 puddleColor = float3(PUDDLE_GREYSCALE);
		float3 wetBaseTexture = lerp(baseTexture.rgb, puddleColor, puddle);
		
		baseTexture.rgb = lerp(wetBaseTexture, puddleColor, puddle);
		
		wetReflection = (cubemapSample + reflTexture * 3.0) * puddle * fFresnel;
	}
	
	float4 result = baseTexture * AmbientColor;
	result.rgb += wetReflection;
	
#	ifdef USE_BASETEXTUREALPHA_SPECULAR
	result.rgb = lerp(result.rgb, cubemapSample.rgb, fSpecFactor);
	
	float specular = BlinnPhongSpecular(SunDir, viewVec, input.normal, 50.0) * 5.0;
	result.rgb += specular * diffuseLight * SunColor.rgb * fSpecFactor;
#	endif // USE_BASETEXTUREALPHA_SPECULAR

#else
	// nothing
	float4 result = baseTexture * AmbientColor;
	
#	ifdef USE_PHONG
	float specular = BlinnPhongSpecular(SunDir, viewVec, input.normal, 50.0) * 1.0;
	result.rgb += specular * diffuseLight * SunColor.rgb * PHONG;
#	endif
#endif // CUBEMAP

	result.rgb += baseTexture.rgb * SunColor.rgb * diffuseLight;
	
#if defined(SELFILLUMINATION) && defined(SPECULAR)
	result += baseTexture*specularMap.g*4;
#endif // SELFILLUMINATION

#ifdef PIXEL_LIGHTING
	result.rgb += ProcessWorldLightsSpecular(input.viewVec, input.normal, fSpecFactor)*baseTexture.rgb;
#else
	result.rgb += input.lights.rgb*baseTexture.rgb;
#endif // PIXEL_LIGHTING

#if defined(DOFOG) && !defined(MODULATE)
	float fog_intep = input.texCoord.w;
	
	float lightsOnFogIntensity = 1.0 - pow(saturate((1.0 - fog_intep)-0.2), 10);
	
	float3 fogEnvMap = texCUBE(FogEnvMap, -viewVec).rgb;	
	result.rgb = lerp(result.rgb, fogEnvMap, saturate(1.0 - fog_intep));
#else
	float lightsOnFogIntensity = 1.0;
#endif // DOFOG

// Some lights are performed after FOG
#ifdef HAS_NIGHTLIGHT
	float4 nightLight = tex2D(BaseTexture2Sampler, input.texCoord.xy);
	result.rgb += nightLight.rgb * ENVPARAMS.y * lightsOnFogIntensity;
#endif // HAS_NIGHTLIGHT

	return result;
}

//----------------------------------------
// separate entry points
//----------------------------------------

#ifdef VERTEX

// can be generated
layout(location = 0) in vec4 input_vPos;
layout(location = 1) in vec2 input_texCoord;
layout(location = 2) in vec4 input_normal;

#ifdef INST
layout(location = 3) in vec4 input_inst_q;
layout(location = 4) in vec4 input_inst_p;
#endif

out PsIn output;

void main()
{
#ifdef INST
	qtransform_t qtr;
	qtr.rot = input_inst_q;
	qtr.pos = input_inst_p;
	output = vs_main(input_vPos, input_texCoord, input_normal, qtr);
#else
	output = vs_main(input_vPos, input_texCoord, input_normal);
#endif
	gl_Position = output.pos;
}
#endif

#ifdef FRAGMENT

in PsIn output;
out vec4 fragColor;

void main()
{
	fragColor = ps_main(output);
}

#endif