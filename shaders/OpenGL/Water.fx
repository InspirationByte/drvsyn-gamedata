// $INCLUDE shadowmap_ps.h
// $INCLUDE phong.h
// $INCLUDE drvsyn_lightning.h

struct PsIn {
	float4 pos;
	
#ifdef DOFOG
	float3 texCoord;
#else
	float2 texCoord;
#endif // DOFOG

	float3 tangent;
	float3 binormal;
	float3 normal;

	float3 viewVec;
	float3 lighting;
	float3 screenCord;
};

uniform float4x4 	WVP;
uniform float4x4 	World;
uniform float3 		ViewPos;

uniform float 		GameTime;
uniform float4 		WaterColor;
uniform float4 		AmbientColor;
uniform float 		SPECULAR_SCALE;

uniform float4		SunColor;
uniform float3		SunDir;

uniform float4 		BaseTextureTransform;
uniform float2 		ENVPARAMS;

#ifdef DOFOG
uniform float3 		FogParams;
uniform samplerCUBE FogEnvMap;
#endif

uniform sampler2D 	BumpTextureSampler;
uniform samplerCUBE CubemapSampler;
uniform sampler2D 	ReflectionSampler;


#define HEIGHT_SCALE (0.01+ENVPARAMS.x*0.1)
#define HEIGHT_SCALE_NORMAL (0.01)
#define HEIGHT_DIST		0.3
#define HEIGHT_DIST2	0.1

PsIn vs_main( 
	float4 vPos,
	float2 texCoord 
	)
{
	PsIn Out;

	float4 vertPos = mul(World,vPos);
	
	float fWaveOfs = GameTime*2.0-ENVPARAMS.x*0.07;
	float2 waveXZ = float2(sin(fWaveOfs+vertPos.x*HEIGHT_DIST), sin(fWaveOfs+vertPos.z*HEIGHT_DIST));
	waveXZ += float2(sin(fWaveOfs+vertPos.x*HEIGHT_DIST2), sin(fWaveOfs+vertPos.z*HEIGHT_DIST2));
	
	waveXZ *= 0.5;
	
	float waterHeight = (waveXZ.x + waveXZ.y)*HEIGHT_SCALE * WaterColor.w;
	
	Out.pos = mul(WVP,vPos+float4(0,waterHeight,0, 0));
	
	Out.screenCord = Out.pos.xyw;
	
	float3 viewVec = ViewPos-vertPos.xyz;

	Out.viewVec = viewVec;
	Out.normal = normalize(float3(waveXZ.x*HEIGHT_SCALE_NORMAL, 1, waveXZ.y*HEIGHT_SCALE_NORMAL));

	Out.tangent = cross(float3(0,0,1), Out.normal);
	Out.binormal = cross(float3(1,0,0), Out.normal);

	Out.lighting = ProcessWorldLightsNoNormal(Out.viewVec);

	Out.texCoord.x = texCoord.x*BaseTextureTransform.x + BaseTextureTransform.z;
	Out.texCoord.y = texCoord.y*BaseTextureTransform.y + BaseTextureTransform.w;
	
#ifdef DOFOG
	Out.texCoord.z = (FogParams.y - length(Out.viewVec)) * FogParams.z;
#endif // DOFOG
	
	return Out;
}

#define BUMP_INTENSITY (0.02+ENVPARAMS.x*0.2)
#define REFL_TEXEL (1.0/512.0)

float4 ps_main(PsIn input)
{
	float3 bump = tex2D(BumpTextureSampler, input.texCoord.xy).rgb;
	bump = bump * 2.0 - 1.0;

	float3 bumpTex = float3(
		dot(input.tangent, bump),
		dot(input.binormal, bump),
		dot(input.normal, bump)
	);

	float3 wbump = lerp(input.normal, bumpTex, BUMP_INTENSITY);

	float4 outcolor = AmbientColor * float4(WaterColor.rgb, 1); // * UnderWaterTexture

	float3 viewVec = normalize(input.viewVec);
	float surfDistance = 1.0 - saturate(1.0 / length(input.viewVec)) * 2.5;

	float3 cubemapCoord = reflect(-viewVec, wbump);
	
	// approximated
	float freshnel = pow(1.0-saturate(dot(viewVec, wbump)), 5.0)*SPECULAR_SCALE;
	float cubeMip = (1.0-freshnel)*1.5;
	
	outcolor.a = surfDistance + freshnel;//+ depth;
	
	float3 cubemapSample = texCUBE(CubemapSampler, cubemapCoord/*, cubeMip*/).rgb;

	{
		float2 screenCoords = (input.screenCord.xy / input.screenCord.z)*0.5+0.5 + REFL_TEXEL*0.5;
		
		screenCoords += float2((wbump.x+wbump.z)*0.1,0.0);
	
		float3 reflTexture = tex2D(ReflectionSampler, screenCoords).rgb;

		cubemapSample += reflTexture * 2.0;
	}
	
	float diffuseLight = saturate(dot(wbump, SunDir));
	
#ifdef SHADOWMAP	
	// apply sun shadow to diffuse light factor
	diffuseLight *= GetSunShadow(input.viewVec + ViewPos);
#endif
	
	outcolor.rgb += WaterColor.rgb*SunColor.rgb*diffuseLight;
	outcolor.rgb += WaterColor.rgb * input.lighting*0.25;
	outcolor.rgb = lerp(outcolor.rgb, cubemapSample, freshnel*(saturate(cubemapSample)*0.5+0.5));

	float specular = BlinnPhongSpecular(SunDir, viewVec, bumpTex, 100.0) * 5.0;
	float specDiffuse = specular*diffuseLight*SPECULAR_SCALE;
	
	//outcolor.a += specDiffuse;
	outcolor.rgb += SunColor.rgb*specDiffuse;
	
#ifdef DOFOG
	float3 fogEnvMap = texCUBE(FogEnvMap, -viewVec).rgb;
	outcolor.rgb = lerp(outcolor.rgb,fogEnvMap,clamp(1.0 - input.texCoord.z,0,1));
#endif

	return outcolor;
}

//----------------------------------------
// separate entry points
//----------------------------------------

#ifdef VERTEX

// can be generated
layout(location = 0) in vec4 input_vPos;
layout(location = 1) in vec2 input_texCoord;
layout(location = 2) in vec4 input_normal;

out PsIn output;

void main()
{
	output = vs_main(input_vPos, input_texCoord/*, input_normal*/);

	gl_Position = output.pos;
}
#endif

#ifdef FRAGMENT

in PsIn output;
out vec4 fragColor;

void main()
{
	fragColor = ps_main(output);
}

#endif