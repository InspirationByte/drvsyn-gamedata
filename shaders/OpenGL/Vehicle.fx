#define 	SHADOW_BIAS_SCALE 0.05

// $INCLUDE phong.h
// $INCLUDE drvsyn_lightning.h
// $INCLUDE shadowmap_ps.h
// $INCLUDE vehicle_vs.h

// input vertex
struct VsIn
{
	float4 vPos;
	float2 texCoord;	// send fog coordinate as texture coords
	float3 normal;
	float4 boneIndex;
	float4 boneWeight;

	float4 vPos1;
	float3 normal1;
};

uniform float4x4 	WVP;
uniform float4x4 	View;
uniform float4x4 	World;
uniform float3 		ViewPos;
uniform float4 		BaseTextureTransform;

#ifdef USE_LPLATE
uniform float4 		LicenseRect;
#endif // USE_LPLATE

#ifdef DOFOG
uniform float3 		FogParams;
uniform samplerCUBE FogEnvMap ;
uniform float3 		FogColor;
#endif

uniform float4 		AmbientColor;
uniform float4		SunColor;
uniform float3		SunDir;
uniform float 		SPECULAR_SCALE;

uniform sampler2D 	BaseTextureSampler;

#ifdef USE_DAMAGE_TEXTURE
uniform sampler2D 	DamageTextureSampler;
#else
#	define 	DamageTextureSampler BaseTextureSampler
#endif // USE_DAMAGE_TEXTURE

#ifdef USE_COLORMAP
uniform sampler2D 	ColormapSampler;
#endif // USE_COLORMAP

#ifdef USE_SPECULAR
uniform sampler2D 	SpecularMapSampler;
#endif // USE_SPECULAR

#ifdef USE_BASETEXTUREALPHA_SPECULAR
#define USE_SPECULAR
#endif // USE_BASETEXTUREALPHA_SPECULAR

#ifdef USE_CUBEMAP
uniform sampler2D 		SphReflection;
uniform samplerCUBE 	CubemapTexture;
#endif

#define MAX_CAR_COLORS 2
uniform float4			CarColor[MAX_CAR_COLORS];

struct PsIn 
{
	float4 pos;
	
#ifdef DOFOG
	float4 texCoord;
#else
	float3 texCoord;
#endif // DOFOG
	
	float3 viewVec;
	float3 normal;
	
#ifndef PIXEL_LIGHTING
	float3 lights;
#endif // PIXEL_LIGHTING

#ifdef USE_CUBEMAP
	float3 cubemapCoord;
#endif
};

PsIn vs_main(VsIn input)
{
	PsIn Out;

	float interpVal 	= S_GetDamageInterpolation(input.boneIndex, input.boneWeight);
	
	float4 transPosit 	= lerp(input.vPos, input.vPos1, interpVal );

	Out.pos 			= mul(WVP,transPosit);
	
	Out.normal 			= mul(mat3(World),lerp(input.normal, input.normal1, interpVal));
	
	float3 wPos 		= mul(World,transPosit).xyz;
	
	Out.viewVec 		= ViewPos-wPos;
	
#ifdef USE_CUBEMAP
	Out.cubemapCoord = reflect(-normalize(Out.viewVec), Out.normal);
	float3 dir = mul(mat3(View), Out.cubemapCoord);
	Out.cubemapCoord = dir * float3(0.5, 0.5, 1) + 0.5;
#endif

#ifdef USE_LPLATE
	Out.texCoord.xy	= input.texCoord * LicenseRect.zw + LicenseRect.xy;
#else
	Out.texCoord.xy	= input.texCoord;
#endif // USE_LPLATE
	
	float damageTextureDot = abs(dot(input.normal, float3(0.0,0.0,1.0)));
	
	Out.texCoord.z = pow(interpVal, 1.0 + damageTextureDot);
	
#ifndef PIXEL_LIGHTING
	Out.lights = ProcessWorldLights(Out.viewVec, Out.normal);
#endif // PIXEL_LIGHTING

#ifdef DOFOG
	Out.texCoord.w = (FogParams.y - length(wPos-ViewPos)) * FogParams.z;
#endif

	return Out;
}

//
// Pixel main function
//
float4 	ps_main(PsIn input, float face) 
{
	float fSpecFactor 		= SPECULAR_SCALE;
	float4 baseTexture 		= tex2D(BaseTextureSampler, input.texCoord.xy);
	
#ifdef ALPHATEST
	clip(baseTexture.a-0.5);
#endif

#ifdef USE_DAMAGE_TEXTURE
	float4 damageTexture 	= tex2D(DamageTextureSampler, input.texCoord.xy);
#else
	float4 damageTexture 	= baseTexture;
#endif // USE_DAMAGE_TEXTURE
	
	// transition between damaged texture
	baseTexture = lerp(baseTexture, damageTexture, input.texCoord.z);

#ifdef USE_COLORMAP
	float4 colorMap = tex2D(ColormapSampler, input.texCoord.xy);

	// start with base details
	float3 resultColor = baseTexture.rgb;
	float resultSpec = fSpecFactor;

	// TODO: more colors!
	for(int i = 0; i < 2; i++)
	{
		//if(colorMap.r == 0)
		//	break;
		
		float3 color = baseTexture.rgb * CarColor[i].rgb;
		float spec = fSpecFactor * CarColor[i].a;
		
		// mix
		resultColor = lerp(resultColor, color, colorMap.r);
		resultSpec = lerp(resultSpec, spec, colorMap.r);
		
		colorMap.xyzw = colorMap.yzwx; // shift
	}
	
	baseTexture.rgb = resultColor;
	fSpecFactor = resultSpec;
#endif // USE_COLORMAP

	// begin result, apply ambient lights
	float4 result = float4(baseTexture.rgb*AmbientColor.rgb, baseTexture.a);

	// backfacing polygons means inside car, modify ambient lighting
	if( face < 0 )
		result *= 0.15;

	// also invert normal
	input.normal *= face;
	
	float3 viewVec = normalize(input.viewVec);
	
	float diffuseLight = saturate(dot(input.normal, SunDir));
	
#ifdef SHADOWMAP	
	// apply sun shadow to diffuse light factor
	diffuseLight *= GetSunShadow(input.viewVec + ViewPos);
#endif
	
	result.rgb += baseTexture.rgb*SunColor.rgb*diffuseLight;

#ifdef USE_SPECULAR

#ifdef USE_BASETEXTUREALPHA_SPECULAR
	float2 specularMap = baseTexture.aa;
#else
	float2 specularMap = tex2D(SpecularMapSampler, input.texCoord.xy).rg;
#endif // USE_BASETEXTUREALPHA_SPECULAR

	fSpecFactor *= specularMap.r;
	
	float specular = BlinnPhongSpecular(SunDir, viewVec, input.normal, 500.0);
	
	result.rgb += specular*diffuseLight*SunColor.rgb*fSpecFactor * 10.0;

#ifdef SELFILLUMINATION
	result += baseTexture*specularMap.g*4;
#endif // SELFILLUMINATION
#endif // USE_SPECULAR

#ifndef PIXEL_LIGHTING
	// add vertex light sources
	result.rgb += baseTexture.rgb*input.lights;
#else
	// calculate pixel light sources
	result.rgb += baseTexture.rgb*ProcessWorldLightsSpecular(input.viewVec, input.normal, fSpecFactor);
#endif // PIXEL_LIGHTING
	
#ifdef USE_CUBEMAP
	// approximated
	float freshnel = pow(1.0-saturate(dot(viewVec, input.normal.xyz)), 1.25) * fSpecFactor;

#if 0
	float cubeMip = freshnel*3.0;
	float3 cubemapSample = texCUBE(CubemapTexture, input.cubemapCoord, cubeMip).rgb;
	result.rgb += cubemapSample * max(diffuseLight,0.5) * freshnel;
#else
	// read the reflection texture and apply it to cubemap
	float3 cubemapSample = tex2D(SphReflection, input.cubemapCoord.xy).rgb;
	result.rgb += cubemapSample * freshnel;
#endif

	
#endif // USE_CUBEMAP

#ifdef DOFOG
	float3 fogEnvMap = texCUBE(FogEnvMap, -viewVec).rgb;	
	result.rgb = lerp(result.xyz, fogEnvMap, saturate(1.0f - input.texCoord.w));
#endif

	return result;
}

//----------------------------------------
// separate entry points
//----------------------------------------

#ifdef VERTEX

// can be generated
layout(location = 0) in vec4 input_vPos;
layout(location = 1) in vec2 input_texCoord;
layout(location = 2) in vec3 input_normal;

layout(location = 3) in vec4 input_boneIndex;
layout(location = 4) in vec4 input_boneWeight;

layout(location = 5) in vec4 input_vPos_dam;
layout(location = 6) in vec3 input_normal_dam;

out PsIn output;

void main()
{
	VsIn input;
	input.vPos = input_vPos;
	input.texCoord = input_texCoord;
	input.normal = input_normal;
	input.boneIndex = input_boneIndex;
	input.boneWeight = input_boneWeight;
	input.vPos1 = input_vPos_dam;
	input.normal1 = input_normal_dam;
	
	output = vs_main(input);

	gl_Position = output.pos;
}
#endif

#ifdef FRAGMENT

in PsIn output;
out vec4 fragColor;

void main()
{
	fragColor = ps_main(output, gl_FrontFacing ? 1 : -1);
}

#endif