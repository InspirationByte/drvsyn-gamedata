// $INCLUDE phong.h
// $INCLUDE drvsyn_lightning.h
// $INCLUDE shadowmap_ps.h

struct PsIn {
	float4 pos;
	
#ifdef DOFOG
	float3 texCoord;
#else
	float2 texCoord;
#endif

	float4 color;
	float3 viewVec;
	
#ifdef ADVANCED_LIGHTING
	float3 lighting	;
#endif // ADVANCED_LIGHTING
};

uniform sampler2D 		BaseTextureSampler;

#ifdef DOFOG
uniform float3 			FogParams;
uniform samplerCUBE 	FogEnvMap;
uniform float3 			FogColor;
#endif

uniform float4x4 		WVP;
uniform float3 			ViewPos;
uniform float4 			AmbientColor;
uniform float4			SunColor;

PsIn vs_main(
	float4 vPos, 
	float2 texCoord,
	float4 color
	)
{
	PsIn Out;

	Out.pos = mul(WVP,vPos);
	Out.viewVec = ViewPos-vPos.xyz;

	Out.color = color;
	
#ifdef ADVANCED_LIGHTING
	Out.lighting = ProcessWorldLightsNoNormal(Out.viewVec);
#endif // ADVANCED_LIGHTING

	Out.texCoord.xy = texCoord;
	
#ifdef DOFOG
	Out.texCoord.z = (FogParams.y - length(Out.viewVec)) * FogParams.z;
#endif // DOFOG
	
	return Out;
}

float4 ps_main(PsIn input)
{
	float4 tex = tex2D(BaseTextureSampler, input.texCoord.xy);

#ifdef ALPHATEST
	clip(tex.a - 0.5);
#endif

	tex *= input.color;

#if defined(ADDITIVE)
	float4 result = tex;
#else
	float4 result = tex * AmbientColor;
#endif
	
#ifdef ADVANCED_LIGHTING
	float diffuse = 1.0;
#ifdef SHADOWMAP	
	// apply sun shadow to diffuse light factor
	diffuse *= GetSunShadow(input.viewVec + ViewPos);
#endif
	result.rgb += tex.rgb * SunColor.rgb * diffuse;
	result.rgb += tex.rgb * input.lighting;
#endif // ADVANCED_LIGHTING

#ifdef DOFOG
	float3 fogEnvMap = texCUBE(FogEnvMap, -input.viewVec).rgb;	
	float fog_intep = input.texCoord.z;
#endif
	
#if defined(ALPHATEST) && defined(DOFOG)
	result.rgb = lerp(result.rgb,fogEnvMap,saturate(1.0 - fog_intep));
#elif defined(ADDITIVE) && defined(DOFOG)
	result.rgb = lerp(result.rgb,float3(0.0, 0.0, 0.0),pow(saturate((1.0 - fog_intep)-0.25), 10));
#elif defined(DOFOG)
	result.a = lerp(result.a,0.0,saturate(1.0 - fog_intep));
#endif

	return max(result, 0.0);
}

//----------------------------------------
// separate entry points
//----------------------------------------

#ifdef VERTEX

// can be generated
layout(location = 0) in vec4 input_vPos;
layout(location = 1) in vec2 input_texCoord;
layout(location = 2) in vec4 input_color;

out PsIn output;

void main()
{
	output = vs_main(input_vPos, input_texCoord, input_color);
	gl_Position = output.pos;
}
#endif

#ifdef FRAGMENT

in PsIn output;
out vec4 fragColor;

void main()
{
	fragColor = ps_main(output);
}

#endif