#ifdef SHADOWMAP
// $INCLUDE rand.h

uniform sampler2D	ShadowMap;
uniform float4x4	SunMatrix;
uniform float4		SHADOWPROPS;

#define SHADOW_OFFSET 			SHADOWPROPS.x
#define SHADOW_TEXEL 			SHADOWPROPS.y

#ifndef SHADOW_BIAS_SCALE
#define SHADOW_BIAS_SCALE		0.02
#endif // SHADOW_BIAS_SCALE

#define SHADOW_NORMAL_BIAS		0.0125

#define SHADOW_DEPTH_BIAS_1 	-SHADOWPROPS.w
#define SHADOW_DEPTH_BIAS_2 	-SHADOWPROPS.w

#define SHADOW_SOFTNESS 		SHADOW_TEXEL

// samples shadow map
float SampleShadow(sampler2D ShadowMap, float sceneDepth, float2 projVector)
{
	return step( sceneDepth, tex2D( ShadowMap, projVector + rand(projVector) ).r );
}

// returns shadow or light (0..1)
float GetSunShadow(float3 worldPos)
{
	float4 shadowPos 	= mul(SunMatrix, float4(worldPos, 1.0));
	float sceneDepth 	= (1.0 - shadowPos.z);
	
	// compute projection
	float2 shadowTexProj = (shadowPos.xy + shadowPos.ww) * 0.5 - SHADOW_OFFSET;//(shadowPos.xy * 0.5 + 0.5);
	// this has to be done because of OpenGL matrix (weird that it's X axis...)
	shadowTexProj = 1.0 - shadowTexProj;
	
	// that corrects some bias
	shadowTexProj.xy -= SHADOWPROPS.z;
	
	// vignette code for shadow fade
	float2 fadeVec = (shadowTexProj * (1.0 - shadowTexProj));
	float shadowFade = saturate(pow(saturate(fadeVec.x) * saturate(fadeVec.y) * 25.0, 0.25));

	float distantSlope = sceneDepth * SHADOW_BIAS_SCALE;
	float cmpDepth = (sceneDepth - distantSlope*distantSlope + SHADOW_DEPTH_BIAS_1) * 0.5 + 0.5;

#ifdef SHADOWMAP_HQ
	float shadowTex = SampleShadow( ShadowMap, cmpDepth, shadowTexProj)*0.1428;
	shadowTex += SampleShadow( ShadowMap, cmpDepth, shadowTexProj+float2(SHADOW_SOFTNESS,SHADOW_SOFTNESS))*0.1428;
	shadowTex += SampleShadow( ShadowMap, cmpDepth, shadowTexProj+float2(-SHADOW_SOFTNESS,-SHADOW_SOFTNESS))*0.1428;
	shadowTex += SampleShadow( ShadowMap, cmpDepth, shadowTexProj+float2(SHADOW_SOFTNESS,0))*0.1428;
	shadowTex += SampleShadow( ShadowMap, cmpDepth, shadowTexProj+float2(0,SHADOW_SOFTNESS))*0.1428;
	shadowTex += SampleShadow( ShadowMap, cmpDepth, shadowTexProj+float2(-SHADOW_SOFTNESS,0))*0.1428;
	shadowTex += SampleShadow( ShadowMap, cmpDepth, shadowTexProj+float2(0,-SHADOW_SOFTNESS))*0.1428;
#else
	float shadowTex = SampleShadow(ShadowMap, cmpDepth, shadowTexProj);
#endif

	return lerp(1.0, shadowTex, shadowFade);
}

#endif