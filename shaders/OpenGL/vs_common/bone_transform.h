//--------------------------------
// Skinning quaternions

// multiply two quaternions
half4 quatMul ( half4 q1, half4 q2 )
{
    half3 im = q1.w * q2.xyz + q1.xyz * q2.w + cross ( q1.xyz, q2.xyz );
    half4 dt = q1 * q2;
    half  re = dot( dt, half4( -1.0, -1.0, -1.0, 1.0 ) );

    return half4( im, re );
}

// rotate vector by quaternion
half4 quatRotate(half3 p, half4 q )
{
    half4 temp = quatMul( q, half4( p, 0.0 ) );
	
    return quatMul ( temp, half4 ( -q.x, -q.y, -q.z, q.w ) );
}

//----------------------------------------------------------------------------------

// maximum bones for EQ. It could be 128, but there are some vertex-shader reserved constants
#define MAX_BONES 100

struct bone_t
{
	float4	quat;
	float4	origin;
};

bone_t Bones[MAX_BONES];

// rotates and translates vector by bone
half3 ApplyBoneTransformToVector(half3 pos, int nBone)
{
	if(nBone < 0)
		return pos;

    return bq.origin.xyz + quatRotate(pos, bq.quat).xyz;
}

// rotates vector by bone
half3 ApplyBoneRotationToVector(half3 pos, int nBone)
{
	if(nBone < 0)
		return pos;
	
    return quatRotate(pos, bq.quat).xyz;
}

half4 ApplyBoneTransformToVertex(half4 pos, int nBone)
{
	if(nBone < 0)
		return pos;

	return half4(boneTransf(bq, pos), 1);
}

// rotates TBN matrix by bone matrix
half3x3 ApplyBoneTransformToTBN(half3x3 TBN, int nBone)
{
	if(nBone < 0)
		return TBN;
	
	half3x3 outTBN;
	bone_t bq = Bones[boneIndex]

	outTBN[0] = vectorTransf(bq, TBN[0]);
	outTBN[1] = vectorTransf(bq, TBN[1]);
	outTBN[2] = vectorTransf(bq, TBN[2]);

	return outTBN;
}

// applies 4 bone transformation to vertex
half4 ApplyFourBoneToVertex(half4 pos, half4 bones, half4 weights)
{
	if(int(bones.x) < 0)
		return pos;

	half4 sbones = bones;
	half4 sweights = weights;

	half3 vPos = boneTransf(Bones[int(sbones.x)], pos).xyz * sweights.x;

	for(int i = 0; i < 3; i++)
	{
		sbones = sbones.yzwx;
		sweights = sweights.yzwx;

		if(int(sbones.x) < 0)
			break;

		vPos += boneTransf(Bones[int(sbones.x)], pos).xyz * sweights.x;
	}

	return half4(vPos, 1.0);
}

// applies 4 bone transformation to vertex and normal
half4 ApplyFourBoneTransformToVertexNormal(half4 pos, inout half3 normal, half4 bones, half4 weights)
{
	if(int(bones.x) < 0)
		return pos;

	half4 sbones = bones;
	half4 sweights = weights;

	half3 vPos = boneTransf(Bones[int(sbones.x)], pos).xyz * sweights.x;

	half3 outNormal;
	outNormal = vectorTransf(Bones[int(sbones.x)], normal) * sweights.x;

	for(int i = 0; i < 3; i++)
	{
		sbones = sbones.yzwx;
		sweights = sweights.yzwx;

		if(int(sbones.x) < 0)
			break;

		vPos += boneTransf(Bones[int(sbones.x)], pos).xyz * sweights.x;

		outNormal += vectorTransf(Bones[int(sbones.x)], normal) * sweights.x;
	}

	normal = outNormal;

	return half4(vPos, 1.0);
}

// applies 4 bone transformation to vertex and TBN
half4 ApplyFourBoneTransformToVertexTBN(half4 pos, inout half3x3 TBN, half4 bones, half4 weights)
{
	if(int(bones.x) < 0)
		return pos;

	half4 sbones = bones;
	half4 sweights = weights;

	half3 vPos = boneTransf(Bones[int(sbones.x)], pos).xyz * sweights.x;

	half3x3 outTBN;

	outTBN[0] = vectorTransf(Bones[int(sbones.x)], TBN[0]) * sweights.x;
	outTBN[1] = vectorTransf(Bones[int(sbones.x)], TBN[1]) * sweights.x;
	outTBN[2] = vectorTransf(Bones[int(sbones.x)], TBN[2]) * sweights.x;

	for(int i = 0; i < 3; i++)
	{
		sbones = sbones.yzwx;
		sweights = sweights.yzwx;

		if(int(sbones.x) < 0)
			break;

		vPos += boneTransf(Bones[int(sbones.x)], pos).xyz * sweights.x;

		outTBN[0] += vectorTransf(Bones[int(sbones.x)], TBN[0]) * sweights.x;
		outTBN[1] += vectorTransf(Bones[int(sbones.x)], TBN[1]) * sweights.x;
		outTBN[2] += vectorTransf(Bones[int(sbones.x)], TBN[2]) * sweights.x;
	}

	TBN[0] = outTBN[0];
	TBN[1] = outTBN[1];
	TBN[2] = outTBN[2];

	return half4(vPos, 1.0);
}