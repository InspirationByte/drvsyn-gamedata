// common base vertex
struct BaseVertex_t
{
	half4 position: 	POSITION0;	// position + tc_X
	half4 normal: 		TEXCOORD0; 	// normal 	+ tc_Y
}