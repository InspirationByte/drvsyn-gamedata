struct PsIn 
{
	float4 pos;
	float2 texCoord;
};

uniform float4x4 	WVP;
uniform sampler2D 	Base1;
uniform sampler2D 	Base2;
uniform float4 		AmbientColor;
uniform float		InterpFactor;

PsIn vs_main(
	float4 vPos,
	float2 texCoord
	)
{
	PsIn Out;

	Out.pos = mul(WVP, vPos);
	Out.texCoord = texCoord;

	return Out;
}

float4 ps_main(PsIn input)
{
	float4 base1 = tex2D(Base1, input.texCoord.xy);
	float4 base2 = tex2D(Base2, input.texCoord.xy);

	float4 result = lerp(base1, base2, InterpFactor) * AmbientColor;

	return result;
}

//----------------------------------------
// separate entry points
//----------------------------------------

#ifdef VERTEX

// can be generated
layout(location = 0) in vec4 input_vPos;
layout(location = 1) in vec2 input_texCoord;

out PsIn output;

void main()
{
	output = vs_main(input_vPos, input_texCoord);
	gl_Position = output.pos;
}
#endif

#ifdef FRAGMENT

in PsIn output;
out vec4 fragColor;

void main()
{
	fragColor = ps_main(output);
}

#endif