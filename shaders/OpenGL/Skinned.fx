// $INCLUDE shadowmap_ps.h
// $INCLUDE phong.h
// $INCLUDE drvsyn_lightning.h
// $INCLUDE skinning_vs.h
// $INCLUDE fluffy_tree_vs.h

struct PsIn 
{
	float4 pos;
	
#ifdef DOFOG
	float3 texCoord;
#else
	float2 texCoord;
#endif // DOFOG

	float3 viewVec;
	float3 normal;
	
	float3	lights;
};

uniform float4x4 		WVP;
uniform float4x4 		View;
#ifndef INST
uniform float4x4 		World;
#endif

uniform float3 			ViewPos;


#ifdef DOFOG
uniform samplerCUBE 	FogEnvMap;
uniform float3 			FogParams;
#endif

uniform float4 			BaseTextureTransform;
uniform sampler2D 		BaseTextureSampler;

#ifdef HAS_NIGHTLIGHT_TEXTURE
uniform sampler2D 		NightLightSampler;
#endif // HAS_NIGHTLIGHT_TEXTURE

uniform float4 			AmbientColor;
uniform float4 			SunColor;
uniform float3			SunDir;
	
uniform float 			SPECULAR_SCALE;
uniform float2 			ENVPARAMS;

#ifdef USE_BASETEXTUREALPHA_SPECULAR
#define USE_SPECULAR
#endif // USE_BASETEXTUREALPHA_SPECULAR

#ifdef SPECULAR
uniform sampler2D 		SpecularMapSampler;
#endif // SPECULAR

#ifdef USE_CUBEMAP
uniform samplerCUBE 	CubemapTexture;
#endif

PsIn vs_main(	
	float4 vPos,
	float2 texCoord,
	float3 normal
	// skinning and instancing can't be used both
#ifdef SKIN
	, float4 boneIndex
	, float4 boneWeight
#endif
#ifdef INST
	, float4x4 World
#endif
	)
{
	PsIn Out;

#ifdef SKIN
	float4 transPosit 	= S_TransformVertexWeightsAndNormal(vPos, normal, boneIndex, boneWeight);
#else
	float4 transPosit	= vPos;
#endif	

#ifdef TREE_LEAVES
	float3 leavesOffset = GetTreeLeavesOffset(vPos.xyz, View, World, texCoord, 1.0 + ENVPARAMS.x * 4.0);
	transPosit += float4(leavesOffset, 0);
#endif // TREE_LEAVES

#ifdef INST
	transPosit 			= mul(World, transPosit);
	float4 wPos 		= transPosit;
#else
	float4 wPos 		= mul(World, transPosit);
#endif

	Out.pos 			= mul(WVP, transPosit);
	Out.viewVec 		= ViewPos - wPos.xyz;

	Out.normal 			= mul(mat3(World), normal);

	Out.texCoord.x 		= texCoord.x*BaseTextureTransform.x + BaseTextureTransform.z;
	Out.texCoord.y 		= texCoord.y*BaseTextureTransform.y + BaseTextureTransform.w;
	
	Out.lights 			= ProcessWorldLights(Out.viewVec, Out.normal);

#ifdef DOFOG
	Out.texCoord.z = (FogParams.y - length(Out.viewVec)) * FogParams.z;
#endif

	return Out;
}

//
// Pixel main function
//
float4 	ps_main(PsIn input) 
{
	float fSpecFactor = SPECULAR_SCALE;
	float4 baseTexture = tex2D(BaseTextureSampler, input.texCoord.xy);

#ifdef ALPHATEST
	clip(baseTexture.a-0.5);
#endif

	float4 result = baseTexture*AmbientColor;
	result.a = baseTexture.a;
	
	float3 viewVec = normalize(input.viewVec);
	
	float diffuseLight = saturate(dot(input.normal, SunDir));
	
#ifdef SHADOWMAP	
	// apply sun shadow to diffuse light factor
	diffuseLight *= GetSunShadow(input.viewVec + ViewPos);
#endif
	
	result.rgb += diffuseLight*baseTexture.rgb*SunColor.rgb;

#ifdef ALPHASELFILLUMINATION
	result += baseTexture*baseTexture.a*4;
#endif // SELFILLUMINATION

	result.rgb += input.lights*baseTexture.rgb;//ProcessWorldLights(input.viewVec, input.normal)*baseTexture.rgb;
	
#ifdef USE_BASETEXTUREALPHA_SPECULAR
	fSpecFactor *= baseTexture.a;
#elif SPECULAR
	fSpecFactor *= tex2D(SpecularMapSampler, input.texCoord.xy).r;
#endif // USE_BASETEXTUREALPHA_SPECULAR

#ifdef USE_CUBEMAP
	float3 cubemapCoord = reflect(-viewVec, input.normal.xyz);

	// approximated
	float freshnel = (1.0-saturate(dot(viewVec, input.normal.xyz)*0.85))*fSpecFactor;

	float cubeMip = freshnel*1.0;

	float3 cubemapSample = texCUBE(CubemapTexture, cubemapCoord/*, cubeMip*/).rgb;

	result.rgb += cubemapSample * max(diffuseLight,0.25) * freshnel;
#endif // USE_CUBEMAP

#if defined(DOFOG) && !defined(MODULATE)
	float fog_intep = input.texCoord.z;
	
	float lightsOnFogIntensity = 1.0-pow(saturate((1.0 - fog_intep)-0.2), 10);
	
	float3 fogEnvMap = texCUBE(FogEnvMap, -viewVec).rgb;	
	result.rgb = lerp(result.rgb, fogEnvMap, saturate(1.0f - fog_intep));
#else
	float lightsOnFogIntensity = 1.0;
#endif

// Some lights are performed after FOG
#ifdef HAS_NIGHTLIGHT_TEXTURE
	float4 nightLight = tex2D(NightLightSampler, input.texCoord.xy);
	result.rgb += nightLight.rgb*ENVPARAMS.y*lightsOnFogIntensity;
#endif // HAS_NIGHTLIGHT_TEXTURE

	return result;
}

//----------------------------------------
// separate entry points
//----------------------------------------

#ifdef VERTEX

// can be generated
layout(location = 0) in vec4 input_vPos;
layout(location = 1) in vec2 input_texCoord;

#ifdef INST
layout(location = 2) in vec3 input_normal;
layout(location = 3) in mat4 input_inst;
#else
layout(location = 4) in vec3 input_normal;
#endif

#ifdef SKIN
layout(location = 5) in vec4 input_boneIndex;
layout(location = 6) in vec4 input_boneWeight;
#endif

out PsIn output;

void main()
{
#if defined(SKIN)
	output = vs_main(input_vPos, input_texCoord, input_normal, input_boneIndex, input_boneWeight);
#elif defined(INST)
	output = vs_main(input_vPos, input_texCoord, input_normal, transpose(input_inst));
#else
	output = vs_main(input_vPos, input_texCoord, input_normal);
#endif

	gl_Position = output.pos;
}
#endif

#ifdef FRAGMENT

in PsIn output;
out vec4 fragColor;

void main()
{
	fragColor = ps_main(output);
}

#endif