#define 	SHADOW_BIAS_SCALE 0.05

// $INCLUDE phong.h
// $INCLUDE drvsyn_lightning.h
// $INCLUDE shadowmap_ps.h
// $INCLUDE vehicle_vs.h

// input vertex
struct VsIn
{
	float4 vPos			: POSITION0;
	float2 texCoord		: TEXCOORD0;	// send fog coordinate as texture coords
	float3 normal		: TEXCOORD1;
	float4 boneIndex	: TEXCOORD2;
	float4 boneWeight	: TEXCOORD3;

	float4 vPos1		: POSITION1;
	float3 normal1		: TEXCOORD4;
};

float4x4 	WVP;
float4x4 	View;
float4x4 	World;
float3 		ViewPos;
float4 		BaseTextureTransform;

#ifdef USE_LPLATE
float4 		LicenseRect;
#endif // USE_LPLATE

#ifdef DOFOG
float3 		FogParams;
samplerCUBE FogEnvMap : register(s6);
float3 		FogColor;
#endif

float4 		AmbientColor;
float4		SunColor;
float3		SunDir;
float 		SPECULAR_SCALE;

sampler2D 	BaseTextureSampler : register(s0);

#ifdef USE_DAMAGE_TEXTURE
sampler2D 	DamageTextureSampler : register(s1);
#else
#	define 	DamageTextureSampler BaseTextureSampler
#endif // USE_DAMAGE_TEXTURE

#ifdef USE_COLORMAP
sampler2D 	ColormapSampler : register(s2);
#endif // USE_COLORMAP

#ifdef USE_SPECULAR
sampler2D 	SpecularMapSampler : register(s8);
#endif // USE_SPECULAR

#ifdef USE_BASETEXTUREALPHA_SPECULAR
#define USE_SPECULAR
#endif // USE_BASETEXTUREALPHA_SPECULAR

#ifdef USE_CUBEMAP
sampler2D 		SphReflection : register(s10);
samplerCUBE 	CubemapTexture : register(s12);
#endif

#define MAX_CAR_COLORS 2
float4			CarColor[MAX_CAR_COLORS];

struct PsIn 
{
	float4 pos		: POSITION;
	
#ifdef DOFOG
	float4 texCoord	: TEXCOORD0;
#else
	float3 texCoord	: TEXCOORD0;
#endif // DOFOG
	
	float3 viewVec		: TEXCOORD1;
	float3 normal		: TEXCOORD2;
	
#ifndef PIXEL_LIGHTING
	float3 lights		: TEXCOORD3;
#endif // PIXEL_LIGHTING

#ifdef USE_CUBEMAP
	float3 cubemapCoord	: TEXCOORD4;
#endif
};

PsIn vs_main(VsIn input)
{
	PsIn Out;

	float interpVal 	= S_GetDamageInterpolation(input.boneIndex, input.boneWeight);
	
	float4 transPosit 	= lerp(input.vPos, input.vPos1, interpVal );

	Out.pos 			= mul(WVP,transPosit);
	
	Out.normal 			= mul((float3x3)World,lerp(input.normal, input.normal1, interpVal));
	
	float3 wPos 		= mul(World,transPosit);
	
	Out.viewVec 		= ViewPos-wPos;
	
#ifdef USE_CUBEMAP
	Out.cubemapCoord = reflect(-normalize(Out.viewVec), Out.normal);
	float3 dir = mul((float3x3)View, Out.cubemapCoord);
	Out.cubemapCoord = dir * float3(0.5, -0.5, 1) + 0.5;
#endif

#ifdef USE_LPLATE
	Out.texCoord.xy	= input.texCoord * LicenseRect.zw + LicenseRect.xy;
#else
	Out.texCoord.xy	= input.texCoord;
#endif // USE_LPLATE
	
	float damageTextureDot = abs(dot(input.normal, float3(0.0,0.0,1.0)));
	
	Out.texCoord.z = pow(interpVal, 1.0 + damageTextureDot);
	
#ifndef PIXEL_LIGHTING
	Out.lights = ProcessWorldLights(Out.viewVec, Out.normal);
#endif // PIXEL_LIGHTING

#ifdef DOFOG
	Out.texCoord.w = (FogParams.y - length(wPos-ViewPos)) * FogParams.z;
#endif

	return Out;
}

//
// Pixel main function
//
float4 	ps_main(PsIn input, float face : VFACE) : COLOR 
{
	float fSpecFactor 		= SPECULAR_SCALE;
	float4 baseTexture 		= tex2D(BaseTextureSampler, input.texCoord.xy);
	
#ifdef ALPHATEST
	clip(baseTexture.a-0.5);
#endif

#ifdef USE_DAMAGE_TEXTURE
	float4 damageTexture 	= tex2D(DamageTextureSampler, input.texCoord.xy);
#else
	float4 damageTexture 	= baseTexture;
#endif // USE_DAMAGE_TEXTURE
	
	// transition between damaged texture
	baseTexture = lerp(baseTexture, damageTexture, input.texCoord.z);

#ifdef USE_COLORMAP
	float4 colorMap = tex2D(ColormapSampler, input.texCoord.xy);

	// start with base details
	float3 resultColor = baseTexture.rgb;
	float resultSpec = fSpecFactor;

	// TODO: more colors!
	for(int i = 0; i < 2; i++)
	{
		//if(colorMap.r == 0)
		//	break;
		
		float3 color = baseTexture.rgb * CarColor[i].rgb;
		float spec = fSpecFactor * CarColor[i].a;
		
		// mix
		resultColor = lerp(resultColor, color, colorMap.r);
		resultSpec = lerp(resultSpec, spec, colorMap.r);
		
		colorMap.xyzw = colorMap.yzwx; // shift
	}
	
	baseTexture.rgb = resultColor;
	fSpecFactor = resultSpec;
#endif // USE_COLORMAP

	// begin result, apply ambient lights
	float4 result = float4(baseTexture.rgb*AmbientColor.rgb, baseTexture.a);

	// backfacing polygons means inside car, modify ambient lighting
	if( face < 0 )
		result *= 0.15;

	// also invert normal
	input.normal *= face;
	
	float3 viewVec = normalize(input.viewVec);
	
	float diffuseLight = saturate(dot(input.normal, SunDir));
	
#ifdef SHADOWMAP	
	// apply sun shadow to diffuse light factor
	diffuseLight *= GetSunShadow(input.viewVec + ViewPos);
#endif
	
	result.rgb += baseTexture.rgb*SunColor.rgb*diffuseLight;

#ifdef USE_SPECULAR

#ifdef USE_BASETEXTUREALPHA_SPECULAR
	float2 specularMap = baseTexture.aa;
#else
	float2 specularMap = tex2D(SpecularMapSampler, input.texCoord.xy).rg;
#endif // USE_BASETEXTUREALPHA_SPECULAR

	fSpecFactor *= specularMap.r;
	
	float specular = BlinnPhongSpecular(SunDir, viewVec, input.normal, 500.0);
	
	result.rgb += specular*diffuseLight*SunColor.rgb*fSpecFactor * 10.0;

#ifdef SELFILLUMINATION
	result += baseTexture*specularMap.g*4;
#endif // SELFILLUMINATION
#endif // USE_SPECULAR

#ifndef PIXEL_LIGHTING
	// add vertex light sources
	result.rgb += baseTexture.rgb*input.lights;
#else
	// calculate pixel light sources
	result.rgb += baseTexture.rgb*ProcessWorldLightsSpecular(input.viewVec, input.normal, fSpecFactor);
#endif // PIXEL_LIGHTING
	
#ifdef USE_CUBEMAP
	// approximated
	float freshnel = pow(1.0-saturate(dot(viewVec, input.normal.xyz)), 1.25) * fSpecFactor;

#if 0
	float cubeMip = freshnel*3.0;
	float3 cubemapSample = texCUBElod(CubemapTexture, float4(input.cubemapCoord, cubeMip)).rgb;
	result.rgb += cubemapSample * max(diffuseLight,0.5) * freshnel;
#else
	// read the reflection texture and apply it to cubemap
	float3 cubemapSample = tex2Dlod(SphReflection, float4(input.cubemapCoord.xy,0,0));
	result.rgb += cubemapSample * freshnel;
#endif

	
#endif // USE_CUBEMAP

#ifdef DOFOG
	float3 fogEnvMap = texCUBElod(FogEnvMap, float4(-viewVec, 0));	
	result.rgb = lerp(result.xyz, fogEnvMap, saturate(1.0f - input.texCoord.w));
#endif

	return result;
}