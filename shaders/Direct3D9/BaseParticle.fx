// $INCLUDE phong.h
// $INCLUDE drvsyn_lightning.h
// $INCLUDE shadowmap_ps.h

struct PsIn {
	float4 pos			: POSITION;
	
#ifdef DOFOG
	float3 texCoord		: TEXCOORD0;
#else
	float2 texCoord		: TEXCOORD0;
#endif

	float4 color		: TEXCOORD1;
	float3 viewVec		: TEXCOORD2;
	
#ifdef ADVANCED_LIGHTING
	float3 lighting		: TEXCOORD3;
#endif // ADVANCED_LIGHTING
};

sampler2D BaseTextureSampler : register(s0);

#ifdef DOFOG
float3 			FogParams;
samplerCUBE 	FogEnvMap : register(s6);
float3 			FogColor;
#endif

float4x4 		WVP;
float3 			ViewPos;
float4 			AmbientColor;
float4			SunColor;

PsIn vs_main(
	float4 vPos: POSITION, 
	float2 texCoord: TEXCOORD0,
	float4 color: TEXCOORD1
	)
{
	PsIn Out;

	Out.pos = mul(WVP,vPos);
	Out.viewVec = ViewPos-vPos.xyz;

	Out.color = color;
	
#ifdef ADVANCED_LIGHTING
	Out.lighting = ProcessWorldLightsNoNormal(Out.viewVec);
#endif // ADVANCED_LIGHTING

	Out.texCoord.xy = texCoord;
	
#ifdef DOFOG
	Out.texCoord.z = (FogParams.y - length(Out.viewVec)) * FogParams.z;
#endif // DOFOG
	
	return Out;
}

float4 ps_main(PsIn input): COLOR 
{
	float4 tex = tex2D(BaseTextureSampler, input.texCoord.xy);

#ifdef ALPHATEST
	clip(tex.a - 0.5);
#endif

	tex *= input.color;

#if defined(ADDITIVE)
	float4 result = tex;
#else
	float4 result = tex * AmbientColor;
#endif
	
#ifdef ADVANCED_LIGHTING
	float diffuse = 1.0;
#ifdef SHADOWMAP	
	// apply sun shadow to diffuse light factor
	diffuse *= GetSunShadow(input.viewVec + ViewPos);
#endif
	result.rgb += tex.rgb * SunColor * diffuse;
	result.rgb += tex.rgb * input.lighting;
#endif // ADVANCED_LIGHTING

#ifdef DOFOG
	float3 fogEnvMap = texCUBElod(FogEnvMap, float4(-input.viewVec, 0));	
	float fog_intep = input.texCoord.z;
#endif
	
#if defined(ALPHATEST) && defined(DOFOG)
	result.rgb = lerp(result.rgb,fogEnvMap,saturate(1.0 - fog_intep));
#elif defined(ADDITIVE) && defined(DOFOG)
	result.rgb = lerp(result.rgb,0,pow(saturate((1.0 - fog_intep)-0.25f), 10));
#elif defined(DOFOG)
	result.a = lerp(result.a,0,saturate(1.0 - fog_intep));
#endif

	return max(result, 0);
}