#define NOISE_TEXTURE
// $INCLUDE shadowmap_ps.h

struct PsIn 
{
	float4 pos			: POSITION;
	
#ifdef DOFOG
	float3 texCoord		: TEXCOORD0;
#else
	float2 texCoord		: TEXCOORD0;
#endif // DOFOG

	float4 	color		: TEXCOORD1;
	float3	viewVec 	: TEXCOORD2;
};

float4x4 	World;
float4x4 	WVP;
float3 		ViewPos;

#ifdef DOFOG
float3 		FogParams;
float3 		FogColor;
#endif

sampler2D 	NoiseTexture 		: register(s8);
sampler2D 	BaseTextureSampler;
float4 		AmbientColor 		: register(c0);

PsIn vs_main(
	float4 vPos: POSITION0,
	float2 texCoord: TEXCOORD0,
	float4 color: TEXCOORD1
	)
{
	PsIn Out;

	float3 vertPos = mul(World,vPos);
	Out.pos = mul(WVP, vPos);
	
	Out.viewVec = ViewPos-vertPos;

	Out.texCoord.x = texCoord.x;
	Out.texCoord.y = texCoord.y;
	Out.color = color;

#ifdef DOFOG
	Out.texCoord.z = (FogParams.y - Out.pos.z) * FogParams.z;
#endif

	return Out;
}

#define SHADOW_SIZE 		800
#define SHADOWTEXEL_SIZE 	(1.0f / SHADOW_SIZE)

float ShadowPCF(sampler2D ShadowMap, float2 projVector)
{
	float fShadow;
	fShadow = tex2D(ShadowMap, projVector).r;
	
	fShadow += tex2D(ShadowMap, projVector + float2(SHADOWTEXEL_SIZE,0)).r;
	fShadow += tex2D(ShadowMap, projVector + float2(0,SHADOWTEXEL_SIZE)).r;
	fShadow += tex2D(ShadowMap, projVector + float2(SHADOWTEXEL_SIZE,SHADOWTEXEL_SIZE)).r;
	
	fShadow += tex2D(ShadowMap, projVector - float2(SHADOWTEXEL_SIZE,0)).r;
	fShadow += tex2D(ShadowMap, projVector - float2(0,SHADOWTEXEL_SIZE)).r;
	fShadow += tex2D(ShadowMap, projVector - float2(SHADOWTEXEL_SIZE,SHADOWTEXEL_SIZE)).r;

	fShadow += tex2D(ShadowMap, projVector - float2(-SHADOWTEXEL_SIZE,SHADOWTEXEL_SIZE)).r;
	fShadow += tex2D(ShadowMap, projVector - float2(SHADOWTEXEL_SIZE,-SHADOWTEXEL_SIZE)).r;

	float shadowResult = fShadow * 0.111;

	return saturate(shadowResult - (frac(pow(shadowResult + tex2D(NoiseTexture, projVector*4096).r, 0.5))-0.25));
}

float4 ps_main(PsIn input): COLOR 
{
	float shadowTex = 1.0-ShadowPCF(BaseTextureSampler, input.texCoord.xy);

#ifdef SHADOWMAP	
	// apply sun shadow to diffuse light factor
	// also apply normal bias
	shadowTex = lerp(0.0, shadowTex, pow(GetSunShadow(input.viewVec + ViewPos), 0.5));
#endif

	float4 result = float4(AmbientColor.rgb, shadowTex*AmbientColor.a*input.color.a);

#ifdef DOFOG
	result.a = lerp(result.a, 0.0, saturate(1.0f - input.texCoord.z));
#endif

	return result;
}