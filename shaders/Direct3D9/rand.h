#ifndef RAND_H
#define RAND_H
float2 rand(float2 co)
{
    return float2(frac(sin(dot(co.xy ,float2(12.9898,78.233))) * 43758.5453),
	frac(sin(dot(co.yx ,float2(12.9898,78.233))) * 43758.5453)) * 0.00047;
}

#endif