// $INCLUDE shadowmap_ps.h
// $INCLUDE phong.h
// $INCLUDE drvsyn_lightning.h
// $INCLUDE skinning_vs.h
// $INCLUDE fluffy_tree_vs.h

struct PsIn 
{
	float4 pos	: POSITION;
	
#ifdef DOFOG
	float3 texCoord	: TEXCOORD0;
#else
	float2 texCoord	: TEXCOORD0;
#endif // DOFOG

	float3 viewVec	: TEXCOORD1;
	float3 normal	: TEXCOORD2;
	
	float3	lights	: TEXCOORD3;
};

float4x4 		WVP;
float4x4 		View;
#ifndef INST
float4x4 		World;
#endif

float3 			ViewPos;


#ifdef DOFOG
samplerCUBE 	FogEnvMap : register(s6);
float3 			FogParams;
#endif

float4 			BaseTextureTransform;
sampler2D 		BaseTextureSampler : register(s0);

#ifdef HAS_NIGHTLIGHT_TEXTURE
sampler2D 		NightLightSampler : register(s1);
#endif // HAS_NIGHTLIGHT_TEXTURE

float4 			AmbientColor;
float4 			SunColor;
float3			SunDir;
	
float 			SPECULAR_SCALE;
float2 			ENVPARAMS;

#ifdef USE_BASETEXTUREALPHA_SPECULAR
#define USE_SPECULAR
#endif // USE_BASETEXTUREALPHA_SPECULAR

#ifdef SPECULAR
sampler2D 		SpecularMapSampler : register(s8);
#endif // SPECULAR

#ifdef USE_CUBEMAP
samplerCUBE 	CubemapTexture : register(s12);
#endif

PsIn vs_main(	
	float4 vPos		: POSITION0,
	float2 texCoord	: TEXCOORD0,
#ifdef INST
	float3 normal	: TEXCOORD1
#else
	float3 normal	: TEXCOORD3
#endif
	// skinning and instancing can't be used both
#ifdef SKIN
	, float4 boneIndex 	: TEXCOORD4
	, float4 boneWeight : TEXCOORD5
#endif
#ifdef INST
	, float4x4 World : TEXCOORD2
#endif
	)
{
	PsIn Out;

#ifdef SKIN
	float4 transPosit 	= S_TransformVertexWeightsAndNormal(vPos, normal, boneIndex, boneWeight);
#else
	float4 transPosit	= vPos;
#endif	

#ifdef TREE_LEAVES
	float3 leavesOffset = GetTreeLeavesOffset(vPos, View, World, texCoord, 1.0 + ENVPARAMS.x * 4.0);
	transPosit += float4(leavesOffset, 0);
#endif // TREE_LEAVES

#ifdef INST
	transPosit 			= mul(World, transPosit);
	float4 wPos 		= transPosit;
#else
	float4 wPos 		= mul(World, transPosit);
#endif

	Out.pos 			= mul(WVP, transPosit);
	Out.viewVec 		= ViewPos - wPos;

	Out.normal 			= mul(World, normal);

	Out.texCoord.x 		= texCoord.x*BaseTextureTransform.x + BaseTextureTransform.z;
	Out.texCoord.y 		= texCoord.y*BaseTextureTransform.y + BaseTextureTransform.w;
	
	Out.lights 			= ProcessWorldLights(Out.viewVec, Out.normal);

#ifdef DOFOG
	Out.texCoord.z = (FogParams.y - length(Out.viewVec)) * FogParams.z;
#endif

	return Out;
}

//
// Pixel main function
//
float4 	ps_main(PsIn input) : COLOR 
{
	float fSpecFactor = SPECULAR_SCALE;
	float4 baseTexture = tex2D(BaseTextureSampler, input.texCoord.xy);

#ifdef ALPHATEST
	clip(baseTexture.a-0.5);
#endif

	float4 result = baseTexture*AmbientColor;
	result.a = baseTexture.a;
	
	float3 viewVec = normalize(input.viewVec);
	
	float diffuseLight = saturate(dot(input.normal, SunDir));
	
#ifdef SHADOWMAP	
	// apply sun shadow to diffuse light factor
	diffuseLight *= GetSunShadow(input.viewVec + ViewPos);
#endif
	
	result.rgb += diffuseLight*baseTexture.rgb*SunColor.rgb;

#ifdef ALPHASELFILLUMINATION
	result += baseTexture*baseTexture.a*4;
#endif // SELFILLUMINATION

	result.rgb += input.lights*baseTexture.rgb;//ProcessWorldLights(input.viewVec, input.normal)*baseTexture.rgb;
	
#ifdef USE_BASETEXTUREALPHA_SPECULAR
	fSpecFactor *= baseTexture.a;
#elif SPECULAR
	fSpecFactor *= tex2D(SpecularMapSampler, input.texCoord.xy).r;
#endif // USE_BASETEXTUREALPHA_SPECULAR

#ifdef USE_CUBEMAP
	float3 cubemapCoord = reflect(-viewVec, input.normal.xyz);

	// approximated
	float freshnel = (1.0-saturate(dot(viewVec, input.normal.xyz)*0.85))*fSpecFactor;

	float cubeMip = freshnel*1.0;

	float3 cubemapSample = texCUBElod(CubemapTexture, float4(cubemapCoord, cubeMip)).rgb;

	result.rgb += cubemapSample * max(diffuseLight,0.25) * freshnel;
#endif // USE_CUBEMAP

#if defined(DOFOG) && !defined(MODULATE)
	float fog_intep = input.texCoord.z;
	
	float lightsOnFogIntensity = 1.0-pow(saturate((1.0 - fog_intep)-0.2), 10);
	
	float3 fogEnvMap = texCUBElod(FogEnvMap, float4(-viewVec, 0));	
	result.rgb = lerp(result.rgb, fogEnvMap, saturate(1.0f - fog_intep));
#else
	float lightsOnFogIntensity = 1.0;
#endif

// Some lights are performed after FOG
#ifdef HAS_NIGHTLIGHT_TEXTURE
	float4 nightLight = tex2D(NightLightSampler, input.texCoord.xy);
	result.rgb += nightLight.rgb*ENVPARAMS.y*lightsOnFogIntensity;
#endif // HAS_NIGHTLIGHT_TEXTURE

	return result;
}