#ifdef PIXEL_LIGHTING
sampler2D LightsTexture 		: register(s7);
#else
sampler2D LightsTexture 		: register(s0);
#endif // PIXEL_LIGHTING

#define MAX_LIGHTS 32

#define SPECULAR_POWER 4.0

#define LIGHTTEX_SIZE 		MAX_LIGHTS
#define LIGHTTEX_TEXEL		(1.0/LIGHTTEX_SIZE)

// apply world lights to position with normal
float3 ProcessWorldLights(float3 vPos, float3 normal)
{
	float3 lighting = float3(0.0, 0.0, 0.0);

	for(int i = 0; i < MAX_LIGHTS; i++)
	{
		float lightTexOfs = LIGHTTEX_TEXEL*i;
		
		float4 lpos = tex2Dlod(LightsTexture, float4(lightTexOfs, 0.25, 0.0, 0.0));
		
		if(lpos.w == 0) // too many temp registers, not working
			return lighting;
	
		float4 lcolor = tex2Dlod(LightsTexture, float4(lightTexOfs, 0.85, 0.0, 0.0));

		float3 lVec = (vPos-lpos.xyz)*lpos.w;
		float3 nlVec = normalize(lVec);
		float fAtten = 1.0 - saturate(dot(lVec, lVec));
		float diffuse = 1.0-pow(1.0-saturate(dot(normal, nlVec)), 2);
		
		lighting += lcolor.rgb * fAtten * diffuse;
	}

	return lighting;
}

// apply world lights to position with normal vector and specular
float3 ProcessWorldLightsSpecular(float3 vPos, float3 normal, float specularScale)
{
	float3 lighting = float3(0.0, 0.0, 0.0);

	for(int i = 0; i < MAX_LIGHTS; i++)
	{
		float lightTexOfs = LIGHTTEX_TEXEL*i;
		
		float4 lpos = tex2Dlod(LightsTexture, float4(lightTexOfs, 0.25, 0.0, 0.0));
		
		if(lpos.w == 0) // too many temp registers, not working
			return lighting;
	
		float4 lcolor = tex2Dlod(LightsTexture, float4(lightTexOfs, 0.85, 0.0, 0.0));

		float3 lVec = (vPos-lpos.xyz)*lpos.w;
		float fAtten = 1.0-saturate(dot(lVec, lVec));

		float3 nlVec = normalize(lVec);
		float diffuse = 1.0-pow(1.0-saturate(dot(normal, nlVec)), 2);
		
		float3 premul = lcolor.rgb * fAtten * diffuse;
		lighting += premul;
		
		float specular = VtxSpecularReflection(nlVec, normal, normalize(vPos), 200.0) * 3.0;
		lighting += premul * specular * specularScale;		
	}

	return lighting;
}

// Apply world lights to position
float3 ProcessWorldLightsNoNormal(float3 vPos)
{
	float3 lighting = float3(0.0, 0.0, 0.0);

	for(int i = 0; i < MAX_LIGHTS; i++)
	{
		float lightTexOfs = LIGHTTEX_TEXEL*i;
		
		float4 lpos = tex2Dlod(LightsTexture, float4(lightTexOfs, 0.25, 0.0, 0.0));
		
		if(lpos.w == 0) // too many temp registers, not working
			return lighting;
		
		float4 lcolor = tex2Dlod(LightsTexture, float4(lightTexOfs, 0.85, 0.0, 0.0));
	
		float3 lVec = (vPos-lpos.xyz)*lpos.w;
		float fAtten = 1.0 - saturate(dot(lVec, lVec));
		
		lighting += lcolor.rgb * fAtten;
	}

	return lighting;
}

