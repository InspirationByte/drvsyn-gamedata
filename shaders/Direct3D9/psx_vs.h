float align(float value, float size)
{
    return floor(value/size)*size;
}

float2 align2(float2 value, float2 size)
{
    return float2(align(value.x, size.x), align(value.y, size.y));
}

#define PSX_SCREEN_TEXEL_X 1.0/320.0
#define PSX_SCREEN_TEXEL_Y 1.0/240.0

#define PSX_VERTEX_VOBBLE(vtx) 	\
	vtx.xy = align2(vtx.xy, vtx.z*float2(PSX_SCREEN_TEXEL_X,PSX_SCREEN_TEXEL_Y));