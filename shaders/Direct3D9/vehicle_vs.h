
#define MAX_CAR_BONES_REGS 4 // 64/4

float4 BodyDamage[MAX_CAR_BONES_REGS] : register(c8);

// TODO: make it through vertex texture.
// 	read from texture is cheaper for 20 instructions
float S_GetDamageInterpolation(float4 bones, float4 weights)
{
	float interpVal = 0.0;

	if(int(bones.x) < 0)
		return 0.0;

	float4 sbones = bones;
	float4 sweights = weights;
	
	int constIdx = int(floor(sbones.x / 4));
	int constElemIdx = int(sbones.x) % 4;
	
	float val = BodyDamage[constIdx][constElemIdx];
	interpVal += val*sweights.x;

	for(int i = 0; i < 3; i++)
	{
		sbones = sbones.yzwx;
		sweights = sweights.yzwx;

		if(int(sbones.x) < 0)
			continue;
			
		constIdx = int(floor(sbones.x / 4));
		constElemIdx = int(sbones.x) % 4;
		
		val = BodyDamage[constIdx][constElemIdx];

		interpVal += val*sweights.x;
	}

	return interpVal;
}