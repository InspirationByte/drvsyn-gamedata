// heightfield vertex
struct hfielddrawvertex_t
{
	half4 position: POSITION;
	half4 normal_lighting: TEXCOORD0;
	half2 texCoord: TEXCOORD1;
}

// level model vertex
struct modelVertex_t
{
	half4 position: POSITION;
	half4 normal_lighting: TEXCOORD0;
	half2 texCoord: TEXCOORD1;
}

//TODO: GL vertex attribute declarations