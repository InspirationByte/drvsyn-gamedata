// EGF13 vertex format
struct EGFVertex_t
{
	half4 position		: POSITION0;		// position
	half4 normal		: TEXCOORD0;		// normal
	half4 binormal		: TEXCOORD1;
	half4 tangent		: TEXCOORD2;
	half4 boneIndex		: TEXCOORD3;
	half4 boneWeight	: TEXCOORD4;
};

// input vertex
struct EGFVertex_Car_t
{
	half4 position		: POSITION0;
	half4 normal		: TEXCOORD1;
	half4 boneIndex		: TEXCOORD2;
	half4 boneWeight	: TEXCOORD3;

	half4 position1		: POSITION1;
	half4 normal1		: TEXCOORD4;
};

//TODO: GL vertex attribute declarations