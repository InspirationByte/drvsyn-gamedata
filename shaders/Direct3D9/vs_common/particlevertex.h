// particle vertex
struct PFXVertex_t
{
	half4 position: POSITION;
	half4 color: TEXCOORD0;
	half2 texCoord: TEXCOORD1;
}

//TODO: GL vertex attribute declarations