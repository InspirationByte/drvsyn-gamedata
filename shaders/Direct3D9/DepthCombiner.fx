struct PsIn 
{
	float4 pos		: POSITION;
	float2 texCoord	: TEXCOORD0;
};

float4x4 	WVP;

sampler2D 	Texture1 : register(s0);
sampler2D 	Texture2 : register(s1);

PsIn vs_main(
	float4 vPos: POSITION0,
	float2 texCoord: TEXCOORD0
	)
{
	PsIn Out;

	Out.pos = mul(WVP, vPos);
	Out.texCoord = texCoord;

	return Out;
}

float4 ps_main(PsIn input): COLOR 
{
	float depth1 = tex2D(Texture1, input.texCoord).r;
	float depth2 = tex2D(Texture2, input.texCoord).r;
	
	return float4( min(depth1, depth2), 0, 0, 1);
}