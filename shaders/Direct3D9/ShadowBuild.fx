// $INCLUDE skinning_vs.h
// $INCLUDE vehicle_vs.h

struct PsIn 
{
	float4 pos		: POSITION;
	float2 texCoord	: TEXCOORD0;
};

float4x4 	WVP;
float4 		BaseTextureTransform;
sampler2D 	BaseTextureSampler : register(s0);

PsIn vs_main(
	float4 vPos: POSITION0, 
	float2 texCoord: TEXCOORD0
	// skinning and instancing can't be used both
#ifdef SKIN
	, float4 boneIndex 	: TEXCOORD4
	, float4 boneWeight : TEXCOORD5
#endif
#ifdef VEHICLE
	, float4 boneIndex	: TEXCOORD2
	, float4 boneWeight	: TEXCOORD3
	, float4 vPos1		: POSITION1
	, float3 normal1	: TEXCOORD4
#endif
#ifdef INST
	, float4x4 World 	: TEXCOORD2
#endif
	)
{
	PsIn Out;
	
#if defined(SKIN)
	float4 transPosit 	= S_TransformVertexWeights(vPos, boneIndex, boneWeight);
#elif defined(VEHICLE)
	float interpVal 	= S_GetDamageInterpolation(boneIndex, boneWeight);
	float4 transPosit 	= lerp(vPos, vPos1, interpVal);
#else
	float4 transPosit 	= vPos;
#endif
	
#ifdef INST
	transPosit = mul(World, vPos);
#endif

	Out.pos = mul(WVP, transPosit);

	Out.texCoord.x = texCoord.x*BaseTextureTransform.x + BaseTextureTransform.z;
	Out.texCoord.y = texCoord.y*BaseTextureTransform.y + BaseTextureTransform.w;

	return Out;
}

float4 ps_main(PsIn input): COLOR 
{
	float4 baseTexture = tex2D(BaseTextureSampler, input.texCoord.xy);

	clip(baseTexture.a-0.5);

	return float4(0,0,0,1);
}