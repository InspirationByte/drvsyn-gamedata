// $INCLUDE shadowmap_ps.h
// $INCLUDE phong.h
// $INCLUDE drvsyn_lightning.h

struct PsIn {
	float4 pos		: POSITION;
	
#ifdef DOFOG
	float3 texCoord	: TEXCOORD0;
#else
	float2 texCoord	: TEXCOORD0;
#endif // DOFOG

	float3 tangent		: TEXCOORD1;
	float3 binormal		: TEXCOORD2;
	float3 normal		: TEXCOORD3;

	float3 viewVec		: TEXCOORD4;
	float3 lighting		: TEXCOORD5;
	float3 screenCord	: TEXCOORD6;
};

float4x4 	WVP;
float4x4 	World;
float3 		ViewPos;

float 		GameTime;
float4 		WaterColor;
float4 		AmbientColor;
float 		SPECULAR_SCALE;

float4		SunColor;
float3		SunDir;

float4 		BaseTextureTransform;
float2 		ENVPARAMS;

#ifdef DOFOG
float3 		FogParams;
samplerCUBE FogEnvMap : register(s6);
#endif

sampler2D 	BumpTextureSampler : register(s0);
samplerCUBE CubemapSampler : register(s12);
sampler2D 	ReflectionSampler : register(s1);


#define HEIGHT_SCALE (0.01+ENVPARAMS.x*0.1)
#define HEIGHT_SCALE_NORMAL (0.01)
#define HEIGHT_DIST		0.3
#define HEIGHT_DIST2	0.1

PsIn vs_main( 
	float4 vPos: POSITION,
	float2 texCoord: TEXCOORD0 
	)
{
	PsIn Out;

	float4 vertPos = mul(World,vPos);
	
	float fWaveOfs = GameTime*2.0-ENVPARAMS.x*0.07;
	float2 waveXZ = float2(sin(fWaveOfs+vertPos.x*HEIGHT_DIST), sin(fWaveOfs+vertPos.z*HEIGHT_DIST));
	waveXZ += float2(sin(fWaveOfs+vertPos.x*HEIGHT_DIST2), sin(fWaveOfs+vertPos.z*HEIGHT_DIST2));
	
	waveXZ *= 0.5;
	
	float waterHeight = (waveXZ.x + waveXZ.y)*HEIGHT_SCALE * WaterColor.w;
	
	Out.pos = mul(WVP,vPos+float4(0,waterHeight,0, 0));
	
	Out.screenCord = Out.pos.xyw;
	
	float3 viewVec = ViewPos-vertPos.xyz;

	Out.viewVec = viewVec;
	Out.normal = normalize(float3(waveXZ.x*HEIGHT_SCALE_NORMAL, 1, waveXZ.y*HEIGHT_SCALE_NORMAL));

	Out.tangent = cross(float3(0,0,1), Out.normal);
	Out.binormal = cross(float3(1,0,0), Out.normal);

	Out.lighting = ProcessWorldLightsNoNormal(Out.viewVec);

	Out.texCoord.x = texCoord.x*BaseTextureTransform.x + BaseTextureTransform.z;
	Out.texCoord.y = texCoord.y*BaseTextureTransform.y + BaseTextureTransform.w;
	
#ifdef DOFOG
	Out.texCoord.z = (FogParams.y - length(Out.viewVec)) * FogParams.z;
#endif // DOFOG
	
	return Out;
}

#define BUMP_INTENSITY (0.02+ENVPARAMS.x*0.2)
#define REFL_TEXEL (1.0/512.0)

float4 ps_main(PsIn input): COLOR 
{
	float3 bump = tex2D(BumpTextureSampler, input.texCoord.xy).rgb;
	bump = bump * 2.0 - 1.0;

	float3 bumpTex = float3(
		dot(input.tangent, bump),
		dot(input.binormal, bump),
		dot(input.normal, bump)
	);

	float3 wbump = lerp(input.normal, bumpTex, BUMP_INTENSITY);

	float4 outcolor = AmbientColor * float4(WaterColor.rgb, 1); // * UnderWaterTexture

	float3 viewVec = normalize(input.viewVec);
	float surfDistance = 1.0 - saturate(1.0 / length(input.viewVec)) * 2.5;

	float3 cubemapCoord = reflect(-viewVec, wbump);
	
	// approximated
	float freshnel = pow(1.0-saturate(dot(viewVec, wbump)), 5.0)*SPECULAR_SCALE;
	float cubeMip = (1.0-freshnel)*1.5;
	
	outcolor.a = surfDistance + freshnel;//+ depth;
	
	float3 cubemapSample = texCUBElod(CubemapSampler, float4(cubemapCoord, cubeMip)).rgb;

	{
		float2 screenCoords = (input.screenCord.xy / input.screenCord.z)*0.5+0.5 + REFL_TEXEL*0.5;
		
		screenCoords += float2((wbump.x+wbump.z)*0.1,0.0);
	
		float3 reflTexture = tex2Dlod(ReflectionSampler, float4(screenCoords,0,0));

		cubemapSample += reflTexture * 2.0;
	}
	
	float diffuseLight = saturate(dot(wbump, SunDir));
	
#ifdef SHADOWMAP	
	// apply sun shadow to diffuse light factor
	diffuseLight *= GetSunShadow(input.viewVec + ViewPos);
#endif
	
	outcolor.rgb += WaterColor.rgb*SunColor.rgb*diffuseLight;
	outcolor.rgb += WaterColor.rgb * input.lighting*0.25;
	outcolor.rgb = lerp(outcolor.rgb, cubemapSample, freshnel*(saturate(cubemapSample)*0.5+0.5));

	float specular = BlinnPhongSpecular(SunDir, viewVec, bumpTex, 100.0) * 5.0;
	float specDiffuse = specular*diffuseLight*SPECULAR_SCALE;
	
	//outcolor.a += specDiffuse;
	outcolor.rgb += SunColor.rgb*specDiffuse;
	
#ifdef DOFOG
	float3 fogEnvMap = texCUBElod(FogEnvMap, float4(-viewVec, 0));
	outcolor.rgb = lerp(outcolor.rgb,fogEnvMap,clamp(1.0 - input.texCoord.z,0,1));
#endif

	return outcolor;
}