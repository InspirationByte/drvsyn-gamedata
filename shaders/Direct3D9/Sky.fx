struct PsIn 
{
	float4 pos		: POSITION;
	float2 texCoord	: TEXCOORD0;
};

float4x4 	WVP;
sampler2D 	Base1 : register(s0);
sampler2D 	Base2 : register(s1);
float4 		AmbientColor : register(c0);
float		InterpFactor : register(c1);

PsIn vs_main(
	float4 vPos: POSITION0,
	float2 texCoord: TEXCOORD0
	)
{
	PsIn Out;

	Out.pos = mul(WVP, vPos);
	Out.texCoord = texCoord;

	return Out;
}

float4 ps_main(PsIn input): COLOR 
{
	float4 base1 = tex2D(Base1, input.texCoord.xy);
	float4 base2 = tex2D(Base2, input.texCoord.xy);

	float4 result = lerp(base1, base2, InterpFactor) * AmbientColor;

	return result;
}
