// multiplies bone
float4 quaternionMul ( float4 q1, float4 q2 )
{
	float3 im = q1.w * q2.xyz + q1.xyz * q2.w + cross( q1.xyz, q2.xyz );
	float4 dt = q1 * q2;
	float re = dot( dt, float4( -1.0, -1.0, -1.0, 1.0 ) );

	return float4( im, re );
}

// vector rotation
float4 quaternionRotate( float3 p, float4 q )
{
    float4 temp = quaternionMul( q, float4( p, 0.0 ) );
    return quaternionMul ( temp, -float4( q.x, q.y, q.z, -q.w ) );
}

//--------------------------------------------------------------------
// operations found by "qtransform_t"
//--------------------------------------------------------------------

struct qtransform_t
{
	float4	rot;
	float4	pos;
};

// transforms bone
float3 qTransformVector(qtransform_t qt, float3 pos)
{
    return qt.pos.xyz + quaternionRotate(pos, qt.rot).xyz;
}

// transforms vector
float3 qRotateVector(qtransform_t qt, float3 pos)
{
    return quaternionRotate(pos, qt.rot).xyz;
}