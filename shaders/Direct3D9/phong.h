#define SPECULAR_POWER 		4.0
#define SPECULAR_FACTOR 	400

float BlinnPhongSpecular(float3 lightVec, float3 viewVec, float3 normal, float fFactor)
{
	// blinn-phong
	float3 halfDir = normalize(lightVec + viewVec);
	float specAngle = max(dot(halfDir, normal), 0.0);
	return pow(specAngle, fFactor);
}

float VtxSpecularReflection(float3 lightVec, float3 normal, float3 viewVec, float fFactor)
{
	// blinn-phong
	//float3 halfDir = normalize(lightVec + viewVec);
	//float specAngle = saturate(dot(halfDir, normal));
   	//float specular = pow(specAngle, fFactor);
	
	// phong
	float3 rn = reflect(lightVec, normal);
	float spec = max(dot(rn, -viewVec), 0.0);
	return pow(spec, fFactor) * SPECULAR_POWER;
	
	//return specular * SPECULAR_POWER; 
}

