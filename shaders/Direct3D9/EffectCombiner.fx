struct PsIn 
{
	float4 pos		: POSITION;
	float2 texCoord	: TEXCOORD0;
};

float4x4 	WVP;

float4 		NoiseProps;			// brightness, factor
float4 		BloomProps;			// saturation, scale, power
float4 		SaturationProps;	// factor
float4 		VignetteProps;		// size params 			(0.6, 40, 1920w, 1080h)
float4 		VignetteProps2;		// brightness params	(0.6, 1, 0, 0)

sampler2D 	BaseTexture : register(s0);
sampler2D 	BloomTexture : register(s1);
sampler2D 	NoiseTexture : register(s2);

PsIn vs_main(
	float4 vPos: POSITION0,
	float2 texCoord: TEXCOORD0
	)
{
	PsIn Out;

	Out.pos = mul(WVP, vPos);
	Out.texCoord = texCoord;

	return Out;
}

// bloom
float3 rgb2hsv(float3 c)
{
    float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    float4 p = lerp(float4(c.bg, K.wz), float4(c.gb, K.xy), step(c.b, c.g));
    float4 q = lerp(float4(p.xyw, c.r), float4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return float3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

float3 hsv2rgb(float3 c)
{
    float4 K = float4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    float3 p = abs(frac(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * lerp(K.xxx, saturate(p - K.xxx), c.y);
}

// edge vignette
// VS part
void ComputeVignetteUVs(out float2 scale, out float2 bias)
{
	float2 aspect = float2(1, (VignetteProps.w / VignetteProps.z));

	float2 edge = (VignetteProps.zw / VignetteProps.y) * 0.5;
	float2 dark = VignetteProps.x - 0.5;
	
	scale = 2.0 * (1.0 + edge);
	bias = -edge + dark;
}

// PS part
float GetVignette(float2 texCoord)
{
	// VS part in PS
	float2 scale, bias;
	ComputeVignetteUVs(scale, bias);
	
	// PS part
	float2 coord = abs(texCoord - 0.5);
	float2 dir = max(coord * scale + bias, 0);

	float factor = -dot(dir, dir) * VignetteProps2.x;// + VignetteProps2.y;// this breaks it

	return 1.0 - saturate(factor*factor);
}

float4 ps_main(PsIn input): COLOR 
{
	float4 result = float4( 0, 0, 0, 1);

	float3 mainImage = tex2D(BaseTexture, input.texCoord);
	
	mainImage = rgb2hsv(mainImage);
	mainImage.y *= SaturationProps.x;
	
	result.rgb = hsv2rgb(mainImage);
	
	//--------------
	// apply HDR gamma
	//result.rgb = pow(result.rgb, 1.0 / 3.0);
	
	//--------------
	// apply bloom
	float3 bloom = tex2D(BloomTexture, input.texCoord).rgb;
	float3 hsvBloom = rgb2hsv(pow(bloom, BloomProps.z));
	hsvBloom.y *= BloomProps.x;
	result.rgb += hsv2rgb(hsvBloom) * BloomProps.y;
	
	//--------------
	// apply noice noise
	float3 noise = tex2D(NoiseTexture, input.texCoord * VignetteProps.zw * NoiseProps.x + NoiseProps.y).w;
	result.rgb = lerp(result.rgb, NoiseProps.z * noise, NoiseProps.w);

	//--------------
	// apply vignette
	result.rgb *= GetVignette(input.texCoord);

	return result;
}