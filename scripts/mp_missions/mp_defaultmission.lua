--------------------------------------------------------------------------------
-- Default mission script loader for multiplayer game
-- remove this from release build
--------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

MISSION.InitLobby = function()

	MISSION.MPSettings.LevelName = "default"
	MISSION.MPSettings.

	-- setup teams
	MISSION.MPSettings.Teams = {
		teamid_cops = { 
			label = "#TEAMNAME_COPS",
			maxPlayers = 4,
			carNames = { "cop_regis" },
			carSelection = MP_CAR_SELECTION_RANDOMIZED,
		},
		teamid_robbers = { 
			label = "#TEAMNAME_ROBBERS",
			maxPlayers = 4,
			carNames = { "rollo", "carla", "mustang", "mustang_f", "torino" }
			carSelection = MP_CAR_SELECTION_FREE
		},
	}
end

MISSION.Init = function()

	MISSION.MusicScript = "vegasday"

	-- put globals here
	MISSION.Data = {
		startPos = Vector3D.new(54,10.31,-45),
		startAng = Vector3D.new(0,0,0),

		-- make spawnpoints
		teamSpawnPoints = {
			teamid_cops = {
				-- position, rotation
				{  }
			},
			teamid_robbers = {
				-- position, rotation
				{}
			},
		}
	}
	
	-- retrieve slots
	local playerSlots = gameses:GetPlayerSlots();
	
	-- NOTE: 	this is a server script. gameses:SetPlayerCar will not work from out here
	-- 			you should assign cars to the player slots.
	--			MP game session will spawn cars automatically
	for i,pl in ipairs(playerSlots) do
	
		local spawnPoint = MISSION:PickSpawnPointForPlayer( pl:GetTeam() )
		
		if spawnPoint ~= nil then
			local car = gameses:CreateCar(pl:GetCarName(), CAR_TYPE_NORMAL)
		
			car:SetOrigin( spawnPoint[1], spawnPoint[2] )
			pl:AssignCar( car )
			
			-- assign each player team spawn points
			if pl:GetTeam() == "teamid_cops" then
				-- TODO: pick spawn poitns, adjust speed, damage
				pl:ShowScreenMessage("#COPSROBBERS_COPS_TITLE", 3.5)
			elseif pl:GetTeam() == "teamid_robbers" then
				-- TODO: pick spawn poitns, adjust speed, damage
				pl:ShowScreenMessage("#COPSROBBERS_ROBBERS_TITLE", 3.5)
			end
		else
			-- if no spawnpoint, just kick or move to spectators
			pl:SetTeam("spectator")
			
			pl:ShowScreenMessage("#MP_SLOTS_INSUFFICIENT", 3.5)
		end
	end
	
	-- make 5 minutes worth
	missionmanager:EnableTimeout( true, 300 )

	-- here we start
	missionmanager:SetRefreshFunc( MISSION.Update )
end

--------------------------------------------------------------------------------

-- main mission update
MISSION.Update = function( delta )

	local playerSlots = gameses:GetPlayerSlots();
	
	

	for i,pl in ipairs(playerSlots) do
		if pl:GetTeam() == "teamid_cops" then

		elseif pl:GetTeam() == "teamid_robbers" then

		end
	end

	--[[
	local playerCar = gameses:GetPlayerCar()

	UpdateCops( playerCar, delta )

	if CheckVehicleIsWrecked( playerCar, MISSION.Data, delta ) then
		playerCar:Lock(true)
		
		gameHUD:ShowAlert("#WRECKED_VEHICLE_MESSAGE", 3.5, HUD_ALERT_DANGER)
		
		missionmanager:SetRefreshFunc( function() 
			return false 
		end ) 

		gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
		return false
	end]]
	
	return true
end