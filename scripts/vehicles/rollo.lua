CarConfigs.TestRollo = {
	name = "Rollo Lua Test",

	cleanmodel = "models/vehicles/rollo.egf",
	damagedmodel = "models/vehicles/rollo_dam.egf",
	wheelBodyGroup = "wheel1b",

	sounds = {
		engine =		"car.engine3",
		engine_low =	"car.engine3_low",
		engine_idle =	"defaultcar.engine_idle",

		horn = "car.horn2",
	},

	camera_height = 1.15,
	camera_distance = 6.2,

	camera_height_in = 	0.25,
	camera_distance_in =	1.5,
	camera_side_in	=	0.25,

	visuals = {
		-- name	type position split dist
		headlights =	{1, 0.66, 0.08, 2.15, 0.1},
		brakelights= 	{0, 0.78, 0.18, -2.34, 0.09},

		frontdimlights = {0.9, 0.07, 1.95, 0.1},
		backdimlights=	{0.8, 0.05, -2.36, 0.09},

		-- position
		backlights =	{0.79, -0.01, -2.36, 0.09},

		engine = {0.0, 0.1, 2.1},
		exhaust = {0.4, -0.22, -2.2, 2},

		driver = {-0.35, 0.15, -0.1},
	},

	hingePoints = {
		front = {0.0, -0.25, 2.15},
		back = {0.0, -0.25, -2.35},
	},

	useBodyColor = true,

	colors = {
		test1 = CarColor(0.0, 0.0, 0.0, 1.0),
		test2 = CarColor(0.01, 0.01, 0.01, 1.0),
	},

	bodysize =	Vector3D.new(0.9, 0.48, 2.35),
	center = 	Vector3D.new(0, 0.0, 0.0),

	gravitycenter =	Vector3D.new(0.0, -0.05, 0.0),
	mass =		2800,
	antiroll=	0.25,

	gears = {
		-1.85,
		2.0,
		1.2,
		0.79,
		0.59,
	},

	differential =		25.0,
	transmissionrate =	1.0,
	torquemultipler	=	0.9,

	maxspeed=		140,

	suspensionLift=		0.01,
		
	wheels=
	{
		-- front wheels
		wheel1=
		{
			SuspensionTop=			{-0.75, 0.12, 1.29},
			SuspensionBottom=		{-0.85, -0.64, 1.29},
			SuspensionSpringConstant=	54000.0,
			SuspensionDampingConstant=	2000,

			Radius =			0.32,
			Width=			0.24,

			BrakeTorque=		12000.0,

			Steer=			1,
			Drive=			false,
			Handbrake=		false,
		},

		wheel2=
		{
			SuspensionTop=			{0.75, 0.12, 1.29},
			SuspensionBottom=		{0.85, -0.64, 1.29},
			SuspensionSpringConstant=	54000.0,
			SuspensionDampingConstant=	2000,

			Radius= 			0.32,
			Width=			0.24,

			BrakeTorque=		12000.0,
			Steer=			1,
			Drive=			false,
			Handbrake=		false,
		},

		-- back wheels
		wheel3=
		{
			SuspensionTop=			{0.75, 0.12, -1.31},
			SuspensionBottom=		{0.85, -0.64, -1.31},
			SuspensionSpringConstant=	54000.0,
			SuspensionDampingConstant=	2000,

			Radius= 			0.32,
			Width=			0.24,

			BrakeTorque=		12000.0,

			Steer=			0,
			Drive=			true,
			Handbrake=		true,
		},

		
		wheel4=
		{
			SuspensionTop=			{-0.75, 0.12, -1.31},
			SuspensionBottom	=	{-0.85, -0.64, -1.31},
			SuspensionSpringConstant=	54000.0,
			SuspensionDampingConstant=	2000,
			Radius =			0.32,
			Width=			0.24,

			BrakeTorque=	12000.0,

			Steer=			0,
			Drive	=		true,
			Handbrake	=	true,
		},
	}
}