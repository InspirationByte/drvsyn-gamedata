name		"Rollo 300";

cleanmodel	"models/vehicles/rollo.egf";
damagedmodel	"models/vehicles/rollo_dam.egf";
wheelBodyGroup	"wheel1b";

sounds
{
	engine		"car.engine3";
	engine_low	"car.engine3_low";
	engine_idle	"defaultcar.engine_idle";

	horn 		"car.horn2";
	// use single engine sound
	//engine_off	"sounds/engines/engine2_off.wav"
};

camera_height 	1.15;
camera_distance 6.2;

camera_height_in 	0.25;
camera_distance_in	1.5;
camera_side_in		0.25;

visuals
{
	// name	type position split dist
	headlights 	1 0.66 0.08 2.10 0.1;
	brakelights 	0 0.78 0.18 -2.25 0.09;

	frontdimlights	0.9 0.07 1.90 0.1;
	backdimlights	0.8 0.05 -2.30 0.09;

	// position
	backlights 	0.76 0.03 -2.31 0.09;

	engine 0.0 0.1 2.1;
	exhaust 0.4 -0.22 -2.2 2;

	driver -0.35 0.15 -0.1;
	passenger1 0.35 0.15 -0.1;
};

hingePoints
{
	front 0.0 -0.25 2.15;
	back 0.0 -0.25 -2.35;
}

useBodyColor 1;

colors
{
	//name		r	g	b	reflectivity
	default 	0.4 	0.0 	0.0	1.0;	// red
	color1 		0.47 	0.6 	0.24	1.0;	// green
	color2 		1.0 	1.0 	1.0	1.0;	// white
	color2 		0.7 	0.7 	0.7	1.0;	// gray
	color3 		0.97 	0.89 	0.55	1.0;	// yellow
	color4 		0.24 	0.21 	0.3	1.0;	// dark violet
	color5 		0.51 	0.73 	0.855	1.0;	// blue
	color7		0.255	0	0.255	1.0;	// Mangenta
	color8		0.32	0.12	0.16	0.8;	// Bordeau
};

bodysize	0.9 0.48 2.35;
center 		0 0.0 0.0;

gravitycenter	0.0 -0.08 0.08;
mass 		2800;
antiroll	0.19;

gears
	-1.85
	2.0
	1.2
	0.79
	0.59;


differential 		25.0;
transmissionrate	1.0;
torquemultipler		0.9;

maxspeed		140;

suspensionLift		0.01;
	
wheels
{
	// front wheels
	wheel
	{
		SuspensionTop			-0.75 -0.08 1.29;
		SuspensionBottom		-0.85 -0.64 1.29;
		SuspensionSpringConstant	40000.0;
		SuspensionDampingConstant	2500;

		Radius 			0.31;
		Width			0.24;

		BrakeTorque		12000.0;

		Steer			1;
		Drive			0;
		Handbrake		0;
	};

	wheel
	{
		SuspensionTop			0.75 -0.08 1.29;
		SuspensionBottom		0.85 -0.64 1.29;
		SuspensionSpringConstant	40000.0;
		SuspensionDampingConstant	2500;

		Radius 			0.31;
		Width			0.24;

		BrakeTorque		12000.0;
		Steer			1;
		Drive			0;
		Handbrake		0;
	};

	// back wheels
	wheel
	{
		SuspensionTop			0.75 -0.08 -1.31;
		SuspensionBottom		0.85 -0.64 -1.31;
		SuspensionSpringConstant	40000.0;
		SuspensionDampingConstant	2500;

		Radius 			0.31;
		Width			0.24;

		BrakeTorque		12000.0;

		Steer			0;
		Drive			1;
		Handbrake		1;
	};

	
	wheel
	{
		SuspensionTop			-0.75 -0.08 -1.31;
		SuspensionBottom		-0.85 -0.64 -1.31;
		SuspensionSpringConstant	40000.0;
		SuspensionDampingConstant	2500;
		Radius 			0.31;
		Width			0.24;

		BrakeTorque		12000.0;

		Steer			0;
		Drive			1;
		Handbrake		1;
	};

};