--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2021
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 1 Feb 2015
--------------------------------------------------------------------------------

include "json.lua"
include "array.lua"
include "common.lua"
include "class.lua"
include "util.lua"

local updateFuncs = {}

local function DoUpdateFuncs(dt)
	for k,v in pairs(updateFuncs) do
		v.time = v.time - dt
		if v.time <= 0 then
			local ran, errorMsg = pcall( v.fn, v.freq + dt )
			if not ran then
				MsgError("Lua error: " .. errorMsg)
			end
			
			v.time = v.freq
		end
	end
end

function SetUpdateFunc(name, fn, freq, delay)
	if fn == nil then
		updateFuncs[name] = nil
	else
		updateFuncs[name] = { fn = fn, freq = freq or 0, time = delay or 0 }
	end
end

function stateUpdate(dt)
	DoUpdateFuncs(dt)
end

----------------------------------------------------------

local function loadMain()
	SetUpdateFunc("loadMain", nil)
	include("loadfiles.lua")
end

-- add extra 2 seconds for Fullscreen mode
local sys_fullscreen = console.FindCvar("sys_fullscreen");
local screenDelay = if_then_else(sys_fullscreen:GetBool(), 2.0, 0.0)

SetUpdateFunc("loadMain", loadMain, 0, 0)

-- set game to run title screen or engine it will kill itself

-- setup our fake sequence screen and allow engine to load the Lua files
SequenceScreens = {
	OnEnter = function()
	end,
	OnLeave = function()
	end,
	current = {
		schemeName = "resources/ui_loading.res",
		InitUIScheme = function()
		end,
		Update = function() 
			return true 
		end
	}
}

EqStateMgr.SetCurrentStateType( GAME_STATE_SEQUENCESCREEN )
