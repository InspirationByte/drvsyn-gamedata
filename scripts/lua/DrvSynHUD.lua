--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2021
--//////////////////////////////////////////////////////////////////////////////////

local mapZoomOn = false

-- for compatibility
HUD_DOBJ_IS_TARGET = HUD_DOBJ_SHOW_ON_MAP + HUD_DOBJ_3D_ARROW

-- make overload
local _CGameHUD_AddMapTargetPoint = CGameHUD.AddMapTargetPoint
CGameHUD.AddMapTargetPoint = function(self, position, flags)
	if flags == nil then
		flags = HUD_DOBJ_IS_TARGET
	end
	return _CGameHUD_AddMapTargetPoint(self, position, flags)
end

------------------------------------------------------------

-- register reset
SetLevelLoadCallbacks("mapZoom", function()
	mapZoomOn = false -- reset
end)

-- register input command
console.CreateInputCommand("hud_mapZoom", function(pressed) 

	if not pressed then
		return
	end

	mapZoomOn = not mapZoomOn

	if mapZoomOn then
		gameHUD:SetMapDisplay(1.5, 2.5, 3.2)
	else
		gameHUD:SetMapDisplay(1, 1, 1)
	end
end, "Toggle map zoom", 0)

-- last message
console.FindOrCreateCommand("hud_showLastMessage", function()
	gameHUD:ShowLastMessage()
end, "Show last message", 0)

----------------------------------------------
-- Adds tachometer update function if HUD has proper elements for it

function AddTachometerPlugin()
	local tachoElem = equi:Cast(gameHUD:FindChildElement("tacho"), "timer")

	if tachoElem ~= nil then
		MissionManager:SetPluginRefreshFunc("tacho", function(dt)
		
			local playerCar = gameses:GetPlayerCar()
			local maxRPM = 9000
			local rpm = playerCar:GetDisplayRPM()
			
			local displayValue = rpm / maxRPM;
			
			tachoElem:SetTimeValue(360 - displayValue * 40 + 20)
		
		end)
	end
end

------------------------------------------------------------
-- Plug-in to handle felony and damage bars

function AddPlayerHUDTracking()
	-- Damage bar
	local damageBar = equi:Cast(gameHUD:FindChildElement("damageBar"), "percentageBar")

	local lightsTime = 0

	if damageBar ~= nil then
		
		local alpha = 1.0
		local oldPercentageValue = 0.0
		local animateDamageBar = 0.0
		
		MissionManager:SetPluginRefreshFunc("hudDamageBar", function(dt)
		
			lightsTime = lightsTime + dt

			local percentage = 0

			local playerCar = gameses:GetPlayerCar()
			
			if playerCar ~= nil then
				percentage = playerCar:GetDamage() / playerCar:GetMaxDamage()				
			end

			if percentage > oldPercentageValue then
				animateDamageBar = animateDamageBar + (percentage - oldPercentageValue) * 25.0
				if animateDamageBar > 1.0 then
					animateDamageBar = 1.0
				end
			end
			
			-- same as CGameHUD::DrawPercentageBar computation
			local damageColor = v4d_lerp(vec4(0.6,0.5,0,alpha), vec4(0.6,0,0,alpha), percentage) * vec4(1.5);

			if percentage > 0.85 then
				local flashValue = math.sin(lightsTime*16.0) * 5.0;
				flashValue = clamp(flashValue, 0.0, 1.0);

				damageColor = v4d_lerp(vec4(0.5, 0.0, 0.0, alpha), vec4(0.8, 0.2, 0.2, alpha), flashValue) * vec4(1.5);
			end
			
			if animateDamageBar > 0 then
				damageColor = damageColor + vec4(animateDamageBar)
				animateDamageBar = animateDamageBar - dt
			end
			
			local animateDamageBarShake = math.pow(animateDamageBar, 4)

			damageBar:SetValue(percentage)
			damageBar:SetColor(damageColor)
			damageBar:SetTransform(vec2(math.sin(animateDamageBarShake * 445.0) * 5.0, 0), vec2(1.0 + animateDamageBarShake*0.05), math.sin(animateDamageBarShake * 445.0) * 0.5)
			
			oldPercentageValue = percentage

		end)
	end
	--[[
	local felonyBar = equi:Cast(gameHUD:FindChildElement("felonyBar"), "percentageBar")
	
	if felonyBar ~= nil then
		MissionManager:SetPluginRefreshFunc("hudFelonyBar", function(dt)
	
			local felonyPercent = 0
			local playerCar = gameses:GetPlayerCar()
			
			if playerCar ~= nil then
				felonyPercent = math.abs(playerCar:GetFelony())
				if felonyPercent >= 0.1 then
					local colorValue = clamp(math.sin(lightsTime * 12.0), -1.0, 1.0)

					local v1 = math.pow(-math.min(0.0,colorValue), 2.0)
					local v2 = math.pow(math.max(0.0,colorValue), 2.0)

					local newColor = v4d_lerp(vec4(v1,0,v2,1), vec4(1,1,1,1), 0.15)
					felonyBar:SetColor(newColor)
				else
					felonyBar:SetColor(vec4(0.2,0.2,1,1))
				end
			end
			
			felonyBar:SetValue(felonyPercent)
		end)
	end]]
	
	-- Felony bar
	local felonyElem = gameHUD:FindChildElement("felonyBar")
	if felonyElem ~= nil then
		local felonyCountElem = felonyElem:FindChild("CountLabel")
		
		if felonyCountElem ~= nil then
			MissionManager:SetPluginRefreshFunc("hudFelonyBar", function(dt)
			
				local felonyPercent = 0
				local playerCar = gameses:GetPlayerCar()

				if playerCar ~= nil then
					felonyPercent = math.floor(math.abs(playerCar:GetFelony()) * 100)
					
					local textColor = vec4(1,1,1,1)
					
					-- modify felony text color
					if felonyPercent >= 10.0 and playerCar:GetPursuedCount() > 0 then
						local colorValue = clamp(math.sin(lightsTime * 16.0) * 16.0, -1.0, 1.0)

						local v1 = math.pow(-math.min(0.0,colorValue), 2.0);
						local v2 = math.pow(math.max(0.0,colorValue), 2.0);

						textColor = v4d_lerp(vec4(v1,0,v2,1), vec4(1,1,1,1), 0.25)
					elseif felonyPercent >= 50.0 then
						textColor = vec4(1.0, 0.2, 0.2, 1);
					elseif felonyPercent >= 10.0 then
						textColor = vec4(1.0, 0.8, 0.0, 1);
					end
					
					felonyElem:SetTextColor(textColor)
					felonyCountElem:SetTextColor(textColor)
					
					felonyCountElem:SetLabel(string.format("%d%%", felonyPercent))
				end
			end)
		end
	end
end

-- Add game HUD initialization
SetMissionInitializedCallback("gameHUD", function()
	AddPlayerHUDTracking()
	--AddTachometerPlugin()
end)
