--//////////////////////////////////////////////////////////////////////////////////
--// Copyright © Inspiration Byte
--// 2009-2021
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 27 Jun 2021
--------------------------------------------------------------------------------

local filesToLoad = {}
local filesCompleted = {}
local curFile = nil

local function makeIncludePath(str)
	local cwd = CALLER_FILENAME:match("(.*[/\\])")

	if str:find("^scripts") ~= nil then
		return str
	end
    return cwd .. str
end

local function fileLoader()
	if curFile == nil then
		curFile = 0
	end
	
	curFile = curFile+1
	
	if curFile <= #filesToLoad then
		if type(filesToLoad[curFile]) == "function" then
			filesToLoad[curFile]()
		else
			include(filesToLoad[curFile])
		end
	else
		SetUpdateFunc("fileLoader", nil)
		
		for i,v in ipairs(filesCompleted) do
			v()
		end
		
		curFile = nil
		filesToLoad = {}
		filesCompleted = {}
	end
end

function loadTableOfFiles(t, onCompleted)
	for k,v in pairs(t) do
		if type(v) == "table" then
			loadTableOfFiles(v)
		else
			table.insert(filesToLoad, makeIncludePath(v))
		end
	end

	if #filesToLoad > 0 then
		table.insert(filesCompleted, onCompleted)
		SetUpdateFunc("fileLoader", fileLoader)
	end
end

local Files = {
	Engine = {
		"eqLua.lua",
		"eqConsole.lua",
		"eqVectorMath.lua",
		"eqKeyValues.lua",
		"eqNetworking.lua",
		"eqLocalizeUtils.lua",
		"eqUI.lua",
	},
	DrvSyn = {
		-- boot
		"DrvSynBoot.lua",
		
		-- sequences
		"DrvSynSequenceScreen.lua",
		
		-- session
		"session/DrvSynVehicleManager.lua",
		"session/DrvSynMissionManager.lua",
		"session/DrvSynMissionLadder.lua",
		
		-- utility stuff
		"utility/DrvSynMissionUtility.lua",
		"utility/DrvSynTrailblazerUtility.lua",
		"utility/DrvSynPursuitUtility.lua",
		
		-- gamemodes
		"gamemodes/DrvSynFreeride.lua",
		"gamemodes/DrvSynPursuitGame.lua",
		"gamemodes/DrvSynCheckpointGame.lua",
		"gamemodes/DrvSynGetawayGame.lua",
		"gamemodes/DrvSynTrailblazerGame.lua",
		"gamemodes/DrvSynSurvivalGame.lua",
		
		-- menus
		"menus/DrvSynMenus.lua",
		
		-- other utilities
		"DrvSynWorldUtils.lua",
		"DrvSynCinematicCamera.lua",
		"DrvSynScriptedObjects.lua",
		"DrvSynSounds.lua",
		"DrvSynHUD.lua",
		"scripts/missions/defaultmission.lua",
		
		-- test
		
	}
}

-- load system files
loadTableOfFiles(Files.Engine)

-- user profile data has to be loaded before all Mods
userProfileManager:LoadSavedData()

loadTableOfFiles(Files.DrvSyn, function()
	-- initialize addons
	addons:InitPostBoot()
	ResetMissionTable()
	
	-- after all scripts loaded we can bring in title screen
	EqStateMgr.SetCurrentStateType( GAME_STATE_TITLESCREEN )
	
	-- do command line stuff here without limitations
	console.ExecuteCommandLine()
end)



------------------------------------------
-- Initialize game to the title screen
--[[
if StartReplay("demoreplays/intro", REPLAYMODE_INTRO) then
	EqStateMgr.ScheduleNextStateType( GAME_STATE_GAME )
else
	EqStateMgr.SetCurrentStateType( GAME_STATE_TITLESCREEN )
end]]
------------------------------------------
