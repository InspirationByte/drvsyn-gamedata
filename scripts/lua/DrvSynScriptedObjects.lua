--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2016
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- Scripted object basic functionality
-- By Shurumov Ilya
-- 29 May 2016
--------------------------------------------------------------------------------

-- include some files
ScriptedObjects = {
	"objects/SoundScriptedObject.lua",
	"objects/SpawnPoints.lua",
	--"objects/MovingEventObjects.lua"
}

-- HELPERS

-- sets the field
CGameObject.Set = function(self_go, fieldName, value)
	local evth = self_go:GetEventHandler()
	if evth == nil then
		evth = {}
	end
	
	-- set field
	evth[fieldName] = value
	
	-- set as event handler
	self_go:SetEventHandler(evth)
end

CGameObject.Get = function(self_go, fieldName)
	local evth = self_go:GetEventHandler()
	if evth == nil then
		evth = {}
	end
	
	return evth[fieldName]
end

--------------------------------------------------------------------------------

g_scriptObjectFactory = {}

function RegisterScriptedObject( name, classname )
	g_scriptObjectFactory[name] = classname
end

function CreateScriptedObject( name, owner )
	local factory = g_scriptObjectFactory[name]
	
	if factory ~= nil then
		-- create with passing the C++ owner object
		return factory( owner )
	end

	return nil
end

--------------------------------------------------------------------------------

-- the basic object
CBaseScriptedObject = class()
	function CBaseScriptedObject:__init( owner )
		-- c++ bound / owner object
		self.owner = owner
	end

	function CBaseScriptedObject:OnSpawn()
		
	end
	
	function CBaseScriptedObject:OnRemove()
		
	end
	
	--function CBaseScriptedObject:Simulate( deltaTime )
	--
	--end
	
loadTableOfFiles(ScriptedObjects)