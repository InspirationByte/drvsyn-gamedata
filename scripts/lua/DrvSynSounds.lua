--//////////////////////////////////////////////////////////////////////////////////
--// Copyright © Inspiration Byte
--// 2009-2021
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 7 Jan 2021
--------------------------------------------------------------------------------

-- there are default known emitter sound script paths
EmitterSounds = 
{
	syndicate_cop_vo 	= "scripts/sounds/sounds_cops.txt",
	iview 				= "scripts/sounds/sounds_iview.txt",
}

-- kind of registry. You can add your custom scripts here and they will be loaded
EmitterSoundRegistry = {

}

-- you can add your cities to the table
CopVoiceOver = {
	default 			= EmitterSounds.syndicate_cop_vo
}

-- returns voice over script for current level file
function GetCopVoiceOverScript()
	local levName = world:GetLevelName()
	local scriptName = CopVoiceOver[levName]
	
	if scriptName == nil then
		return CopVoiceOver["default"]
	end
	
	return scriptName
end

function PrecacheEmitterSounds()
	-- register
	for k,v in pairs(EmitterSoundRegistry) do
		sounds:LoadScript(v)
	end

	-- also we would like to load cop voice over
	if MISSION.CopVoiceOver ~= nil then
		sounds:LoadScript(MISSION.CopVoiceOver)
	end	
end

-- register emitter sounds
SetMissionLoadedCallback("PrecacheEmitterSounds", PrecacheEmitterSounds)


