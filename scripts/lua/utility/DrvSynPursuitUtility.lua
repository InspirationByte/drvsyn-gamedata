--------------------------------------------------------------------------------
-- Pursuit utility
--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 31 Jan 2021
--------------------------------------------------------------------------------

local pursuit_debug = console.FindOrCreateCvar("pursuit_debug", "0", "Pursuit debugging", 0)

PursuitUtility = {
	HitCars = {},
	LoadedCarReplays = {}
}

-- install replay loading function with optional parameters
CGameSessionBase._LoadCarReplay = CGameSessionBase.LoadCarReplay

CGameSessionBase.LoadCarReplay = function(self, car, replayFilename, loadEvents)
	loadEvents =  if_then_else(loadEvents ~= nil, loadEvents, true)

	local duration = self:_LoadCarReplay(car, replayFilename, loadEvents)
		
	if pursuit_debug:GetBool() then
		-- store filename
		if loadEvents then
			PursuitUtility.LoadedCarReplays[car] = replayFilename
			PursuitUtility.LastLoadedReplay = replayFilename
		end
	
		Msg("LoadCarReplay Attach\n")
		
		PursuitUtility.HitCars = {}
		car:Set("OnCarCollision", PursuitUtility.OnCollision)
		
		MissionManager:ScheduleEvent(function()
			PursuitUtility.Save()
		end, duration)
		

	end
	
	return duration
end

--------------------------------------------------------------------------------
-- the below shit is added due to fact that replay randomization is fucked
-- please fix the game engine!
--------------------------------------------------------------------------------

-- replay_debug_vehicle replayData/bustout01_2.vr

function PursuitUtility.OnCollision(self, props)
	local hitReplayId = props.hitBy:GetScriptID()

	if hitReplayId ~= -1 then
		PursuitUtility.PushItem(hitReplayId)
	end
end

function PursuitUtility.Save()

	local kvs = KeyValues.new()
	
	local tbl = {
		remove = PursuitUtility.HitCars,
	}
	
	kvs:GetRoot():FromTable(tbl, false)

	kvs:SaveFile(PursuitUtility.LastLoadedReplay .. ".def", SP_MOD)
	
	MsgInfo(PursuitUtility.LastLoadedReplay .. ".def saved\n")
end

function PursuitUtility.PushItem(newItem)

	for k,v in ipairs(PursuitUtility.HitCars) do
		if v == newItem then
			return
		end
	end

	Msg(string.format("Chased car hit %d\n", newItem))
	table.insert(PursuitUtility.HitCars, newItem)
end

function PursuitUtility.LeadAttachDebug()
	Msg("AttachLead\n")
	PursuitUtility.HitCars = {}
	
	local leadCar = gameses:GetLeadCar()
	
	leadCar:Set("OnCarCollision", PursuitUtility.OnCollision)
end

function PursuitUtility.ReplayAttachDebug()

	Msg("ReplayAttachDebug\n")

	PursuitUtility.HitCars = {}

	-- this will be called right after mission starts
	SetGameUpdateCallback("PursuitUtility", function()
		local playerCar = gameses:GetPlayerCar()
		
		Msg("START\n")
		playerCar:Set("OnCarCollision", PursuitUtility.OnCollision)
		
		SetGameUpdateCallback("PursuitUtility", nil)
	end, 1)
end

function cmd_debug_vehiclereplay( args )

	if #args == 0 then
		PursuitUtility.LeadAttachDebug()
		return
	end

	-- load replay
	if game.StartReplay(args[1], REPLAYMODE_STORED_REPLAY) then
		EqStateMgr.SetCurrentStateType( GAME_STATE_GAME )
		SetMissionLoadedCallback("debug_vehiclereplay", PursuitUtility.ReplayAttachDebug)
	end
end

console.FindOrCreateCommand("replay_debug_vehicle", cmd_debug_vehiclereplay, "Starts replay vehicle debugging", CV_CHEAT)