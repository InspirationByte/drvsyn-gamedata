--------------------------------------------------------------------------------
-- Trailbralzer recorder
-- Records trail blazer cones as well as cone gates
--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 31 Jul 2017
--------------------------------------------------------------------------------

-- the basic object
CTrailblazerRecorder = class()
	function CTrailblazerRecorder:__init()
		self.numCones = 0
		self.gates = gates
		self.positionArray = {}
		self.conesArray = {}
	end

	function CTrailblazerRecorder:Record(gates)

		local playerCar = gameses:GetPlayerCar()
	
		MISSION.Settings.EnableCops = false
		ai:SetTrafficCarsEnabled(false)
		ai:RemoveAllCars()
		
		self.coneLastPos = vecCopy(playerCar:GetOrigin())
		self.startPos = self.coneLastPos;
		self.startRot = vecCopy(playerCar:GetAngles())
	
		self.numCones = 0
		self.gates = gates
		self.positionArray = {}
		self.conesArray = {}
		
		local collideMask = bit.bor(playerCar:GetCollideMask(), bit.bnot(OBJECTCONTENTS_SPECIAL_TRAILBLAZER_CONE))
		playerCar:SetCollideMask( collideMask )
	end
	
	function CTrailblazerRecorder:Test( pointsFile )
	
		local playerCar = gameses:GetPlayerCar()
	
		self:Cleanup()
	
		if pointsFile ~= nil then
			self.numCones = 0
			self.positionArray = {}
			self.conesArray = {}
			self.gates = true
			
			local kvs = KeyValues.new()
			
			if kvs:LoadFile(pointsFile .. ".txt", SP_MOD) then
				local tbl = kvs:GetRoot():ToTable()
				
				self.startPos = toVector(tbl.StartPosition)
				self.startRot = toVector(tbl.StartRotation)
				
				for k,v in pairs(tbl.Positions) do
				
					v.Position = toVector(v.Position);
					v.Direction = toVector(v.Direction);

					self.positionArray[k] = v
				end
			end
		end
	
		MISSION.Settings.EnableCops = false
		
		ai:SetTrafficCarsEnabled(true)
		ai:RemoveAllCars()
		
		playerCar:SetOrigin(self.startPos)
		playerCar:SetAngles(self.startRot)
		playerCar:SetVelocity(Vector3D.new(0))
		
		local collideMask = bit.bor(playerCar:GetCollideMask(), OBJECTCONTENTS_SPECIAL_TRAILBLAZER_CONE)
		playerCar:SetCollideMask( collideMask )
		
		objects:QueryNearestRegions(self.startPos, true)
		
		local sortedKeys = table.sortedKeys(self.positionArray, function(a, b) return a.Id < b.Id end)
		
		for _,key in ipairs(sortedKeys) do
		
			local v = self.positionArray[key]
		
			local coneDistPerc = v.Id / 100
		
			self:SpawnConeGate(v.Position, v.Direction, coneDistPerc)
		end
	end
	
	function CTrailblazerRecorder:SpawnConeGate(pos, dir, coneDistPerc)
		
		local coneDistRatio 	= (1.0 - coneDistPerc)*1.0
		
		local filter = CPhysCollisionFilter.new()
		local result = gameutil.TestLine(pos, pos-Vector3D.new(0,10,0), OBJECTCONTENTS_SOLID_GROUND, filter)
		
		if result:GetFract() >= 1.0 then
			return
		end
		
		if self.gates == true then
			local rightVec = cross(result:GetNormal(), dir) * Vector3D.new(TrailblazerGame.GateDist + TrailblazerGame.GateScaling*coneDistRatio)

			local resultL = gameutil.TestLine(pos-rightVec+Vector3D.new(0,1,0), pos-rightVec-Vector3D.new(0,10,0), OBJECTCONTENTS_SOLID_GROUND, filter)
			local resultR = gameutil.TestLine(pos+rightVec+Vector3D.new(0,1,0), pos+rightVec-Vector3D.new(0,10,0), OBJECTCONTENTS_SOLID_GROUND, filter)
		
			if resultL:GetFract() < 1.0 and resultR:GetFract() < 1.0 then
				local coneL = objects:CreateObject("cone")
				local coneR = objects:CreateObject("cone")
				
				coneL:SetOrigin(resultL:GetPosition() + Vector3D.new(0,0.35,0))
				coneR:SetOrigin(resultR:GetPosition() + Vector3D.new(0,0.35,0))
				
				coneL:Spawn()
				coneR:Spawn()
				
				coneL:SetContents(OBJECTCONTENTS_SPECIAL_TRAILBLAZER_CONE)
				coneR:SetContents(OBJECTCONTENTS_SPECIAL_TRAILBLAZER_CONE)
				coneL:SetCollideMask(0)
				coneR:SetCollideMask(0)
				
				table.insert(self.conesArray, coneL)
				table.insert(self.conesArray, coneR)
				
				self.numCones = self.numCones + 1
			end
		else
			local cone = objects:CreateObject("cone")
			cone:SetOrigin(result:GetPosition() + Vector3D.new(0,0.35,0))
			cone:SetContents(OBJECTCONTENTS_SPECIAL_TRAILBLAZER_CONE)
			cone:SetCollideMask(0)
			
			cone:Spawn()
			
			table.insert(self.conesArray, cone)
			
			self.numCones = self.numCones + 1
		end
	end
	
	function CTrailblazerRecorder:Update( delta )
		local playerCar 		= gameses:GetPlayerCar()
		local playerCarPos		= playerCar:GetOrigin()
		local playerCarForward	= playerCar:GetForwardVector()
		
		local coneDistPerc 		= self.numCones / 100
		local coneIntervalRatio = 1.0
		
		local distToLastCone = length(self.coneLastPos - playerCarPos)
		
		local FIXED_INTERVAL = TrailblazerGame.FixedInterval
		local DYNAMIC_INTERVAL = TrailblazerGame.DynamicInterval
		
		if self.gates then
			FIXED_INTERVAL = TrailblazerGame.GateFixedInterval
			DYNAMIC_INTERVAL = TrailblazerGame.GateDynamicInterval
		end

		if distToLastCone > FIXED_INTERVAL+DYNAMIC_INTERVAL*coneIntervalRatio and self.numCones < 100 then
		
			self.positionArray[tostring(self.numCones)] = {
				Id = self.numCones, 
				Position = vecCopy(playerCarPos), 
				Direction = playerCarForward
			};
				
			gameHUD:ShowScreenMessage("Point " .. self.numCones+1 .. " / 100", 1.5)
	
			self:SpawnConeGate(playerCarPos, playerCarForward, coneDistPerc);
				
			self.coneLastPos = vecCopy(playerCarPos)

		elseif self.numCones == 100 then
			self:Stop()
		end
	end
	
	function CTrailblazerRecorder:Cleanup()
	
		if self.conesArray ~= nil then
			for i,v in ipairs(self.conesArray) do
				objects:RemoveObject(v)
			end
		end

		self.conesArray = {}
	end
	
	function CTrailblazerRecorder:Stop()
	
		self:Cleanup()

		local kvs = KeyValues.new()
		
		local tbl = {
			StartPosition = self.startPos,
			StartRotation = self.startRot,
			Positions = create_section(self.positionArray, function(a, b) return a.Id < b.Id end),
		}
		
		kvs:GetRoot():FromTable(tbl, false)
		
		gameHUD:ShowScreenMessage("Saved to 'trailblazer_generated.txt'", 3.5)
		
		kvs:SaveFile("trailblazer_generated.txt", SP_MOD)

		MissionManager:SetPluginRefreshFunc("trailblazer", nil);
	end
	
g_trailblazerRecorder = CTrailblazerRecorder()

function cmd_trailblazerStartRecording( args )
	Msg("Started trail blazer recording...\n")
	g_trailblazerRecorder:Record(false)
	MissionManager:SetPluginRefreshFunc("trailblazer", function( dt )
		g_trailblazerRecorder:Update( dt )
	end);
end

console.FindOrCreateCommand("trailbrazer_record", cmd_trailblazerStartRecording, "Starts trailblazer recording", CV_CHEAT)

function cmd_trailblazerStartRecording( args )
	Msg("Started cone gates recording...\n")
	g_trailblazerRecorder:Record(true)
	MissionManager:SetPluginRefreshFunc("trailblazer", function( dt )
		g_trailblazerRecorder:Update( dt )
	end);
end

console.FindOrCreateCommand("trailbrazer_gates_record", cmd_trailblazerStartRecording, "Starts cone gates recording", CV_CHEAT)

function cmd_trailblazerStop( args )
	g_trailblazerRecorder:Stop()
end

console.FindOrCreateCommand("trailbrazer_stop", cmd_trailblazerStop, "Stops cone gates recording", CV_CHEAT)

function cmd_trailblazerTest( args )
	if #args > 0 then
		g_trailblazerRecorder:Test(args[1])
	else
		g_trailblazerRecorder:Test()
	end
end

console.FindOrCreateCommand("trailbrazer_test", cmd_trailblazerTest, "Starts cone gates test game", CV_CHEAT)