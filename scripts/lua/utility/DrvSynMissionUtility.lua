--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2015
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- Mission utility functions
-- By Shurumov Ilya
-- 15 Mar 2015
--------------------------------------------------------------------------------

g_practice = console.FindOrCreateCvar("g_practice", "1", "Practice mode", 0)
g_trafficMaxCars = console.FindCvar("g_trafficMaxCars")
g_difficulty = console.FindCvar("g_difficulty")

-- definedd by g_difficulty value
CopDifficultyTable = {
	[0] = { -- 'Super Easy' - should not be used really
		RoadBlockTime = 40.0,
		RoadBlockMinFelony = 0.95,
		RoadBlockExtraCars = -2,
		MinCops = 1,
		MaxCops = 2,
		MinCopBackupFelony = 0.5,
		CopRespawnInterval = 120,				-- not wanted
		CopWantedRespawnInterval = 60,			-- when no pursuit
		CopWantedRespawnInterval2 = 40, 		-- when pursuit is active
		MaxCopDamage = 4.0,
		CopAccelerationScale = 1.0,
		CopMaxSpeed = 140,
		CopHandlingDifficulty = CAR_HANDLING_NORMAL,
		CopMinPursuitFelony = 0.1,
		CopViewPreset = {
			{ Radius = 18.0, FOV = 55.0, Far = 70.0 }, -- normal
			{ Radius = 45.0, FOV = 90.0, Far = 90.0 }, -- wanted
			{ Radius = 70.0, FOV = 90.0, Far = 90.0 }, -- pursued
			{ Radius = 55.0, FOV = 55.0, Far = 15.0 }, -- roadblock
		}
	},
	[1] = { -- 'Easy'
		RoadBlockTime = 25.0,
		RoadBlockMinFelony = 0.75,
		RoadBlockExtraCars = -1,
		MinCops = 1,
		MaxCops = 2,
		MinCopBackupFelony = 0.4,
		CopRespawnInterval = 120,				-- not wanted
		CopWantedRespawnInterval = 85,			-- when no pursuit
		CopWantedRespawnInterval2 = 50, 		-- when pursuit is active
		MaxCopDamage = 4.0,
		CopAccelerationScale = 1.35,
		CopMaxSpeed = 160,
		CopHandlingDifficulty = CAR_HANDLING_PURSUER1,
		CopMinPursuitFelony = 0.1,
		CopViewPreset = {
			{ Radius = 18.0, FOV = 55.0, Far = 70.0 }, -- normal
			{ Radius = 45.0, FOV = 90.0, Far = 90.0 }, -- wanted
			{ Radius = 70.0, FOV = 90.0, Far = 90.0 }, -- pursued
			{ Radius = 55.0, FOV = 55.0, Far = 15.0 }, -- roadblock
		}
	},
	[2] = { -- 'Normal'
		RoadBlockTime = 20.0,
		RoadBlockMinFelony = 0.5,
		RoadBlockExtraCars = -1,
		MinCops = 1,
		MaxCops = 2,
		MinCopBackupFelony = 0.3,
		CopRespawnInterval = 100,				-- not wanted
		CopWantedRespawnInterval = 80,			-- when no pursuit
		CopWantedRespawnInterval2 = 60, 		-- when pursuit is active
		MaxCopDamage = 5.0,
		CopAccelerationScale = 2.2,
		CopMaxSpeed = 180,
		CopHandlingDifficulty = CAR_HANDLING_PURSUER2,
		CopMinPursuitFelony = 0.1,
		CopViewPreset = {
			{ Radius = 18.0, FOV = 55.0, Far = 70.0 }, -- normal
			{ Radius = 45.0, FOV = 90.0, Far = 90.0 }, -- wanted
			{ Radius = 70.0, FOV = 90.0, Far = 90.0 }, -- pursued
			{ Radius = 55.0, FOV = 55.0, Far = 15.0 }, -- roadblock
		}
	},
	[3] = { -- 'Hard'
		RoadBlockTime = 15.0,
		RoadBlockMinFelony = 0.4,
		RoadBlockExtraCars = 0,
		MinCops = 1,
		MaxCops = 3,
		MinCopBackupFelony = 0.2,
		CopRespawnInterval = 80,				-- not wanted
		CopWantedRespawnInterval = 70,			-- when no pursuit
		CopWantedRespawnInterval2 = 50, 		-- when pursuit is active
		MaxCopDamage = 6.0,
		CopAccelerationScale = 3.0,
		CopMaxSpeed = 200,
		CopHandlingDifficulty = CAR_HANDLING_PURSUER3,
		CopMinPursuitFelony = 0.1,
		CopViewPreset = {
			{ Radius = 18.0, FOV = 55.0, Far = 70.0 }, -- normal
			{ Radius = 45.0, FOV = 90.0, Far = 90.0 }, -- wanted
			{ Radius = 70.0, FOV = 90.0, Far = 90.0 }, -- pursued
			{ Radius = 55.0, FOV = 55.0, Far = 15.0 }, -- roadblock
		}
	}
}

----------------------------------------------
-- Mission settings

function MakeDefaultMissionSettings()

	local misSettings = {
		-- default settings for traffic
		MaxTrafficCars = g_trafficMaxCars:GetInt(), -- can be altered by missions
		EnableTraffic = true,
		TrafficSpawnInterval = 0,			-- density
	
		-- default settings for cops
		EnableCops = true,
		EnableRoadBlock = true,
		StopCops = false,
		StopCopsPosition = vec3_zero,
		StopCopsRadius = 800,
		StopCopsEndThreshold = 0.5,
		
		KeepPursuitMusic = false			-- keeps pursuit music after pursuit is done
	}

	-- apply difficulty table
	local difficultyValue = g_difficulty:GetInt()
	return table.combine(misSettings, CopDifficultyTable[difficultyValue])
end

----------------------------------------------
-- Default variables

function InitMissionDefaults()
	MISSION.RandomSeed = math.floor(math.random(0, 1000))

	MISSION.Settings = MakeDefaultMissionSettings()
	MISSION.MusicState = MUSIC_STATE_AMBIENT
	MISSION.CopVoiceOver = GetCopVoiceOverScript()

	MISSION.CopState = {
		InitState = true,
		InPursuit = false,
		NextRoadblockTime = MISSION.Settings.RoadBlockTime,
		ToldAboutRoadblock = false,
		InStopCopsZone = false
	}
end

----------------------------------------------
-- Makes new empty valid mission table

function ResetMissionTable( shouldKeepData )

	local keepData = if_then_else(MISSION ~= nil, function() return MISSION.Data end, {})
	
	MISSION = {
		Data = if_then_else(shouldKeepData == true, keepData, {}),
		Init = (function()
		end),
		Finalize = (function()
		end),
		Hud = "resources/hud/defaulthud.res",
		LoadingScreen = "resources/loadingscreen_default.res",
		MusicScript = "vegas_day"
	}
	
	InitMissionDefaults()
end

-- stores current mission
function StoreMissionCompletionData( key, tbl )
	
	local missionData = userProfileManager:GetProgressStore(USER_STORE_MISSION_DATA)
	
	missionData:SetTableKeyBase(key, tbl)

	userProfileManager:Save()
end

function RestoreMissionCompletionData( key )
	local missionData = userProfileManager:GetProgressStore(USER_STORE_MISSION_DATA):ToTable()
	
	return missionData[key]
end

----------------------------------------------
-- Makes engine to change mission script.
-- Does not change engine FSM state and must be done manually

function LoadMission( gamemode, name )

	ResetMissionTable()

	local gameModeMissions = missions[gamemode];
	
	if gameModeMissions == nil then
		MsgError("LoadMission: no such game mode " .. gamemode .. "\n");
		return false;
	end
	
	local missionFound = false
	
	for k,v in ipairs(gameModeMissions) do
	
		local missionName = v
		if type(missionName) == "table" then
			missionName = missionName[1]
		end
	
		if missionName == name then
			missionFound = true;
			break
		end
	end
	
	if missionFound == false then
		MsgWarning("LoadMission: no such mission " .. name .. " for mode " .. gamemode .. ", attempting to load...\n");
	end
	
	-- setup the game mode
	MissionManager:SetGameMode(gamemode)
	
	local result = game.SetMissionScript(gamemode .. "/" .. name)

	return result;
end

--------------------------------------------------------------------------------

MUSIC_STATE_AMBIENT = 0
MUSIC_STATE_PURSUIT = 1

----------------------------------------------
-- Sets the music name. Called before mission initialization
--
function SetMusicName( scriptName )
	MISSION.MusicScript = scriptName
end

----------------------------------------------
-- Sets the music state. Called mostly by UpdateCops
--
function SetMusicState( state )
	local musicName = "music." .. MISSION.MusicScript

	MISSION.MusicState = state
 
	local ep = EmitParams.new( musicName, EMITSOUND_FLAG_FORCE_CACHED )
	ep:set_sampleId(MISSION.MusicState)
	
	sounds:Emit2D( ep, -1 )
end

-- cmd
console.FindOrCreateCommand("mission_setMusic", 
	function(args)
		if #(args) == 0 then
			return
		end
		
		SetMusicName(args[1])
		SetMusicState( MISSION.MusicState )
	end, "Sets music name", 0)

----------------------------------------------
-- Applies mission settings to the AI subsystem. Called every time.

function SetupSettings()
	local settings = MISSION.Settings
	
	ai:SetMaxTrafficCars( MISSION.Settings.MaxTrafficCars )
	ai:SetTrafficCarsEnabled( MISSION.Settings.EnableTraffic )
	ai:SetTrafficSpawnInterval( MISSION.Settings.TrafficSpawnInterval )

	-- adjust cop AI settings
	ai:SetCopsEnabled( settings.EnableCops and (not MISSION.CopState.InStopCopsZone) )
	ai:SetCopMaxDamage( settings.MaxCopDamage )
	ai:SetCopAccelerationModifier( settings.CopAccelerationScale )
	ai:SetCopMaxSpeed( settings.CopMaxSpeed )
	ai:SetCopHandlingType( settings.CopHandlingDifficulty )
	
	-- adjust felony system settings
	felonySystem:SetMinPursuitFelony(settings.CopMinPursuitFelony)
	for i,v in ipairs(settings.CopViewPreset) do
		felonySystem:SetCopViewPreset(i - 1, v.Radius, v.FOV, v.Far)
	end
end
	
----------------------------------------------
-- Mission settings getting applied before mission starts

function SetupInitialSettings()
	SetupSettings();
	
	local settings = MISSION.Settings

	ai:SetMaxCops( settings.MinCops )
	ai:SetCopRespawnInterval( settings.CopRespawnInterval )
end

----------------------------------------------
-- Updates cop stop function
-- Searches for all cops around the target position
-- Makes cop decelerate when they get close to target
-- Makes player unattractive to cops when player is very close to target

function UpdateStopCops(veh)
	local settings = MISSION.Settings
	local copState = MISSION.CopState
	local plDist = length(veh:GetOrigin() - settings.StopCopsPosition)
	
	if settings.StopCops == true and plDist < settings.StopCopsRadius then
		-- handle entering the stop zone
		local cars = ai:QueryTrafficCars(settings.StopCopsRadius, settings.StopCopsPosition)
	
		if (not copState.InStopCopsZone) and plDist < settings.StopCopsRadius*0.5 then
			copState.InStopCopsZone = true
			
			-- set player unattractive to cops
			if veh:GetFelony() > 0 then
				veh:SetFelony(-veh:GetFelony())
			end
		end
		
		if copState.CopsInStopZone == nil then
			copState.CopsInStopZone = {}
		end
	
		-- walk through all cop cars spawned nearby
		for i,v in ipairs(cars) do
			local aiCar = gameutil.CastObject(v, "CAIPursuerCar", GO_CAR_AI)
		
			if aiCar ~= nil and aiCar:IsPursuer() then
				local aiDist = length(aiCar:GetOrigin() - settings.StopCopsPosition)
				
				if aiDist < settings.StopCopsRadius * settings.StopCopsEndThreshold then
					if aiCar:InPursuit() then
						aiCar:EndPursuit(false)
					end
				elseif aiDist < settings.StopCopsRadius then
					aiCar:SetTorqueScale(0.5)
					aiCar:SetMaxSpeed(60)
				end
			end
		end
	elseif copState.InStopCopsZone then
	
		-- Handle exiting the stop zone
		
		local cars = ai:QueryTrafficCars(settings.StopCopsRadius, settings.StopCopsPosition)
		
		-- reset cop cars
		for i,v in ipairs(cars) do
			local aiCar = gameutil.CastObject(v, "CAIPursuerCar", GO_CAR_AI)
		
			if aiCar ~= nil and aiCar:IsPursuer() then
				SetupCopCarSettings(aiCar)
			end
		end
		
		copState.InStopCopsZone = false

		-- restore attractiveness back
		if veh:GetFelony() < 0 then
			veh:SetFelony(-veh:GetFelony())
		end
	end
end
	
----------------------------------------------
-- Updates cop logic based on vehicle felony and mission settings
--
function UpdateCops( veh, fDt )
	local settings = MISSION.Settings
	local copState = MISSION.CopState

	SetupSettings()
	
	UpdateStopCops(veh)

	if veh ~= nil then
	
		local oldInPursuit = copState.InPursuit
		copState.InPursuit = (veh:GetPursuedCount() + veh:GetPursuedCopCount()) > 0
	
		-- music updates
		if copState.InitState or copState.InPursuit ~= oldInPursuit then

			if copState.InPursuit then
				if MISSION.MusicState ~= MUSIC_STATE_PURSUIT then
					SetMusicState( MUSIC_STATE_PURSUIT )
				end
			else
				if settings.KeepPursuitMusic == false or copState.InitState then
					SetMusicState( MUSIC_STATE_AMBIENT )
				end
			end
		end
		
		if not settings.EnableCops then
			return
		end
		
		-- cops backup control
		if veh:GetFelony() < settings.MinCopBackupFelony then
		
			ai:SetMaxCops( settings.MinCops )
			ai:SetCopRespawnInterval( settings.CopRespawnInterval )
			
		else
		
			local copsInPursuit = copState.InPursuit and veh:GetPursuedCopCount() > 0
		
			if copsInPursuit and ai:GetMaxCops() ~= settings.MaxCops then
				-- or (copsInPursuit == false and veh:GetPursuedCopCount() < settings.MaxCops and ai:GetMaxCops() ~= veh:GetPursuedCopCount()) then
				ai:MakeCopSpeech("cop.backup", true, 0)
			end
		
			ai:SetMaxCops( settings.MaxCops )
			
			if veh:GetFelony() < 0.5 then
				-- quick respawn interval
				ai:SetCopRespawnInterval( settings.CopWantedRespawnInterval )
			else
				-- much quicker respawn interval
				ai:SetCopRespawnInterval( settings.CopWantedRespawnInterval2 )
			end

			-- place road blocks
			if copsInPursuit and settings.EnableRoadBlock and veh:GetFelony() > settings.RoadBlockMinFelony then
				
				copState.NextRoadblockTime = copState.NextRoadblockTime - fDt;
				
				local requestRoadBlock = (ai:IsRoadBlockSpawn() == false) and (copState.NextRoadblockTime <= 0) and (veh:GetSpeed() > 10)
				
				if requestRoadBlock == true then
				
					local targetAngle = VectorAngles( v3d_normalize( veh:GetVelocity() ) ):get_y()+45.0;
				
					if ai:CreateRoadBlock( veh:GetOrigin(), targetAngle, settings.RoadBlockExtraCars, -1 ) then
						copState.NextRoadblockTime = settings.RoadBlockTime
					end
				end

			end
		end
		
		-- initialization done
		copState.InitState = false

	end
end

----------------------------------------------
-- Updates rubber banding parameters for specific car
-- parameters table example:
--	{
--		[15] = {		-- distance
--			torqueScale = 1.0,
--			maxSpeed = 135,
--		},
--		[40] = {		-- distance
--			torqueScale = 1.5,
--			maxSpeed = 155,
--		},
--	}
--
function UpdateRubberBanding(car, position, params)
    local dist = length(position - car:GetOrigin())
    
    local torqueScale = 1.0
    local speed = 140

	for d,p in orderedPairs(params) do
		if dist < d then
		    torqueScale = p.torqueScale
			speed = p.maxSpeed
			break
		end
	end

    car:SetTorqueScale(torqueScale)
    car:SetMaxSpeed(speed)
end

----------------------------------------------
-- Returns true if vehicle is wrecked
--
function CheckVehicleIsWrecked( veh, props, delta )
	-- check player vehicle is wrecked
	if veh:IsAlive() == false then
		return true
	end

	if props.notOnGroundTime == nil then
		props.notOnGroundTime = 10.0
	end
	
	if veh:IsFlippedOver(true) then
		props.notOnGroundTime = props.notOnGroundTime-delta
	else
		props.notOnGroundTime = 10.0
	end
	
	if props.notOnGroundTime <= 0 then
		return true;
	end
	
	return false
end

----------------------------------------------
-- Sets up cop car settings. Useful for newly spawned cops by scripts

function SetupCopCarSettings( car )
	local settings = MISSION.Settings
	
	car:SetTorqueScale( settings.CopAccelerationScale )
	car:SetMaxSpeed( settings.CopMaxSpeed)
	car:SetMaxDamage( settings.MaxCopDamage )
end

----------------------------------------------
-- Walks pedestrian to specific target

function PedWalkToTarget(ped, targetPos, run)
	local pedAngles = ped:GetAngles()

	local targetToPedVec = targetPos - ped:GetOrigin();
	local dir = normalize(targetToPedVec);
	local dirAngles = VectorAngles(dir);

	local angleDiff = AngleDiff(pedAngles:get_y(), dirAngles:get_y());

	local controlButtons = if_then_else(run == true, IN_BURNOUT + IN_ACCELERATE, IN_ACCELERATE)
	
	--Msg("angleDiff: "..angleDiff.. "pedAngles.y " .. pedAngles.y .. "dirAngles.y " .. dirAngles.y)

	if math.abs(angleDiff) > 1.0 then
		if (angleDiff > 0) then
			controlButtons = controlButtons + IN_STEERLEFT
		else
			controlButtons = controlButtons + IN_STEERRIGHT
		end
	end
	
	ped:SetControlButtons(controlButtons)
	
	return length(targetToPedVec)
end

----------------------------------------------

function cmd_rocketpunch( args )
	local playerCar = gameses:GetPlayerCar()
	
	if playerCar == nil then
		return
	end

	local speed = 80
	if #args > 0 then
		speed = tonumber(args[1])
	end

	playerCar:SetVelocity(playerCar:GetForwardVector() * Vector3D.new(speed))
end

console.FindOrCreateCommand("rocketpunch", cmd_rocketpunch, "Launches the car to the space", CV_CHEAT)

function cmd_dumpCameraProps( args )
	local camera = world:GetView()
	
	if camera == nil then
		return
	end
	
	local pos = camera:GetOrigin()
	
	local ang = camera:GetAngles()

	Msg(string.format("position = Vector3D.new(%.2f, %.2f, %.2f)", pos:get_x(),pos:get_y(),pos:get_z()))
	Msg(string.format("angles = Vector3D.new(%.2f, %.2f, %.2f)", ang:get_x(),ang:get_y(),ang:get_z()))
end

console.FindOrCreateCommand("lua_dump_camera_props", cmd_dumpCameraProps, "", 0)

function cmd_dumpCarProps( args )
	local playerCar = gameses:GetPlayerCar()
	
	if playerCar == nil then
		return
	end
	
	local pos = playerCar:GetOrigin()
	
	local ang = playerCar:GetAngles()

	Msg(string.format("position = Vector3D.new(%.2f, %.2f, %.2f)", pos:get_x(),pos:get_y(),pos:get_z()))
	Msg(string.format("angles = Vector3D.new(%.2f, %.2f, %.2f)", ang:get_x(),ang:get_y(),ang:get_z()))
end

console.FindOrCreateCommand("lua_dump_car_props", cmd_dumpCarProps, "", 0)

function cmd_killMyCar( args )
	local playerCar = gameses:GetPlayerCar()
	
	if playerCar == nil then
		return
	end
	
	local killVel = Vector3D.new(0,-30,0)
	local killTimer = 1.0
	local nextKill = 0
	
	MissionManager:SetPluginRefreshFunc("kill", function( dt )
	
		killTimer = killTimer-dt
		nextKill = nextKill-dt
	
		if nextKill <= 0 then
			playerCar:SetVelocity(killVel)
			nextKill = 0.05
		end
		
		if killTimer <= 0 then
			MissionManager:SetPluginRefreshFunc("kill", nil)
		end
	end)

end

console.FindOrCreateCommand("kill", cmd_killMyCar, "", 0)

---------------------------------------------------------------------------------------
-- moved here because command line

function cmd_start(args)
	if #args == 0 then
		return
	end

	-- always set level name
	world:SetLevelName( args[1] );

	local scriptFileName = string.format("scripts/missions/%s.lua", args[1])
	if fileSystem:FileExist(scriptFileName, SP_MOD) then
		-- try load mission script
		game.SetMissionScript(args[1])
	else
		game.SetMissionScript("defaultmission")
	end

	EqStateMgr.SetCurrentStateType( GAME_STATE_GAME )
end

console.FindOrCreateCommand("start", cmd_start, "start a game with specified mission script name", 0)

---------------------------------------------------------------------------------------

-- install mission ladder status tracker to the SignalMissionStatus
CGameSessionBase._SignalMissionStatus = CGameSessionBase.SignalMissionStatus

CGameSessionBase.SignalMissionStatus = function(self, newStatus, delay, doneFunc)

	if newStatus == MIS_STATUS_SUCCESS then
		missionladder:OnMissionCompleted()
	end
	
	-- set the done function
	MissionManager:SetOnGameDone(doneFunc)
	
	self:_SignalMissionStatus(newStatus, delay)
end