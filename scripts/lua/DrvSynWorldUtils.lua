--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2017
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- World utils and events
-- By Shurumov Ilya
-- 03 Aug 2017
--------------------------------------------------------------------------------

-- this table can be accessed everywhere
WorldEvents = {}

------------------------------------------------------

gameutil.CastObject = function(obj, class, goType)
	return gameobjcast[class](obj, goType)
end

OBJECTCONTENTS_SPECIAL_TRAILBLAZER_CONE = OBJECTCONTENTS_CUSTOM_START

------------------------------------------------------

local LevelLoadEvents = {}
local LevelUnloadEvents = {}
local LevelScheduledEvents = {}

local GameUpdateEvents = {}
local ObjectSpawnedEvents = {}
local ObjectRemovedEvents = {}
local RegionLoadEvents = {}
local RegionUnloadEvents = {}

function DumpSpawnedEvents()
	util.printObject(ObjectSpawnedEvents)
end

-- schedules the function execution
function ScheduleLevelEvent( func, delay )
	table.insert(LevelScheduledEvents, {func = func, delay=delay or 0})
end

-- registers event callback of world update
function SetGameUpdateCallback(name, func)
	GameUpdateEvents[name] = func
end

-- registers event callbacks of region loading/unloading
function SetRegionLoadCallbacks(name, loadFunc, unloadFunc)
	RegionLoadEvents[name] = loadFunc
	RegionUnloadEvents[name] = unloadFunc
end

-- registers event callbacks of region loading/unloading
function SetLevelLoadCallbacks(name, loadFunc, unloadFunc)
	LevelLoadEvents[name] = loadFunc
	LevelUnloadEvents[name] = unloadFunc
end

function SetObjectSpawnRemoveCallbacks(name, spawnFunc, removeFunc)
	ObjectSpawnedEvents[name] = spawnFunc
	ObjectRemovedEvents[name] = removeFunc
end

------------------------------------------------------

world.OnLevelLoad = function( self )
	for k,v in pairs(LevelLoadEvents) do
		if v ~= nil then 
			v()
		end
	end
end

world.OnLevelUnload = function( self )
	for k,v in pairs(LevelUnloadEvents) do
		if v ~= nil then 
			v()
		end
	end
end

------------------------------------------------------

objects.Init = function(self)
	RegionLoadEvents = {}
	RegionUnloadEvents = {}
	LevelScheduledEvents = {}
	ObjectSpawnedEvents = {}
	ObjectRemovedEvents = {}
	
	-- reset
	WorldEvents = {}
	SetGameUpdateCallback("WorldEvents", nil)
	
	-- load world event file
	local eventFilename = "scripts/lua/levels/" .. world:GetLevelName() .. "_events.lua"
	if fileSystem:FileExist(eventFilename, SP_MOD) then
		local events = include(eventFilename)

		if events == nil then
			MsgWarning("-- Error! '" .. eventFilename .. "' is not a correct event script file! --\n")
			MsgWarning("   - Make sure script returns table and has 'Init' and 'Update' function\n")

			return
		end

		-- assign the world events
		WorldEvents = events

		SetGameUpdateCallback("WorldEvents", function() 
			-- Init event objects
			WorldEvents:Init()
			
			-- set to use updates
			SetGameUpdateCallback("WorldEvents", function(delta)
				WorldEvents:Update(delta)
			end)
		end)
	end
end

objects.Update = function(self, delta)
	for k,v in pairs(GameUpdateEvents) do
		if v ~= nil then
			v(delta)
		end
	end
	
	-- play scheduled events
	for k,v in ipairs(LevelScheduledEvents) do
		v.delay = v.delay - delta
		
		if v.delay <= 0 then
			v.func( )
			
			table.remove(LevelScheduledEvents, k)
		end
	end
end

objects.OnRegionLoaded = function( self, regionIndx )
--	Msg( string.format(" + Region %d loaded\n", regionIndx) )
	for k,v in pairs(RegionLoadEvents) do
		if v ~= nil then 
			v(regionIndx)
		end
	end
end

objects.OnRegionUnloaded = function( self, regionIndx )
	for k,v in pairs(RegionUnloadEvents) do
		if v ~= nil then 
			v(regionIndx)
		end
	end
end

objects.OnSpawned = function(self, gameObject)
	local func = ObjectSpawnedEvents[gameObject:GetName()]
	if func ~= nil then
		func(gameObject)
	else
		--MsgWarning(string.format("Your spawned object '%s' supposed to have Spawn callback, right?\n", gameObject:GetName()))
	end
end

objects.OnRemoved = function(self, gameObject)
	local func = ObjectRemovedEvents[gameObject:GetName()]
	if func ~= nil then
		func(gameObject)
	end
end
