--//////////////////////////////////////////////////////////////////////////////////
--// Copyright © Inspiration Byte
--// 2009-2019
--//////////////////////////////////////////////////////////////////////////////////

-------------------------------------------------------------
--	Equilibrium Engine Lua functions
-------------------------------------------------------------

function script_name() 
    return debug.getinfo(2, "S").source
end

function dofile(filename)

	local filebuf = fileSystem:GetFileBuffer(filename, SP_MOD)
	
	if filebuf == nil or (filebuf ~= nil and filebuf:len() == 0) then
		error("dofile error: Cannot open file "..filename.." for parsing!")
		return nil
	end
	
	local result = load(filebuf, filename)
	
	if result == nil then
		error("dofile error: not a valid Lua file '"..filename.."'!")
		return nil
	end
	
	return result()
end

function GC_CRITICAL()
	local before = collectgarbage("count")
	collectgarbage()
	local after = collectgarbage("count")
end

-- TODO: more functions related to Eq Engine filesystem



local kcode_used = false
function kkkode()
	if kcode_used ~= true then
		kcode_used = true
		MenuCarsList = table.combine(MenuCarsList, MenuSecretCars)
	end
end