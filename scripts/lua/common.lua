--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2015
--//////////////////////////////////////////////////////////////////////////////////

-------------------------------------------------------------
--	script utility functions
-------------------------------------------------------------

-- nil?
local function isempty(s)
  return s == nil or s == ''
end

-- is integer???
function isint(n)
  return n==math.floor(n)
end

function getFilePath(str, sep)
    sep = sep or '/'
    return str:match("(.*"..sep..")")
end

function getFileNameOnly(str)
    return str:match("(.+)%..+")
end

function getFileName(str)
  return str:match("^.+/(.+)$")
end

function getFileExtension(str)
  return str:match("^.+(%..+)$")
end

-- cond ? a : b
function if_then_else(cond, a, b)

	local condResult = false
	
	if type(cond) == "function" then
		condResult = cond()
	else
		condResult = cond
	end

	if condResult then
		if type(a) == "function" then
			return a()
		end
	
		return a
	else
		if type(b) == "function" then
			return b()
		end
	
		return b
	end
end

-- range helper
--[[
	Example:
        for i in range( 10) print(i) end -- iterate 1 to 10, increment by 1
        for i in range(-10) print(i) end -- iterate -1 to -10, decrement by 1
        for i in range(7, -2) print(i) end -- iterate 7 to -2, decrement by 1
        for i in range(3, 27, 3) print(i) end -- iterate 3 to 27, increment by 3
        for i in range(0, 1, -1) print(i) end -- iterate 0 to 1, decrementing by 1 (loop forever downward)
        for i in range() print(i) end -- error() because the call to the "returned" iterator is a nil value
]]
function range(a, b, step)
	if not b then
		b = a
		a = 1
	end
	step = step or 1
	local f =
		step > 0 and
			function(_, lastvalue)
				local nextvalue = lastvalue + step
				if nextvalue <= b then return nextvalue end
			end or
		step < 0 and
			function(_, lastvalue)
				local nextvalue = lastvalue + step
				if nextvalue >= b then return nextvalue end
			end or
			function(_, lastvalue) return lastvalue end
	return f, nil, a - step
end

--[[
Ordered table iterator, allow to iterate on the natural order of the keys of a
table.
]]

function cmp_multitype(op1, op2)
    local type1, type2 = type(op1), type(op2)
    if type1 ~= type2 then --cmp by type
        return type1 < type2
    elseif type1 == "number" or type1 == "string" then --type2 is equal to type1
        return op1 < op2 --comp by default
    elseif type1 == "boolean" then
        return op1 == true
    else
        return tostring(op1) < tostring(op2) --cmp by address
    end
end

function __genOrderedIndex( t )
    local orderedIndex = {}
	
    for key in pairs(t) do
        table.insert( orderedIndex, key )
    end
	table.sort( orderedIndex, cmp_multitype ) 
    return orderedIndex
end

function orderedNext(t, state)
    -- Equivalent of the next function, but returns the keys in the alphabetic
    -- order. We use a temporary ordered key table that is stored in the
    -- table being iterated.

    local key = nil
    --print("orderedNext: state = "..tostring(state) )
    if state == nil then
        -- the first time, generate the index
        t.__orderedIndex = __genOrderedIndex( t )
        key = t.__orderedIndex[1]
    else
        -- fetch the next value
        for i = 1,#t.__orderedIndex do
            if t.__orderedIndex[i] == state then
                key = t.__orderedIndex[i+1]
            end
        end
    end

    if key then
        return key, t[key]
    end

    -- no more value to return, cleanup
    t.__orderedIndex = nil
    return
end

function orderedPairs(t)
	-- cleanup of latest state in some special cases
	t.__orderedIndex = nil

    -- Equivalent of the pairs() function on tables. Allows to iterate
    -- in order
    return orderedNext, t, nil
end

function consttable( table )
   return setmetatable({}, {
     __index = table,
     __newindex = function(table, key, value)
                    error("Attempt to modify read-only table")
                  end,
     __metatable = false
   });
end