--//////////////////////////////////////////////////////////////////////////////////
--// Copyright © Inspiration Byte
--// 2009-2019
--//////////////////////////////////////////////////////////////////////////////////

UserInputs = {}

-- helper function for console execution
function ConsolePrint( v )
	util.printObject(v)
end

console.CreateInputCommand = function(name, func, desc, flags)
	console.FindOrCreateCommand("+"..name, function()
		if not UserInputs[name] then
			func(true)
		end
		UserInputs[name] = true
	end, desc, flags)
	console.FindOrCreateCommand("-"..name, function()
		if UserInputs[name] then
			func(false)
		end
		UserInputs[name] = nil
	end, desc, flags)
end

console.FindOrCreateCvar = function(name, value, desc, flags)
	local fcvar = console.FindCvar(name)

	if fcvar ~= nil then
		return fcvar
	end
	
	return console.CreateCvar(name, value, desc, flags or 0)
end

console.FindOrCreateCommand = function(name, func, desc, flags)
	local fcmd = console.FindCommand(name)

	if fcmd ~= nil then
		return fcmd
	end
	
	return console.CreateCommand(name, func, desc, flags or 0)
end
