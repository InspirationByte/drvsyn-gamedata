--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2017
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- Scripted spawn point object
-- By Shurumov Ilya
-- 27 Sep 2017
--------------------------------------------------------------------------------

--SetLevelLoadCallbacks("spawnpoints", PrecacheSoundPathesForLevel, CleanupSoundPatches)

---------------------------------------------------------------------------------------------

-- the basic object
local CNullObject = class(CBaseScriptedObject)
	function CNullObject:OnSpawn()
	
		--local objectName = self.owner:GetName()
		
		-- TODO: register spawn point in mission registry
		objects:RemoveObject( self.owner )
	end
	
RegisterScriptedObject("SCRIPTED_SpawnPoint", CNullObject)
RegisterScriptedObject("SCRIPTED_Cycler", CNullObject)