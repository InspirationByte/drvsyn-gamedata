--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2016
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- Scripted sound object
-- By Shurumov Ilya
-- 29 May 2016
--------------------------------------------------------------------------------

g_soundPaths = {}

local function CleanupSoundPatches()
	g_soundPaths = {}
end

local function ValidateSoundPath( path )
	if path.sound == nil then
		MsgError(string.format("ValidateSoundPath - missing sound variable (string)!"))
		return false
	end
	
	if path.points == nil then
		MsgError(string.format("ValidateSoundPath - missing points (array)!"))
		return false
	end
	
	if path.cyclePoints == nil then
		path.cyclePoints = false
	end
	
	if type(path.as2D) == "number" then
		path.as2D = if_then_else(path.as2D > 0, true, false)
	end

	if path.as2D == nil then
		path.as2D = false
	end
	
	if path.as2D == nil and path.distance == nil then
		path.distance = 10.0
	end
	
	if path.environment ~= nil and type(path.environment) == "string" then
		path.environment = {path.environment}
	end
	
	-- list of 'point' keys
	local pointVectorArray = {}
	
	for k,v in orderedPairs(path.points) do
		local point = toVector(v)
		table.insert(pointVectorArray, point)
	end

	if #pointVectorArray < 2 then
		MsgError(string.format("ValidateSoundPath - insufficient points!"))
		return false
	end
	
	path.points = pointVectorArray
	
	return true
end

local function PrecacheSoundPathesForLevel()

	local levelName = world:GetLevelName()
	local filename =  "scripts/levels/" .. levelName .. "_soundpaths.txt"

	local kvs = KeyValues.new()
	
	g_soundPaths = {}

	if kvs:LoadFile(filename, SP_MOD) then
		local tableOfSounds = kvs:GetRoot():ToTable()
		
		for k,v in pairs(tableOfSounds) do
			if ValidateSoundPath( v ) == false then
				MsgWarning(string.format("Cannot validate sound path '%s'\n", k))
			else
				g_soundPaths[k] = v;
			end
		end
	else
		MsgError("Failed to load sound paths from '" .. filename .. "'!\n")
	end
end

SetLevelLoadCallbacks("scriptsound", PrecacheSoundPathesForLevel, CleanupSoundPatches)

---------------------------------------------------------------------------------------------

-- the basic object
local CScriptedSoundPath = class(CBaseScriptedObject)
	function CScriptedSoundPath:OnSpawn()
	
		local objectName = self.owner:GetName()
		self.bestPosition = nil
		
		-- find a config by object name
		self.path = g_soundPaths[objectName]
		
		if self.path == nil then
			MsgError(string.format("Unknown sound path '%s'\n", objectName))
			return
		end
		
		if self.path.environment ~= nil then
			local currentEnv = world:GetEnvironmentName()
			
			local shouldUse = false
		
			for k,v in ipairs(self.path.environment) do
				if currentEnv == v then
					shouldUse = true
					break
				end
			end
			
			if shouldUse == false then
				objects:RemoveObject( self.owner )
				return
			end
		end

		sounds:Precache( self.path.sound )
		
		local ep = EmitParams.new( self.path.sound )
		self.sound = sounds:CreateController( ep )
		
		if self.sound == nil then
			MsgError(string.format("Unknown sound '%s'\n", self.path.sound))
		else
			if self.path.as2D == true then
				self.sound:SetVolume(0.0)
			end
		end
	end
	
	function CScriptedSoundPath:OnRemove()
		if self.sound ~= nil then
			sounds:RemoveController( self.sound )
		end
		self.sound = nil
	end
	
	function CScriptedSoundPath:Simulate( dt )

		if self.path == nil then
			return
		end
		
		local view = world:GetView()
		local camPos = view:GetOrigin()
		
		local lastPoint = nil
		
		if self.path.cyclePoints == true then
			lastPoint = self.path.points[#(self.path.points)]
		end

		local minDist = 100000.0
		
		for i,v in ipairs(self.path.points) do
			-- cycle points if needed
			if lastPoint ~= nil then
				debugoverlay:Line3D(lastPoint, v, Vector4D.new(1,1,1,1), Vector4D.new(1,1,1,1), 0.0)

				local pr = lineProjection( lastPoint, v, camPos)
				pr = clamp(pr, 0.0, 1.0)
				local posOnLine = lerp(lastPoint, v, pr)
				
				local distToCam = length(posOnLine - camPos)
				
				if distToCam < minDist then
					self.bestPosition = posOnLine
					minDist = distToCam
				end
			end
			
			lastPoint = v -- set
		end
		
		if self.bestPosition ~= nil and self.sound ~= nil then
		
			self.sound:SetOrigin( self.bestPosition )
			
			if self.path.as2D == true then
				local bestVolume = math.pow(1.0-clamp(minDist / self.path.distance, 0.0, 1.0), 2.0)
				self.sound:SetVolume(bestVolume)
			end
			
			if self.sound:IsStopped() then
				self.sound:Play()
			end
			
			debugoverlay:Box3D(self.bestPosition - Vector3D.new(0.2), self.bestPosition + Vector3D.new(0.2), Vector4D.new(1,1,0,1), 0.0)
		end
	end
	
RegisterScriptedObject("SCRIPTED_SoundPath", CScriptedSoundPath)