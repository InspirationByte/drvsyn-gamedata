--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2016
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- DEPRECATED
--------------------------------------------------------------------------------

g_movingEvents = {}

local function CleanupMovingEventObjects()
	g_movingEvents = {}
end

local function ValidateMovePath( path )
	if path.points == nil and path.objectpoints == nil then
		MsgError(string.format("ValidateMovePath - missing points (array)!"))
		return false
	end
	
	if path.cyclePoints == nil then
		path.cyclePoints = false
	end
	
	if path.environment ~= nil and type(path.environment) == "string" then
		path.environment = {path.environment}
	end
	
	-- list of 'point' keys
	local pointVectorArray = {}

	if path.points ~= nil then
		for k,v in orderedPairs(path.points) do
			local pos = toVector3D(v)
			local angle = toVector3D(v, 3)
			local speed = v[7]
			
			local rotation = eulersToQuaternion(VDEG2RAD(NormalizeAngles360(angle)))
			rotation:normalize()
			
			table.insert(pointVectorArray, { pos = pos, rot = rotation, speed = speed })
			
			if type(pos) == 'table' or type(angle) == 'table' then
				MsgError(string.format("ValidateMovePath - invalid points!"))
				return false
			end
		end
	elseif path.objectpoints ~= nil then
		-- use points from level objects
		local name = path.objectpoints[1]
		local count = path.objectpoints[2]
		local speed = path.objectpoints[3]
		
		for i,v in range(1, count) do	
			local obj = world:FindObjectOnLevel(name .. i)
						
			if obj.position ~= nil and obj.rotation ~= nil then
				local rotation = eulersToQuaternion(VDEG2RAD(NormalizeAngles360(obj.rotation)))
				rotation:normalize()

				table.insert(pointVectorArray, { pos = obj.position, rot = rotation, speed = speed })
			end
		end
	end
	
	if #pointVectorArray < 2 then
		MsgError(string.format("ValidateMovePath - insufficient points!"))
		return false
	end
	
	path.points = pointVectorArray
	
	return true
end

local function LoadMovingEventObjects()

	local levelName = world:GetLevelName()
	local filename =  "scripts/levels/" .. levelName .. "_eventobjects.txt"

	local kvs = KeyValues.new()
	
	g_movingEvents = {}

	if kvs:LoadFile(filename, SP_MOD) then
		local tableObjects = kvs:GetRoot():ToTable()
		
		for k,v in pairs(tableObjects) do
			ValidateMovePath(v)
			g_movingEvents[k] = v;
		end
	else
		MsgError("Cannot load moving events from '" .. filename .. "'!\n")
	end
end

local function AttachEventHandlerToObject(eventData, gameObj)
	--MsgWarning("object "..gameObj:GetName().." found in event table\n")
	local handler = gameObj:GetEventHandler()
	
	if eventData.environment ~= nil then
		local currentEnv = world:GetEnvironmentName()
		
		local shouldUse = false
	
		for k,v in ipairs(eventData.environment) do
			if currentEnv == v then
				shouldUse = true
				break
			end
		end
		
		if shouldUse == false then
			world:RemoveObject( gameObj )
			return
		end
	end
	
	if handler == nil then
		handler = {}
	end
	
	handler.Data = {
		currentPoint = -1,
		nextPoint = 0,
		pointProgress = 0,
		distanceToPoint = -1,
		sound = nil
	}
	
	-- init sound
	if eventData.sound ~= nil then
		sounds:Precache( eventData.sound )
	
		local ep = EmitParams.new( eventData.sound )
		handler.Data.sound = sounds:CreateController( ep )
		
		if handler.Data.sound == nil then
			MsgError(string.format("Unknown sound '%s'\n", self.path.sound))
		end
	end
	
	-- setup simulation function
	handler.OnSimulate = function(self, fDt)	
		if handler.Data.pointProgress >= handler.Data.distanceToPoint then
			-- init
			handler.Data.currentPoint = handler.Data.nextPoint
			handler.Data.nextPoint = (handler.Data.nextPoint+1) % (#eventData.points)
			
			local pointA = eventData.points[handler.Data.currentPoint+1]
			local pointB = eventData.points[handler.Data.nextPoint+1]
			
			-- estimate distance
			if eventData.rotateOnly then
				handler.Data.distanceToPoint = pointA.speed
			else
				handler.Data.distanceToPoint = length(pointA.pos - pointB.pos)
				
			end
			handler.Data.pointProgress = 0
			
			-- if it's null
			if handler.Data.pointProgress >= handler.Data.distanceToPoint then
				return
			end
		end
		
		local pointA = eventData.points[handler.Data.currentPoint+1]
		local pointB = eventData.points[handler.Data.nextPoint+1]
		
		local lerpFac = handler.Data.pointProgress / handler.Data.distanceToPoint
		local interpolatedPos = lerp(pointA.pos, pointB.pos, lerpFac)
		local interpolatedAng = qslerp( (pointA.rot), (pointB.rot), lerpFac)
		
		gameObj:SetOrigin(interpolatedPos)
		gameObj:SetAngles(VRAD2DEG(qeulers(interpolatedAng)))
		
		-- update sound
		if handler.Data.sound ~= nil then
			handler.Data.sound:SetOrigin( interpolatedPos )

			if handler.Data.sound:IsStopped() then
				handler.Data.sound:Play()
			end
		end
		
		-- update velocity
		if not eventData.rotateOnly then
			local velocity = normalize(pointB.pos-pointA.pos)*Vector3D.new(pointA.speed)
			gameObj:SetVelocity(velocity)
			
			if handler.Data.sound ~= nil then
				handler.Data.sound:SetVelocity( velocity )
			end
		end
		
		-- advance progress
		if eventData.rotateOnly then
			handler.Data.pointProgress = handler.Data.pointProgress+fDt;
		else
			handler.Data.pointProgress = handler.Data.pointProgress+fDt*pointA.speed
		end
	end
	
	gameObj:SetEventHandler(handler)
	Msg("Handler OK?")
end

SetLevelLoadCallbacks("movingevents", LoadMovingEventObjects, CleanupMovingEventObjects)

SetObjectSpawnRemoveCallbacks("movingevents",
	function(gameObj) 	-- spawned
	
		local eventData = g_movingEvents[gameObj:GetName()]
		if eventData ~= nil then
			AttachEventHandlerToObject(eventData, gameObj)
		end
	end, 
	function(gameObj)	-- removed
	
	end)