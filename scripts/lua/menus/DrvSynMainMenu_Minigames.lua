--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2020
--//////////////////////////////////////////////////////////////////////////////////

local EQUI_SCHEME_NAME = "ui_mainmenu_minigames"

MiniGameExtraElements = {}

function GetMenuItemsForGameMode(gamemode, practice_allowed)

	local delegate = function()

		if practice_allowed == nil then
			practice_allowed = false
		end

		local gameModeMissions = missions[gamemode];

		local newItems = {}
		
		if practice_allowed then
			table.insert(newItems,
				MenuStack.MakeChoiceCvarParam("#MENU_GAME_MODE", g_practice, {
					{ "1", "#MENU_GAME_PRACTICE" },
					{ "0", "#MENU_GAME_TIMEATTACK" },
				}))
		end

		for _,v in ipairs(gameModeMissions) do
			local missionName = v
			local missionLabel = v

			if type(missionName) == "table" then
				missionName = v[1]
				missionLabel = v[2]
			end
			
			local menuItem = {
				label = missionLabel,
				isFinal = true,
				onEnter = function( self, stack )
					
					if LoadMission(gamemode, missionName) then
						EqStateMgr.ScheduleNextStateType(GAME_STATE_GAME)
					else
						return {menuCommand="Pop"}
					end
					
					return {}
				end,
			}
			
			table.insert(newItems, menuItem)
		end
		
		return newItems
	end

	return delegate
	
end

function MiniGameElementsFunc()
    local elems = {
        {
            label = "#MENU_GAME_INTERVIEW",
            isFinal = true,
            onEnter = function( self, stack )
                
                if LoadMission("training", "iview") then
                    EqStateMgr.ScheduleNextStateType(GAME_STATE_GAME)
                else
                    return {menuCommand="Pop"}
                end
                
                return {}
            end,
        },
        MenuStack.MakeSubMenu("#MENU_GAME_LOSETHETAIL", GetMenuItemsForGameMode("minigame/getaway"), nil, EQUI_SCHEME_NAME),
        MenuStack.MakeSubMenu("#MENU_GAME_PURSUIT", GetMenuItemsForGameMode("minigame/pursuit"), nil, EQUI_SCHEME_NAME),
        MenuStack.MakeSubMenu("#MENU_GAME_CHECKPOINT", GetMenuItemsForGameMode("minigame/checkpoint"), nil, EQUI_SCHEME_NAME),
       -- MenuStack.MakeSubMenu("#MENU_GAME_CHECKPOINTRUSH", GetMenuItemsForGameMode("minigame/checkpointrush"), nil, EQUI_SCHEME_NAME),
        MenuStack.MakeSubMenu("#MENU_GAME_TRAILBLAZER", GetMenuItemsForGameMode("minigame/trailblazer", true), nil, EQUI_SCHEME_NAME),
        MenuStack.MakeSubMenu("#MENU_GAME_GATES", GetMenuItemsForGameMode("minigame/gates", true), nil, EQUI_SCHEME_NAME),
        MenuStack.MakeSubMenu("#MENU_GAME_SURVIVAL", GetMenuItemsForGameMode("minigame/survival"), nil, EQUI_SCHEME_NAME)
    }

	for i,v in ipairs(MiniGameExtraElements) do
		table.insert(elems, i, v)
	end

    return elems
end

return MenuStack.MakeSubMenu("#MENU_MAIN_MINIGAMES", MiniGameElementsFunc, nil, EQUI_SCHEME_NAME)