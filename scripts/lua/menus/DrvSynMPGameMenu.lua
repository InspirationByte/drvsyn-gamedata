--//////////////////////////////////////////////////////////////////////////////////
--// Copyright © Inspiration Byte
--// 2009-2015
--//////////////////////////////////////////////////////////////////////////////////

AddLanguageFile("gamemenu_mp")


----------------------------------------------------------------------------------------------------
-- Multiplayer Game menu
----------------------------------------------------------------------------------------------------

local MPGameMenuElements = {

	{
		label = "#MENU_GAME_CONTINUE",
		isFinal = false,
		onEnter = function( self, stack )
			return {command="continue"}
		end,
	},
	{
		label = "#MENU_GAME_SHOWMAP",
		isFinal = false,
		onEnter = function( self, stack )
			return {command="showMap"}
		end,
	},
	{
		label = "#MENU_GAME_SETTINGS",
		isFinal = false,
		onEnter = function( self, stack )
			return {nextMenu=SettingsElements, titleToken="#MENU_GAME_SETTINGS"}
		end,
	},
	{
		label = "#MENU_GAME_EXIT",
		isFinal = false,
		onEnter = function( self, stack )
			return {nextMenu=ExitGameMenu, titleToken="#MENU_GAME_QUEST_SURE"}
		end,
	},
}

MPGameMenuStack = MenuStack( MPGameMenuElements, "#MENU_GAME_TITLE_MENU" )