--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2020
--//////////////////////////////////////////////////////////////////////////////////

AddLanguageFile("buttons")

local EQUI_SCHEME_NAME = "ui_mainmenu_controls"

----------------------------------------------------------------------------------------------------
-- GRAPHICS MENU
----------------------------------------------------------------------------------------------------

local function GetVideoModesMenuItems()
	local r_screen = console.FindCvar("r_screen")
	local allModes = host.GetVideoModes()
	
	local modes = {}
	
	local uniqueModes = {}
	
	-- make unique list of modes
	local uniqueModes = {}
	for i,v in ipairs(allModes) do
		if v.displayId == r_screen:GetInt() then
			local modeStr = string.format("%dx%d", v.width, v.height)
			uniqueModes[modeStr] = v
		end
	end
	
	-- sort resolution keys
	local resolutionSortedKeys = table.sortedKeys(uniqueModes, function(a,b) 
		return a.width < b.width
	end)
	
	for i,k in ipairs(resolutionSortedKeys) do
		local v = uniqueModes[k]
		if v.displayId == r_screen:GetInt() then 
			local modeStr = string.format("%dx%d", v.width, v.height)
		
			table.insert(modes, { modeStr, modeStr })
		end
	end

	return modes
end

local function ApplyScreenResolution()
	host.ApplyVideoMode()
	return {}
end

local SettingsGraphicsElements = {
	 
	-- Screen resolution
	MenuStack.MakeChoiceCvarParam("#MENU_SETTINGS_SCREENRESOLUTION", "sys_vmode", GetVideoModesMenuItems()),
	 
	MenuStack.MakeBoolCvarParam("#MENU_SETTINGS_FULLSCREEN", "sys_fullscreen"),
	
	MenuStack.MakeFunction("#MENU_SETTINGS_APPLY", ApplyScreenResolution, false),
	
	MenuStack.MakeSpacer(),
	 
	-- Texture quality
	MenuStack.MakeChoiceCvarParam("#MENU_SETTINGS_TEXTUREQUALITY", "r_loadmiplevel", {
        { "2", "#MENU_SETTING_LOW" },
        { "1", "#MENU_SETTING_MEDIUM" },
        { "0", "#MENU_SETTING_HIGH" },
    }),
	
	MenuStack.MakeChoiceCvarParam("#MENU_SETTINGS_REFLECTIONQUALITY", "r_drawReflections", {
        { "0", "#MENU_SETTING_OFF" },
        { "1", "#MENU_SETTING_LOW" },
        { "2", "#MENU_SETTING_HIGH" },
    }),
	 
	-- Drawing distance
	MenuStack.MakeFloatCvarParam("#MENU_SETTINGS_DRAWDISTANCE", "r_zfar", 100, 350, 10 ),

	-- Lights rendering distance
	MenuStack.MakeFloatCvarParam("#MENU_SETTINGS_LIGHTDISTANCE", "r_lightDistance", 50, 150, 10),
    
	-- Enable shadows
	MenuStack.MakeChoiceCvarParam("#MENU_SETTINGS_SHADOWS", "r_shadows", {
        { "0", "#MENU_SETTING_OFF" },
        { "1", "#MENU_SETTING_LOW" },
		{ "2", "#MENU_SETTING_MEDIUM" },
        { "3", "#MENU_SETTING_HIGH" },
    }),
	
    MenuStack.MakeChoiceCvarParam("#MENU_SETTINGS_SKIDMARKS", "r_drawSkidMarks", {
        { "0", "#MENU_SETTING_OFF" },
        { "1", "#MENU_SETTING_LOW" },
        --{ "2", "#MENU_SETTING_HIGH" },
    }),

	-- Show FPS meter
	MenuStack.MakeBoolCvarParam("#MENU_SETTINGS_SHOWFPS", "r_showFPS"),
}

----------------------------------------------------------------------------------------------------
-- SOUND MENU
----------------------------------------------------------------------------------------------------

local SettingsSoundElements = {
	-- Master sound volume
	MenuStack.MakeFloatCvarParam("#MENU_SETTINGS_MASTERVOLUME", "snd_mastervolume", 0, 1, 0.05, 100 ),
	
	-- Voice sound volume
	MenuStack.MakeFloatCvarParam("#MENU_SETTINGS_VOICEVOLUME", "snd_voicevolume", 0, 1, 0.05, 100 ),
	
	-- Music volume
	MenuStack.MakeFloatCvarParam("#MENU_SETTINGS_MUSICVOLUME", "snd_musicvolume", 0, 1, 0.05, 100 ),
}

----------------------------------------------------------------------------------------------------
-- BINDINGS MENU
----------------------------------------------------------------------------------------------------

local JoypadSettingsElements = {
	MenuStack.MakeInputBinder("#MENU_SETTINGS_JOY_AXIS_STEERING", "ax_steering"),
	MenuStack.MakeInputBinder("#MENU_SETTINGS_JOY_AXIS_ACCELBRAKE", "ax_accel_brake"),
	MenuStack.MakeInputBinder("#MENU_SETTINGS_JOY_AXIS_ACCEL", "ax_accel"),
	MenuStack.MakeInputBinder("#MENU_SETTINGS_JOY_AXIS_BRAKE", "ax_brake"),
	MenuStack.MakeSpacer(),

	MenuStack.MakeFloatCvarParam("#MENU_SETTINGS_JOY_DEADZONE", "in_joy_deadzone", 0, 0.5, 0.01, 100 ),
	MenuStack.MakeChoiceCvarParam("#MENU_SETTINGS_JOY_STEER_SMOOTH", "in_joy_steer_smooth", {
        { "1", "#MENU_NO" },
		{ "0", "#MENU_YES" },
    }),
	MenuStack.MakeFloatCvarParam("#MENU_SETTINGS_JOY_STEER_LINEAR", "in_joy_steer_linear", 0.25, 4.0, 0.05, 1 ),
	MenuStack.MakeFloatCvarParam("#MENU_SETTINGS_JOY_ACCEL_LINEAR", "in_joy_accel_linear", 0.25, 4.0, 0.05, 1 ),
	MenuStack.MakeFloatCvarParam("#MENU_SETTINGS_JOY_BRAKE_LINEAR", "in_joy_brake_linear", 0.25, 4.0, 0.05, 1 ),
}

local SettingsInputElements = {
	MenuStack.MakeSubMenu("#MENU_SETTINGS_JOYPAD_SETTINGS", JoypadSettingsElements, nil, EQUI_SCHEME_NAME),
	MenuStack.MakeSpacer(),
	
	MenuStack.MakeInputBinder("#MENU_SETTINGS_CONTROL_ACCEL", "accel"),
	MenuStack.MakeInputBinder("#MENU_SETTINGS_CONTROL_BRAKE", "brake"),
	
	MenuStack.MakeInputBinder("#MENU_SETTINGS_CONTROL_LEFT", "steerleft"),
	MenuStack.MakeInputBinder("#MENU_SETTINGS_CONTROL_RIGHT", "steerright"),
	MenuStack.MakeInputBinder("#MENU_SETTINGS_CONTROL_HANDBRAKE", "handbrake"),
	MenuStack.MakeInputBinder("#MENU_SETTINGS_CONTROL_BURNOUT", "burnout"),
	MenuStack.MakeInputBinder("#MENU_SETTINGS_CONTROL_FASTSTEER", "faststeer"),
	MenuStack.MakeInputBinder("#MENU_SETTINGS_CONTROL_HORN", "horn"),
	MenuStack.MakeInputBinder("#MENU_SETTINGS_CONTROL_SIREN", "siren"),

	MenuStack.MakeInputBinder("#MENU_SETTINGS_CONTROL_HAZARDS", "signal_emergency"),
	MenuStack.MakeInputBinder("#MENU_SETTINGS_CONTROL_BEAMS", "switch_beams"),
	MenuStack.MakeSpacer(),
	
	MenuStack.MakeInputBinder("#MENU_SETTINGS_CONTROL_CAMERA", "changecamera"),
	MenuStack.MakeInputBinder("#MENU_SETTINGS_CONTROL_LOOKLEFT", "lookleft"),
	MenuStack.MakeInputBinder("#MENU_SETTINGS_CONTROL_LOOKRIGHT", "lookright"),
	MenuStack.MakeInputBinder("#MENU_SETTINGS_CONTROL_LOOKBACK", "lookback"),
	MenuStack.MakeInputBinder("#MENU_SETTINGS_CONTROL_MOUSELOOK", "freelook"),
	MenuStack.MakeInputBinder("#MENU_SETTINGS_CONTROL_SHOWMESSAGE", "hud_showLastMessage"),
	MenuStack.MakeInputBinder("#MENU_SETTINGS_CONTROL_MAPZOOM", "hud_mapZoom"),
}

local SettingsGameElements = {
	MenuStack.MakeChoiceCvarParam("#MENU_SETTINGS_DIFFICULTY", "g_difficulty", {
        { "1", "#MENU_SETTING_EASY" },
        { "2", "#MENU_SETTING_NORMAL" },
        { "3", "#MENU_SETTING_HARD" },
    }),
	MenuStack.MakeBoolCvarParam("#MENU_SETTINGS_STEERINGHELP", "g_autoHandbrake"),
}

----------------------------------------------------------------------------------------------------
-- ADDONS MENU
----------------------------------------------------------------------------------------------------

local function AddonsMenuElementsFunc()

	local modsElems = {}
	
	local modEnableDisable = {
		{ false, "#MENU_OFF" },
		{ true, "#MENU_ON" },
	}
	
	addonListKeys = table.sortedKeys(addons.list, function(a,b)
		return a.CreatedBy:upper() < b.CreatedBy:upper()
	end)

	if #addonListKeys == 0 then
		return {
			MenuStack.MakeMenuPop("#MENU_NO_ADDONS"),
		}
	end
	
	for _,k in ipairs(addonListKeys) do
	
		local v = addons.list[k]
	
		local modGetSet = {
			get = function()
				return v.Enabled
			end,
			set = function(value)
				if value == true then
					if addons:Enable(k) == false then
						return
					end
				else
					addons:Disable(k)
				end
				
				-- save state of mods
				addons:SaveEnabled()
			end
		}
		
		local str = "< %s >   " .. string.format("%s (%s) - %s", v.Title, v.Version, v.CreatedBy)
		local newElem = MenuStack.MakeChoiceParam(str, modGetSet, modEnableDisable)

		table.insert(modsElems, newElem)
	end
	
	return modsElems
end

local SettingsElements = {
	MenuStack.MakeSubMenu("#MENU_SETTINGS_GAME", SettingsGameElements),
	MenuStack.MakeSubMenu("#MENU_SETTINGS_CONTROLS", SettingsInputElements, nil, EQUI_SCHEME_NAME),
	MenuStack.MakeSubMenu("#MENU_SETTINGS_GRAPHICS", SettingsGraphicsElements),
	MenuStack.MakeSubMenu("#MENU_SETTINGS_SOUND", SettingsSoundElements),
	MenuStack.MakeSubMenu("#MENU_MAIN_ADDONS", AddonsMenuElementsFunc),
}

return MenuStack.MakeSubMenu("#MENU_MAIN_OPTIONS", SettingsElements)