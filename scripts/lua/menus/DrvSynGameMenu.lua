--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2017
--//////////////////////////////////////////////////////////////////////////////////

AddLanguageFile("gamemenu")

----------------------------------------------------------------------------------------------------
-- Game menu
----------------------------------------------------------------------------------------------------

local ExitGameMenu = {
	
	-- No
	MenuStack.MakeMenuPop("#MENU_NO"),
	
	-- Yes
	MenuStack.MakeCommand("#MENU_YES", "quitToMainMenu", true)
}

local RestartGameMenu = {

	-- No
	MenuStack.MakeMenuPop("#MENU_NO"),
	
	-- Yes
	MenuStack.MakeCommand("#MENU_YES", "restartGame", true),
}

local JoypadSettingsElements = {
	MenuStack.MakeFloatCvarParam("#MENU_SETTINGS_JOY_DEADZONE", "in_joy_deadzone", 0, 0.5, 0.01, 100 ),
	MenuStack.MakeChoiceCvarParam("#MENU_SETTINGS_JOY_STEER_SMOOTH", "in_joy_steer_smooth", {
        { "1", "#MENU_NO" },
		{ "0", "#MENU_YES" },
    }),
	MenuStack.MakeFloatCvarParam("#MENU_SETTINGS_JOY_STEER_LINEAR", "in_joy_steer_linear", 0.25, 4.0, 0.05, 1 ),
	MenuStack.MakeFloatCvarParam("#MENU_SETTINGS_JOY_ACCEL_LINEAR", "in_joy_accel_linear", 0.25, 4.0, 0.05, 1 ),
	MenuStack.MakeFloatCvarParam("#MENU_SETTINGS_JOY_BRAKE_LINEAR", "in_joy_brake_linear", 0.25, 4.0, 0.05, 1 ),
}

local SettingsGraphicsElements = {

	MenuStack.MakeChoiceCvarParam("#MENU_SETTINGS_REFLECTIONQUALITY", "r_drawReflections", {
        { "0", "#MENU_SETTING_OFF" },
        { "1", "#MENU_SETTING_LOW" },
        { "2", "#MENU_SETTING_HIGH" },
    }),

	-- Drawing distance
	MenuStack.MakeFloatCvarParam("#MENU_SETTINGS_DRAWDISTANCE", "r_zfar", 100, 350, 10 ),

	-- Lights rendering distance
	MenuStack.MakeFloatCvarParam("#MENU_SETTINGS_LIGHTDISTANCE", "r_lightDistance", 50, 150, 10),

	-- Night brightness
	MenuStack.MakeFloatCvarParam("#MENU_SETTINGS_NIGHTBRIGHTNESS", "r_nightBrightness", 1.0, 6.65, 0.05, 30),
    
	-- Enable shadows
	MenuStack.MakeBoolCvarParam("#MENU_SETTINGS_SHADOWS", "r_shadows"),
	
    MenuStack.MakeChoiceCvarParam("#MENU_SETTINGS_SKIDMARKS", "r_drawSkidMarks", {
        { "0", "#MENU_SETTING_OFF" },
        { "1", "#MENU_SETTING_LOW" },
        { "2", "#MENU_SETTING_HIGH" },
    }),
}

local SettingsElements = {
	-- Master sound volume
	MenuStack.MakeFloatCvarParam("#MENU_SETTINGS_MASTERVOLUME", "snd_mastervolume", 0, 1, 0.05, 100 ),
	
	-- Voice sound volume
	MenuStack.MakeFloatCvarParam("#MENU_SETTINGS_VOICEVOLUME", "snd_voicevolume", 0, 1, 0.05, 100 ),
	
	-- Music volume
	MenuStack.MakeFloatCvarParam("#MENU_SETTINGS_MUSICVOLUME", "snd_musicvolume", 0, 1, 0.05, 100 ),
	
	-- Show Head-up Display
	MenuStack.MakeBoolCvarParam("#MENU_SETTINGS_SHOWHUD", "hud_draw"),
	
	-- Camera velocity effects
	--MenuStack.MakeBoolCvarParam("#MENU_SETTINGS_CAMERAEFFECTS", "cam_velocityeffects"),

	-- Music volume
	MenuStack.MakeFloatCvarParam("#MENU_SETTINGS_CAMERAFOV", "cam_fov", 50, 80, 1, 1 ),
	
	-- Show FPS meter
	MenuStack.MakeBoolCvarParam("#MENU_SETTINGS_SHOWFPS", "r_showFPS"),
	
	-- Service lights brightness
	MenuStack.MakeFloatCvarParam("#MENU_SETTINGS_SLIGHT_BRIGHTNESS", "r_carServiceLightBrightness", 0, 1, 0.05, 100),
	
	-- Joypad settings
	MenuStack.MakeSubMenu("#MENU_SETTINGS_JOYPAD_SETTINGS", JoypadSettingsElements),
	
	-- Graphics settings
	MenuStack.MakeSubMenu("#MENU_SETTINGS_GRAPHICS", SettingsGraphicsElements),
}

local GameMenuElements = {
	MenuStack.MakeCommand("#MENU_GAME_CONTINUE", "continue"),

	MenuStack.MakeSubMenu("#MENU_GAME_RESTART", RestartGameMenu, "#MENU_GAME_QUEST_SURE"),
	MenuStack.MakeSubMenu("#MENU_GAME_SETTINGS", SettingsElements, "#MENU_GAME_SETTINGS"),
	
	MenuStack.MakeCommand("#MENU_GAME_QUICKREPLAY", "quickReplay", true),
	MenuStack.MakeCommand("#MENU_GAME_DIRECTOR", "goToDirector", true),

	MenuStack.MakeSubMenu("#MENU_GAME_EXIT", ExitGameMenu, "#MENU_GAME_QUEST_SURE"),
}

GameMenuStack = MenuStack( GameMenuElements, "#MENU_GAME_TITLE_PAUSE" )

----------------------------------------------------------------------------------------------------
-- Story mission success menu HERE
----------------------------------------------------------------------------------------------------

local MissionSuccessMenuElements = {

	MenuStack.MakeItem("#MENU_GAME_NEXTMISSION", true, {
		onEnter = function( self, stack )
			missionladder:RunContinue()
			return { command = "nextState" }
		end,
	}),
	MenuStack.MakeSubMenu("#MENU_GAME_RESTART", RestartGameMenu, "#MENU_GAME_QUEST_SURE"),
	MenuStack.MakeCommand("#MENU_GAME_QUICKREPLAY", "quickReplay", true),
	MenuStack.MakeCommand("#MENU_GAME_DIRECTOR", "goToDirector", true),
	MenuStack.MakeCommand("#MENU_GAME_EXIT", "quitToMainMenu", true)
}

MissionSuccessMenuStack = MenuStack( MissionSuccessMenuElements, "#MENU_GAME_TITLE_MISSION_SUCCESS" )

----------------------------------------------------------------------------------------------------
-- Mission failed menu
----------------------------------------------------------------------------------------------------

local MissionFailedMenuElements = {

	MenuStack.MakeCommand("#MENU_GAME_RESTART", "restartGame", true),
	MenuStack.MakeCommand("#MENU_GAME_QUICKREPLAY", "quickReplay", true),
	MenuStack.MakeCommand("#MENU_GAME_DIRECTOR", "goToDirector", true),
	MenuStack.MakeCommand("#MENU_GAME_EXIT", "quitToMainMenu", true)
}

MissionFailedMenuStack = MenuStack( MissionFailedMenuElements, "#MENU_GAME_TITLE_MISSION_FAILED" )

----------------------------------------------------------------------------------------------------
-- Mission end menu
----------------------------------------------------------------------------------------------------

local MissionEndMenuElements = {

	MenuStack.MakeSubMenu("#MENU_GAME_RESTART", RestartGameMenu, "#MENU_GAME_QUEST_SURE"),
	MenuStack.MakeCommand("#MENU_GAME_QUICKREPLAY", "quickReplay", true),
	MenuStack.MakeCommand("#MENU_GAME_DIRECTOR", "goToDirector", true),
	MenuStack.MakeCommand("#MENU_GAME_EXIT", "quitToMainMenu", true)
}

MissionEndMenuStack = MenuStack( MissionEndMenuElements, "#MENU_GAME_TITLE_END" )

----------------------------------------------------------------------------------------------------
-- Minigame success/failed menu
----------------------------------------------------------------------------------------------------

local MiniGameEndMenuElements = {

	MenuStack.MakeSubMenu("#MENU_GAME_RESTART", RestartGameMenu, "#MENU_GAME_QUEST_SURE"),
	MenuStack.MakeCommand("#MENU_GAME_SHOWSCORETABLE", "enterScoreTable", true),
	MenuStack.MakeCommand("#MENU_GAME_QUICKREPLAY", "quickReplay", true),
	MenuStack.MakeCommand("#MENU_GAME_DIRECTOR", "goToDirector", true),
	MenuStack.MakeSubMenu("#MENU_GAME_EXIT", ExitGameMenu, "#MENU_GAME_QUEST_SURE"),
}

MiniGameEndMenuStack = MenuStack( MiniGameEndMenuElements, "#MENU_GAME_TITLE_END" )

----------------------------------------------------------------------------------------------------
-- Replay end menu
----------------------------------------------------------------------------------------------------

local ReplayEndMenuElements = {
	MenuStack.MakeCommand("#MENU_GAME_RESTART", "quickReplay", true),
	MenuStack.MakeCommand("#MENU_GAME_DIRECTOR", "goToDirector", true),
	MenuStack.MakeCommand("#MENU_GAME_EXIT", "quitToMainMenu", true)
}

ReplayEndMenuStack = MenuStack( ReplayEndMenuElements, "#MENU_GAME_QUICKREPLAY" )

local DirectorMenuElements = {
	MenuStack.MakeCommand("#MENU_GAME_QUICKREPLAY", "quickReplay", true),
	MenuStack.MakeCommand("#MENU_GAME_EXIT", "quitToMainMenu", true)
}

DirectorMenuStack = MenuStack( DirectorMenuElements, "#MENU_GAME_DIRECTOR" )