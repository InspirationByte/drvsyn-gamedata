--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2015
--//////////////////////////////////////////////////////////////////////////////////

--[[

Building menus:

'element'

{

	label = [string],					--- The text or #ID
	isFinal = true,						--- indicates if needs to exit from state

	labelValue = [function] : (string)	--- returns formatted string for the label value (%s)
	onChange = [function](dir) : void	--- when left/right is pressed

	onEnter = [function, table]			--- When enters this menu. Returns table of new 'elements'. Return nil if this a epilog function
	
		Example:
		
			onEnter = function( self, stack )
				return {nextMenu=SettingsElements, titleToken="MENU_GAME_SETTINGS"}
			end,
			
		or 
			
			onEnter = function( self, stack )
				return {command="continue"}
			end,
	
	isEnabled = [function]				--- When not 'nil' function returns state of this element
	
	-- other extra properties
}

]]

MenuFiles = {
	
	"DrvSynMainMenu.lua",
	"DrvSynGameMenu.lua",
	"DrvSynMPGameMenu.lua",
}

-- menu stack class
MenuStack = class()

function MenuStack:__init( menu, titleToken, exitFunc )
	self.initialMenu = menu
	self.titleToken = titleToken or ""
	self.scheme = nil
	self.exitFunc = exitFunc
end

function MenuStack:Reset()
	self.stack = nil
	GC_CRITICAL()
end

function MenuStack:Update( delta )
	if self.stack.updateFunc ~= nil then
		self.stack.updateFunc( delta )
	end
end

function MenuStack:Push( prevSelection, params )	

	if params.nextMenu == nil then
		-- only set prevSelection
		-- but don't change stack
		self.stack.prevSelection = prevSelection
		return
	end

	local newStack = {
		params = params, -- needed for InitItems

		-- get all needed fields from "params" returned which are returned by "onEnter"
		title = params.titleToken or self.titleToken,
		schemeName = params.schemeName,
	}

	if self.stack ~= nil then
		local prevStack = self.stack
		
		prevStack.prevSelection = prevSelection
		
		newStack.prevStack = prevStack 
		newStack.prevSelection = 0
	end

	self.stack = newStack
end

function MenuStack:Pop()
	if self.stack.prevStack ~= nil then
		self.stack = self.stack.prevStack
	elseif self.exitFunc ~= nil then
		self.exitFunc()
	end
end

function MenuStack:InitItems()
	local params = self.stack.params
	
	-- tnit items
	local nextMenu = params.nextMenu
	if nextMenu ~= nil then
		if type(nextMenu) == 'function' then
			nextMenu = nextMenu(self.scheme, self.stack)
		end
		
		self.stack.items = nextMenu
		--self.stack.updateFunc = params.updateFunc
	end
end

function MenuStack:UpdateUIScheme( equicontrol )
	self.scheme = equicontrol
end
	
-- static funcs

function MenuStack.SetItemOptions(item, options)
	if item and options then
		if type(options) == "table" then
			-- apply options to item
			for k,v in pairs(options) do
				item[k] = v
			end
		else
			error("MenuStack.SetItemOptions: expected a table for options argument!")
		end
	end
end

function MenuStack.MakeItem(token, final, options)
	local label = token
	
	local item = {
		label = label or "",
		labelValue = function(self)
			return ""
		end,
		isFinal = final or false,
	}
	
	MenuStack.SetItemOptions(item, options)
	return item
end

function MenuStack.FindChoiceIndex(choices, val)
	for k,v in ipairs(choices) do
		if v[1] == val then
			return k
		end
	end
	
	return -1
end

function MenuStack.MakeCvarItem(token, cvar, typeName, options)
	if type(cvar) == "string" then
		local _cvar = console.FindCvar(cvar)
		if _cvar == nil then
			MsgError("MenuStack.MakeCvarItem: couldn't find cvar '".. cvar .."'!".."\n")
		end
		cvar = _cvar
	end
	
	local cvarItem = {
		get = function()
			return cvar:GetString()
		end,
		set = function(value)
			cvar:SetString(value)
		end
	}
	
	if typeName == "number" then
		cvarItem = {
			get = function()
				return cvar:GetFloat()
			end,
			set = function(value)
				cvar:SetFloat(value)
			end
		}
	elseif typeName == "boolean" then
		cvarItem = {
			get = function()
				return cvar:GetBool()
			end,
			set = function(value)
				cvar:SetBool(value)
			end
		}
	end
	
	-- TODO:
	--cvar:GetClampMin()
	--cvar:GetClampMax()
	
	return MenuStack.MakeVariableItem(token, cvarItem, typeName, options)

end

function MenuStack.MakeVariableItem(token, variable, typeName, options)

	local item = MenuStack.MakeItem(token, false, options)
	
	local defaults = {
		labelValue = function(self)
			return (variable and variable.get()) or ""
		end,
	}

	local behaviors = {
		["default"] = {
			labelValue = defaults.labelValue,
			onChange = defaults.onChange,
			onEnter = defaults.onEnter,
		},
		["boolean"] = {
			labelValue = function(self)
				local val = variable.get()
				
				local token = val and (
					self.onToken or LOCTOK("#MENU_ON")
				) or (
					self.offToken or LOCTOK("#MENU_OFF")
				)
				
				return token
			end,
			onChange = function(self, dir)
				dir = clamp(dir, -1, 1)
			
				local curVal = variable.get()
				local newVal = (dir > 0)
				
				if curVal ~= newVal then
					variable.set(newVal, dir)
				end
			end,
			onEnter = function(self, stack)
				self:onChange(1)
				return {}
			end,
		},
		["number"] = {
			labelValue = function(self)
				local val = variable.get()
				
				return val * (self.multiplier or 1.0)
			end,
			onChange = function(self, dir)
				dir = clamp(dir, -1, 1)
			
				local minVal = self.clampMin
				local maxVal = self.clampMax
				
				local curVal = variable:get()
				local newVal = clamp(curVal + (dir * (self.step or 1.0)), minVal, maxVal)
				
				if curVal ~= newVal then
					variable.set(newVal, dir)
				end
			end,
			onEnter = defaults.onEnter,
		},
		-- choice list
		["table"] = {
			labelValue = function(self)
			
				local choices = self.choices
				if type(choices) == 'function' then
					choices = choices()
				end
			
				local val = variable.get()
				local index = MenuStack.FindChoiceIndex(choices, val)
				
				local choice = if_then_else(index > 0, function() return choices[index] end, {-1, "Invalid value"})
				
				return LOCTOK(choice[2])
			end,
			onChange = function(self, dir)
				local choices = self.choices
				if type(choices) == 'function' then
					choices = choices()
				end
			
				local count = #choices
				local newIndex
				local curIndex = MenuStack.FindChoiceIndex(choices, variable.get()) - 1

				if dir == 666 then
					dir = clamp(dir, -1, 1)
					newIndex = curIndex + dir
					
					newIndex = math.fmod(newIndex, count)
					
					if newIndex < 0 then
						newIndex = count-1;
					end
				else
					dir = clamp(dir, -1, 1)
					newIndex = clamp(curIndex + dir, 0, count-1)
				end

				local newChoice = if_then_else(newIndex >= 0, function() return choices[newIndex + 1] end, {-1, "Invalid value"})

				if curIndex ~= newIndex then
					variable.set(newChoice[1], dir)
				end
			end,
			onEnter = defaults.onEnter,
		},
	}
	
	local behavior = behaviors[typeName]
	MenuStack.SetItemOptions(item, behavior)

	return item
end

function MenuStack.MakeSubMenu(token, nextElems, titleToken, schemeName, updateFunc)
	return MenuStack.MakeItem(token, false, {
		onEnter = function(self, stack)
			return { 
				nextMenu = nextElems, 
				titleToken = titleToken or token,
				schemeName = schemeName,
				updateFunc = updateFunc
			}
		end,
	})
end

function MenuStack.MakeMenuPop(token)
	return MenuStack.MakeItem(token, false, {
		onEnter = function(self, stack)
			return { menuCommand = "Pop" }
		end,
	})
end

function MenuStack.MakeCommand(token, command, final)
	return MenuStack.MakeItem(token, final, {
		onEnter = function(self, stack)
			return { command = command }
		end,
	})
end

function MenuStack.MakeFunction(token, func, final)
	return MenuStack.MakeItem(token, final, {
		onEnter = func
	})
end

function MenuStack.MakeInputBinder(token, action)
	return MenuStack.MakeItem(token, false, {
		inputActionName = action,
		
		onEnter = function(self, stack)
			return {command="bindAction"}
		end,
		onDelete = function(self, stack)
			return {command="unbindAction"}
		end,
	})
end

function MenuStack.MakeTextInput(token, action)
	return MenuStack.MakeItem(token, false, {
		textInput = true,
		
		onEnter = function(self, stack)
			return {command="bindAction"}
		end,
	})
end

function MenuStack.MakeSpacer()
	return MenuStack.MakeItem(nil, false, {_spacer = true})
end

-- makes float param into the menu
function MenuStack.MakeFloatCvarParam(token, cvar, clampMin, clampMax, step, multiplier)
	-- use 'clampMin' as default value
	return MenuStack.MakeCvarItem(token, cvar, "number", {
		clampMin = clampMin,
		clampMax = clampMax,
		step = step,
		multiplier = multiplier,
	})
end

function MenuStack.MakeBoolCvarParam(token, cvar, onToken, offToken)
	if onToken and type(onToken) == "string" then
		onToken = LOCTOK(onToken)
	end
	if offToken and type(offToken) == "string" then
		offToken = LOCTOK(offToken)
	end
	
	return MenuStack.MakeCvarItem(token, cvar, "boolean", {
		onToken = onToken,
		offToken = offToken,
	})
end

function MenuStack.MakeChoiceCvarParam(token, cvar, choices)
	if choices and #choices > 0 then
		return MenuStack.MakeCvarItem(token, cvar, "table", {
			choices = choices,
		})
	else
		error("MenuStack.MakeChoiceCvarParam: invalid choices!")
		return MenuStack.MakeItem(token, false, cvar)
	end
end

function MenuStack.MakeChoiceParam(token, variable, choices)

	if type(choices) == 'function' then
		return MenuStack.MakeVariableItem(token, variable, "table", {
			choices = choices,
		})
	elseif choices and #choices > 0 then
		return MenuStack.MakeVariableItem(token, variable, "table", {
			choices = choices,
		})
	end
	
	error("MenuStack.MakeChoiceParam: invalid choices!")
	return MenuStack.MakeItem(token, false, cvar)
end

loadTableOfFiles(MenuFiles, function()
	-- Registered game menus
	GameModeMenus = {
		["default"] = {
			Replay = ReplayEndMenuStack,
			Director = DirectorMenuStack,
			Ingame = GameMenuStack,
			MissionSuccess = MissionEndMenuStack,
			MissionFailed = MissionEndMenuStack,
		},
		["story"] = {
			Replay = ReplayEndMenuStack,
			Director = DirectorMenuStack,
			Ingame = GameMenuStack,
			MissionSuccess = MissionSuccessMenuStack,
			MissionFailed = MissionFailedMenuStack,
		},
		["minigame"] = {
			Replay = ReplayEndMenuStack,
			Director = DirectorMenuStack,
			Ingame = GameMenuStack,
			MissionSuccess = MiniGameEndMenuStack,
			MissionFailed = MissionEndMenuStack,
		},
		["multiplayer"] = {
			Replay = ReplayEndMenuStack,
			Director = DirectorMenuStack,
			Ingame = MPGameMenuStack,
			MissionSuccess = MissionEndMenuStack,
			MissionFailed = MissionEndMenuStack,
		}
	}

	-- set default table of game menu stacks
	CurrentGameMenuTable = GameModeMenus["default"]
end)
