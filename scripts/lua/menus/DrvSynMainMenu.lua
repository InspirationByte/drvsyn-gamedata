--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2015
--//////////////////////////////////////////////////////////////////////////////////

AddLanguageFile("mainmenu")

----------------------------------------------------------------------------------------------------
-- Main menu
----------------------------------------------------------------------------------------------------

StoryGameExtraElems = {}

local StoryGameElems = function() 
	
	local elems = {
		{
			label = "#MENU_SYNDICATE_NEWGAME",
			isFinal = true,
			onEnter = function(self, stack)
			
				-- reset and run ladder
				missionladder:Run( "story_demo", missions["story_demo"] )
		
				return {}
			end,
		}
	}
	
	for i,v in ipairs(StoryGameExtraElems) do
		table.insert(elems, i, v)
	end
	
	if missionladder:HasNextMission() then
	
		table.insert(elems, {
			label = "#MENU_SYNDICATE_CONTINUE",
			isFinal = true,
			onEnter = function(self, stack)
				
				-- continue mission
				missionladder:RunContinue()
				
				return {}
			end,
		})
	
	end

	return elems
	
end

local ExitGameMenu = {
	
	-- No
	MenuStack.MakeMenuPop("#MENU_NO"),
	
	-- Yes
	{
		label = "#MENU_YES",
		isFinal = true,
		onEnter = function( self, stack )
			return {}
		end,
	},
}

local function ReplaysMenuElementsFunc()

	local replays = {}

	local replayPath = "demoreplays/";
	local replaysFind = CFileSystemFind.new(replayPath .. "*.rdat", SP_MOD)
	
	-- search for demo replays
	local demoId = 1
	while replaysFind:Next() do
		local name = getFileNameOnly(replaysFind:GetPath())
		local path = replayPath .. name
		local elem = {
			label = "Demo " .. demoId,
			isFinal = true,
			onEnter = function( self, stack )
				if game.StartReplay(path, REPLAYMODE_DEMO) then
					EqStateMgr.ScheduleNextStateType( GAME_STATE_GAME )
				else
					return {menuCommand="Pop"}
				end
			end
		}

		table.insert(replays, elem)
		demoId = demoId + 1
	end
	
	-- now search user replays
	replayPath = "UserReplays/";
	replaysFind = CFileSystemFind.new(replayPath .. "*.rdat", SP_MOD)
	while replaysFind:Next() do
		local name = getFileNameOnly(replaysFind:GetPath())
		local path = replayPath .. name
		local elem = {
			label = name,
			isFinal = true,
			onEnter = function( self, stack )
				if game.StartReplay(path, REPLAYMODE_STORED_REPLAY) then
					EqStateMgr.ScheduleNextStateType( GAME_STATE_GAME )
				else
					return {menuCommand="Pop"}
				end
			end
		}

		table.insert(replays, elem)
	end
	
	return replays

end

local MainMenuElements = {
	MenuStack.MakeSubMenu("#MENU_MAIN_SYNDYCATE", StoryGameElems),
	--MenuStack.MakeSpacer(),
	include("DrvSynMainMenu_Freeride.lua"), -- include entire file
	include("DrvSynMainMenu_Minigames.lua"),

	--MenuStack.MakeSpacer(),
	include("DrvSynMainMenu_Settings.lua"),
	MenuStack.MakeSubMenu("#MENU_MAIN_REPLAYS", ReplaysMenuElementsFunc),
	
	MenuStack.MakeSpacer(),

	MenuStack.MakeSubMenu("#MENU_MAIN_EXITTOSYSTEM", ExitGameMenu),
}

local cvar = console.FindCvar("net_name")

local function onExitFromMainMenu()
	EqStateMgr.ScheduleNextStateType( GAME_STATE_TITLESCREEN )
end

-- Use this main menu stack
MainMenuStack = MenuStack( MainMenuElements, nil, onExitFromMainMenu )