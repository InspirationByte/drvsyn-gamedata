--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2020
--//////////////////////////////////////////////////////////////////////////////////

local EQUI_SCHEME_NAME = "ui_mainmenu_takearide"

-- Additional menu items. This allows you to add custom elements
TakeARideExtraElements = {}

-- HACK: to pass value by reference use the table
local currentCityName = "default"
local currentCityNameGetSet = { 
	get = function()
		return currentCityName
	end,
	set = function(value)
		currentCityName = value
	end
}

-- HACK: to pass value by reference use the table
local timeOfDayName = "day_clear"
local timeOfDayNameGetSet = { 
	get = function()
		return timeOfDayName
	end,
	set = function(value)
		timeOfDayName = value
	end
}

local function TakeARideElementsFunc(equiScheme, stack)

	-- reset city type if mods were disabled
	if MenuStack.FindChoiceIndex(MenuCityList, currentCityName) == -1 then
		currentCityName = MenuCityList[1][1]
	end
	
	local g_Car = console.FindCvar("g_car")
	
	-- reset car type if mods were disabled
	if MenuStack.FindChoiceIndex(MenuCarsList, g_Car:GetString()) == -1 then
		g_Car:SetString(MenuCarsList[1][1])
	end
	
	-- init elements
	local selElem = {}
	local prevElem = {}
	local nextElem = {}

	selElem.ui = equi:Cast(equiScheme:FindChild("carselection_img"), "image")
	selElem.ui:SetMaterial("ui/vehicles/"..g_Car:GetString())

	prevElem.ui = equi:Cast(equiScheme:FindChild("carselection_prev"), "image")
	prevElem.ui:SetMaterial("ui/vehicles/"..g_Car:GetString())

	nextElem.ui = equi:Cast(equiScheme:FindChild("carselection_next"), "image")
	nextElem.ui:SetMaterial("ui/vehicles/"..g_Car:GetString())

	
	-- make update function to show and hide prev/next items
	local updateItems = function()
		local value = g_Car:GetString()
		local choiceIdx = MenuStack.FindChoiceIndex(MenuCarsList, value)
		
		local imageName = "ui/vehicles/"..value
		
		-- check for image existence
		if not fileSystem:FileExist("materials/"..imageName..".mat", SP_MOD) then
			imageName = "ui/vehicles/unknown_vehicle"
		end
		
		selElem.ui:SetMaterial(imageName)
		
		if choiceIdx-1 > 0 then
			value = MenuCarsList[choiceIdx-1][1]
			imageName = "ui/vehicles/"..value
		
			-- check for image existence
			if not fileSystem:FileExist("materials/"..imageName..".mat", SP_MOD) then
				imageName = "ui/vehicles/unknown_vehicle"
			end
		
			prevElem.ui:SetMaterial(imageName)
			prevElem.ui:Show()
		else
			prevElem.ui:Hide()
		end
		
		if choiceIdx+1 <= #MenuCarsList then
			value = MenuCarsList[choiceIdx+1][1]
			
			imageName = "ui/vehicles/"..value
		
			-- check for image existence
			if not fileSystem:FileExist("materials/"..imageName..".mat", SP_MOD) then
				imageName = "ui/vehicles/unknown_vehicle"
			end
			
			nextElem.ui:SetMaterial(imageName)
			nextElem.ui:Show()
		else
			nextElem.ui:Hide()
		end		
	end
	
	updateItems() -- first time update
	
	local movetime = 0
	local movedir = 1

	-- make get/set for both CVar and element
	local currentCarNameGetSet = { 
		get = function()
			return g_Car:GetString()
		end,
		set = function(value, dir)
			movetime = 0.25
			movedir = dir
			
			g_Car:SetString(value)
			
			updateItems()
		end
	}
	
	-- this updates UI every frame
	stack.updateFunc = function(delta)

		local moveVal = movetime*movetime * 1.0
		local scale = 1.0 - moveVal*4
		selElem.ui:SetTransform(vec2(movetime * movedir * 500, 0), vec2(scale,scale), movedir * movetime * 25.0)
		
		local unselScale = 0.75+movetime
	
		if movedir < 0 then
			nextElem.ui:SetTransform(vec2(movetime * -80, movetime * 80), vec2(unselScale,unselScale), 0)
			prevElem.ui:SetTransform(vec2(0, 0), vec2(0.75,0.75), 0)
		else
			prevElem.ui:SetTransform(vec2(movetime * 80, movetime * 80), vec2(unselScale,unselScale), 0)
			nextElem.ui:SetTransform(vec2(0, 0), vec2(0.75,0.75), 0)
		end
		
		if movetime > 0 then
			movetime = movetime-delta
		end
	end

	-- make menu elements
	-- car selection, city selection and time of day
	local elems = {
		MenuStack.MakeChoiceParam("#MENU_TAKEARIDE_CAR", currentCarNameGetSet, MenuCarsList),
		MenuStack.MakeChoiceParam("#MENU_TAKEARIDE_TIMEOFDAY", timeOfDayNameGetSet, MenuTimeOfDayList),
		MenuStack.MakeSpacer(),
		{
			label = "#MENU_TAKEARIDE_START",
			isFinal = true,
			onEnter = function(self, stack)
	
				if LoadFreeride(currentCityName, timeOfDayName) then
					EqStateMgr.ScheduleNextStateType(GAME_STATE_GAME)
				else
					return {menuCommand = "Pop"}
				end
				
				return {}
			end,
		},
	}
	
	if #MenuCityList > 1 then
		local cityListElem = MenuStack.MakeChoiceParam("#MENU_TAKEARIDE_CITY", currentCityNameGetSet, MenuCityList)
		table.insert(elems, 1, cityListElem)
	end

	for i,v in ipairs(TakeARideExtraElements) do
		table.insert(elems, i, v)
	end

	return elems
end

-- return entire menu item
return MenuStack.MakeSubMenu("#MENU_GAME_TAKEARIDE", TakeARideElementsFunc, nil, EQUI_SCHEME_NAME)