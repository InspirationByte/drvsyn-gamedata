--//////////////////////////////////////////////////////////////////////////////////
--// Copyright © Inspiration Byte
--// 2009-2021
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 28 Jan 2021
--------------------------------------------------------------------------------

local cam_fov = console.FindCvar("cam_fov")

-----------------------------------------------------------------------------
-- Game car camera
-----------------------------------------------------------------------------

function ConfigureCarCamera(car, cameraConfig)
	--[[
	local carSpd = car:GetSpeed()
	local carSpdFOVFactor = (carSpd * 0.35 - 10)

	if carSpdFOVFactor > 2 then
		carSpdFOVFactor = 2
	end
	
	carSpdFOVFactor = carSpdFOVFactor + carSpd * 0.1
	cameraConfig:set_fov(cameraConfig:get_fov() + carSpdFOVFactor)
	]]
end

-----------------------------------------------------------------------------
-- Cutscene camera
-----------------------------------------------------------------------------

HeadsUpCam = {}

function HeadsUpCam.IsEnabled()
	return HeadsUpCam.Data.enable
end

function HeadsUpCam.InTransition()
	local data = HeadsUpCam.Data
	if data.inHeadsUpCam ~= data.enable then
		return true
	end

	return false
end

local function HeadsUpCam_Return()
	local data = HeadsUpCam.Data
	
	data.transition = 0
	cameraAnimator:SetScripted(false)
	cameraAnimator:SetMode(data.prevCamMode)
	gameses:SetTimescale(1)
	gameHUD:SetMotionBlur(0)
	gameHUD:Enable(true)
	MissionManager:SetPluginRefreshFunc("headsUpCam", nil)
end

-- updates headsup
local function HeadsUpCam_Update(dt)
	local data = HeadsUpCam.Data
	-- use host frametime since we timescale the game time
	dt = host.GetFrameTime()
	
	if data.transition > 0 then
		data.transition = data.transition - dt
		
		if data.transition < 0 then
		
			if data.inHeadsUpCam then
				HeadsUpCam_Return()
			end
			
			data.inHeadsUpCam = data.enable
		end
	end
	
	local targetTimescale = 1
	
	if data.enable then		
		local targetCameraPos = data.playerPos + vec3_up * vec3(2)
		local targetCameraAngles = quat(0, DEG2RAD(data.playerAngle), 0) * quat(DEG2RAD(8), 0, 0)
		targetCameraAngles:normalize()
		
		data.cameraPos = lerp(data.cameraPos, targetCameraPos, dt * 5.0)
		
		if qdot(targetCameraAngles, data.cameraRot) < 0.01 then
			data.cameraRot = qslerp(targetCameraAngles, data.cameraRot, dt * 5.0)
		else
			data.cameraRot = qslerp(data.cameraRot, targetCameraAngles, dt * 5.0)
		end
	
		data.cameraRot:normalize()
			
		local angles = qeulersSel(data.cameraRot, QuatRot_zxy)
		data.cameraAngles = NormalizeAngles180(VRAD2DEG(vec3(angles[2], angles[1], angles[3]))) -- Eq cameras use ZXY
		
		--cameraAnimator:SetMode( CAM_MODE_TRIPOD )
		cameraAnimator:SetOrigin(data.cameraPos)
		cameraAnimator:SetAngles(data.cameraAngles)
		
		
		cameraAnimator:Update(dt, gameses:GetPlayerCar())
		cameraAnimator:SetFOV(data.cameraFOV * math.max(0.5, data.timescale))
				
		targetTimescale = data.targetTimescale
	elseif data.transition > 0 then
	
		-- a trick to calculate the chase/incar cameras
		cameraAnimator:SetOrigin(vec3_zero)
		cameraAnimator:SetAngles(vec3_zero)
		cameraAnimator:SetMode(data.prevCamMode)
		
		for i = 1, 10 do
			cameraAnimator:Update(dt, gameses:GetPlayerCar())
		end
		
		if data.transition > 0.25 then
			
			local ang = VDEG2RAD(cameraAnimator:GetComputedView():GetAngles())
			local targetCameraPos = cameraAnimator:GetComputedView():GetOrigin()
			local targetCameraAngles = quat(ang:x(), ang:y(), ang:z())
			targetCameraAngles:normalize()
			
			data.cameraPos = lerp(data.cameraPos, targetCameraPos, dt * 10.0)
			
			if qdot(targetCameraAngles, data.cameraRot) < 0 then
				data.cameraRot = qslerp(targetCameraAngles, data.cameraRot, dt * 5.0)
			else
				data.cameraRot = qslerp(data.cameraRot, targetCameraAngles, dt * 5.0)
			end
		
			data.cameraRot:normalize()
			
			local angles = qeulersSel(data.cameraRot, QuatRot_zxy)
			data.cameraAngles = NormalizeAngles180(VRAD2DEG(vec3(angles[2], angles[1], angles[3]))) -- Eq cameras use ZXY
			
			cameraAnimator:SetMode( CAM_MODE_TRIPOD )

			cameraAnimator:SetOrigin(data.cameraPos)
			cameraAnimator:SetAngles(data.cameraAngles)
			
			cameraAnimator:Update(dt, gameses:GetPlayerCar())
		end
		
		cameraAnimator:SetFOV(data.cameraFOV * math.max(0.5, data.timescale))
	end
	
	if data.transition > 0 then
		data.timescale = lerp(data.timescale, targetTimescale, dt * 8.0)
		gameses:SetTimescale(math.max(0.25, data.timescale))
		gameHUD:SetMotionBlur(1.0 - data.timescale)
	end
end

-- enables headsup camera
function HeadsUpCam.Enable(enable, timescale)

	if enable and cameraAnimator:IsScripted() then
		return
	end

	local data = HeadsUpCam.Data
	data.enable = enable
	
	if data.enable then
		data.transition = 0.2
		MissionManager:SetPluginRefreshFunc("headsUpCam", HeadsUpCam_Update)
	else
		data.transition = 0.5
	end

	if data.enable then
	
		local camAngles = VDEG2RAD(world:GetView():GetAngles())
	
		-- initial camera position
		data.cameraPos = vecCopy(world:GetView():GetOrigin())
		data.cameraAngles = vecCopy(world:GetView():GetAngles())
		data.cameraRot = quat(camAngles:x(), camAngles:y(), camAngles:z())
		data.cameraRot:normalize()
		data.cameraFOV = cameraAnimator:GetFOV()
		data.playerPos = data.cameraPos
		data.playerAngle = VectorAngles(gameses:GetPlayerCar():GetForwardVector()):y()
		data.prevCamMode = cameraAnimator:GetMode()
		data.targetTimescale = timescale or 0.25
		
		gameHUD:Enable(false)
		cameraAnimator:SetScripted(true)
		cameraAnimator:SetMode(CAM_MODE_TRIPOD)
	end
end

-----------------------------------------------------------------------------
-- Cutscene camera
-----------------------------------------------------------------------------

local function defaultSmoothFn(x) 
	return x
end 

--------------------------
-- Cutscene camera unit
--------------------------
CutCamera = class()

	function CutCamera:__init(position, angles, mode, fov, targetObject, duration, smoothFunc, interp)
		self.mode = mode
		self.fov = fov or cam_fov:GetFloat()
		self.position = position
		self.angles = angles
		self.duration = duration
		self.targetObject = targetObject
		self.smoothFunc = smoothFunc or defaultSmoothFn
		self.interp = interp
	end
	
	function CutCamera:GetFOV(factor)
		if type(self.fov) == "table" then
			return lerp(self.fov[1], self.fov[2], factor)
		else
			return self.fov
		end
	end
	
	function CutCamera:GetPosition(factor)
		if type(self.position) == "table" then
			local count = #self.position - 1
			local idx, fac = math.modf(factor * count)
			idx = idx + 1
			
			if idx > count then
				idx = count
				fac = 1
			end
			return lerp(self.position[idx], self.position[idx+1], self.smoothFunc(fac))
		else
			return self.position
		end
	end
	
	function CutCamera:GetAngles(factor)
		if type(self.angles) == "table" then
		--
			local count = #self.angles - 1
			local idx, fac = math.modf(factor * count)
			idx = idx + 1
			
			if idx > count then
				idx = count
				fac = 1
			end
			
			--[[
			local a = VDEG2RAD(self.angles[idx])
			local b = VDEG2RAD(self.angles[idx + 1])
			local qa = quat(a:get_x(), a:get_y(), a:get_z())
			local qb = quat(b:get_x(), b:get_y(), b:get_z())
			
			local res
			
			if qdot(qa, qb) > 0 then
				res = qslerp(qa, qb, self.smoothFunc(fac))
			else
				res = qslerp(qb, qa, self.smoothFunc(fac))
			end

			local angles = qeulersSel(res, QuatRot_zxy)
			
			return NormalizeAngles180(VRAD2DEG(vec3(angles[3], angles[1], angles[2]))) -- Eq cameras use ZXY
			]]
			
			return lerp(self.angles[idx], self.angles[idx+1], self.smoothFunc(fac))
		else
			return self.angles
		end
	end
	
------------------------------------------------------------------------------
-- makes a fly-by camera from {position, angles, [fov]} array
------------------------------------------------------------------------------
function CutFlyByCamera( posAngArray, target, duration, smoothFunc, interp )
	local cutCameraPos = {}
	local cutCameraAng = {}
	local cutCameraFov = {}
	for k,v in ipairs(posAngArray) do
		table.insert(cutCameraPos, v[1])
		table.insert(cutCameraAng, v[2])
		
		if v[3] ~= nil then
			table.insert(cutCameraFov, v[3])
		end
	end
	
	local fovArray = if_then_else(#cutCameraFov > 0, cutCameraFov, nil)
	
	local camera = CutCamera(cutCameraPos, cutCameraAng, CAM_MODE_TRIPOD, fovArray, target, duration, smoothFunc, interp)
	
	return camera
end

------------------------------------------------------------------------------
------------------------------------------------------------------------------

CutsceneCamera = {
}


-- internal update function
local function CutsceneCamera_Update(delta)
	local data = CutsceneCamera.Data

	data.CameraTime = data.CameraTime + delta
	data.CutsceneTime = data.CutsceneTime + delta

	local camera = data.Cameras[data.CameraIndex]
	local nextCamera = data.Cameras[data.CameraIndex+1]
	
	local factor = data.CameraTime / camera.duration

	local position = camera:GetPosition(factor)
	local angles = camera:GetAngles(factor)
	local fov = camera:GetFOV(factor)
	
	if camera.interp ~= nil then
		-- interpolate
		if data.Interp.position ~= nil then
			data.Interp.position = lerp(data.Interp.position, position, delta * camera.interp)
			position = data.Interp.position
		else
			-- setup defaults
			data.Interp.position = position
		end
		
		if data.Interp.angles ~= nil then
			data.Interp.angles = lerp(data.Interp.angles, angles, delta * camera.interp)
			angles = data.Interp.angles
		else
			data.Interp.angles = angles
		end
		
		if data.Interp.fov ~= nil then
			data.Interp.fov = lerp(data.Interp.fov, fov, delta * camera.interp)
			fov = data.Interp.fov
		else
			data.Interp.fov = fov
		end
	else
		data.Interp = {} -- reset interpolation
	end

	cameraAnimator:SetMode( camera.mode )
	cameraAnimator:SetOrigin( position )
	cameraAnimator:SetAngles( angles )
	cameraAnimator:SetFOV( fov )
	
	cameraAnimator:Update(delta, camera.targetObject)
	
	-- iterate cameras
	if data.CameraTime > camera.duration then
		if nextCamera ~= nil then
			data.Interp = {}
			data.CameraIndex = data.CameraIndex + 1
			data.CameraTime = data.CameraTime - camera.duration
		elseif data.CameraTime > camera.duration + CutsceneCamera.extraWait then
			CutsceneCamera.End() -- auto complete
		end
	end
end


-- starts the camera sequence playback
-- once completed, it calls onCompleted after extraWait
function CutsceneCamera.Start(cameras, onCompleted, extraWait)

	if CutsceneCamera.isPlaying then
		CutsceneCamera.End()
	end

	CutsceneCamera.origMode = cameraAnimator:GetMode()
	CutsceneCamera.onCompleted = onCompleted
	CutsceneCamera.extraWait = extraWait or 0
	CutsceneCamera.isPlaying = true
	
	CutsceneCamera.Data = {
		CameraSet = 1,
		CameraIndex = 1,
		CameraTime = 0.0,
		CutsceneTime = 0.0,
		Cameras = cameras,
		Interp = {}
	}
	
	-- register update
	MissionManager:SetPluginRefreshFunc("CutsceneCamera", CutsceneCamera_Update)
	cameraAnimator:SetScripted(true)
end

-- stops cutscene camera playback and restores states
function CutsceneCamera.End()
	
	if not CutsceneCamera.isPlaying then
		return
	end
	
	cameraAnimator:SetMode(CutsceneCamera.origMode)
	cameraAnimator:SetScripted(false)
	
	-- unregister update
	MissionManager:SetPluginRefreshFunc("CutsceneCamera", nil)

	local onCompleted = CutsceneCamera.onCompleted

	CutsceneCamera.onCompleted = nil
	CutsceneCamera.isPlaying = false

	if onCompleted ~= nil then
		onCompleted()
	end
end

-- register reset
SetLevelLoadCallbacks("CinematicCamera", function()
	HeadsUpCam.Data = {
		enable = false,
		inHeadsUpCam = false,
		transition = 0,
		timescale = 1,
		playerPos = vec3(0),
		playerAngle = 0,
		
		prevCamMode = CAM_MODE_TRIPOD,
		cameraPos = vec3(0),
		cameraAngles = vec3(0),
		cameraRot = quat(0,0,0),
		cameraFOV = 0
	}

	CutsceneCamera.origMode = CAM_MODE_OUTCAR
	CutsceneCamera.onCompleted = nil
	CutsceneCamera.extraWait = 0
	CutsceneCamera.isPlaying = false
end)