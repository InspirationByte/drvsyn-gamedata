﻿--//////////////////////////////////////////////////////////////////////////////////
--// Copyright © Inspiration Byte
--// 2009-2020
--//////////////////////////////////////////////////////////////////////////////////

-------------------------------------------------------------
--	Achievement system
-------------------------------------------------------------

AllAchievements = {
	interview_under30 = -- key is an ID
	{
		label = "#ACHIEVEMENT_INTERVIEW_UNDER30",
		desc = "#ACHIEVEMENT_INTERVIEW_UNDER30_DESC",
		amount = 1
	},
}

AchievementManager = class()

	function AchievementManager:__init()

	end

	function AchievementManager:Reset()

	end

	function AchievementManager:IsCompleted( achievement_id )
		return false
	end

	function AchievementManager:Event( achievement_id )
		-- if count >= amount then OnAchievementCompleted
	end

achievements = AchievementManager()

