--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2015
--//////////////////////////////////////////////////////////////////////////////////

AddLanguageFile("mp_lobby")

MULTIPLAYER_LOBBY_MSG = 10

CMPLobbyNetworkEvent = class()
	function CMPLobbyNetworkEvent:__init()
		
	end
	
	function CMPLobbyNetworkEvent:Pack( msgBuffer, netthread )
		
	end
	
	function CMPLobbyNetworkEvent:Unpack( msgBuffer, netthread )
		
	end
	
	function CMPLobbyNetworkEvent:Process( netthread )
		
	end


RegisterNetEvent( MULTIPLAYER_LOBBY_MSG, CMPLobbyNetworkEvent )

--------------------------------------------------------------------------

CMultiplayerLobby = class()
	function CMultiplayerLobby:__init()
		self.players = {}
	end