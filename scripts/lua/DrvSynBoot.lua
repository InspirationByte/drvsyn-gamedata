--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2020
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 25 Feb 2020
--------------------------------------------------------------------------------

--[[

Addon manager. Loads Addons/<AddonName>/ModInit.lua and adds search paths to engine FS

Example ModInit.lua:

	local ModInit = {
		Title = "Your crazy mod name",

		CreatedBy = "Mod Author Name",
		Version = 1.0,
		
		-- indicates if it needs initialization before all scripts - useful if mod changes Lua scripts
		PreBoot = true, -- either function or boolean. Boolean value only enables PreBoot while function enables it and executes extra logic
		
		-- dependency checking (folder names)
		Dependencies = { "OtherModName" }
		
		-- conflicts checking (folder names) for known mods
		Conflicts = { "OtherModName" }
	}
	
	-- initialization function
	function ModInit:Init()
		
		-- add to menu car list
		table.insert(MenuCarsList, {"flair500", "'74 Flair 500"})
	end,

	-- deinitialization function
	function ModInit:DeInit()
		-- remove from menu car list
		for i,v in ipairs(MenuCarsList) do
			if v[1] == "flair500" then
				table.remove( MenuCarsList, i)
			end
		end
	end
]]

ADDON_DISABLED	= 0
ADDON_PREBOOT 	= 1
ADDON_LOADED 	= 2

local MODCONFIG_FILENAME = "Addons/ADDON.CONFIG"
local VANILLA_ADDONS = {
	"BrimblePack",
	"BrimbleClassicPack"
}

AddonManager = class()

	function AddonManager:__init()
		self.list = {}
		
		self:Search()
		
		-- also init pre-boot mods
		self:InitPreBoot()
	end
	
	-- initializes mods before all game scripts loaded
	function AddonManager:InitPreBoot()
		for k,v in pairs(self.list) do
		
			-- pre-boot could be 'boolean' or 'function'
			if v.PreBoot ~= nil and v.Enabled == true then

				local doPreboot = if_then_else(type(v.PreBoot) == "boolean", v.PreBoot, true)
			
				if doPreboot then

					v.InitState = ADDON_PREBOOT
					
					-- before pre-boot we add search path immediately
					fileSystem:AddSearchPath(k, v.Path)
					
					if type(v.PreBoot) == "function" then
						v:PreBoot()
					end
				end
			end
		end
	end
	
	function AddonManager:GetDependencies( modId )
	
		local list = {}
	
		local modInfo = self.list[modId]
		
		if type(modInfo.Dependencies) == "table" then
			for i,v in ipairs(modInfo.Dependencies) do
				local modInfo = self:Find(v)
				
				if modInfo == nil then
					table.insert(list, { Name = v, NotFound = true })
				else
					table.insert(list, modInfo)
				end
			end
		end
		
		return list
	end
	
	function AddonManager:GetConflicts( modId )
	
		local list = {}
	
		local modInfo = self.list[modId]
		
		if type(modInfo.Conflicts) == "table" then
			for i,v in ipairs(modInfo.Conflicts) do
				local modInfo = self:Find(v)
				
				if modInfo == nil then
					table.insert(list, { Name = v, NotFound = true })
				else
					table.insert(list, modInfo)
				end
			end
		end
		
		return list
	end
	
	-- initializes mods after all game scripts are loaded
	function AddonManager:InitPostBoot()
		-- TODO: load file
		
		for k,v in pairs(self.list) do
			if v.Enabled == true then
				self:Enable(k)
			end
		end
	end
	
	function AddonManager:Find(name)
		for k,v in pairs(self.list) do
			if v.Name == name then
				return v
			end
		end
		
		return nil
	end
	
	function AddonManager:GetId(name)
		for k,v in pairs(self.list) do
			if v.Name == name then
				return k
			end
		end
		
		return nil
	end
	
	-- enables specific mod
	function AddonManager:Enable(modId)
		local modInfo = self.list[modId]
				
		if modInfo == nil then
			return false -- invalid mod
		end
		
		if modInfo.InitState == ADDON_LOADED then
			return true
		end
		
		-- disable conflicting mods
		local conflicts = self:GetConflicts(modId)
	
		for i,v in ipairs(conflicts) do
			if v.NotFound == nil then
				local depModId = self:GetId(v.Name)
				
				-- disable conflicting mod
				self:Disable(depModId)
			end
		end
		
		-- check and enable dependencies
		local deps = self:GetDependencies(modId)
		
		for i,v in ipairs(deps) do
			if v.NotFound ~= nil then
				MsgWarning("Cannot enable addon: Dependency addon '"..v.Name.. "' not found" )
				return false
			else
				local depModId = self:GetId(v.Name)
				
				if self:Enable(depModId) == false then
					MsgWarning("Cannot enable addon: Dependency addon '"..v.Name.. "' might be not valid" )
					return false
				end
			end
		end
		
		local doesPreboot = if_then_else(type(modInfo.PreBoot) == "boolean", modInfo.PreBoot, false)
		
		-- mods which does require pre-boot needs engine game restart
		if (doesPreboot == false or modInfo.InitState == ADDON_PREBOOT) then
	
			-- add search path now because mod doesn't have pre-booting
			if modInfo.InitState ~= ADDON_PREBOOT then
				fileSystem:AddSearchPath(modId, modInfo.Path)
			end
			
			if modInfo.Init ~= nil then
				modInfo:Init()
			end
			
			Msg("Addon '".. modInfo.Title .."' enabled\n")
			
			modInfo.InitState = ADDON_LOADED
		end
		
		modInfo.Enabled = true
		
		return true
	end

	-- disables specific mod
	function AddonManager:Disable(modId)
		local modInfo = self.list[modId]
		
		if modInfo == nil then
			return -- invalid mod
		end
		
		if modInfo.InitState ~= ADDON_LOADED then
			return
		end
		
		for i,v in ipairs(modInfo.RequiredTo) do
			local reqModId = self:GetId(v)
			self:Disable(reqModId)
		end

		if modInfo.DeInit ~= nil then
			modInfo:DeInit()
		end
		
		fileSystem:RemoveSearchPath(modId, modInfo.Path)
		
		Msg("Addon '".. modInfo.Title .."' disabled\n")
		
		modInfo.InitState = ADDON_DISABLED
		modInfo.Enabled = false
	end
	
	function AddonManager:SaveEnabled()
		local modTable = {}
		local kvs = KeyValues.new()
		
		for k,v in pairs(self.list) do
			modTable[v.Name] = v.Enabled
		end
		
		kvs:GetRoot():FromTable(modTable, false)
		kvs:SaveFile(MODCONFIG_FILENAME, SP_ROOT)
	end

	-- searches for mods
	function AddonManager:Search()

		local modTable = {}

		-- load MOD.CONFIG file
		local kvs = KeyValues.new()
		if kvs:LoadFile(MODCONFIG_FILENAME, SP_ROOT) then
			modTable = kvs:GetRoot():ToTable()
		end
		
		local modsFind = CFileSystemFind.new("Addons/*.*", SP_ROOT)

		local modIdx = #self.list
		while modsFind:Next() do
			if modsFind:IsDirectory() then
				local name = modsFind:GetPath()

				if name ~= ".." and name ~= "." and self:Find(name) == nil then

					local modPath = "Addons/".. name
					local modId = "$MOD$_".. modIdx

					local modInitFilename = modPath .."/ModInit.lua"
					
					if fileSystem:FileExist(modInitFilename, SP_ROOT) then
					
						local modInfo = include(modInitFilename)
						modInfo.Name = name
						modInfo.Path = modPath
						modInfo.RequiredTo = {} -- dependency table
						
						modInfo.InitState = ADDON_DISABLED						
						modInfo.Enabled = false
						
						if modTable[name] ~= nil then
							modInfo.Enabled = modTable[name] > 0
						end
						
						self.list[modId] = modInfo
					
						MsgInfo("*** Registered addon '".. modInfo.Title .."' ***\n")
					end
					
					modIdx = modIdx+1
				end
			end
		end
		
		for k,v in pairs(self.list) do
			local list = self:GetDependencies(k)
			
			for i,vv in ipairs(list) do
				if v.NotFound == nil then
					table.insert(vv.RequiredTo, v.Name)
				end
			end
		end
	end

addons = AddonManager()

