--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2015
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 21 Sep 2015
--------------------------------------------------------------------------------

-- alias some classes to the global scope
CNetThread = _G["Networking::CNetworkThread"]
CNetMessage = _G["Networking::CNetMessageBuffer"]

-- native game events are not allowed, only Lua-defined messages.

g_luaNetEvents = {} -- lua network events.

-- ��������� ����� ������� � ����������� ��� id
-- ����������� LMSG_ ��������������, ���������� ��� � ����.
-- ������: AddNetEvent(<��� ������>, LMSG_<���>)
function RegisterNetEvent( id, factoryClass )
	g_luaNetEvents[id] = factoryClass
end

-- ������ ������ �������. ��� �� ��� ���������� �������, ���������� �� C++
-- �� ����� �� ������ ������������ � ���.
function CreateNetEvent( id )

	if g_luaNetEvents[id] ~= nil then
	
		local eventObj = {}
		eventObj = v() -- construct
		eventObj.event_id = id
		
		return eventObj
	end
	
	return nil
end