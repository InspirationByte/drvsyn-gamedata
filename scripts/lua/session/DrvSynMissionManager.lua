--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2015
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- Driver Syndicate mission manager
-- By Shurumov Ilya
-- 1 Feb 2015
--------------------------------------------------------------------------------

g_infiniteTime = console.FindOrCreateCvar("mission_stopTimer", "0", "Disables mission timer", CV_CHEAT)

local MissionLoadedCallback = {}

-- sets callback which will be called right after mission file is loaded
-- useful for sounds precaching
function SetMissionLoadedCallback( name, func )
	MissionLoadedCallback[name] = func
end

local MissionInitializedCallback = {}

-- sets callback which will be called after mission initialization
function SetMissionInitializedCallback( name, func )
	MissionInitializedCallback[name] = func
end

--------------------------------------------------------------------------------
MissionManager = {
	m_gameMode = "none",
	m_enableTimeout = false,
	m_timeoutTime = 0,
	m_scheduledEvents = {},

	m_pluginRefreshFuncs = {},
	m_refreshFunc = (function()end),
	m_onGameDone = (function() end),
}

-- sets the game mode
-- should be called once
function MissionManager:SetGameMode( newGameMode )
	-- store
	self.m_gameMode = newGameMode
	
	-- get the root game mode (story, minigame, multiplayer, etc)
	local delim = string.find(newGameMode, "/") or 0
	local gameModeRootName = string.sub(newGameMode, 1, delim-1)
	
	-- get the sub game mode (getaway, checkpoint, etc)
	local gameModeSubName = string.sub(newGameMode, delim+1)
	
	Msg("*** Game mode is "..gameModeRootName.." - "..gameModeSubName.."***\n")
	
	local newMenu = GameModeMenus[gameModeRootName]

	if newMenu == nil then
		newMenu = GameModeMenus["default"]
	end
	
	-- setup the picked menu table
	CurrentGameMenuTable = newMenu
end

function MissionManager:GetGameMode()
	return self.m_gameMode
end

-- sets the function which is called after mission is completed and game forced to pause
function MissionManager:SetOnGameDone( func )
	local oldfunc = self.m_onGameDone
	self.m_onGameDone = func
	return oldfunc
end

-- sets the current mission function
function MissionManager:SetRefreshFunc( func )
	local oldfunc = self.m_refreshFunc
	self.m_refreshFunc = func
	return oldfunc
end

-- sets additional refresh function
function MissionManager:SetPluginRefreshFunc( name, func )
	self.m_pluginRefreshFuncs[name] = func
end

-- schedules the function execution
function MissionManager:ScheduleEvent( func, delay )
	table.insert(self.m_scheduledEvents, {func = func, delay=delay or 0})
end

function MissionManager:AddSeconds( seconds )
	self.m_timeoutTime = self.m_timeoutTime + seconds
end

function MissionManager:EnableTimeout( enable, seconds )
	self.m_enableTimeout = enable

	if seconds ~= nil then
		self.m_timeoutTime = seconds
	else
		self.m_timeoutTime = 0
	end
end

function MissionManager:IsTimedOut()

	if g_infiniteTime:GetBool() then
		return false
	end

	return self.m_enableTimeout and self.m_timeoutTime <= 0
end


function MissionManager:ShowTime(enable)
	self.m_showTime = enable
end

---------------------------------------------------------------------------------
-- Functions called from C++
--

--
-- Callback when mission file is loaded
--
function MissionManager:OnMissionLoaded()
	Msg("----  MissionManager:OnMissionLoaded() ----\n");
	
	InitMissionDefaults()
	
	-- ladder init
	missionladder:MissionInit()
	
	-- do mission loading callbacks
	for i,callback in pairs(MissionLoadedCallback) do
		if callback ~= nil then 
			callback( delta )
		end
	end
end

--
-- Initializes mission
--
function MissionManager:InitMission()
	Msg("----  MissionManager:InitMission() ----\n");
	
	GC_CRITICAL()
	
	gameHUD:SetHudScheme(MISSION.Hud)

	gameHUD.ShowScreenMessage = function(self,text,delay)
		self:ShowMessage(text,delay)
	end

	self.m_enableTimeout = false
	self.m_showTime = false
	self.m_timeoutTime = 0
	self.m_gameTime = 0
	self.m_pluginRefreshFuncs = {}
	self.m_scheduledEvents = {}
	self.m_refreshFunc = (function() end)
	self.m_onGameDone = (function() end)

	InitMissionDefaults()

	MISSION.Init()

	-- make initial AI settings set up
	SetupInitialSettings()
	
	-- do mission init callbacks
	for i,callback in pairs(MissionInitializedCallback) do
		if callback ~= nil then 
			callback( delta )
		end
	end
end

-- connects to game engine
function MissionManager:GetRandomSeed()
	return MISSION.RandomSeed
end

-- returns mission settings to engine
function MissionManager:GetMissionSettingsKeyValues()
	local settingsSec = create_section(MISSION.Settings)
	--KV_PrintSection(settingsSec)
	return settingsSec
end

-- returns mission data to engine
function MissionManager:GetMissionDataKeyValues()
	local dataSec = create_section(MISSION.Data)
	--KV_PrintSection(dataSec)
	return dataSec
end

function MissionManager:Finalize()
	Msg("----  MissionManager:Finalize() ----\n");
	MISSION.Finalize()
	ResetMissionTable()
	GC_CRITICAL()
end

--
-- Updates the mission itself.
--
function MissionManager:Update( delta )

	self.m_gameTime = self.m_gameTime + delta

	-- call refresh func
	if self.m_refreshFunc( delta ) then		
		if self.m_enableTimeout then
			self.m_timeoutTime = self.m_timeoutTime - delta
		else
			self.m_timeoutTime = self.m_timeoutTime + delta
		end
	
	end
	
	if self.m_timeoutTime < 0 then
		self.m_timeoutTime = 0
	end

	-- play scheduled events
	for k,v in ipairs(self.m_scheduledEvents) do
		v.delay = v.delay - delta
		
		if v.delay <= 0 then
			v.func( )
			
			table.remove(self.m_scheduledEvents, k)
		end
	end
	
	for i,updfunc in pairs(self.m_pluginRefreshFuncs) do
		if updfunc ~= nil then
			updfunc( delta )
		end
	end

	-- since the music is updated from cop state, check if UpdateCops called
	local copState = MISSION.CopState

	if copState.InitState then
		copState.InitState = false
		SetMusicState( MISSION.MusicState )
	end
	
	-- update timer display
	gameHUD:SetTimeDisplay(self.m_enableTimeout or self.m_showTime, self.m_timeoutTime)
	
	-- call our game done function
	if gameses:IsGameDone(true) then
		if self.m_onGameDone ~= nil then
			self.m_onGameDone()
		end
	end
end

function MissionManager:GetMissionTime()
	return self.m_timeoutTime
end

-- Old
missionmanager = MissionManager