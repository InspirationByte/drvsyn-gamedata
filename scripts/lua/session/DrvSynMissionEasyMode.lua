--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2015
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- Mission easy mode utility functions
-- By Shurumov Ilya
-- 07 Jan 2020
--------------------------------------------------------------------------------

local function _OnMissionFailed()
	local playerCar = gameses:GetPlayerCar()

	playerCar:Lock(true)

	gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
	
	MissionManager:SetRefreshFunc( function() 
		return false 
	end ) 
end

local function _OnMissionUpdate(delta)
	
	local playerCar = gameses:GetPlayerCar()

	if MissionManager:IsTimedOut() then
		gameHUD:ShowAlert(MISSIONSTATE.FailedAlert, 4.0, HUD_ALERT_DANGER)	
		_OnMissionFailed()
	end
	
	if CheckVehicleIsWrecked( playerCar, MISSION.Data, delta ) then
		gameHUD:ShowAlert(MISSIONSTATE.WreckedAlert, 4.0, HUD_ALERT_DANGER)	
		_OnMissionFailed()
	end
	
	if MissionTarget ~= nil then
		if MissionTarget.get_x() ~= nil then	-- Vector3D
			-- check distance
		else -- game object
			-- other logic
		end
	end
	
	return true
end

MISSIONSTATE = {
	Level = "default",
	Music = "vegas_day",
	Environment = "day_clear",
	
	PlayerCarStartPos = Vector3D.new(0,0,0),
	PlayerCarStartAngle = 0,					-- or Vector3D
	PlayerCarName = "mustang_f",

	TitleAlert = nil,
	TitleMessage = nil,
	
	WreckedAlert = "#WRECKED_VEHICLE_MESSAGE",
	WreckedMessage = nil,
	
	FailedAlert = "#FAILED_MESSAGE";
	FailedMessage = nil,
	
	SuccessAlert = "#WELL_DONE_MESSAGE",
	SuccessMessage = nil,
	
	MissionTimeout = 0,
	MissionTarget = nil, 						-- game object or vector3D
}

function ScheduleFunc(func, delay)
	MissionManager:ScheduleEvent(func, delay)
end

function Wait(delay)
	local oldFunc = MissionManager.m_refreshFunc
	
	MissionManager:SetRefreshFunc( function() 
		return false
	end)
	
	ScheduleFunc(function() 
		MissionManager:SetRefreshFunc(oldFunc)
	end, delay)
end

function WaitForObjectiveToComplete()
	-- execute current stack of MISSIONSTATE
	
	-- set the refresh function
	MissionManager:SetRefreshFunc( _OnMissionUpdate )
end

function SetTimeout( timeout )
	MISSIONSTATE.MissionTimeout = timeout
end

function SetTarget( targetObjectOrPosition )
	MISSIONSTATE.MissionTarget = targetObjectOrPosition
end
