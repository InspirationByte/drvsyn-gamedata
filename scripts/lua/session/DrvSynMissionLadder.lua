--//////////////////////////////////////////////////////////////////////////////////
--// Copyright © Inspiration Byte
--// 2009-2017
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 14 Jun 2017
--------------------------------------------------------------------------------

--[[

Mission Ids
	Mission Id is just a mission script name (story_c1_m04)

Mission status

	m_missionStatus is name-value pair list of objects
		{ status, data }
		
	Data can contain mission some completion information (time, last position, whatever you want)
	Data must consist of simple types that can be serialized to engine's KeyValues
	Data and mission status can be accessed to define mission parameters
	
Ladder's Mission list
	
	m_missionList is name-value pair list that contains predefined list of missions
		{
			id					// mission name
			mode				// game mode
			completed			// flag
		}
]]

CMissionLadder = class()

	function CMissionLadder:__init()
		self.m_missionList = {}
		self.m_ladderPrefix = ""
		self.m_curMissionNumber = 1
		self.m_ladderRunning = false
		
		self:LoadProgress()
	end
	
	-- sets the new ladder completely ruining the previous one progress
	function CMissionLadder:SetLadder(prefix, missionList, allAvailable)
	
		-- let's make a ladder mission list
		self.m_ladderPrefix = prefix
		self.m_missionList = {}
		self.m_curMissionNumber = 1
		
		if allAvailable == nil then
			allAvailable = false
		end
		
		for k,v in ipairs(missionList) do
		
			local mission = {
				mode = prefix,
				completed = allAvailable,
			}

			if type(v) == "table" then

				if v.screen == nil then
					mission.id = v[1]
					mission.label = v[2]
				else
					mission.screen = v.screen
				end
			else
				mission.id = v
				mission.label = v
			end

			table.insert(self.m_missionList, mission)
		end
	end
	
	-- starts running new mission ladder
	function CMissionLadder:Run(prefix, missionList)
		self:SetLadder(prefix, missionList)
		
		-- intial save
		self:SaveProgress()
		
		self:StartMission()
	end
	
	-- deletes progress for specific mission ladder
	function CMissionLadder:DeleteProgress( ladderName )
		local ladderProfileData = userProfileManager:GetProgressStore(USER_STORE_LADDER)
		ladderProfileData:Cleanup()
		
		local missionData = userProfileManager:GetProgressStore(USER_STORE_MISSION_DATA)
		missionData:Cleanup()
		
		userProfileManager:Save()
		
		if self.m_ladderPrefix == ladderName then
			self.m_missionList = {}
			self.m_ladderPrefix = ""
		end
	end
	
	-- loads progress from profile data for specific mission ladder
	function CMissionLadder:LoadProgress( ladderName )
		local ladderProfileData = userProfileManager:GetProgressStore(USER_STORE_LADDER):ToTable()
		
		-- if specific ladder name not defined, use one from current progress
		if ladderName == nil then
			ladderName = ladderProfileData.current
		end

		local ladderTable = ladderProfileData[ladderName]
		
		if ladderTable then
			self.m_ladderPrefix = ladderName
			self.m_curMissionNumber = ladderTable.currentMissionNumber
			self.m_missionList = kvutil.AsArray(ladderTable.missionList)
		end
		
		--util.printObject(ladderProfileData)
	end
	
	-- saves progress of currently running mission ladder
	function CMissionLadder:SaveProgress()
		local ladderProfileData = userProfileManager:GetProgressStore(USER_STORE_LADDER)
		
		-- set current
		ladderProfileData:SetKeyString("current", self.m_ladderPrefix)
		ladderProfileData:SetTableKeyBase(self.m_ladderPrefix, {
			currentMissionNumber = self.m_curMissionNumber,
			missionList = self.m_missionList
		})

		userProfileManager:Save()
	end
	
	function CMissionLadder:GetNextMission()
		local nextMissionNumber = self:GetNextMissionNumber()
		return self.m_missionList[nextMissionNumber]
	end
	
	-- starts next mission
	function CMissionLadder:RunContinue( missionIndex )
		if missionIndex ~= nil then
			self.m_curMissionNumber = missionIndex-1
		end
		self.m_curMissionNumber = self:GetNextMissionNumber()
		self:StartMission()
	end
		
	-- indicates if has incomplete missions
	function CMissionLadder:HasNextMission()
		for k,v in ipairs(self.m_missionList) do
			if not v.completed then
				return true
			end
		end
		
		return false
	end
	
	-- returns first incomplete mission number of current ladder
	function CMissionLadder:GetNextMissionNumber()
	
		local furthestMission = -1
	
		for i,v in ipairs(self.m_missionList) do
			if not v.completed then
				furthestMission = i
				break
			end
		end
		
		local newMissionNumber = self.m_curMissionNumber + 1
		
		-- don't get past the furthest mission
		if newMissionNumber > furthestMission then
			return furthestMission
		end
		
		return newMissionNumber
	end
	
	-- returns completed missions list of current mission ladder
	function CMissionLadder:GetCompletedMissions()
		local missionList = {}
		for k,v in ipairs(self.m_missionList) do
			if v.completed then
				table.insert(missionList, v)
			end
		end
		
		return missionList
	end
	
	-- ladder initialization on mission. Called from mission manager
	function CMissionLadder:MissionInit()
	
		local curMissionName = game.GetMissionScriptName()
		
		for k,v in ipairs(self.m_missionList) do
			if v.id ~= nil then
				local ladderMissionName = v.mode .. "/" .. v.id;
				
				if ladderMissionName == curMissionName then
					self.m_ladderRunning = true
					return
				end
			end
		end
		
		self.m_ladderRunning = false
	end
	
	-- loads mission and changes the engine state
	function CMissionLadder:StartMission()
		local curMission = self.m_missionList[self.m_curMissionNumber]
		
		-- load first mission
		if curMission ~= nil then
		
			if curMission.id ~= nil and LoadMission(curMission.mode, curMission.id) then
				-- but menu is going to be overridden
				CurrentGameMenuTable = GameModeMenus["story"]
				
				EqStateMgr.ScheduleNextStateType(GAME_STATE_GAME)
				return
			elseif curMission.screen ~= nil then				
				-- create sequence screen and run it
				SequenceScreens.current = SequenceScreens.Create(curMission.screen)

				Msg("Next story sequence\n")

				if SequenceScreens.current ~= nil then
					EqStateMgr.ScheduleNextStateType(GAME_STATE_SEQUENCESCREEN)
					return
				end
			else
				MsgWarning("Unhandled mission ladder item!")
			end	
		end
		
		-- Get back to main menu
		EqStateMgr.ScheduleNextStateType(GAME_STATE_MAINMENU)
	end
	
	-- performs mission ladder update
	function CMissionLadder:OnMissionCompleted()
		if not self.m_ladderRunning then
			return
		end
		
		local curMission = self.m_missionList[self.m_curMissionNumber]
		
		if curMission.completed then
			return
		end
		
		-- track mission status
		curMission.completed = true
		
		-- trigger save
		self:SaveProgress()
	end
	
missionladder = CMissionLadder()

function DebugLadder(name, mission_index)

	missionladder:SetLadder(name, missions[name], true)
	missionladder:SaveProgress()
	missionladder.m_curMissionNumber = mission_index
	missionladder:StartMission()
	
	EqStateMgr.SetCurrentStateType( GAME_STATE_GAME )

end
