--//////////////////////////////////////////////////////////////////////////////////
--// Copyright © Inspiration Byte
--// 2009-2015
--//////////////////////////////////////////////////////////////////////////////////

-------------------------------------------------------------
--	vehicle utility functions
-------------------------------------------------------------

VehicleManager = {}

function VehicleManager:Initialize()
	local kvs = KeyValues.new()
	
	-- open the file which contains the points
	if kvs:LoadFile("scripts/vehicles.def", SP_MOD) == false then
		error("Cannot open vehicles.def file !!!")
	end
	
	local tbl = kvs:GetRoot():ToTable()

	for k,v in pairs(tbl) do
	
		if getFileExtension(v) == ".lua" then
			dofile(v)
		end
	end
	
	for k,v in pairs(CarConfigs) do
		local sec = create_section(v)
		gameses:RegisterVehicleConfig(k, sec)
	end
end
