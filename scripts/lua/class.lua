---------------------------------------------------------------------
-- class module
--

-- class ID generator
local __g_classIDCounter = 0

function __NewClassID()
	-- get
	local id = __g_classIDCounter;

	-- incr
	__g_classIDCounter = __g_classIDCounter + 1

	return id
end

-- function that creates class template
function class(parentclass)
	local desc = {};
	desc.__index = desc;

	desc.parentclass = parentclass;
	desc._classid = __NewClassID();

	return setmetatable(desc,
		{
			_classid = desc._classid;
			parentclass = parentclass,

			-- search here, then move up
			__index = function(self, key)
				local value = rawget(self, key)

				if (value == nil) then
					local parentclass = rawget(self, "parentclass")

					if parentclass ~= nil then
						return parentclass[key]
					end
				end

				return value
			end,

			-- creates our 'class' instance
			__call = function (self, ...)
				local objinst = setmetatable({}, desc)

				-- call constructor
				if desc.__init then
					desc.__init(objinst, ...)
				end

				return objinst
			end
		}
	)
end;

-- check for type
function check_type( object, type_decl )
	-- don't check non-tables
	if type(object) ~= "table" then
		return false
	end

	local classid = object._classid

	if classid == nil then
		return false
	end

	if type_decl._classid == classid then
		return true
	end

	return false
end

--[[--

---------------------------------------------------------------------
-- ������ �������������:

---------------------------------------------------------------------
-- �������� �������� ������
CMyClass = class();

	-- ������ ����� ����������, ����� ������������ ������ ������ ������, �������� myObject = CMyClass(nil);
	function CMyClass:__init()
		self.value = 0;
	end;

	function CMyClass:foo()
		self.value = self.value + 1;
	end;

---------------------------------------------------------------------
-- �������� ������� �������� ������
myObject = CMyClass();
...
myObject:foo()

---------------------------------------------------------------------
-- �������� ��������������� ������
CMySecondClass = class(CMyClass);

	function CMySecondClass:__init(_factor)
		CMyClass.__init(self);

		self.factor = _factor;
	end;

	function CMySecondClass:foo()
		CMyClass.foo(self); -- ��� ����������� ����� ������������� CMyClass:foo(), ���� ��� ����������

		self.value = self.value * self.factor;
	end;

---------------------------------------------------------------------
-- �������� ������� ��������������� ������
mySecondObject = CMySecondClass(0.5);
...
mySecondObject:foo();

--]]--
