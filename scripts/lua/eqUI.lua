--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2017
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- EQUI helpers
-- By Shurumov Ilya
-- 12 Oct 2017
--------------------------------------------------------------------------------

local Window = class()

function Window:__init( schemePath )
	-- create
	self.ui = equi:CreateElement("panel")
	equi:AddPanel(self.ui, "panel");
	
	local kvs = KeyValues.new()
	if kvs:LoadFile( schemePath, SP_MOD ) then
		self.ui:InitFromKeyValues( kvs:GetRoot(), true )
	end
	
	-- bind this object as event listener
	--self.ui:BindEvents( self )
end

equi.Window = Window

------------------------------

equi._CreateElement = equi.CreateElement;

function equi:CreateElement(typename)
	return self:Cast(self:_CreateElement(typename), typename)
end

function equi:Cast(control, typename)

	if equi_cast[typename] == nil then
		error("equi:Cast - invalid typename '" .. typename .. "'\n")
		return nil
	end

	return equi_cast[typename](control)
end