--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2017
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 02 Aug 2017
--------------------------------------------------------------------------------

kvutil = {}

function kvutil.ConvertAutodetectValues(valueArray)
	local newArray = {}
	
	for i,v in ipairs(valueArray) do
		local numb = tonumber(v)
	
		if numb ~= nil then
			table.insert(newArray, numb)
		else
			table.insert(newArray, v)
		end
	end
	
	return newArray
end

function kvutil.AsArray(v)
	if type(v) ~= "table" then
		return { v }
	end

	if table.isArray(v) then
		return v
	end
	
	return { v }
end

function create_section( tbl, sort )
	local that = kvkeybase_t.new() 
	that:FromTable(tbl, false, sort)
	
	return that
end

function kvkeybase_t:SetTableKeyBase(name, tbl)

	local existingSec = self:FindKeyBase(name, 0)
	if existingSec ~= nil then
		existingSec:FromTable(tbl, true, sort)
	else
		self:AddTableKeyBase(name, tbl)
	end
end

function kvkeybase_t:AddTableKeyBase(name, tbl)
	local sec = create_section(tbl)
	sec:SetName(name)
	
	self:AddExistingKeyBase(sec)
end

function kvkeybase_t:ToTable( autoConvert )

	if autoConvert == nil then
		autoConvert = true
	end

	local resultTable = {}

	for i in range(0, self:KeyCount()-1) do
		local key = self:KeyAt(i)
		local keyName = key:GetName()

		local values = {}
	
		-- TODO: more details in value handling
		for j in range(0, key:ValueCount()-1) do
			local val = key:ValueAt(j)
			
			if val:get_type() == KVPAIR_INT then
				table.insert(values, val:get_nValue())
			elseif val:get_type() == KVPAIR_FLOAT then
				table.insert(values, val:get_fValue())
			elseif val:get_type() == KVPAIR_BOOL then
				table.insert(values, val:get_bValue())
			elseif val:get_type() == KVPAIR_SECTION then
				-- TODO: section values
			else
				table.insert(values, val:get_value())
			end
		end
		
		if autoConvert == true then
			values = kvutil.ConvertAutodetectValues(values)
		end
		
		local sectionData = key:ToTable(autoConvert)
		
		local finalKeyName = keyName
		
		-- check if it has number in it
		if tonumber(finalKeyName) ~= nil then
			finalKeyName = tonumber(finalKeyName)
		end
		
		local counter = 1;
		while type(resultTable[finalKeyName]) ~= "nil" do
			finalKeyName = keyName .. "_" .. counter;
			counter = counter+1;
		end
		
		if next(sectionData) ~= nil and #values > 0 then
			resultTable[finalKeyName] = {values, sectionData}
		elseif next(sectionData) ~= nil then
			resultTable[finalKeyName] = sectionData
		elseif #values > 0 then
			-- convert from array if there is only one element
			if #values == 1 then
				values = values[1]
			end
		
			resultTable[finalKeyName] = values
		end
	end

	return resultTable
end

function kvkeybase_t:ValuesFromArray(tbl)

	local numTypes = {
		[KVPAIR_INT] = 0,
		[KVPAIR_FLOAT] = 0,
		[KVPAIR_STRING] = 0,
		[KVPAIR_BOOL] = 0,
	}
	
	for i,v in ipairs(tbl) do
		if type(v) == "number" then
			if isint(v) then
				numTypes[KVPAIR_INT] = numTypes[KVPAIR_INT]+1
			else
				numTypes[KVPAIR_FLOAT] = numTypes[KVPAIR_FLOAT]+1
			end
		elseif type(v) == "boolean" then
			numTypes[KVPAIR_BOOL] = numTypes[KVPAIR_BOOL]+1
		else
			numTypes[KVPAIR_STRING] = numTypes[KVPAIR_STRING]+1
		end
	end
	
	local bestType = KVPAIR_STRING
	
	if #tbl == numTypes[KVPAIR_INT] then
		bestType = KVPAIR_INT
	elseif numTypes[KVPAIR_FLOAT] > 0 then
		bestType = KVPAIR_FLOAT
	elseif numTypes[KVPAIR_BOOL] > 0 then
		bestType = KVPAIR_BOOL
	end
	
	self:SetType(bestType)

	for i,v in ipairs(tbl) do
		--Msg("value for "..self:GetName().." = "..v.."\n");
		
		if type(v) == "number" then
			if isint(v) then
				self:AddValueInt(v)
			else
				self:AddValueFloat(v)
			end
		elseif type(v) == "boolean" then
			self:AddValueBool(v)
		elseif type(v) == "table" then
			-- TODO: section values
		else
			self:AddValueString(v)
		end
	end
end

function kvkeybase_t:FromTable( tbl, clear, sort )

	if clear == nil then
		clear = true
	end

	if clear == true then
		self:Cleanup()
	end
	
	local toIterate = tbl
	
	if sort ~= nil then
		toIterate = table.sortedKeys(tbl, sort)
	end

	-- parse table
	for k,v in pairs(toIterate) do
	
		if sort ~= nil then
			k = v
			v = tbl[v]
		end
	
		local valType = type(v)
		
		if valType == "userdata" then
		
			local metaTypeName = getmetatable(v)[1]()
		
			if metaTypeName == "kvkeybase_t" then
				-- let's assume there is kvkeybase_t
				v:SetName(k)
				self:AddExistingKeyBase(v) -- and then cpp takes ownership
			elseif string.starts(metaTypeName, "Vector") then
				local key = self:AddKeyBase(k, "", KVPAIR_STRING)
				key:ClearValues()
				
				key:ValuesFromArray(v:ToTable())
			else
				error("kvkeybase_t:FromTable error - unknown userdata type " .. metaTypeName);
			end
	
		else
			local key = self:AddKeyBase(k, "", KVPAIR_STRING)
			key:ClearValues()
			
			if valType == "table" then	
				-- detect if it's array or key-value table
				local isArray = table.isArray(v)
				
				if isArray then
					if type(v[1]) == "table" then
						-- assume they are all tables
						for i,atb in ipairs(v) do
							-- save with number as index
							-- it can be easily converted back to table with it
							local tbkey = key:AddKeyBase(tostring(i), "", KVPAIR_STRING)
							tbkey:FromTable(atb, true)
						end
					else
						key:ValuesFromArray(v)
					end					
				else
					key:FromTable(v, false)
				end
			else
				key:ValuesFromArray({v})
			end
		end
	end
end

function TestKeyValues()
	local test = create_section { 
		test = "123abc",
		test2 = create_section {
			localbeast = "turbo",
			rofl = "123",
			arrayValue = {0,1,2},
			array2 = {1},
			section_array = {
				{
					test = 1,
					test2 = 0
				},
				{
					test = 21,
					test3 = 4,
				},
				{
					test = 42,
					test3 = 666,
				},
			},
			testVector = Vector3D.new(3,4,5)
		},
	}

	util.printObject(getmetatable(test)[1]())

	KV_PrintSection(test)
	
	util.printObject(test:ToTable())
end