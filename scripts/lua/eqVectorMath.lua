--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2015
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 10 Mar 2015
--------------------------------------------------------------------------------
-- some functions from cpp exports

local function _v_gsx(self, value)
	if value ~= nil then
		self:set_x(value)
	end
	return self:get_x()
end

local function _v_gsy(self, value)
	if value ~= nil then
		self:set_y(value)
	end
	return self:get_y()
end

local function _v_gsz(self, value)
	if value ~= nil then
		self:set_z(value)
	end
	return self:get_z()
end

local function _v_gsw(self, value)
	if value ~= nil then
		self.set_w(value)
	end
	return self.get_w()
end

Vector2D.x = _v_gsx
Vector2D.y = _v_gsy

IVector2D.x = _v_gsx
IVector2D.y = _v_gsy

Vector3D.x = _v_gsx
Vector3D.y = _v_gsy
Vector3D.z = _v_gsz

Vector4D.x = _v_gsx
Vector4D.y = _v_gsy
Vector4D.z = _v_gsz
Vector4D.w = _v_gsw

Quaternion.x = _v_gsx
Quaternion.y = _v_gsy
Quaternion.z = _v_gsz
Quaternion.w = _v_gsw

ivec2 = IVector2D.new
vec2 = Vector2D.new
vec3 = Vector3D.new
vec4 = Vector4D.new
quat = Quaternion.new

vec3_right 		= vec3(1,0,0)
vec3_up 		= vec3(0,1,0)
vec3_forward 	= vec3(0,0,1)

vec4_zero		= vec4(0,0,0,0)
vec3_zero		= vec3(0,0,0)
vec2_zero		= vec2(0,0)

function RAD2DEG(x) 
	return x * (180.0 / math.pi)
end

function DEG2RAD(x) 
	return x * (math.pi / 180.0)
end

function VDEG2RAD(v)
	if type(v) == "number" then
		return DEG2RAD(v)
	else
		if v.get_z == nil then
			return Vector2D.new(DEG2RAD(v:get_x()), DEG2RAD(v:get_y()))
		elseif v.get_w ~= nil then
			return Vector4D.new(DEG2RAD(v:get_x()), DEG2RAD(v:get_y()), DEG2RAD(v:get_z()), DEG2RAD(v:get_w()))
		else
			return Vector3D.new(DEG2RAD(v:get_x()), DEG2RAD(v:get_y()), DEG2RAD(v:get_z()))
		end
	end
end

function VRAD2DEG(v)
	if type(v) == "number" then
		return RAD2DEG(v)
	else
		if v.get_z == nil then
			return Vector2D.new(RAD2DEG(v:get_x()), RAD2DEG(v:get_y()))
		elseif v.get_w ~= nil then
			return Vector4D.new(RAD2DEG(v:get_x()), RAD2DEG(v:get_y()), RAD2DEG(v:get_z()), RAD2DEG(v:get_w()))
		else
			return Vector3D.new(RAD2DEG(v:get_x()), RAD2DEG(v:get_y()), RAD2DEG(v:get_z()))
		end
	end
end

-- Remap a value in the range [A,B] to [C,D].
function RemapVal( val, A, B, C, D)
	return (C + (D - C) * (val - A) / (B - A))
end

-- Remap a value in the range [A,B] to [C,D]. same way, clamped
function RemapValClamp( val, A, B, C, D)
	return clamp(RemapVal(val, A, B, C, D), C, D)
end

-- linear interpolation
function lerp(a,b,x)
	if type(a) == "number" then
		return f_lerp(a,b,x)
	else
		if a.get_z == nil then
			return v2d_lerp(a,b,x)
		else
			return v3d_lerp(a,b,x)
		end
	end
end

-- restore linear factor
function lineProjection(a,b,x)
	if a.get_z == nil then
		return v2d_lineProjection(a,b,x)
	else
		return v3d_lineProjection(a,b,x)
	end
end

-- cubic interpolation
function cerp(a,b,c,d,x)
	if type(a) == "number" then
		return f_cerp(a,b,c,d,x)
	else
		if a.get_z == nil then
			return v2d_cerp(a,b,c,d,x)
		else
			return v3d_cerp(a,b,c,d,x)
		end
	end
end

function approachValue(v, t, s)

	local newValue = v + s;

	local diffBefore = v - t;
	local diffAfter = newValue - t;

	return if_then_else(diffBefore * diffAfter < 0, t, newValue);
end

function bezierQuadratic(p0, p1, p2, t)
	local mum1 = (1 - t)
	return mum1^2 * p0 + 2 * mum1 * t * p1 + t^2 * p2
end

function bezierCubic(p0, p1, p2, p3, t)
	local mum1 = (1 - t)
	return mum1^3*p0 + 3*mum1^2*t*p1 + 3*mum1*t^2*p2 + t^3*p3
end

-- bezier interpolation
function bezierQuadratic3(p1, p2, p3,mu)
	local mum1,mum12,mu2
	local p = {}
	mu2 = mu ^ 2
	mum1 = 1.0 - mu
	mum12 = mum1 ^ 2
	
	p.x = p1:x() * mum12 + 2.0 * p2:x() * mum1 * mu + p3:x() * mu2
	p.y = p1:y() * mum12 + 2.0 * p2:y() * mum1 * mu + p3:y() * mu2
	p.z = p1:z() * mum12 + 2.0 * p2:z() * mum1 * mu + p3:z() * mu2
	
	return vec3(p.x, p.y, p.z)
end

-- sign
function sign(a)
	if type(a) == "number" then
		return f_sign(a)
	else
		if a.get_z == nil then
			return v2d_sign(a)
		else
			return v3d_sign(a)
		end
	end
end

-- clamps value to min/max
function clamp(a, _min, _max)
	if type(a) == "number" then
		return f_clamp(a, _min, _max)
	else
		if a.get_z == nil then
			return v2d_clamp(a, _min, _max)
		else
			return v3d_clamp(a, _min, _max)
		end
	end
end

-- squared distance between vector origins
function distance(a,b)
	if a.get_z == nil then
		return v2d_distance(a,b)
	else
		return v3d_distance(a,b)
	end
end

-- vector length
function length(a)
	if a.get_z == nil then
		return v2d_length(a)
	else
		return v3d_length(a)
	end
end

-- vector squared length
function lengthSqr(a)
	if a.get_z == nil then
		return v2d_lengthSqr(a)
	else
		return v3d_lengthSqr(a)
	end
end

-- dot product
function dot(a,b)
	if a.get_z == nil then
		return v2d_dot(a,b)
	else
		return v3d_dot(a,b)
	end
end

function normalize(a)
	if a.get_z == nil then
		return v2d_normalize(a)
	else
		return v3d_normalize(a)
	end
end

function cross(a,b)
	return v3d_cross(a,b)
end

-- vector reflection
function reflect(d, normal)
	return v3d_reflect(d, normal)
end

function vecCopy(v)
	if type(v) == "number" then
		return v
	else
		local typename = getmetatable(v)[1]()
		
		if typename == "IVector2D" then
			return IVector2D.new(v:get_x(), v:get_y())
		elseif v.get_z == nil then
			return Vector2D.new(v:get_x(), v:get_y())
		elseif v.get_w ~= nil then
			return Vector4D.new(v:get_x(), v:get_y(), v:get_z(), v:get_w())
		else
			return Vector3D.new(v:get_x(), v:get_y(), v:get_z())
		end
	end
end

function toVector2D( v, ofsIdx )
	ofsIdx = ofsIdx or 0
	if type(v) == "table" and table.isArray(v) then
		if #v >= 2-ofsIdx then
			return Vector2D.new(v[ofsIdx+1], v[ofsIdx+2])
		end
	end
	
	-- don't convert
	return v
end

function toVector3D( v, ofsIdx )
	ofsIdx = ofsIdx or 0
	if type(v) == "table" and table.isArray(v) then
		if #v >= 3-ofsIdx then
			return Vector3D.new(v[ofsIdx+1], v[ofsIdx+2], v[ofsIdx+3])
		end
	end
	
	-- don't convert
	return v
end

function toVector( v, ofsIdx )
	ofsIdx = ofsIdx or 0
	if type(v) == "table" and table.isArray(v) then
		if #v == ofsIdx+1 then
			return v[ofsIdx+1]
		elseif #v == ofsIdx+2 then
			return Vector2D.new(v[ofsIdx+1], v[ofsIdx+2])
		elseif #v == ofsIdx+3 then
			return Vector3D.new(v[ofsIdx+1], v[ofsIdx+2], v[ofsIdx+3])
		elseif #v == ofsIdx+4 then
			return Vector4D.new(v[ofsIdx+1], v[ofsIdx+2], v[ofsIdx+3], v[ofsIdx+4])
		end
	end
	
	-- don't convert
	return v
end

function eulersToQuaternion( v )
	if type(v) == "table" and table.isArray(v) then
		return Quaternion.new(v[1], v[2], v[3])
	elseif type(v) == "userdata" then -- assume there's Vector3D/4D
		return Quaternion.new(v:get_x(), v:get_y(), v:get_z())
	end
	MsgWarning("eulersToQuaternion type == "..type(v).."\n")
	return Quaternion.new(0,0,0)
end

local function quat_to_table(v)
	local q = v
	
	if type(q) == "userdata" then
		q = {x = q:get_x(), y = q:get_y(), z = q:get_z(), w = q:get_w()}
	end
	return q
end

function qdot(q1, q2)
	q1 = quat_to_table(q1)
	q2 = quat_to_table(q2)
	return q1.x * q2.x + q1.y * q2.y + q1.z * q2.z + q1.w * q2.w;
end
--[[
function quaternionToEulers(v, rotSeq)
	local function threeAxisRot(r11, r12, r21, r31, r32)
		local res = {}
		res[1] = math.atan2( r31, r32 );
		res[2] = math.asin ( r21 );
		res[3] = math.atan2( r11, r12 );
		
		return res
	end
	
	local q = quat_to_table(v)
	
	local seqMethods = {
		["zyx"] = function()
			local res = threeAxisRot( 2*(q.x*q.y + q.w*q.z),
							 q.w*q.w + q.x*q.x - q.y*q.y - q.z*q.z,
							-2*(q.x*q.z - q.w*q.y),
							 2*(q.y*q.z + q.w*q.x),
							 q.w*q.w - q.x*q.x - q.y*q.y + q.z*q.z)
			return res
		end,
		
		["zxy"] = function()
			local res = threeAxisRot( -2*(q.x*q.y - q.w*q.z),
                      q.w*q.w - q.x*q.x + q.y*q.y - q.z*q.z,
                      2*(q.y*q.z + q.w*q.x),
                     -2*(q.x*q.z - q.w*q.y),
                      q.w*q.w - q.x*q.x - q.y*q.y + q.z*q.z)
			return res
		end,
		
		["yxz"] = function()
			local res = threeAxisRot( 2*(q.x*q.z + q.w*q.y),
                     q.w*q.w - q.x*q.x - q.y*q.y + q.z*q.z,
                    -2*(q.y*q.z - q.w*q.x),
                     2*(q.x*q.y + q.w*q.z),
                     q.w*q.w - q.x*q.x + q.y*q.y - q.z*q.z);
			return res
		end,
		
		["yzx"] = function()
			local res = threeAxisRot( -2*(q.x*q.z - q.w*q.y),
                      q.w*q.w + q.x*q.x - q.y*q.y - q.z*q.z,
                      2*(q.x*q.y + q.w*q.z),
                     -2*(q.y*q.z - q.w*q.x),
                      q.w*q.w - q.x*q.x + q.y*q.y - q.z*q.z)
					  
			return res
		end,
		
		["xyz"] = function()
			local res = threeAxisRot( -2*(q.y*q.z - q.w*q.x),
                    q.w*q.w - q.x*q.x - q.y*q.y + q.z*q.z,
                    2*(q.x*q.z + q.w*q.y),
                   -2*(q.x*q.y - q.w*q.z),
                    q.w*q.w + q.x*q.x - q.y*q.y - q.z*q.z)
					
			return res
		end,
		
		["xzy"] = function()
			local res = threeAxisRot( 2*(q.y*q.z + q.w*q.x),
                     q.w*q.w - q.x*q.x + q.y*q.y - q.z*q.z,
                    -2*(q.x*q.y - q.w*q.z),
                     2*(q.x*q.z + q.w*q.y),
                     q.w*q.w + q.x*q.x - q.y*q.y - q.z*q.z);
					 
			return res
		end,
	}

	local func = seqMethods[rotSeq]

	if func == nil then
		error("unknown rotSeq")
	end

	return func()
end]]

function Vector2D:ToTable()
	return {self:get_x(), self:get_y()}
end

function IVector2D:ToTable()
	return {self:get_x(), self:get_y()}
end

function Vector3D:ToTable()
	return {self:get_x(), self:get_y(), self:get_z()}
end

function Vector4D:ToTable()
	return {self:get_x(), self:get_y(), self:get_z(), self:get_w()}
end

function Quaternion:ToTable()
	return {self:get_x(), self:get_y(), self:get_z(), self:get_w()}
end