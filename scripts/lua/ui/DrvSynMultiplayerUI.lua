--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2017
--//////////////////////////////////////////////////////////////////////////////////

AddLanguageFile("mp_settings")

MPSettingsWindow = class( equi.Window )

function MPSettingsWindow:__init()
	equi.Window.__init(self, "resources/ui_testwindow.res")
	self.ui:CenterOnScreen()
	
	-- add events
	local button = self.ui:FindChild("testClickButton")
	g_button = button
	--button:OnEvent("click", CUILuaEventHandler.new(function(args)
	--	Msg("Message function were called from LUA. Saving settings!")
	--end))
end