--//////////////////////////////////////////////////////////////////////////////////
--// Copyright (C) Inspiration Byte
--// 2009-2018
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- Trailblazer game mode
-- By Shurumov Ilya
-- 11 Sep 2018
--------------------------------------------------------------------------------

TrailblazerGame = {

	-- difficulty level
	GateDist = 1.5,			-- fixed distance between cones
	GateScaling = 1.6,		-- dynamic factor based on percentage

	-- variables used for recorder

	-- for trailblazer
	FixedInterval = 15,
	DynamicInterval = 8,

	-- for gates
	GateFixedInterval = 15,
	GateDynamicInterval = 8,

}

local Point = class()
	function Point:__init( pos, dir, coneDistPerc )
		self.pos = pos
		self.dir = dir
		self.coneDistPerc = coneDistPerc
		self.objects = {}
		
		self.hit = false
		self.skip = false
		self.done = false
	end
	
	function Point:Spawn()
	
		if #self.objects > 0 then
			return
		end
	
		local filter = CPhysCollisionFilter.new()
		local result = gameutil.TestLine(self.pos, self.pos-Vector3D.new(0,10,0), OBJECTCONTENTS_SOLID_GROUND, filter)
		
		if result:GetFract() >= 1.0 then
			return
		end
		
		local cone = objects:CreateObject("trailblazer_cone")
		cone:SetOrigin(result:GetPosition() + Vector3D.new(0,0.35,0))	
		
		cone:Spawn()
		
		cone:SetContents(OBJECTCONTENTS_SPECIAL_TRAILBLAZER_CONE)
		cone:SetCollideMask(0)
		
		cone:Set("OnCarCollision", function() 
			self:OnHit()
			cone:Set("OnCarCollision", nil)
		end)

		table.insert(self.objects, cone)
	end
	
	function Point:OnHit()
		self.done = true
	end
	
	function Point:Remove()
		for k,v in ipairs(self.objects) do
			objects:RemoveObject(v)
		end
		
		self.objects = {}
	end
	
	function Point:CheckAndComplete( pos )
	
		local pl = Plane.new(self.dir, -dot(self.pos, self.dir))
	
		-- cone is skept if we too far from it
		if pl:Distance(pos) > 3.0 then
			self.skip = (self.done == false)
			return true
		end
	
		return self.done
	end
	
------------------------------------------------------
	
local GatePoint = class(Point)

	function GatePoint:Spawn()
	
		if #self.objects > 0 then
			return
		end
	
		-- FIXME: determine distance by the width of the car?
		self.coneDistRatio = (1.0 - self.coneDistPerc)*1.0
	
		local filter = CPhysCollisionFilter.new()
		local result = gameutil.TestLine(self.pos, self.pos-Vector3D.new(0,10,0), OBJECTCONTENTS_SOLID_GROUND, filter)
		
		if result:GetFract() >= 1.0 then
			return
		end
	
		local rightVec = cross(result:GetNormal(), self.dir) * Vector3D.new(TrailblazerGame.GateDist + TrailblazerGame.GateScaling*self.coneDistRatio)

		local resultL = gameutil.TestLine(self.pos-rightVec+Vector3D.new(0,1,0), self.pos-rightVec-Vector3D.new(0,10,0), OBJECTCONTENTS_SOLID_GROUND, filter)
		local resultR = gameutil.TestLine(self.pos+rightVec+Vector3D.new(0,1,0), self.pos+rightVec-Vector3D.new(0,10,0), OBJECTCONTENTS_SOLID_GROUND, filter)
	
		if resultL:GetFract() < 1.0 and resultR:GetFract() < 1.0 then
			local coneL = objects:CreateObject("trailblazer_cone")
			local coneR = objects:CreateObject("trailblazer_cone")
			
			coneL:SetOrigin(resultL:GetPosition() + Vector3D.new(0,0.35,0))
			coneR:SetOrigin(resultR:GetPosition() + Vector3D.new(0,0.35,0))
			
			coneL:Spawn()
			coneR:Spawn()
			
			coneL:SetContents(OBJECTCONTENTS_SPECIAL_TRAILBLAZER_CONE)
			coneR:SetContents(OBJECTCONTENTS_SPECIAL_TRAILBLAZER_CONE)
			coneL:SetCollideMask(0)
			coneR:SetCollideMask(0)
			
			coneL:Set("OnCarCollision", function() 
				self:OnHit()
				coneL:Set("OnCarCollision",nil)
			end)
			
			coneR:Set("OnCarCollision", function() 
				self:OnHit()
				coneR:Set("OnCarCollision",nil)
			end)
			
			table.insert(self.objects, coneL)
			table.insert(self.objects, coneR)
		end
	end
	
	function GatePoint:OnHit()
		self.hit = true
	end
	
	function GatePoint:CheckAndComplete( pos )
	
		local pl = Plane.new(self.dir, -dot(self.pos, self.dir))
		
		if pl:Distance(pos) > 0 then
			-- gate is skept if we're not between the cones (determined by radius)
			if length(pos - self.pos) < (TrailblazerGame.GateDist + TrailblazerGame.GateScaling*self.coneDistRatio) then		
				self.skip = false
			else
				self.skip = true
			end

			return true
		end

		return false
	end

------------------------------------------------------
	
TrailblazerGame.Point = Point
TrailblazerGame.GatePoint = GatePoint

------------------------------------------------------

function TrailblazerGame.Init( initPointsFile, gates )
	if initPointsFile == nil then
		error("Missing argument for TrailblazerGame.Init")
	end
	
	if gates == null then
		gates = false
	end
	
	-- create trail blazer cone object definition
	local coneObjectDesc = create_section {
		model		= "models/misc/trailblazer_cone.egf",
		smashsound	= "cone.smash",
	}
	objects:AddObjectDef("debris", "trailblazer_cone", coneObjectDesc)

	-- configure game
	MISSION.Settings.EnableCops = false
	MISSION.Settings.PracticeMode = g_practice:GetBool()
	MISSION.Settings.TrafficSpawnInterval = 5
	
	if MISSION.Settings.PracticeMode then
		MISSION.Settings.EnableTraffic = false
	end
	
	MISSION.Data = {
		waitTimeRound = 4,
		waitTime = 3.0,
		gates = gates,
		points = {},
		currentPoint = 0,
		completedPoints = 0,
		missedPoints = 0,
		beatenCones = 0
	}

	MISSION.TRAILBLAZER_TIME = 30
	MISSION.GATES_TIME = 30
	
	-- HUD init
	local coneStatusText = gameHUD:FindChildElement("CheckStatusText")
	MISSION.coneStatusCounter = coneStatusText:FindChild("CountLabel")

	local missedStatusText = gameHUD:FindChildElement("MissedStatusText")
	MISSION.missedCounter = missedStatusText:FindChild("CountLabel")
	
	if gates then
		local beatenStatusText = gameHUD:FindChildElement("BeatenStatusText")
		MISSION.beatenCounter = beatenStatusText:FindChild("CountLabel")
	end

	local kvs = KeyValues.new()
	
	-- open the file which contains the points
	-- points can be recorded by using "trailblazer_record" concommand
	if kvs:LoadFile("scripts/trailblazer/" .. initPointsFile .. ".txt", SP_MOD) == false then
		error("Cannot open trail blazer file '" .. initPointsFile .. "' !!!")
	end

	local tbl = kvs:GetRoot():ToTable()
	
	-- prepare player car
	local playerStartPos = toVector(tbl.StartPosition)
	local playerStartRot = toVector(tbl.StartRotation)

	local playerCar = gameses:GetPlayerCar()
	MISSION.playerCar = playerCar
	
	playerCar:SetOrigin( playerStartPos )
	playerCar:SetAngles( playerStartRot )
	playerCar:Lock(true)
	
	-- player car collision mask must be set to beat cones
	local collideMask = bit.bor(playerCar:GetCollideMask(), OBJECTCONTENTS_SPECIAL_TRAILBLAZER_CONE)
	playerCar:SetCollideMask( collideMask )

	-- prepare objects
	local sortedKeys = table.sortedKeys(tbl.Positions, function(a, b) return a.Id < b.Id end)
	for _,key in ipairs(sortedKeys) do
	
		local v = tbl.Positions[key]
		
		v.Position = toVector(v.Position);
		v.Direction = toVector(v.Direction);
	
		local coneDistPerc = v.Id / 100
	
		if MISSION.Data.gates then
			local gate = GatePoint(v.Position, v.Direction, coneDistPerc)
			table.insert(MISSION.Data.points, gate)
		else
			local point = Point(v.Position, v.Direction, coneDistPerc)
			table.insert(MISSION.Data.points, point)
		end
	end

	-- functions
	
	MISSION.RefreshLabels = function()
		local formatText = string.format("%d/%d", MISSION.Data.completedPoints, #MISSION.Data.points )
		
		MISSION.coneStatusCounter:SetLabel(formatText)
		
		MISSION.missedCounter:SetLabel(tostring(MISSION.Data.missedPoints))
		
		if MISSION.beatenCounter ~= nil then
			MISSION.beatenCounter:SetLabel(tostring(MISSION.Data.beatenCones))
		end
	end
	
	MISSION.OnCompleted = function()
		local playerCar = MISSION.playerCar
	
		-- complete the game
		playerCar:Lock(true)
		gameHUD:ShowAlert("#WELL_DONE_MESSAGE", 3.5, HUD_ALERT_SUCCESS)
		
		MissionManager:SetRefreshFunc( function() 
			return false 
		end ) 

		gameses:SignalMissionStatus( MIS_STATUS_SUCCESS, 4.0 )
	end
	
	-- advance the cone/gate
	MISSION.Advance = function()
	
		local playerCar = MISSION.playerCar

		local currentPoint = MISSION.Data.points[ MISSION.Data.currentPoint ]
		MISSION.Data.currentPoint = MISSION.Data.currentPoint + 1
		
		if currentPoint then
		
			if currentPoint.hit then
				-- if hit was registered, decrease the time
				if MISSION.Settings.PracticeMode == false then
					gameHUD:ShowScreenMessage(string.format("-%d sec", 1), 1.0)
					MissionManager:AddSeconds( -1 )
				else
					gameHUD:ShowScreenMessage("#OOPSIE_MESSAGE", 1.0)
				end
				
				MISSION.Data.beatenCones = MISSION.Data.beatenCones + 1
			else
				-- add time if were not skipped
				if currentPoint.skip == false then
					if MISSION.Settings.PracticeMode == false then
						MissionManager:AddSeconds( 1 )
					end
					MISSION.Data.completedPoints = MISSION.Data.completedPoints + 1
				else
					MISSION.Data.missedPoints = MISSION.Data.missedPoints + 1
				end
			end
		
			-- schedule the removal
			MissionManager:ScheduleEvent( function()
				currentPoint:Remove()
			end, 1.0 )
		end

		if MISSION.Data.points[ MISSION.Data.currentPoint ] then

			-- spawn next + 3 ahead
			for i,v in range(MISSION.Data.currentPoint, MISSION.Data.currentPoint + 3) do
			
				if MISSION.Data.points[ i ] then
					MISSION.Data.points[ i ]:Spawn()
				end
			end
		else
			MISSION.OnCompleted()
		end
		
		MISSION.RefreshLabels()
	end
	
	-- beginning of mission
	MISSION.WaitForStart = function( delta )
		if MISSION.Data.waitTime > 0 then
		
			if MISSION.Data.waitTimeRound ~= math.floor(MISSION.Data.waitTime) then
				MISSION.Data.waitTimeRound = math.floor(MISSION.Data.waitTime)
				gameHUD:ShowScreenMessage(tostring(MISSION.Data.waitTimeRound+1), 1.0)
			end
		
			MISSION.Data.waitTime = MISSION.Data.waitTime - delta
			return false
		else
			MISSION.Begin()
		end
		
		return true
	end
	
	MISSION.Begin = function()
		local playerCar = MISSION.playerCar

		playerCar:Lock(false)
		
		MISSION.Advance()

		gameHUD:ShowScreenMessage("#CHECKRUN_GO", 2.0)
		
		if MISSION.Settings.PracticeMode then
			MissionManager:ShowTime( true )
		else
			local timeout = if_then_else(MISSION.Data.gates, MISSION.GATES_TIME, MISSION.TRAILBLAZER_TIME)
			MissionManager:EnableTimeout( true, timeout ) -- enable, time
		end

		-- here we start
		MissionManager:SetRefreshFunc( MISSION.Update )
	end
	
	--------------------------------------------------------------------------------

	-- main mission update
	function MISSION.Update( delta )
	
		local playerCar = MISSION.playerCar
	
		local currentPoint = MISSION.Data.points[ MISSION.Data.currentPoint ]
		
		if currentPoint:CheckAndComplete( playerCar:GetOrigin() ) then
			MISSION.Advance()
		end
	
		if MissionManager:IsTimedOut() then
			playerCar:Lock(true)

			gameHUD:ShowAlert("#TIME_UP_MESSAGE", 4.0, HUD_ALERT_DANGER)
			
			MissionManager:SetRefreshFunc( function() 
				return false 
			end ) 
			
			gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
			return false
		end
		
		-- check player vehicle is wrecked
		if CheckVehicleIsWrecked( playerCar, MISSION.Data, delta ) then
			playerCar:Lock(true)
			gameHUD:ShowAlert("#WRECKED_VEHICLE_MESSAGE", 3.5, HUD_ALERT_DANGER)
			
			MissionManager:SetRefreshFunc( function() 
				return false 
			end ) 

			gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
			return false
		end

		return true
	end
	
	if MISSION.Data.gates then
		gameHUD:ShowAlert("#GATES_TITLE_MESSAGE", 4.0, HUD_ALERT_NORMAL)
	else
		gameHUD:ShowAlert("#TRAILBLAZER_TITLE_MESSAGE", 4.0, HUD_ALERT_NORMAL)
	end
	
	MISSION.RefreshLabels()
	
	-- setup
	MissionManager:SetRefreshFunc( MISSION.WaitForStart )
end