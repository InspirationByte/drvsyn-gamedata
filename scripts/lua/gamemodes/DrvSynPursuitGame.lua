--//////////////////////////////////////////////////////////////////////////////////
--// Copyright (C) Inspiration Byte
--// 2009-2021
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- Pursuit game mode
-- By Shurumov Ilya
-- 23 Jan 2021
--------------------------------------------------------------------------------

PursuitGame = {}

PursuitGame.DevMode = false
PursuitGame.LOSE_DISTANCE = 160

PursuitGame.PlayerRubberBandingParams = {
	[15.0] = {
		torqueScale = 1.0,
		maxSpeed = 135,
	},
	[40.0] = {
		torqueScale = 1.2,
		maxSpeed = 150,
	},
	[3000.0] = {
		torqueScale = 1.45,
		maxSpeed = 175,
	}
}

function PursuitGame.ReleaseTarget()
	if MISSION.targetHandle ~= nil then
		gameHUD:RemoveTrackingObject(MISSION.targetHandle)
		MISSION.targetHandle = nil
	end
end

function PursuitGame.OnCompleted()
	local playerCar = gameses:GetPlayerCar()
	local opponentCar = MISSION.opponentCar

	PursuitGame.ReleaseTarget()

	-- lock and disable infinite mass
	opponentCar:SetInfiniteMass(false)
	opponentCar:Lock(true)
	
	-- lock player car too
	playerCar:Lock(true)
	
	-- stop the vehicle playing loaded frames
	gameses:StopCarReplay(opponentCar)

	--show message to player
	if playerCar:GetDamage() == playerCar:GetMaxDamage() then
		gameHUD:ShowAlert("#DONE_BINGO_MESSAGE", 3.5, HUD_ALERT_SUCCESS)
	else
		gameHUD:ShowAlert("#WELL_DONE_MESSAGE", 3.5, HUD_ALERT_SUCCESS)
	end
	
	-- signal success
	gameses:SignalMissionStatus( MIS_STATUS_SUCCESS, 4.0 )
	
	-- stop the timer, updates, everything
	MissionManager:SetRefreshFunc( function() 
		return false 
	end )
end

function PursuitGame.OnFailed(message)

	local playerCar = gameses:GetPlayerCar()
	
	PursuitGame.ReleaseTarget()

	gameHUD:ShowAlert( message, 4.0, HUD_ALERT_DANGER)

	playerCar:Lock(true)
	
	MissionManager:SetRefreshFunc( function() 
		return false 
	end ) 
	
	gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
end

function PursuitGame.LoadOpponentReplay(replayName)
	-- replay must be reset
	gameses:LoadCarReplay(MISSION.opponentCar, "replayData/" .. replayName .. "_" .. MISSION.Data.replayId)
	
	MISSION.targetHandle = gameHUD:AddTrackingObject(MISSION.opponentCar, HUD_DOBJ_IS_TARGET + HUD_DOBJ_CAR_DAMAGE)
end

function PursuitGame.OpponentHit(self, props)
	local playerCar = gameses:GetPlayerCar()
	local opponentCar = MISSION.opponentCar
	
	-- if hit by player car, add huge damage number
	if props.hitBy == playerCar and props.velocity > 4 then
		opponentCar:SetDamage(opponentCar:GetDamage() + 10000)
	end
end

function PursuitGame.Init( playerCar, opponentCar, numHits, timeout, replayId, replayName )

	-- data
	MISSION.Data = {
		waitTime = 3.0,
		waitTimeRound = 4,
		replayId = replayId
	}

	-- disable cops
	MISSION.Settings.EnableCops = false

	-- enable infinite mass on opponent
	opponentCar:SetInfiniteMass(true)
	opponentCar:SetMaxDamage(numHits * 10000)
	opponentCar:Lock(true)
	
	-- also lock player car
	playerCar:Lock(true)

	-- setup opponent car and replay
	MISSION.opponentCar = opponentCar
	MISSION.RandomSeed = replayId
		
	-- functions

	--- Countdown to start the game.
	MISSION.WaitForStart = function( delta )
		if MISSION.Data.waitTime > 0 then
		
			if MISSION.Data.waitTimeRound ~= math.floor(MISSION.Data.waitTime) then
				MISSION.Data.waitTimeRound = math.floor(MISSION.Data.waitTime)
				gameHUD:ShowScreenMessage(tostring(MISSION.Data.waitTimeRound+1), 1.0)
			end
		
			MISSION.Data.waitTime = MISSION.Data.waitTime - delta
			return false
		else
			MISSION.Begin()
		end
		
		return true
	end
	
	--- Starts the game
	MISSION.Begin = function()
		opponentCar:Lock(false)
		playerCar:Lock(false)
		
		-- set opponent car hit registrator
		opponentCar:Set("OnCarCollision", PursuitGame.OpponentHit)
		
		PursuitGame.LoadOpponentReplay(replayName)
		
		gameHUD:ShowScreenMessage("#PURSUIT_GO_MESSAGE", 3.5)
		
		MissionManager:SetRefreshFunc( MISSION.Update )
	end
	
	-- main mission update
	MISSION.Update = function( delta )
	
		-- update player rubber banding
		UpdateRubberBanding(playerCar, opponentCar:GetOrigin(), PursuitGame.PlayerRubberBandingParams)
	
		-- check the conditions
		if opponentCar:IsAlive() == false then
			PursuitGame.OnCompleted()
			return false
		end

		-- check player vehicle is wrecked
		if CheckVehicleIsWrecked( playerCar, MISSION.Data, delta ) then
			PursuitGame.OnFailed( "#WRECKED_VEHICLE_MESSAGE" )
			return false
		end
		
		local dist = length(playerCar:GetOrigin() - opponentCar:GetOrigin())
		
		-- fail on lose
		if dist > PursuitGame.LOSE_DISTANCE then
			PursuitGame.OnFailed( "#TARGET_AWAY_MESSAGE" )
			return false
		end

		-- fail on timeout
		if MissionManager:IsTimedOut() then
			PursuitGame.OnFailed( "#TIME_UP_MESSAGE" )
			return false
		end
		
		return true
	end
	
	-- setup timer
	MissionManager:EnableTimeout( if_then_else(timeout == nil, false, true), timeout or 0 ) -- enable, time

	if PursuitGame.DevMode == true then
		gameses:SetPlayerCar(opponentCar)
		
		opponentCar:Lock(false)
		playerCar:Lock(false)

		gameHUD:ShowScreenMessage("Replay ID "..MISSION.Data.replayId, 3.5)

		MissionManager:SetRefreshFunc( function() 
			return true
		end )
	else
		gameses:SetPlayerCar(playerCar)
		gameses:SetLeadCar(opponentCar)
		
		MISSION.Settings.EnableTraffic = false
		
		gameHUD:ShowAlert( "#PURSUIT_TITLE", 4.0, HUD_ALERT_NORMAL)
		
		MissionManager:SetRefreshFunc( MISSION.WaitForStart )
	end
end