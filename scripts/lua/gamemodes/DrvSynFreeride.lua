--------------------------------------------------------------------------------
-- Take a ride mission script
--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 11 Mar 2015
--------------------------------------------------------------------------------

-- mods can add to all that lists

MenuSecretCars = {
	{"challenger", "'74 Retaliator"},
	{"cop_regis", "'78 Regis Cop Car"},
}

MenuCarsList = {
	{"rollo", "'74 Rollo"},
	{"torino", "'72 Torinio"},
	{"carla", "'77 Carla"},
	{"mustang", "'80 Mustang"},
	{"mustang_f", "'80 Mustang Fastback"},
	{"ranchero", "'78 Rachero Pickup"},
	{"taxi", "'82 Impala Taxi"},
	{"van", "'84 Van"},
	{"k5", "'82 Bruiser"}
}

MenuCityList = {
    {"default", "Syndicate City"}
}

-- Take a Ride 
MenuTimeOfDayList = {
    {"day_clear", "#MENU_TAKEARIDE_WEATHER_DAY"},
	{"day_stormy", "#MENU_TAKEARIDE_WEATHER_DAY_STORM" },
	{"dawn_clear", "#MENU_TAKEARIDE_WEATHER_DAWN" },
	{"night_clear", "#MENU_TAKEARIDE_WEATHER_NIGHT" },
	{"night_stormy", "#MENU_TAKEARIDE_WEATHER_NIGHT_STORM" },
}

-- city time of day music specifier fot Take a Ride
CityTimeOfDayMusic = {
	["default"] = {
		day_clear = "chicago_day",
		day_stormy = "rio_day",
		dawn_clear = "vegas_night",
		night_clear = "chicago_night",
		night_stormy = "havana_night"
	}
}

function LoadFreeride(levelName, envName, musicName)

	local cityMusic = CityTimeOfDayMusic[levelName]
	
	if cityMusic == nil then
		cityMusic = CityTimeOfDayMusic["default"]
	end

	-- set level and env name
	world:SetLevelName(levelName)
	world:SetEnvironmentName(envName)

	local result = LoadMission("freeride", levelName)
	
	if not result then	-- just load failsafe
		result = LoadMission("freeride", "failsafe_freeride")
	end
	
	SetMusicName(musicName or cityMusic[envName])

	return result
end


----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

local g_car = console.FindCvar("g_car")

FreerideGame = {}

function FreerideGame.Init( car_name, car_color, spawnpoint_name )

	-- put globals here
	MISSION.Data = {
		startPos = Vector3D.new(54,10.31,-45),
		startAng = Vector3D.new(0,0,0),
		notOnGroundTime = 10.0
	}
	
	local freerideSpawn = objects:FindObjectOnLevel(spawnpoint_name)

	MISSION.Data.startPos = if_then_else(freerideSpawn.position ~= nil, freerideSpawn.position, Vector3D.new(0,0,0))
	MISSION.Data.startAng = if_then_else(freerideSpawn.rotation ~= nil, freerideSpawn.rotation, Vector3D.new(0,0,0))

	-- car name		maxdamage	pos ang
	local playerCar = gameses:CreateCar(car_name, CAR_TYPE_NORMAL)

	if playerCar == nil then
		return
	end
	
	MISSION.playerCar = playerCar
	
	playerCar:SetMaxDamage(16)
	playerCar:SetColorScheme( car_color )

	playerCar:SetOrigin( MISSION.Data.startPos )
	playerCar:SetAngles( MISSION.Data.startAng )
	
	playerCar:AlignToGround()
	
	playerCar:Spawn()
	
	gameses:SetPlayerCar( playerCar )
	gameHUD:ShowScreenMessage("#TAKE_RIDE_TITLE", 3.5)
	
	-- here we start
	MissionManager:SetRefreshFunc( 
		function( delta )
			local playerCar = gameses:GetPlayerCar()

			UpdateCops( playerCar, delta )

			if CheckVehicleIsWrecked( playerCar, MISSION.Data, delta ) then
				playerCar:Lock(true)
				
				gameHUD:ShowAlert("#WRECKED_VEHICLE_MESSAGE", 3.5, HUD_ALERT_DANGER)
				
				MissionManager:SetRefreshFunc( function() 
					return false 
				end ) 

				gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
				return false
			end
			
			return true
		end
	)
	
end

function cmd_freeride( args )

	if #args == 0 then
		MsgError("freeride: arguments required")
		return
	end
	
	local level = args[1]
	local env = if_then_else(#args>=2, function() return args[2] end, "day_clear")
	local music = if_then_else(#args>=3, function() return args[3] end, "vegasday")

	if LoadFreeride(level, env, music) then
		EqStateMgr.ChangeStateType(GAME_STATE_GAME)
	end
end

cmd_freeride = console.FindOrCreateCommand("freeride", cmd_freeride, "Launches freeride game")
cmd_freeride:SetVariantsCallback(function() 
	return {"default", "phys_test"}
end)