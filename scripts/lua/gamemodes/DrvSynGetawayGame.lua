--//////////////////////////////////////////////////////////////////////////////////
--// Copyright (C) Inspiration Byte
--// 2009-2017
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- Getaway game mode
-- By Shurumov Ilya
-- 29 Aug 2019
--------------------------------------------------------------------------------

GetawayGame = {}

function GetawayGame.Init( copCarPos, copCarAngles, timeout )

	local playerCar = gameses:GetPlayerCar()

	-- setup player car
	playerCar:SetMaxSpeed(140)
	playerCar:SetTorqueScale(1.1)
	playerCar:SetMaxDamage(6.0)
	playerCar:SetFelony(0.1)

	-- setup AI props
	MISSION.Settings.EnableCops = false

	local opponentCar = gameses:CreatePursuerCar("cop_regis", PURSUER_TYPE_COP)

	opponentCar:SetOrigin( copCarPos )
	opponentCar:SetAngles( copCarAngles )

	opponentCar:Spawn()
	opponentCar:AlignToGround()
	
	opponentCar:SetMaxDamage(100.0)
	opponentCar:SetMaxSpeed(160)
	opponentCar:SetTorqueScale(1.8)
	
	opponentCar:SetPursuitTarget( playerCar )
	opponentCar:BeginPursuit(0.0)

	-- start immediately
	SetMusicState(MUSIC_STATE_PURSUIT)
	
	MissionManager:EnableTimeout( true, timeout ) -- enable, time
	
	-- functions
	
	-- main mission update
	function MISSION.Update( delta )
		if playerCar:GetPursuedCount() == 0 then
			gameHUD:ShowAlert("#WELL_DONE_MESSAGE", 3.5, HUD_ALERT_SUCCESS)
			
			if objects:IsValidObject(opponentCar) then
				opponentCar:Enable(false)
			end

			MissionManager:SetRefreshFunc( function() 
				return false 
			end ) 

			gameses:SignalMissionStatus( MIS_STATUS_SUCCESS, 4.0 )
			return false
		end
		
		-- check player vehicle is wrecked
		if CheckVehicleIsWrecked( playerCar, MISSION.Data, delta ) then
			gameHUD:ShowAlert("#WRECKED_VEHICLE_MESSAGE", 3.5, HUD_ALERT_DANGER)
			
			playerCar:Lock(true)
			
			MissionManager:SetRefreshFunc( function() 
				return false 
			end ) 

			gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
			return false
		end

		if MissionManager:IsTimedOut() then
			--playerCar:Lock(true)
			
			gameHUD:ShowAlert( "#TIME_UP_MESSAGE", 4.0, HUD_ALERT_DANGER)
			
			MissionManager:SetRefreshFunc( function() 
				return false 
			end ) 
			
			gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
			
			return false
		end
		
		return true
	end
	
	-- setup
	MissionManager:ScheduleEvent( function()
		gameHUD:ShowScreenMessage("#LOSE_TAIL_MESSAGE", 3.5)
		MissionManager:SetRefreshFunc( MISSION.Update )
	end, 1.0 )
	
end
