--//////////////////////////////////////////////////////////////////////////////////
--// Copyright (C) Inspiration Byte
--// 2009-2017
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- Checkpoint game mode
-- By Shurumov Ilya
-- 29 Aug 2017
--------------------------------------------------------------------------------

CheckpointGame = {}

local Checkpoint = class()
	function Checkpoint:__init( pos, addsec )
		self.pos = pos
		--self.obj = nil
		self.addsec = addsec
		
	end
	
	function Checkpoint:Enable()
		self.targetHandle = gameHUD:AddMapTargetPoint( self.pos )
	end
	
	function Checkpoint:Disable()
		gameHUD:RemoveTrackingObject(self.targetHandle)
	end
	
	function Checkpoint:CheckAndComplete(pos)
	
		if length(pos - self.pos) < MISSION.CHECKPOINT_CHECK_RADIUS then
			self:Disable()
			return true
		end
	
		return false
	end
	
CheckpointGame.Checkpoint = Checkpoint
	
--------------------------------------------------------------------------------
function CheckpointGame.Init( initCheckpoints, checkpointRun )

	-- constants
	MISSION.CHECKPOINT_CHECK_RADIUS = 2

	-- data
	MISSION.Data = {
		checkidx = 0,
		waitTime = 3.0,
		waitTimeRound = 4,
		checkpoints = {}
	}
	
	if checkpointRun then
		SetMusicState(MUSIC_STATE_PURSUIT)
	end

	MISSION.playerCar = gameses:GetPlayerCar()
	MISSION.playerCar:Lock(true)
	
	for i,v in ipairs(initCheckpoints) do
	
		local newCheckpoint = CheckpointGame.Checkpoint(v[1], v[2])
	
		table.insert(MISSION.Data.checkpoints, newCheckpoint);
	end
	
	-- settings
	MISSION.Settings.EnableCops = false
	
	-- HUD init
	local checkpointStatusText = gameHUD:FindChildElement("CheckStatusText")
	MISSION.checkpointStatusCounter = checkpointStatusText:FindChild("CountLabel")
	
	sounds:Precache( "menu.click" )
	
	-- functions
	
	MISSION.UpdateLabels = function()
		local formatText = string.format("%d/%d", MISSION.Data.checkidx, #MISSION.Data.checkpoints )
		MISSION.checkpointStatusCounter:SetLabel(formatText)
	end
	
	MISSION.OnCompleted = function()
		local playerCar = MISSION.playerCar
	
		-- complete the game
		playerCar:Lock(true)
		gameHUD:ShowAlert("#WELL_DONE_MESSAGE", 3.5, HUD_ALERT_SUCCESS)
		
		MissionManager:SetRefreshFunc( function() 
			return false 
		end ) 

		gameses:SignalMissionStatus( MIS_STATUS_SUCCESS, 4.0 )
	end
	
	-- Advances the checkpoint.
	MISSION.AdvanceCheckpoint = function()
	
		local playerCar = MISSION.playerCar
		
		MISSION.UpdateLabels()

		local currentCheckIdx = MISSION.Data.checkidx
		local currentCheckpoint = MISSION.Data.checkpoints[ currentCheckIdx ]

		MISSION.Data.checkidx = MISSION.Data.checkidx + 1;

		if MISSION.Data.checkpoints[ MISSION.Data.checkidx ] then
			-- add time set by checkpoint and play a sound
			if currentCheckpoint then
				if checkpointRun then
					gameHUD:ShowScreenMessage(string.format("+%d sec", currentCheckpoint.addsec), 1.0)
					MissionManager:AddSeconds( currentCheckpoint.addsec )
				else
					gameHUD:ShowScreenMessage(string.format("Checkpoint %d", currentCheckIdx), 1.0)
				end
				
				-- play sound
				sounds:Emit( EmitParams.new("menu.click", playerCar:GetOrigin()) )
			end
			
			-- enable next checkpoint
			MISSION.Data.checkpoints[ MISSION.Data.checkidx ]:Enable()
		else
			MISSION.OnCompleted()
		end
	end
	
	--- Countdown to start the game.
	MISSION.WaitForStart = function( delta )
		if MISSION.Data.waitTime > 0 then
		
			if MISSION.Data.waitTimeRound ~= math.floor(MISSION.Data.waitTime) then
				MISSION.Data.waitTimeRound = math.floor(MISSION.Data.waitTime)
				gameHUD:ShowScreenMessage(tostring(MISSION.Data.waitTimeRound+1), 1.0)
			end
		
			MISSION.Data.waitTime = MISSION.Data.waitTime - delta
			return false
		else
			MISSION.Begin()
		end
		
		return true
	end

	--- Starts the game
	MISSION.Begin = function()
		local playerCar = MISSION.playerCar

		playerCar:Lock(false)
		gameHUD:Enable(true)
		
		gameHUD:ShowScreenMessage("#CHECKRUN_GO", 2.0)
		
		if checkpointRun then
			MissionManager:EnableTimeout( true, 15 ) -- enable, time
		else
			MissionManager:ShowTime( true ) -- enable, time
		end
		
		MISSION.AdvanceCheckpoint()
		
		-- here we start
		MissionManager:SetRefreshFunc( MISSION.Update )
	end

	--------------------------------------------------------------------------------

	-- main mission update
	function MISSION.Update( delta )

		local checkpoint = MISSION.Data.checkpoints[ MISSION.Data.checkidx ]
		local playerCar = MISSION.playerCar
		
		-- Process point-sphere intersection and advance if done
		if checkpoint:CheckAndComplete( playerCar:GetOrigin() ) then
			MISSION.AdvanceCheckpoint()
		end

		-- process timeout
		if MissionManager:IsTimedOut() then
			playerCar:Lock(true)

			gameHUD:ShowAlert("#TIME_UP_MESSAGE", 4.0, HUD_ALERT_DANGER)
			
			MissionManager:SetRefreshFunc( function() 
				return false 
			end ) 
			
			gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
			return false
		end
		
		-- check player vehicle is wrecked
		if CheckVehicleIsWrecked( playerCar, MISSION.Data, delta ) then
			playerCar:Lock(true)
			gameHUD:ShowAlert("#WRECKED_VEHICLE_MESSAGE", 3.5, HUD_ALERT_DANGER)
			
			MissionManager:SetRefreshFunc( function() 
				return false 
			end ) 

			gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
			return false
		end

		return true
	end
	
	gameHUD:ShowAlert("#CHECKRUN_TITLE_MESSAGE", 4.0, HUD_ALERT_NORMAL)
	
	MISSION.UpdateLabels()
	
	-- setup
	MissionManager:SetRefreshFunc( MISSION.WaitForStart )
end
