SurvivalGame = {}

function SurvivalGame.Init( copCount )

    local playerCar = gameses:GetPlayerCar()
	-- car name		maxdamage	pos ang
    MISSION.playerCar = playerCar
    
	MISSION.Data = {}
	
	-- force cop difficulty
	table.combine(MISSION.Settings, CopDifficultyTable[3])
	
	-- adjust cops
	MISSION.Settings.EnableCops = true
	MISSION.Settings.EnableRoadBlock = false
	MISSION.Settings.CopAccelerationScale = 5.0
	MISSION.Settings.CopMaxSpeed = 1000;
	MISSION.Settings.MinCops = copCount
	MISSION.Settings.MaxCops = copCount
	MISSION.Settings.MaxCopDamage = 100000;


	MissionManager:ShowTime( true ) -- enable time display
	
	gameHUD:ShowScreenMessage("#SURVIVAL_TITLE", 3.5)
	
	-- this is temporary to spawn cops
	MISSION.Settings.CopRespawnInterval = 0
    MISSION.Settings.CopWantedRespawnInterval2 = 0
    
    function MISSION.OnFailed()
        gameses:SetLeadCar( MISSION.playerCar )
        MISSION.playerCar:Lock(true)
    end
    
    -- main mission update
    function MISSION.Update( delta )

        local playerCar = MISSION.playerCar

        UpdateCops( playerCar, delta )
        ai:MakePursued( playerCar )

        -- check player vehicle is wrecked
        if CheckVehicleIsWrecked( playerCar, MISSION.Data, delta ) then
            gameHUD:ShowAlert("#WRECKED_VEHICLE_MESSAGE", 3.5, HUD_ALERT_DANGER)
            
            MISSION.OnFailed()
            
            MissionManager:SetRefreshFunc( function() 
                return false 
            end ) 

            gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
            return false
        end
        
        return true
    end

	-- here we start
	MissionManager:SetRefreshFunc( function(dt)
	
		-- do first game update frame to spawn cops and adjust params
		ai:MakePursued( playerCar )
		playerCar:SetFelony(1.0)

		-- continue with standard update routine
		MissionManager:SetRefreshFunc(MISSION.Update)
		
		return false
	end	)
end