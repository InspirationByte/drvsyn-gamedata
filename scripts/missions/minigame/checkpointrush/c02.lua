--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2017
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- Mission script for checkpoints
--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 11 Mar 2015
--------------------------------------------------------------------------------

world:SetLevelName("default")
world:SetEnvironmentName("day_rainy")

MISSION.MusicScript = "la_night"
MISSION.Hud = "resources/hud/checkpoint.res"

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

function MISSION.Init()
	local playerCar = gameses:CreateCar("torino", CAR_TYPE_NORMAL)

	playerCar:SetOrigin( Vector3D.new(246.11,8.95,759.49) )
	playerCar:SetAngles( Vector3D.new(0, 165, 0) )
	playerCar:Spawn()
	playerCar:SetColorScheme( 2 )
	
	gameses:SetPlayerCar( playerCar )
	
	-- init game mode
	
	local checkpoints = {
		{Vector3D.new(140,5.95,670), 15 },
		{Vector3D.new(96,3.05,467), 15 },
		{Vector3D.new(339,10.44,437), 15 },
		{Vector3D.new(308, 0.34, 272), 15 },
		{Vector3D.new(439, 8.96, 500), 15 },
		{Vector3D.new(340,17.83,770), 0 },
	}
	
	CheckpointGame.Init( checkpoints, true )
end