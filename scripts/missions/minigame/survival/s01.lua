--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2015
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- Cop test script
--------------------------------------------------------------------------------

world:SetLevelName("default")
world:SetEnvironmentName("dusk_overcast")

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

MISSION.MusicScript = "rio_day"

MISSION.Init = function()

	-- car name		maxdamage	pos ang
	local playerCar = gameses:CreateCar("mustang_f", CAR_TYPE_NORMAL)

	playerCar:SetOrigin( Vector3D.new(41.44, 0.48, 107.64) )
	playerCar:SetAngles( Vector3D.new(-128.42, -85.25, 128.33) )
	playerCar:SetColorScheme(3)
	playerCar:Spawn()

	gameses:SetPlayerCar( playerCar )
	
	SurvivalGame.Init( 8 )
end

--------------------------------------------------------------------------------
