--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2017
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- Mission script for trail blazer/gates
--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 11 Sep 2018
--------------------------------------------------------------------------------

world:SetLevelName("default")
world:SetEnvironmentName("night_clear")

MISSION.MusicScript = "frisco_night"
MISSION.Hud = "resources/hud/gates.res"

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

function MISSION.Init()
	local playerCar = gameses:CreateCar("torino", CAR_TYPE_NORMAL)
	playerCar:Spawn()
	playerCar:SetColorScheme( 1 )
	gameses:SetPlayerCar( playerCar )

	-- init game mode
	TrailblazerGame.Init( "gates/gt02", true )
end