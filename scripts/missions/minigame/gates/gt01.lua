--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2017
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- Mission script for trail blazer/gates
--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 11 Sep 2018
--------------------------------------------------------------------------------

world:SetLevelName("default")
world:SetEnvironmentName("dawn_clear")

MISSION.MusicScript = "nyc_day"
MISSION.Hud = "resources/hud/gates.res"

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

function MISSION.Init()
	local playerCar = gameses:CreateCar("mustang_f", CAR_TYPE_NORMAL)
	playerCar:Spawn()
	playerCar:SetColorScheme( 2 )
	gameses:SetPlayerCar( playerCar )

	-- init game mode
	TrailblazerGame.Init( "gates/gt01", true )
end