--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2017
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- Mission script for trail blazer/gates
--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 11 Sep 2018
--------------------------------------------------------------------------------

world:SetLevelName("default")
world:SetEnvironmentName("day_clear")

MISSION.MusicScript = "havana_day"
MISSION.Hud = "resources/hud/trailblazer.res"

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

function MISSION.Init()	
	local playerCar = gameses:CreateCar("carla", CAR_TYPE_NORMAL)
	playerCar:Spawn()
	playerCar:SetColorScheme( 3 )
	gameses:SetPlayerCar( playerCar )
	
	-- init game mode
	TrailblazerGame.Init( "gt01", false )
end

