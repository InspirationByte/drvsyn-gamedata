--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2015
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- Test mission script on "Default"
--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 05 Dec 2014
--------------------------------------------------------------------------------

world:SetLevelName("default")
world:SetEnvironmentName("dusk_overcast")

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

MISSION.replayId = 1

MISSION.MusicScript = "nyc_day"

MISSION.Init = function()
	MISSION.Data = {
		loseTailMessage = false,
		cutsceneTime = 2.0,
	}
	
	-- car name		maxdamage	pos ang
	local playerCar = gameses:CreateCar("carla", CAR_TYPE_NORMAL)
	MISSION.playerCar = playerCar

	playerCar:SetMaxDamage(12.0)
	
	playerCar:SetOrigin( vec3(36.90, 3.13, 401.20) )
	playerCar:SetAngles( vec3(-179.86, -8.68, 179.98) )
	
	playerCar:Spawn()
	playerCar:SetColorScheme( 2 )
	
	local opponentCar = gameses:CreateCar("torino", CAR_TYPE_NORMAL)
	MISSION.opponentCar = opponentCar
	
	-- opponent initial position
	opponentCar:SetOrigin( vec3(44.07, 3.00, 369.89) )
	opponentCar:SetAngles( vec3(-179.74, -0.10, 176.75) )

	opponentCar:Spawn()
	opponentCar:SetColorScheme( 1 )
	opponentCar:SetLight(1, true)
	
	-- finally init game
	PursuitGame.Init( playerCar, opponentCar, 5, 98, MISSION.replayId, "p01" )

	-- count for the next game
	MISSION.replayId = MISSION.replayId + 1
	
	if MISSION.replayId > 5 then
		MISSION.replayId = 1
	end
end
