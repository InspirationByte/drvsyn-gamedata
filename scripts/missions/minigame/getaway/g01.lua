--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2015
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- Cop test script
--------------------------------------------------------------------------------

world:SetLevelName("default")
world:SetEnvironmentName("dusk_night_transition")

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

MISSION.MusicScript = "vegas_day"
	
MISSION.Init = function()
		
	local GameProps = {
		CopPosition = Vector3D.new(174.96,0.31,124.11),
		CopAngles = Vector3D.new(0, 90, 0),
		Timeout = 98
	}
		
	-- car name		maxdamage	pos ang
	local playerCar = gameses:CreateCar("challenger", CAR_TYPE_NORMAL)

	playerCar:SetOrigin( Vector3D.new(134.96,0.51,124.11) )
	playerCar:SetAngles( Vector3D.new(0,90,0) )
	playerCar:Spawn()
	playerCar:SetColorScheme(2)
	
	gameses:SetPlayerCar( playerCar )
	
	GetawayGame.Init(GameProps.CopPosition, GameProps.CopAngles, GameProps.Timeout)

end