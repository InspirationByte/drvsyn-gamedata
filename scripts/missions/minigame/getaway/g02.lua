--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2015
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- Cop test script
--------------------------------------------------------------------------------

world:SetLevelName("default")
world:SetEnvironmentName("day_clear")

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

MISSION.MusicScript = "la_day"
	
MISSION.Init = function()

	local GameProps = {
		CopPosition = Vector3D.new(338.69,14.95,612.68),
		CopAngles = Vector3D.new(0,180,0),
		Timeout = 98
	}
		
	-- car name		maxdamage	pos ang
	local playerCar = gameses:CreateCar("mustang_f", CAR_TYPE_NORMAL)

	playerCar:SetOrigin( Vector3D.new(338.69,15.09,587.68) )
	playerCar:SetAngles( Vector3D.new(0,180,0) )
	playerCar:Spawn()
	playerCar:SetColorScheme(3)
	
	gameses:SetPlayerCar( playerCar )
	
	GetawayGame.Init(GameProps.CopPosition, GameProps.CopAngles, GameProps.Timeout)
	
end

-------------------------------------------------------------------------------
