--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2017
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- Mission script for checkpoints
--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 11 Mar 2015
--------------------------------------------------------------------------------

world:SetLevelName("default")
world:SetEnvironmentName("dusk_clear")

MISSION.MusicScript = "rio_night"
MISSION.Hud = "resources/hud/checkpoint.res"

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

function MISSION.Init()
	local playerCar = gameses:CreateCar("mustang", CAR_TYPE_NORMAL)

	playerCar:SetOrigin( Vector3D.new(58.50, 0.53, -126.01) )
	playerCar:SetAngles( Vector3D.new(-0.17, 0.60, -0.00) )
	playerCar:Spawn()
	playerCar:SetColorScheme( 3 )
	
	gameses:SetPlayerCar( playerCar )
	
	-- init game mode
	
	local checkpoints = {
		{Vector3D.new(-64, 0.54, 33.28), 10 },
		{Vector3D.new(95.68, 3.14, 340.6), 15 },
		{Vector3D.new(95.12,4.63,551.8), 10 },
		{Vector3D.new(340.25, 15.13, 499.15), 10 },
		{Vector3D.new(598.41, 0.64, 400.8), 15 },
		{Vector3D.new(390.01,4.64,437.59), 0 },
	}
	
	CheckpointGame.Init( checkpoints, false )
end