--------------------------------------------------------------------------------
-- Default mission script loader
-- remove this from release build
--------------------------------------------------------------------------------

missions = {
	["intro"] = {
		"intro",
	},
	["freeride"] = {
		"default",	-- comes as level name
	},
	["minigame/checkpoint"] = {
		{"c01", "#MENU_GAME_CHECKPOINT_01"},
		{"c02", "#MENU_GAME_CHECKPOINT_02"},
	},
	["minigame/checkpointrush"] = {
		{"c01", "#MENU_GAME_CHECKPOINT_01"},
		{"c02", "#MENU_GAME_CHECKPOINT_02"},
	},
	["minigame/getaway"] = {
		{"g01", "#MENU_GAME_LOSETHETAIL_01"},
		{"g02", "#MENU_GAME_LOSETHETAIL_02"},
	},
	["minigame/pursuit"] = {
		{"p01", "#MENU_GAME_PURSUIT_01"},
	},
	["minigame/survival"] = {
		{"s01", "#MENU_GAME_SURVIVAL_01"},
	},
	["minigame/gates"] = {
		{"gt01", "#MENU_GAME_GATES_01"},
		{"gt02", "#MENU_GAME_GATES_02"},
	},
	["minigame/trailblazer"] = {
		{"gt01", "#MENU_GAME_TRAILBLAZER_01"},
	},
	["rollingdemo"] = {
		"demo01",
	},
	["story"] = {
		"m02",
	},
	["story_demo"] = {
		"mission1",
		"mission2",
		{screen = "story_demo_end"}
	},
	["training"] = {
		"iview",
	},
	["_testing"] = {
		"utrailer",
		"cop_test",
		"ai_traffic_test",
	},
}