--------------------------------------------------------------------------------
-- Intro title
--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 11 Mar 2015
--------------------------------------------------------------------------------

world:SetLevelName("_intro")
world:SetEnvironmentName("night_stormy")

MISSION.MusicScript = "music.intro"

MISSION.Init = function()

	-- put globals here
	MISSION.Data = {
		startPos = Vector3D.new(11.48, 0.60, -70.20),
		startAng = Vector3D.new(0,-90,0),
		startVel = Vector3D.new(18,0,0)
	}
	
	local cop = gameses:CreateCar("cop_regis",CAR_TYPE_NORMAL)
	cop:SetOrigin( MISSION.Data.startPos )
	cop:SetAngles( MISSION.Data.startAng )
	cop:Spawn()

	cop:SetVelocity( MISSION.Data.startVel )
	
	--local replayDuration1 = gameses:LoadCarReplay(cop, "replayData/intro_cop")
	
	-- spawn misc cars
	local car = gameses:CreateCar("mustang_f", CAR_TYPE_NORMAL);
	car:SetOrigin( Vector3D.new(145.95, 0.64, -61.27) )
	car:SetAngles( Vector3D.new(177.22, 85.17, 177.23) )
	car:SetColorScheme(1)
	car:Spawn()
	
	car = gameses:CreateCar("torino", CAR_TYPE_NORMAL);
	car:SetOrigin( Vector3D.new(138.68, 0.45, -49.66) )
	car:SetAngles( Vector3D.new(-179.87, 30.93, -179.93) )
	car:SetColorScheme(3)
	car:Spawn()
	
	--replayDuration1 = gameses:LoadCarReplay(car, "replayData/intro_torino")
	
	car = gameses:CreateCar("carla", CAR_TYPE_NORMAL);
	car:SetOrigin( Vector3D.new(130.48, 0.60, -46.35) )
	car:SetAngles( Vector3D.new(-179.45, -5.95, 179.94) )
	car:SetColorScheme(5)
	car:Spawn()
	
	--replayDuration1 = gameses:LoadCarReplay(car, "replayData/intro_carla")
	
	
	gameses:SetPlayerCar( car )
	
	--local replayDuration1 = gameses:LoadCarReplay(MISSION.cutGang1, "replayData/mission1_cut_car1")
	
	gameHUD:FadeIn(false)

	MissionManager:ScheduleEvent(function() 
		gameHUD:FadeOut()
		gameses:SignalMissionStatus(MIS_STATUS_SUCCESS, 2);
	end, 22)
	
	-- here we start
	MissionManager:SetRefreshFunc( MISSION.Update )
end

--------------------------------------------------------------------------------

-- main mission update
MISSION.Update = function( delta )

	
	
	return true
end