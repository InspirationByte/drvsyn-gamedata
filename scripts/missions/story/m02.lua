--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2015
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- "City" mission, ride to airport
--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 12 Apr 2016
--------------------------------------------------------------------------------

world:SetLevelName("city")
world:SetEnvironmentName("night_stormy")
SetMusicName("chicagonight")

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

MISSION.Init = function()
	MISSION.Data = {
		targetPosition = Vector3D.new(346,0.6,-1808)
	}
	
	MISSION.Settings.EnableCops = true
	
	local playerCar = gameses:CreateCar("mustang", CAR_TYPE_NORMAL)
	
	MISSION.playerCar = playerCar
	
	playerCar:SetMaxDamage(12.0)
	
	playerCar:SetOrigin( Vector3D.new(-107.704,0.669,1643.71) )
	playerCar:SetAngles( Vector3D.new(173.173,84.87, 169.73) )
	
	--playerCar:SetOrigin( Vector3D.new(102.133, 0.62, -1814.9) )
	--playerCar:SetAngles( Vector3D.new(120.318,-85.275, -120.312) )
	
	playerCar:Spawn()
	playerCar:SetColorScheme( 5 )
	playerCar:SetTorqueScale(1.1)

	-- for the load time, set player car
	gameses:SetPlayerCar( playerCar )
	
	MISSION.targetHandle = gameHUD:AddMapTargetPoint(MISSION.Data.targetPosition)

	MissionManager:EnableTimeout( true, 176 ) -- enable, time

	-- here we start
	MissionManager:SetRefreshFunc( MISSION.Update )
	
	gameHUD:ShowScreenMessage("Get client to the airport.", 3.5)
end

--------------------------------------------------------------------------------

function MISSION.OnDone()
	gameHUD:RemoveTrackingObject(MISSION.targetHandle)

	playerCar:Lock(true)
	
	return true
end

function MISSION.Outro()
	-- load replay
	local replayDuration = gameses:LoadCarReplay(MISSION.playerCar, "replayData/m02_arrival")
	
	-- todo: setup cameras

	-- schedule mission success
	MissionManager:ScheduleEvent( function() 
		gameHUD:ShowScreenMessage("Well done, he is now leaving the city.", 3.5)
		gameses:SignalMissionStatus( MIS_STATUS_SUCCESS, 4.0 )
		MISSION.playerCar:Lock( true )
	end, replayDuration )
end

-- main mission update
MISSION.Update = function( delta )

	local camera = world:GetView()

	local playerCar = MISSION.playerCar
	
	UpdateCops( playerCar, delta )

	-- check player vehicle is wrecked
	if CheckVehicleIsWrecked( playerCar, MISSION.Data, delta ) then
		gameHUD:ShowScreenMessage("#WRECKED_VEHICLE_MESSAGE", 3.5)
		
		MISSION.OnDone()
		
		MissionManager:SetRefreshFunc( function() 
			return false 
		end ) 

		gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
		return false
	end

	if length(playerCar:GetOrigin() - MISSION.Data.targetPosition) < 100.0 then
	
		gameHUD:RemoveTrackingObject(MISSION.targetHandle)
		
		-- TODO: cutscene black bar curtains
		MissionManager:ScheduleEvent( MISSION.Outro, 1.0 )
	
		MissionManager:SetRefreshFunc( function() 
			return false 
		end ) 
	
		return false
	end
	
	if MissionManager:IsTimedOut() then
		OnMissionDone()
	
		gameHUD:ShowScreenMessage("#TIME_UP_MESSAGE", 3.5)
		
		MissionManager:SetRefreshFunc( function() 
			return false 
		end ) 
		
		gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
		return false
	end
	
	return true
end