--------------------------------------------------------------------------------
-- Default mission script
--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 11 Mar 2015
--------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

world:SetLevelName("default")
world:SetEnvironmentName("day_clear")
SetMusicName("miami_day")

MISSION.Init = function()

	world.OnRegionLoaded = function( self, args )
	--	Msg( string.format(" + Region %d loaded\n", args.regionId) )
	end

	world.OnRegionUnloaded = function( self, args )
	--	Msg( string.format(" - Region %d unloaded\n", args.regionId) )
	end

	-- put globals here
	MISSION.Data = {
		startPos = Vector3D.new(579.02, 9.23, 605.61),
		startAng = Vector3D.new(0.09, -26.49, -0.03),
		notOnGroundTime = 10.0
	}

	-- car name		maxdamage	pos ang
	MISSION.playerCar = gameses:CreateCar("mustang_f",CAR_TYPE_NORMAL)
	MISSION.playerCar:SetMaxDamage(16)
	MISSION.playerCar:SetColorScheme(1)
	
	MISSION.playerCar:SetOrigin( MISSION.Data.startPos )
	MISSION.playerCar:SetAngles( MISSION.Data.startAng )
	
	MISSION.playerCar:Spawn()
	
	gameses:SetPlayerCar( MISSION.playerCar )
	
	-- here we start
	MissionManager:SetRefreshFunc( MISSION.Update )
end

--------------------------------------------------------------------------------

-- main mission update
MISSION.Update = function( delta )
	local playerCar = gameses:GetPlayerCar()

	UpdateCops( playerCar, delta )
	
	return true
end