--------------------------------------------------------------------------------
-- Default mission script
--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 11 Mar 2015
--------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

local g_car = console.FindCvar("g_car")

world:SetLevelName("city")
world:SetEnvironmentName("dawn_clear")

function DefaultMission_Init()

	SetMusicName("vegasnight")

	camera = world:GetCameraParams()

	world.OnRegionLoaded = function( self, args )
	--	Msg( string.format(" + Region %d loaded\n", args.regionId) )
	end

	world.OnRegionUnloaded = function( self, args )
	--	Msg( string.format(" - Region %d unloaded\n", args.regionId) )
	end

	-- put globals here
	defaultMissionData = {
		startPos = Vector3D.new(-675,0.52,1334),
		startAng = Vector3D.new(0,-90,0),
		notOnGroundTime = 10.0
	}

	-- car name		maxdamage	pos ang
	playerCar = gameses:CreateCar(g_car:GetString(),CAR_TYPE_NORMAL)
	playerCar:SetMaxDamage(12)
	playerCar:SetColorScheme(1)
	
	playerCar:SetOrigin( defaultMissionData.startPos )
	playerCar:SetAngles( defaultMissionData.startAng )
	
	playerCar:Spawn()
	
	--ai:SetMaxCops(4)
	
	gameses:SetPlayerCar( playerCar )
	
	-- here we start
	missionmanager:SetRefreshFunc( DefaultMission_Update )
	
	gameHUD:ShowScreenMessage("#TAKE_RIDE_TITLE", 3.5)
	-- gameses:BeginGame()
end

--------------------------------------------------------------------------------

-- main mission update
function DefaultMission_Update( delta )

	UpdateCops( playerCar, delta )

	if CheckVehicleIsWrecked( playerCar, defaultMissionData, delta ) then
		playerCar:Lock(true)
		
		gameHUD:ShowScreenMessage("#WRECKED_VEHICLE_MESSAGE", 3.5)
		
		missionmanager:SetRefreshFunc( function() 
			return false 
		end ) 

		gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
		return false
	end
	
	return true
end

-- put mission initializer
missionmanager:Init( DefaultMission_Init )