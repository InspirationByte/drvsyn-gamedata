--------------------------------------------------------------------------------
-- Default mission script
--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 11 Mar 2015
--------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

world:SetLevelName("default")
world:SetEnvironmentName("day_clear")
SetMusicName("vegasnight")

local g_car = console.FindCvar("g_car")

local g_recordCones = console.FindOrCreateCvar("g_recordCones", "0", "Disables mission timer", CV_CHEAT)

OBJECTCONTENTS_SPECIAL_CONE = OBJECTCONTENTS_CUSTOM_START

function Mission_Init()

	-- put globals here
	missionData = {
		startpos = Vector3D.new(-284,0.52,-462),
		startang = Vector3D.new(175,-83,-175),
		notOnGroundTime = 10.0,
		numCones = 0
	}
	
	ai:SetCopsEnabled(false)
	
	missionData.coneLastPos = missionData.startpos

	-- car name		maxdamage	pos ang
	playerCar = gameses:CreateCar(g_car:GetString(),CAR_TYPE_NORMAL)
	playerCar:SetMaxDamage(12)
	playerCar:SetColorScheme(1)
	
	playerCar:SetOrigin( missionData.startpos )
	playerCar:SetAngles( missionData.startang )
	
	playerCar:Spawn()
	--playerCar:SetCollideMask( playerCar:GetCollideMask() + OBJECTCONTENTS_SPECIAL_CONE )
	
	gameses:SetPlayerCar( playerCar )
	
	-- here we start
	missionmanager:SetRefreshFunc( Mission_Update )
	
	-- gameses:BeginGame()
end

--------------------------------------------------------------------------------

-- main mission update
function Mission_Update( delta )

	local coneDistPerc = missionData.numCones / 100
	local coneDistRatio 	= (1.0 - coneDistPerc)*1.0
	local coneIntervalRatio = 1.0

	if g_recordCones:GetBool() and length(missionData.coneLastPos - playerCar:GetOrigin()) > 20+10*coneIntervalRatio and missionData.numCones < 100 then
	
		local result = gameutil.TestLine(playerCar:GetOrigin(), playerCar:GetOrigin()-Vector3D.new(0,10,0), OBJECTCONTENTS_SOLID_GROUND)
	
		if result:GetFract() < 1.0 then
		
			local rightVec = cross(result:GetNormal(), playerCar:GetForwardVector()) * Vector3D.new(1.2 + coneDistRatio)
	
			local resultL = gameutil.TestLine(playerCar:GetOrigin()-rightVec+Vector3D.new(0,1,0), playerCar:GetOrigin()-rightVec-Vector3D.new(0,10,0), OBJECTCONTENTS_SOLID_GROUND)
			local resultR = gameutil.TestLine(playerCar:GetOrigin()+rightVec+Vector3D.new(0,1,0), playerCar:GetOrigin()+rightVec-Vector3D.new(0,10,0), OBJECTCONTENTS_SOLID_GROUND)
		
			if resultL:GetFract() < 1.0 and resultR:GetFract() < 1.0 then
				local coneL = world:CreateObject("cone")
				local coneR = world:CreateObject("cone")
				
				coneL:SetOrigin(resultL:GetPosition() + Vector3D.new(0,0.35,0))
				coneR:SetOrigin(resultR:GetPosition() + Vector3D.new(0,0.35,0))
				
				coneL:Spawn()
				coneR:Spawn()
				
				coneL:SetContents(OBJECTCONTENTS_SPECIAL_CONE)
				coneR:SetContents(OBJECTCONTENTS_SPECIAL_CONE)
				coneL:SetCollideMask(0)
				coneR:SetCollideMask(0)
				
				missionData.numCones = missionData.numCones + 1
			end

		end
	
		missionData.coneLastPos = vecCopy(playerCar:GetOrigin())
	end
	
	return true
end

-- put mission initializer
missionmanager:Init( Mission_Init )