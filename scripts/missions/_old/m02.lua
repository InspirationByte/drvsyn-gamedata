--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2015
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- "City" mission, ride to airport
--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 12 Apr 2016
--------------------------------------------------------------------------------

world:SetLevelName("city")
world:SetEnvironmentName("night_stormy")
SetMusicName("chicagonight")

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

function Mission_Init()
	missionData = {
		targetPosition = Vector3D.new(346,0.6,-1808)
	}
	
	missionSettings.EnableCops = true
	
	playerCar = gameses:CreateCar("mustang", CAR_TYPE_NORMAL)
	playerCar:SetMaxDamage(12.0)
	
	playerCar:SetOrigin( Vector3D.new(-107.704,0.669,1643.71) )
	playerCar:SetAngles( Vector3D.new(173.173,84.87, 169.73) )
	
	--playerCar:SetOrigin( Vector3D.new(102.133, 0.62, -1814.9) )
	--playerCar:SetAngles( Vector3D.new(120.318,-85.275, -120.312) )
	
	playerCar:Spawn()
	playerCar:SetColorScheme( 5 )
	playerCar:SetTorqueScale(1.1)

	-- for the load time, set player car
	gameses:SetPlayerCar( playerCar )
	
	missionData.targetHandle = gameHUD:AddMapTargetPoint(missionData.targetPosition)
	
	arrowObject = world:CreateObject("dummy")
	arrowObject:SetModel("models/misc/arrow.egf")
	arrowObject:SetOrigin(missionData.targetPosition)
	arrowObject:Spawn()

	missionmanager:EnableTimeout( true, 176 ) -- enable, time
	

	-- here we start
	missionmanager:SetRefreshFunc( Mission_Update )
	
	gameHUD:ShowScreenMessage("Get client to the airport.", 3.5)
end

--------------------------------------------------------------------------------

function OnMissionDone()
	if arrowObject ~= nil then
		arrowObject:Remove()
		arrowObject = nil
		gameHUD:RemoveTrackingObject(missionData.targetHandle)
	end

	playerCar:Lock(true)
	
	return true
end

function Mission_Outro()
	-- load replay
	local replayDuration = gameses:LoadCarReplay(playerCar, "replayData/m02_arrival")
	
	-- todo: setup cameras

	-- schedule mission success
	missionmanager:ScheduleEvent( function() 
		gameHUD:ShowScreenMessage("Well done, he is now leaving the city.", 3.5)
		gameses:SignalMissionStatus( MIS_STATUS_SUCCESS, 4.0 )
		playerCar:Lock( true )
	end, replayDuration )
end

-- main mission update
function Mission_Update( delta )

	local camera = world:GetView()

	UpdateCops( playerCar, delta )

	if arrowObject ~= nil then
		arrowObject:SetOrigin( missionData.targetPosition + Vector3D.new(0,0.8 + math.abs(math.sin(missionmanager.m_timeoutTime*4.0)), 0) )
		arrowObject:SetAngles( Vector3D.new(0,camera:GetAngles():get_y(), 0) )
	end

	-- check player vehicle is wrecked
	if CheckVehicleIsWrecked( playerCar, missionData, delta ) then
		gameHUD:ShowScreenMessage("#WRECKED_VEHICLE_MESSAGE", 3.5)
		
		OnMissionDone()
		
		missionmanager:SetRefreshFunc( function() 
			return false 
		end ) 

		gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
		return false
	end

	if length(playerCar:GetOrigin() - missionData.targetPosition) < 100.0 then
	
		if arrowObject ~= nil then
			arrowObject:Remove()
			arrowObject = nil
			gameHUD:RemoveTrackingObject(missionData.targetHandle)
		end
		
		-- TODO: cutscene black bar curtains
		missionmanager:ScheduleEvent( Mission_Outro, 1.0 )
	
		missionmanager:SetRefreshFunc( function() 
			return false 
		end ) 
	
		return false
	end
	
	if missionmanager:IsTimedOut() then
		OnMissionDone()
	
		gameHUD:ShowScreenMessage("#TIME_UP_MESSAGE", 3.5)
		
		missionmanager:SetRefreshFunc( function() 
			return false 
		end ) 
		
		gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
		return false
	end
	
	return true
end

-- start with this function
missionmanager:Init( Mission_Init )