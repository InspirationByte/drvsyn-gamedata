--------------------------------------------------------------------------------
-- Default mission script
--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 11 Mar 2015
--------------------------------------------------------------------------------

world:SetLevelName("parking")
world:SetEnvironmentName("dusk_clear")
SetMusicName("vegasnight")

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

function DefaultMission_Init()
	-- put globals here
	defaultMissionData = {
		startPos = Vector3D.new(16.88,0.53,37.62),
		startAng = Vector3D.new(179.54,-0.51,-180),
	}

	-- car name		maxdamage	pos ang
	playerCar = gameses:CreateCar("rollo",CAR_TYPE_NORMAL)
	playerCar:SetMaxDamage(16)
	playerCar:SetColorScheme(1)
	
	playerCar:SetOrigin( defaultMissionData.startPos )
	playerCar:SetAngles( defaultMissionData.startAng )
	
	playerCar:AlignToGround()
	
	playerCar:Spawn()
	
	local copReplay = gameses:CreateCar("cop_regis",CAR_TYPE_NORMAL)
	copReplay:SetOrigin( Vector3D.new(-6.27,0.66,-14.09) )
	copReplay:SetAngles( Vector3D.new(4.38,83.04,4.35) )
	copReplay:Spawn()
	
	local duration = gameses:LoadCarReplay(copReplay, "replayData/iview_cop")
	local duration2 = gameses:LoadCarReplay(playerCar, "replayData/iview_cop_escape")

	gameses:SetPlayerCar( playerCar )
	
	missionData = {
		parkedCars = {
		
			-- cars by left side
			{ "rollo", Vector3D.new(-18,0.55,0), Vector3D.new(0,-90,0), 0 },
			{ "torino", Vector3D.new(-18,0.47,4), Vector3D.new(0,-90,0), 1 },
			{ "carla", Vector3D.new(-18,0.48,24), Vector3D.new(0,-90,0), 5 },
			{ "torino", Vector3D.new(-18,0.47,29), Vector3D.new(0,-90,0), 4 },
			{ "mustang", Vector3D.new(-18,0.69,44), Vector3D.new(0,-90,0), 6 },
			{ "rollo", Vector3D.new(-18,0.55,48), Vector3D.new(0,-90,0), 3 },
			{ "mustang", Vector3D.new(-18,0.69,38), Vector3D.new(0,-90,0), 2 },
			
			-- cars by right side
			{ "carla", Vector3D.new(10.2,0.48,2.95), Vector3D.new(0,90,0), 2 },
			{ "rollo", Vector3D.new(-1.67,0.53,27.75), Vector3D.new(0,-90,0), 6 },
			
			{ "carla", Vector3D.new(28,0.48,1), Vector3D.new(0,90,0), 4 },
			{ "rollo", Vector3D.new(28,0.55,5), Vector3D.new(0,90,0), 3 },
			{ "torino", Vector3D.new(28,0.47,10), Vector3D.new(0,90,0), 2 },
			
			{ "carla", Vector3D.new(28,0.46,22), Vector3D.new(0,90,0), 5 },
			
			{ "carla", Vector3D.new(28,0.46,38), Vector3D.new(0,90,0), 2 },
			{ "mustang", Vector3D.new(28,0.69,42), Vector3D.new(0,90,0), 5 },
			{ "torino", Vector3D.new(28,0.47,46), Vector3D.new(0,90,0), 3 },
			{ "rollo", Vector3D.new(28,0.55,50), Vector3D.new(0,90,0), 5 },
			
			{ "mustang", Vector3D.new(-18,0.69,35), Vector3D.new(0,-90,0), 1 },
		},
	}
	
	for i,v in ipairs( missionData.parkedCars ) do
		local parkedCar = gameses:CreateCar( v[1], CAR_TYPE_NORMAL )

		parkedCar:SetOrigin( v[2] )
		parkedCar:SetAngles( v[3] )
		
		parkedCar:Enable(false)

		parkedCar:Spawn()
		parkedCar:AlignToGround();

		parkedCar:SetColorScheme( v[4] )
	end
	
	-- here we start
	missionmanager:SetRefreshFunc( DefaultMission_Update )
end

--------------------------------------------------------------------------------

-- main mission update
function DefaultMission_Update( delta )
	return true
end

-- put mission initializer
missionmanager:Init( DefaultMission_Init )