--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2019
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 26 Apr 2019
--------------------------------------------------------------------------------

world:SetLevelName("default")
world:SetEnvironmentName("night_clear")
SetMusicName("miami_night")

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

function MISSION.SpawnSceneryCars()
	local car1 = gameses:CreateCar("carla", CAR_TYPE_NORMAL)
	car1:SetOrigin( Vector3D.new(109.42,0.48,-42.17) )
	car1:SetAngles( Vector3D.new(0,-90,0) )
	car1:Enable(false)
	car1:Spawn()
	car1:SetColorScheme(2)
	
	local car2 = gameses:CreateCar("torino", CAR_TYPE_NORMAL)
	car2:SetOrigin( Vector3D.new(109.42,0.34,-34.17) )
	car2:SetAngles( Vector3D.new(0,-90,0) )
	car2:Enable(false)
	car2:Spawn()
	car2:SetColorScheme(2)
end

MISSION.Init = function()
	MISSION.Data = {
		targetPosition = Vector3D.new(291.30,6.13,685),
		target2Position = Vector3D.new(-229.11,0.3,56.26),
		target3Position = Vector3D.new(585.91, 0.53, -549.11)
	}
	
	MISSION.Settings.EnableCops = false
	
	local playerCar = gameses:CreateCar("rollo", CAR_TYPE_NORMAL)
	
	MISSION.playerCar = playerCar
	
	playerCar:SetMaxDamage(12.0)
	
	-- real
	playerCar:SetOrigin( Vector3D.new(109.42,0.43,-38.17) )
	playerCar:SetAngles( Vector3D.new(0,-90,0) )

	playerCar:Spawn()
	playerCar:SetColorScheme( 5 )

	MISSION.SpawnSceneryCars()

	-- for the load time, set player car
	gameses:SetPlayerCar( playerCar )
	
	playerCar:Lock(true)

	gameHUD:Enable(false)
	gameHUD:FadeIn(false, 2.5)
	

	MissionManager:ScheduleEvent( function() 
		gameHUD:ShowAlert("#MI01_TITLE_MESSAGE", 3.5, HUD_ALERT_NORMAL)
	end, 2.5)

	--[[ testing
	
	playerCar:SetOrigin( Vector3D.new(291.05, 6.14, 684.68) )
	playerCar:SetAngles( Vector3D.new(-176.79, 78.24, -176.86) )
	MISSION.CarsCutscene()
	
	--]]

	MISSION.SetupFlybyCutscene()
end

function MISSION.Phase1Start()
	
	local playerCar = MISSION.playerCar
	
	gameHUD:Enable(true)
	playerCar:Lock(false)
	
	MISSION.targetHandle = gameHUD:AddMapTargetPoint(MISSION.Data.targetPosition)

	-- here we start
	MissionManager:SetRefreshFunc( MISSION.Phase1Update )

	gameHUD:ShowScreenMessage("#MI01_GO_MESSAGE", 3.5)

	MissionManager:EnableTimeout( true, 50 ) -- enable, time
end

function MISSION.SetupFlybyCutscene()

	local playerCar = MISSION.playerCar

	MissionManager:ScheduleEvent( function() 
		playerCar:SetLight(CAR_LIGHT_LOWBEAMS, false);
	end, 0);

	MissionManager:ScheduleEvent( function() 
		playerCar:SetLight(CAR_LIGHT_LOWBEAMS, true);
	end, 6.5)
	
	local function smoothFunc(x)
		return bezierCubic(0, 0.15, 0.85, 1, x)
	end
	
	----[[
	local cutCameras = {
		CutCamera( {vec3(85, 25, -45), vec3(85, 10, -45)}, {vec3(-21.12, 112.20, 0.00), vec3(-2.16, 111.84, 0.00)}, CAM_MODE_TRIPOD, {40, 50}, playerCar, 3, smoothFunc ),
		CutCamera( {vec3(61,1.8,-12), vec3(90.87, 2.48, -29.13)}, {vec3(-4.3, 238.5, 0), vec3(-3.22, 239.70, 0.00)}, CAM_MODE_TRIPOD, {20, 30}, playerCar, 3, smoothFunc ),
		CutCamera( vec3(114,0.5,-35), vec3(-7,115.5, 0), CAM_MODE_TRIPOD, nil, playerCar, 1.5 ),
	}--]]
	--[[
	local cameraArray =
	{
		{vec3(55.58, 15.31, -86.25)	, vec3(8.58, 360.48, 0.00)},
		{vec3(49.15, 9.97, -47.16)	, vec3(-0.42, 294.36, 0.00)},
		{vec3(66.97, 8.78, -27.71)	, vec3(13.38, 263.94, 0.00)},
		{vec3(83.78, 5.82, -44.44)	, vec3(-2.94, 287.04, 0.00)},
		{vec3(99.30, 5.23, -39.18)	, vec3(14.64, 267.54, 0.00)},
		{vec3(103.32, 1.69, -38.17)	, vec3(0.00, 270.00, 0.00)},
	}
	
	local cutCameras = {CutFlyByCamera(cameraArray, playerCar, 6, smoothFunc, 2.0)}
	--]]
	CutsceneCamera.Start(cutCameras, MISSION.Phase1Start, 0)	
end

function MISSION.SecretTrigger()

	if MISSION.secretTrigger then
		return
	end

	local playerCar = MISSION.playerCar
	local triggerPos = vec3(308.71, 14.83, 681.68)

	if length(playerCar:GetOrigin() - triggerPos) < 4.0 then
		MISSION.secretTrigger = true
		playerCar:Lock(true)
		gameses:SetTimescale(0.5)
		
		local carPositions = {
			{vec3(292.11, 6.13, 683.22), vec3(-2.43, 69.89, -2.40)},
			{vec3(288.62, 6.14, 679.13), vec3(-179.66, 61.00, -179.70)}
		}
		
		local pedPositions = {
			{ vec3(292.80, 6.4, 680.58), 64, vec3(283.65, 6.10, 684.65) },
			{ vec3(290.19, 6.4, 681.83), 240, vec3(293.73, 6.09, 676.49) },
		}
		
		-- spawn some gangs and make the scene done
		local cutGang1 = gameses:CreateCar("carla", CAR_TYPE_NORMAL)
		local cutGang2 = gameses:CreateCar("mustang", CAR_TYPE_NORMAL)
		cutGang1:SetLight(1, true)
		cutGang2:SetLight(1, true)

		cutGang1:SetOrigin( vec3(292.11, 6.22, 683.22) )
		cutGang1:SetAngles( vec3(-2.43, 69.89, -2.40) )

		cutGang2:SetOrigin( vec3(288.62, 6.22, 679.13) )
		cutGang2:SetAngles( vec3(-179.66, 61.00, -179.70) )

		cutGang1:Spawn()
		cutGang2:Spawn()

		cutGang1:SetColorScheme( 5 )
		cutGang2:SetColorScheme( 5 )
		
		MISSION.OnDone()
		
		-- spawn pedestrians
		local peds = {}
		for k,v in ipairs(pedPositions) do
			local ped = gameses:CreatePedestrian("models/characters/ped02.egf", -1)
			ped:SetOrigin(v[1])
			ped:SetAngles(vec3(0,v[2],0))
			ped:Spawn()
			ped:SetBodyGroups(1 + 2)        -- 2 to make briefcase
			
			table.insert(peds, ped)
		end
		
		MissionManager:SetRefreshFunc(function()
			return false
		end)
		
		MissionManager:ScheduleEvent( function()
			-- set to only move pedestirans
			MissionManager:SetRefreshFunc( function() 
			
				for k,ped in ipairs(peds) do
					PedWalkToTarget(ped, pedPositions[k][3], true)
				end

				return false 
			end)
			
			gameHUD:ShowScreenMessage("You screw up other deal! Mission failed.", 3.5)
			gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
		end, 0.5)
		
		local cutCameras = {
			CutCamera( vec3(284.08, 6.39, 678.40), vec3(-13.06, 292.24, 0.00), CAM_MODE_TRIPOD, 50, playerCar, 5 ),
		}

		CutsceneCamera.Start(cutCameras, nil, 10)
	end
end

--------------------------------------------------------------------------------

function MISSION.OnDone()
	local playerCar = MISSION.playerCar
	
	gameHUD:RemoveTrackingObject(MISSION.targetHandle)

	playerCar:Lock(true)
	
	return true
end

function MISSION.OnCompleted()
	MISSION.OnDone()
	
	MissionManager:SetRefreshFunc( function() 
		return false 
	end )
	
	gameses:SignalMissionStatus( MIS_STATUS_SUCCESS, 4.0 )
	
	gameHUD:ShowAlert("#MENU_GAME_TITLE_MISSION_SUCCESS", 3.5, HUD_ALERT_SUCCESS)
	
	gameHUD:ShowScreenMessage("#MI01_SUCCESS_MESSAGE", 3.5)
end

function MISSION.CarsCutscene()
	
	ai:RemoveAllCars()
	gameHUD:Enable(false)
	
	-- triggered before means double
	if MISSION.secretTrigger then
		MISSION.SecretCarsCutscene()
		return
	end
	
	MISSION.Settings.EnableCops = false
	
	-- spawn some gangs and make the scene done
	MISSION.cutGang1 = gameses:CreateCar("mustang", CAR_TYPE_NORMAL)
	MISSION.cutGang2 = gameses:CreateCar("mustang", CAR_TYPE_NORMAL)
	MISSION.cutGang1:SetLight(1, true)
	MISSION.cutGang2:SetLight(1, true)
	
	MISSION.cutGang1:SetOrigin( Vector3D.new(237.97, 9.14, 703.04) )
	MISSION.cutGang1:SetAngles( Vector3D.new(0,180,0) )
	
	MISSION.cutGang2:SetOrigin( Vector3D.new(237.88, 9.14, 710.41) )
	MISSION.cutGang2:SetAngles( Vector3D.new(0,180,0) )

	MISSION.cutGang1:Spawn()
	MISSION.cutGang2:Spawn()
	
	MISSION.cutGang1:SetColorScheme( 5 )
	MISSION.cutGang2:SetColorScheme( 5 )
	
	-- also cutscene cops
	MISSION.cutCop1 = gameses:CreateCar("cop_regis", CAR_TYPE_NORMAL)
	MISSION.cutCop1:SetOrigin( Vector3D.new(260.06, 3.03, 626.15) )
	MISSION.cutCop1:SetAngles( Vector3D.new(-65.67, 85.19, -65.69) )
	MISSION.cutCop1:Spawn()
	
	MISSION.cutCop2 = gameses:CreateCar("cop_regis", CAR_TYPE_NORMAL)
	MISSION.cutCop2:SetOrigin( Vector3D.new(186.18, 9.03, 713.87) )
	MISSION.cutCop2:SetAngles( Vector3D.new(-104.02, -85.11, 104.00) )
	MISSION.cutCop2:Spawn()
	
	MISSION.cutCop1:SetLight(1, true)
	MISSION.cutCop2:SetLight(1, true)
	
	-- for recording replays
	--gameses:SetPlayerCar(MISSION.cutCop2)
	MISSION.playerCar:Lock(false)
	
	-- load replays
	local replayDuration1 = 0
	replayDuration1 = gameses:LoadCarReplay(MISSION.cutGang1, "replayData/mission1_cut_car1", false)
	replayDuration1 = gameses:LoadCarReplay(MISSION.cutGang2, "replayData/mission1_cut_car2", false)
	replayDuration1 = gameses:LoadCarReplay(MISSION.cutCop1, "replayData/mission1_cut_cop1", false)
	replayDuration1 = gameses:LoadCarReplay(MISSION.cutCop2, "replayData/mission1_cut_cop2", false)
	replayDuration1 = gameses:LoadCarReplay(MISSION.playerCar, "replayData/mission1_cut_esc")

	local cutCameras = {
		CutCamera( vec3(309,7.2,698.1), vec3(355, 127, 0), CAM_MODE_TRIPOD, nil, MISSION.cutGang1, 3 ),
		CutCamera( vec3(272.81,6.2,653.1), vec3(0), CAM_MODE_TRIPOD_FOLLOW_ZOOM, nil, MISSION.cutGang1, 5 ),
		CutCamera( vec3(293.49,7.26,690.69), vec3(1.32,208.80, 0), CAM_MODE_TRIPOD, nil, MISSION.cutGang1, 3 ),
		CutCamera( vec3(277.37, 6.23, 680.31), vec3(-6.90, 301.26, 0.00), CAM_MODE_TRIPOD, nil, MISSION.cutGang1, 2 ),
		CutCamera( vec3(234.66, 5.90, 658.11), vec3(-1.08, -56.00, 0.00), CAM_MODE_TRIPOD, nil, MISSION.cutCop1, 2),
		CutCamera( vec3(259.65, 6.58, 677.61), vec3(0), CAM_MODE_TRIPOD_FOLLOW_ZOOM, nil, MISSION.cutCop2, 1),
		CutCamera( vec3(262.97, 6.48, 689.31), vec3(-5.84, -141.16, 0.00), CAM_MODE_TRIPOD, nil, MISSION.playerCar, 1)
	}
	
	CutsceneCamera.Start(cutCameras, MISSION.Phase2Start)
end

function MISSION.SecretCarsCutscene()

	local playerCar = MISSION.playerCar
	
	MISSION.cutGang1 = gameses:CreateCar("mustang", CAR_TYPE_NORMAL)

	MISSION.cutGang1:SetOrigin( Vector3D.new(320.98, 15.14, 679.05) )
	MISSION.cutGang1:SetAngles( Vector3D.new(-167.01, 85.13, -167.01) )

	MISSION.cutGang1:Spawn()
	MISSION.cutGang1:SetColorScheme( 5 )
	MISSION.cutGang1:SetLight(1, true)
	
	MissionManager:ScheduleEvent( function()
		MISSION.cutGang1:SetVelocity(vec3(-18,0,7))
	end, 10)

	MISSION.secretTrigger = false

	local cutCameras = {
		CutCamera( vec3(309,7.2,698.1), vec3(355, 127, 0), CAM_MODE_TRIPOD, nil, MISSION.cutGang1, 6 ),
		CutCamera( vec3(293.49,7.26,690.69), vec3(1.32,208.80, 0), CAM_MODE_TRIPOD, nil, MISSION.cutGang1, 3 ),
		CutCamera( vec3(284.08, 6.39, 678.40), vec3(-13.06, 292.24, 0.00), CAM_MODE_TRIPOD, 50, playerCar, 2 ),
		CutCamera( vec3(262.97, 6.48, 689.31), vec3(-5.84, -141.16, 0.00), CAM_MODE_TRIPOD, nil, MISSION.playerCar, 1)
	}

	CutsceneCamera.Start(cutCameras, MISSION.Phase2Start, 0)
end

function MISSION.Phase2Start()
	local playerCar = MISSION.playerCar

	playerCar:Lock(false)

	-- spawn cops, remove cutscene cops
	local cop1 = gameses:CreatePursuerCar("cop_regis", PURSUER_TYPE_COP)
	local cop2 = gameses:CreatePursuerCar("cop_regis", PURSUER_TYPE_COP)
	
	cop1:SetOrigin(Vector3D.new(265.3, 6.27, 672.5))
	cop1:SetAngles( MISSION.playerCar:GetAngles())
	cop2:SetOrigin(Vector3D.new(261.3, 6.27, 665.5))
	cop2:SetAngles( MISSION.playerCar:GetAngles())
	
	if MISSION.cutCop1 ~= nil then
		MISSION.cutCop1:Remove()
	end
	if MISSION.cutCop2 ~= nil then
		MISSION.cutCop2:Remove()
	end
	
	cop1:Spawn()
	SetupCopCarSettings(cop1)
	
	cop2:Spawn()
	SetupCopCarSettings(cop2)
	
	------------

	local playerCar = MISSION.playerCar
	playerCar:SetFelony(0.3)
	playerCar:Lock(false)
	
	MISSION.Settings.EnableCops = true
	
	gameHUD:Enable(true)
	
	ai:MakePursued( playerCar )
	
	MissionManager:EnableTimeout( true, 135 )
	
	MISSION.safeHouseTarget = MISSION.Data.target2Position
	MISSION.targetHandle = gameHUD:AddMapTargetPoint(MISSION.safeHouseTarget)
	MISSION.safeHouseCops = false
	MISSION.finalTarget = false
	
	gameHUD:ShowScreenMessage("#MI01_SETUP_MESSAGE", 3.5)
	
	MissionManager:SetRefreshFunc( MISSION.Phase2Update )
end

MISSION.UpdateAll = function(delta)

	local camera = world:GetView()
	
	local playerCar = MISSION.playerCar
	
	UpdateCops( playerCar, delta )

	-- check player vehicle is wrecked
	if CheckVehicleIsWrecked( playerCar, MISSION.Data, delta ) then
		gameHUD:ShowAlert("#WRECKED_VEHICLE_MESSAGE", 3.5, HUD_ALERT_DANGER)
		
		MISSION.OnDone()
		
		MissionManager:SetRefreshFunc( function() 
			return false 
		end ) 

		gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
		return false
	end
	
	if MissionManager:IsTimedOut() then
		gameHUD:ShowAlert("#TIME_UP_MESSAGE", 3.5, HUD_ALERT_DANGER)

		MISSION.OnDone()
		
		MissionManager:SetRefreshFunc( function() 
			return false 
		end ) 
		
		gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
		return false
	end
	
	return true
end

-- main mission update
MISSION.Phase1Update = function( delta )

	local playerCar = MISSION.playerCar

	local distToTarget = length(playerCar:GetOrigin() - MISSION.Data.targetPosition)
	local playerSpeed = playerCar:GetSpeed()
	
	MISSION.SecretTrigger()

	if distToTarget < 80.0 then
	
		if playerCar:GetPursuedCount() > 0 then
			gameHUD:ShowScreenMessage("#LOSE_TAIL_MESSAGE", 1.5)
		elseif playerSpeed < 60 then

			if distToTarget < 4.0 then
			
				gameHUD:RemoveTrackingObject(MISSION.targetHandle)
				
				playerCar:Lock(true)
				
				MissionManager:EnableTimeout( false, 80 )
				
				-- TODO: cutscene black bar curtains
				MissionManager:ScheduleEvent( MISSION.CarsCutscene, 2.5 )
			
				MissionManager:SetRefreshFunc( function() 
					return false 
				end ) 
			
				return false
			end
		else
			gameHUD:ShowScreenMessage("#SLOW_DOWN_MESSAGE", 1.0)
		end
	end
	
	return MISSION.UpdateAll(delta)
end

function MISSION.SetUpCops()

	if MISSION.safeHouseCops then
		return
	end
	
	local cop1 = gameses:CreatePursuerCar("cop_regis", PURSUER_TYPE_COP)
	
	cop1:SetOrigin( Vector3D.new(-232.46,0.45,38.5) )
	cop1:SetAngles( Vector3D.new(0,38,0) )
	cop1:Enable(false)
	cop1:SetLight(CAR_LIGHT_SERVICELIGHTS, true)

	cop1:Spawn()
	SetupCopCarSettings(cop1)
	
	local cop2 = gameses:CreatePursuerCar("cop_regis", PURSUER_TYPE_COP)
	
	cop2:SetOrigin( Vector3D.new(-223.96,0.48,51.2) )
	cop2:SetAngles( Vector3D.new(0,120,0) )
	cop2:Enable(false)
	cop2:SetLight(CAR_LIGHT_SERVICELIGHTS, true)
	
	cop2:Spawn()
	SetupCopCarSettings(cop2)

	MISSION.safeHouseCops = true
end

function MISSION.ChangeTargets()
	gameHUD:ShowScreenMessage("#MI01_NOTSAFE_MESSAGE", 3.5)
	
	gameHUD:RemoveTrackingObject(MISSION.targetHandle)
	
	MISSION.safeHouseTarget = MISSION.Data.target3Position
	MISSION.targetHandle = gameHUD:AddMapTargetPoint(MISSION.safeHouseTarget)
	
	MISSION.finalTarget = true
	
	-- enable stop cops
	MISSION.Settings.StopCops = true
	MISSION.Settings.StopCopsRadius = 300
	MISSION.Settings.StopCopsPosition = MISSION.safeHouseTarget
end

MISSION.Phase2Update = function( delta )

	local playerCar = MISSION.playerCar

	local distToTarget = length(playerCar:GetOrigin() - MISSION.safeHouseTarget)
	
	if MISSION.finalTarget then
	
		MISSION.Settings.StopCops = true
		MISSION.Settings.StopCopsRadius = 300
		MISSION.Settings.StopCopsPosition = MISSION.safeHouseTarget
	
		if distToTarget < 100 then
			--MISSION.Settings.EnableCops = false
			
			if distToTarget < 70 then
				if playerCar:GetPursuedCount() > 0 then
					gameHUD:ShowScreenMessage("#LOSE_TAIL_MESSAGE", 1.5)
				elseif distToTarget < 5 then
					
					MISSION.OnCompleted()
				end
			end
		end
	else
		if distToTarget < 70 then
			-- spawn cop
			MISSION.SetUpCops()
			
			if distToTarget < 25 then
				MISSION.ChangeTargets()
			end
		end
	end
	
	return MISSION.UpdateAll(delta)
end