--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2019
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 26 Apr 2019
--------------------------------------------------------------------------------

world:SetLevelName("default")
world:SetEnvironmentName("day_dusk_rainy_transition")
SetMusicName("miami_day")

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

MISSION.Init = function()
	MISSION.Data = {
		targetPosition = Vector3D.new(574,0.5,-672),
		target2Position = Vector3D.new(374.14, 15.08, 655.78),
	}
	
	MISSION.Settings.EnableCops = true
	
	local playerCar = gameses:CreateCar("rollo", CAR_TYPE_NORMAL)
	
	MISSION.playerCar = playerCar
	
	playerCar:SetMaxDamage(12.0)
	
	-- real
	playerCar:SetOrigin( Vector3D.new(1095.54, 0.48, 256.20) )
	playerCar:SetAngles( Vector3D.new(-19.77, 85.03, -22.96) )
	
	-- testing
	--playerCar:SetOrigin( Vector3D.new(557,0.53,-633) )
	--playerCar:SetAngles( Vector3D.new(0,180,0) )

	playerCar:Spawn()
	playerCar:SetColorScheme( 5 )
	
	-- for the load time, set player car
	gameses:SetPlayerCar( playerCar )
	
	playerCar:Lock(true)

	gameHUD:Enable(false)
	gameHUD:FadeIn(false, 2.5)
	gameHUD:ShowAlert("#MI02_TITLE_MESSAGE", 3.5, HUD_ALERT_NORMAL)

	--MISSION.CarsCutscene()

	MISSION.SetupFlybyCutscene()


end

function MISSION.Phase1Start()
	
	local playerCar = MISSION.playerCar
	
	gameHUD:Enable(true)
	playerCar:Lock(false)
	
	MISSION.targetHandle = gameHUD:AddMapTargetPoint(MISSION.Data.targetPosition)

	-- here we start
	MissionManager:SetRefreshFunc( MISSION.Phase1Update )

	gameHUD:ShowScreenMessage("#MI02_GO_MESSAGE", 3.5)
	
	MissionManager:EnableTimeout( true, 110 ) -- enable, time
end

function MISSION.SetupFlybyCutscene()

	local playerCar = MISSION.playerCar

	local function smoothFunc(x)
		local ret = bezierCubic(0, 0, 1, 1, x)
		return ret
	end

	local cutCameras = {
		CutCamera( {vec3(1090.99, 10.89, 256.19), vec3(1090.99, 0.89, 256.19)}, vec3(-5.64, 251.22, 0.00), CAM_MODE_TRIPOD, 65, playerCar, 4, smoothFunc ),
	}
	
	CutsceneCamera.Start(cutCameras, MISSION.Phase1Start, 1)
end

--------------------------------------------------------------------------------

function MISSION.OnDone()
	local playerCar = MISSION.playerCar
	
	gameHUD:RemoveTrackingObject(MISSION.targetHandle)

	playerCar:Lock(true)
	
	return true
end

function MISSION.OnCompleted()
	MISSION.OnDone()
	
	MissionManager:SetRefreshFunc( function() 
		return false 
	end ) 
	
	gameses:SignalMissionStatus( MIS_STATUS_SUCCESS, 4.0 )
	
	gameHUD:ShowAlert("#MENU_GAME_TITLE_MISSION_SUCCESS", 3.5, HUD_ALERT_SUCCESS)
	
	gameHUD:ShowScreenMessage("#MI02_SUCCESS_MESSAGE", 3.5)
end

function MISSION.CarsCutscene()
	
	ai:RemoveAllCars()
	gameHUD:Enable(false)
	
	MISSION.playerCar:SetOrigin( Vector3D.new(576.80,0.53,-678.06) )
	MISSION.playerCar:SetAngles( Vector3D.new(0,-85.0,0) )
	
	-- spawn some gangs and make the scene done
	MISSION.newPlayerCar = gameses:CreateCar("mustang_f", CAR_TYPE_NORMAL)

	MISSION.newPlayerCar:SetOrigin( Vector3D.new(586.89,0.63,-662.06) )
	MISSION.newPlayerCar:SetAngles( Vector3D.new(0,180,0) )
	
	MISSION.newPlayerCar:Spawn()
	
	MISSION.newPlayerCar:SetColorScheme( 3 )
	MISSION.newPlayerCar:SetFelony(0.35)
	
	
	MISSION.uTrailer = gameses:CreateCar("utrailer",CAR_TYPE_NORMAL)
	MISSION.uTrailer:SetMaxDamage(4)
	MISSION.uTrailer:SetOrigin( Vector3D.new(586.89,0.63,-662.06) + Vector3D.new(0,0,4) )
	MISSION.uTrailer:SetAngles( Vector3D.new(0,180,0) )
	MISSION.uTrailer:AlignToGround()
	MISSION.uTrailer:Spawn()
	
	-- connect trailer to our car
	MissionManager:ScheduleEvent( function()
		MISSION.newPlayerCar:HingeVehicle(1, MISSION.uTrailer, 0);
	end, 0 )
	
	-- drop old player car
	MISSION.playerCar:Enable(false)
	MISSION.playerCar = MISSION.newPlayerCar
	gameses:SetPlayerCar(MISSION.newPlayerCar)

	----[[ COMMENT THIS FOR DEVMODE
	
	local replayDuration1 = gameses:LoadCarReplay(MISSION.newPlayerCar, "replayData/mission2_car")
	
	-- load replays

	local cutCameras = {
		CutCamera(vec3(565.34, 0.74, -664.86), vec3(-6.78, 235.57, 0.00), CAM_MODE_TRIPOD, 31, MISSION.playerCar, 8 ),
	}

	CutsceneCamera.Start(cutCameras, MISSION.Phase2Start, 1)
	
	--]]
end

function MISSION.Phase2Start()
	local playerCar = MISSION.playerCar
	playerCar:Lock(false)
	
	gameHUD:Enable(true)
	
	MissionManager:EnableTimeout( false, 0 )
	MissionManager:ShowTime( true )
	
	MISSION.targetHandle = gameHUD:AddMapTargetPoint(MISSION.Data.target2Position)
	
	gameHUD:ShowScreenMessage("#MI02_DELIVER_MESSAGE", 3.5)
	
	MissionManager:SetRefreshFunc( MISSION.Phase2Update )
end

MISSION.UpdateAll = function(delta)

	local camera = world:GetView()
	
	local playerCar = MISSION.playerCar
	
	UpdateCops( playerCar, delta )

	-- check player vehicle is wrecked
	if CheckVehicleIsWrecked( playerCar, MISSION.Data, delta ) then
		gameHUD:ShowAlert("#WRECKED_VEHICLE_MESSAGE", 3.5, HUD_ALERT_DANGER)
		
		MISSION.OnDone()
		
		MissionManager:SetRefreshFunc( function() 
			return false 
		end ) 

		gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
		return false
	end
	
	if MissionManager:IsTimedOut() then
		gameHUD:ShowAlert("#TIME_UP_MESSAGE", 3.5, HUD_ALERT_DANGER)

		MISSION.OnDone()
		
		MissionManager:SetRefreshFunc( function() 
			return false 
		end ) 
		
		gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
		return false
	end
	
	return true
end

-- main mission update
MISSION.Phase1Update = function( delta )

	local playerCar = MISSION.playerCar

	local distToTarget = length(playerCar:GetOrigin() - MISSION.Data.targetPosition)
	local playerSpeed = playerCar:GetSpeed()

	if distToTarget < 80.0 then
	
		if playerCar:GetPursuedCount() > 0 then
			gameHUD:ShowScreenMessage("#LOSE_TAIL_MESSAGE", 1.5)
		elseif playerSpeed < 60 then

			if distToTarget < 4.0 then
			
				gameHUD:RemoveTrackingObject(MISSION.targetHandle)
				
				playerCar:Lock(true)
				
				MissionManager:EnableTimeout( false, 80 )
				
				-- TODO: cutscene black bar curtains
				MissionManager:ScheduleEvent( MISSION.CarsCutscene, 2.5 )
			
				MissionManager:SetRefreshFunc( function() 
					return false 
				end ) 
			
				return false
			end
		else
			gameHUD:ShowScreenMessage("#SLOW_DOWN_MESSAGE", 1.0)
		end
	end
	
	return MISSION.UpdateAll(delta)
end

MISSION.Phase2Update = function( delta )

	local playerCar = MISSION.playerCar
	
	if playerCar:GetHingedVehicle() == nil then
		gameHUD:ShowAlert("#TRAILER_DAMAGED", 3.5, HUD_ALERT_DANGER)
	
		gameses:SetViewObject(MISSION.uTrailer)
	
		gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
		
		MissionManager:SetRefreshFunc( function() 
			return false 
		end ) 
		
		return false
	end

	local distToTarget = length(playerCar:GetOrigin() - MISSION.Data.target2Position)
	
	if distToTarget < 70 then
		if playerCar:GetPursuedCount() > 0 then
			gameHUD:ShowScreenMessage("#LOSE_TAIL_MESSAGE", 1.5)
		elseif distToTarget < 5 then
			MISSION.OnCompleted()
		end
	end
	
	return MISSION.UpdateAll(delta)
end