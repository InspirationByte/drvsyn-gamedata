--------------------------------------------------------------------------------
-- Take a ride mission script
--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 11 Mar 2015
--------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

MISSION.Init = function()
	local g_car = console.FindCvar("g_car")
	
	FreerideGame.Init( g_car:GetString(), 1, "freeride_spawn" )
	
	-- spawn secret cars, etc
	-- add other plugin functions for secret cars, etc
end
