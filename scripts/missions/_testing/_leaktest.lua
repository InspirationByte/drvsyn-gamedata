world:SetLevelName("phys_test")

MISSION.Init = function()
	-- put globals here
	MISSION.Data = {
		startPos = Vector3D.new(54,10.31,-45),
		startAng = Vector3D.new(0,0,0),
		notOnGroundTime = 10.0
	}

	-- car name		maxdamage	pos ang
	MISSION.playerCar = gameses:CreateCar("mustang",CAR_TYPE_NORMAL)
	MISSION.playerCar:SetMaxDamage(16)
	MISSION.playerCar:SetColorScheme(1)
	
	MISSION.playerCar:SetOrigin( MISSION.Data.startPos )
	MISSION.playerCar:SetAngles( MISSION.Data.startAng )
	
	MISSION.playerCar:AlignToGround()
	
	MISSION.playerCar:Spawn()
	
	gameses:SetPlayerCar( MISSION.playerCar )
	
	-- here we start
	MissionManager:SetRefreshFunc( MISSION.Update )
end

--------------------------------------------------------------------------------

-- main mission update
MISSION.Update = function( delta )

	if MissionManager.m_timeoutTime > 0.1 then
		console.ExecuteString("quit")
	end

	return true
end