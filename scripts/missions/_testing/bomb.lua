--------------------------------------------------------------------------------
-- Mission tutorial script by sherbetjutsu
--------------------------------------------------------------------------------

world:SetLevelName("default")
world:SetEnvironmentName("day_clear")
--MISSION.Hud = "resources/hud/havana.res"

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------


MISSION.Init = function()
	
	-- create trail blazer cone object definition
	--[[local coneObjectDesc = create_section {
		model = "models/props/bomb.egf",
		--glow =	  {0, 0, 0, 		4, 	0.95, 0.95, 0.20, 	1, 3},
	}
	world:AddObjectDef("physics", "bombprop", coneObjectDesc)]]
	
	local smokeObjectDesc = create_section {
		type = "smoke",
		atlas_textures = "smoke1",--{"smoke1", "smoke2",},
		winddrift = 0.5,
		velocity = 5,
		dir_angles = {-90, 0, 0,},
		color = {0, 0, 0, 0.5},
		color2 = {0, 0, 0, 0.5},
		start_size = 2.5,
		end_size = 6.5,
		lifetime = 7,
		emitrate = 1,
		emitter_lifetime = 0.0,
	}
	MISSION.smokeObjectDesc = smokeObjectDesc
	--world:AddObjectDef("emitter", "black_smoke", smokeObjectDesc)
	
	local smokeObjectDesc2 = create_section {
		type = "smoke",
		atlas_textures = "smoke1",--{"smoke1", "smoke2",},
		winddrift = 1,
		velocity = 7,
		dir_angles = {-90, 0, 0,},
		color = {1, 0.80, 0, 1.0},
		color2 = {1, 0.0, 0, 0.6},
		start_size = 2.5,
		end_size = 3.5,
		lifetime = 2,
		emitrate = 1,
		emitter_lifetime = 0.0,
	}
	MISSION.smokeObjectDesc2 = smokeObjectDesc2
	--world:AddObjectDef("emitter", "red_smoke", smokeObjectDesc2)
	
	-- table for bombs to be stored
	MISSION.objects = {}
	
	--[[sounds:LoadScript("scripts/sounds/havana_bomb.txt")
	sounds:Precache( "deathsound" )
	sounds:Precache( "bombprop_explosion" )]]
	
	MISSION.Settings.EnableCops = false
	
	local playerCar = gameses:CreateCar("rollo", CAR_TYPE_NORMAL)
	
	MISSION.playerCar = playerCar
	
	
	-- real
	playerCar:SetOrigin( vec3(36.90, 3.13, 401.20) )
	playerCar:SetAngles( vec3(-179.86, -8.68, 179.98) )

	playerCar:Spawn()
	playerCar:AlignToGround()
	playerCar:SetMaxDamage(12.0)
	playerCar:AlignToGround()
	playerCar:SetColorScheme( 5 )
	playerCar:SetTorqueScale(1)
	
	local targetCar = gameses:CreateCar("torino", CAR_TYPE_NORMAL)
	
	MISSION.targetCar = targetCar
	
	targetCar:SetOrigin( vec3(44.07, 3.00, 369.89) )
	targetCar:SetAngles( vec3(-179.74, -0.10, 176.75) )

	targetCar:Spawn()
	targetCar:AlignToGround()
	targetCar:SetMaxDamage(5000) -- damage script will add damage
	targetCar:SetColorScheme(2)
	
	-- make target car slower than player car
	--[[targetCar:SetTorqueScale(1.25)
	targetCar:SetMaxSpeed(130)]]
	targetCar:SetInfiniteMass(true) -- target's car should be immovable

	-- for the load time, set player car
	gameses:SetPlayerCar( playerCar )
	
	playerCar:Lock(true)

	gameHUD:Enable(false)
	gameHUD:ShowAlert("BOMBERMAN", 3.5, HUD_ALERT_NORMAL)
	
	-- set to 1 to record replay
	-- set back to 0 for testing and release
	local recordMode = 0
	
	if recordMode == 0 then
		MISSION.Phase1Start()
		missionmanager:EnableTimeout( true, 98)
		-- 90 second chase
	
		missionmanager:ScheduleEvent( function() 
			PointSpawn()
		end, 3 )
		
		-- comment to stop playing replay
		local replayNumber = math.random(1,5)
		local replayDuration = gameses:LoadCarReplay(targetCar, "replaydata/p01_"..replayNumber)
		
	else
		gameses:SetPlayerCar( targetCar )
		missionmanager:SetRefreshFunc( MISSION.RecordReplay )
		missionmanager:EnableTimeout( true, 103)
		-- time should be longer than regular mission's time
		
		gameHUD:FadeIn(true, 2.5)
		
		MISSION.playerCar = targetCar
		
		gameHUD:Enable(true)
	end
end

-- Places objective on map and unlocks player car
function MISSION.Phase1Start()

	MISSION.MusicScript = "havana_night"
	SetMusicState(MUSIC_STATE_PURSUIT)

	local playerCar = MISSION.playerCar
	local targetCar = MISSION.targetCar
	
	gameHUD:Enable(true)
	playerCar:Lock(false)
	
	-- add target to map, show target health bar
	MISSION.targetHandle = gameHUD:AddTrackingObject(MISSION.targetCar, HUD_DOBJ_CAR_DAMAGE + HUD_DOBJ_IS_TARGET)

	-- here we start
	missionmanager:SetRefreshFunc( MISSION.Phase1Update )
	
	targetCar:SetEventHandler({
		OnCarCollision = MISSION.targetHit
	})

	gameHUD:ShowScreenMessage("Next truck's THICC with the jelli.", 6)
	
end

-- Checks if objective has been met
MISSION.Phase1Update = function( delta )

	local playerCar = MISSION.playerCar
	local targetCar = MISSION.targetCar

	local distToTarget = length(playerCar:GetOrigin() - targetCar:GetOrigin())
	
	-- catchup to make it possible recover from mistakes
	playerCar:SetMaxSpeed( 135 + (distToTarget * 0.2) )
	playerCar:SetTorqueScale( 1 + (distToTarget * 0.005) )
	
	if distToTarget > 150 then
		gameHUD:ShowAlert("#TARGET_AWAY_MESSAGE", 3.5, HUD_ALERT_DANGER)
		
		gameses:SignalMissionStatus( MIS_STATUS_FAILED, 3.0 )
		playerCar:Lock(true)
		missionmanager:SetRefreshFunc( function() 
			return false 
		end ) 
		MISSION.OnDone()
	elseif distToTarget > 130 then 
		gameHUD:ShowScreenMessage("He's getting away!",1)
	end
	
	if targetCar:IsAlive() == false then
		MISSION.OnCompleted()
		
		playerCar:Lock(true)
		
		playerCar:SetTorqueScale( 1 ) -- reset's player's torque scale
		
		-- release target car
		gameses:StopCarReplay(targetCar)
		targetCar:Lock(true)
		targetCar:SetInfiniteMass(false)
	end
	
	return MISSION.UpdateAll(delta)
end

-- function for counting the clock while recording replays
MISSION.RecordReplay = function( delta )
	return MISSION.UpdateAll( delta )
end

--------------------------------------------------------------------------------

function MISSION.OnDone()
	local playerCar = MISSION.playerCar
	
	--gameHUD:RemoveTrackingObject(MISSION.targetHandle)

	playerCar:Lock(true)
	
	return true
end

function MISSION.OnCompleted()
	MISSION.OnDone()
	
	missionmanager:SetRefreshFunc( function() 
		return false 
	end ) 
	
	gameses:SignalMissionStatus( MIS_STATUS_SUCCESS, 4.0 )
	
	gameHUD:ShowAlert("QUEST COMPLETE", 3.5, HUD_ALERT_SUCCESS)
	
	--gameHUD:ShowScreenMessage("#MI01_SUCCESS_MESSAGE", 3.5)
end

function MISSION.targetHit(self, props)
	local targetCar = MISSION.targetCar
	local playerCar = MISSION.playerCar

	-- if hit by player car, add huge damage number
	if props.hitBy == playerCar and props.velocity > 3.5 then
		-- 1st number = guaranteed damage, 2nd number = more damage from harder hit
		local damageNumber = math.floor( 300 + (props.velocity * 25) )
		targetCar:SetDamage(targetCar:GetDamage() + damageNumber)
		
		-- uncomment to see damage done
		--gameHUD:ShowScreenMessage("Hit for "..damageNumber.."", 2)
	end
end

MISSION.UpdateAll = function(delta)
	
	local camera = world:GetView()
	
	local playerCar = MISSION.playerCar
	
	--UpdateCops( playerCar, delta )

	-- check player vehicle is wrecked
	if CheckVehicleIsWrecked( playerCar, MISSION.Data, delta ) then
		gameHUD:ShowAlert("#WRECKED_VEHICLE_MESSAGE", 3.5, HUD_ALERT_DANGER)
		playerCar:Lock(true)
		
		MISSION.OnDone()
		
		missionmanager:SetRefreshFunc( function() 
			return false 
		end ) 

		gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
		return false
	end
	
	if missionmanager:IsTimedOut() then
		gameHUD:ShowAlert("#TIME_UP_MESSAGE", 3.5, HUD_ALERT_DANGER)

		MISSION.OnDone()
		
		missionmanager:SetRefreshFunc( function() 
			return false 
		end ) 
		
		gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
		return false
	end
	
	return true
end

local Point = class()

function PointSpawn()
	
	if MISSION.Over == 1 then
		return false
	end

	local targetCar = MISSION.targetCar
	
	-- Why is it called a cone? Because I stole it from the knock-over-a-cone game mode
	local cone = world:CreateObject("dumpster")
	cone:SetOrigin(targetCar:GetOrigin() + targetCar:GetForwardVector() * vec3(-4))
	cone:SetAngles(targetCar:GetAngles())	
		
	cone:Spawn()
	
	cone:SetVelocity(targetCar:GetForwardVector() * Vector3D.new(1))
		
	table.insert(MISSION.objects, cone)
	Msg("TICK\n")
	missionmanager:ScheduleEvent(function()
		--gameHUD:ShowScreenMessage("kaboom",1)
		local explodePosition = MISSION.objects[1]:GetOrigin()
		
		--ParticleEffects.CreateSmoke(explodePosition, MISSION.smokeObjectDesc)
		--ParticleEffects.CreateSmoke(explodePosition, MISSION.smokeObjectDesc2)
		
		Msg("EVENT\n")
		
		sounds:Emit( EmitParams.new("generic.impact", explodePosition) )
		
		--local nearestCars = ai:QueryTrafficCars(15, explodePosition)
		local nearestCars = ai:QueryTrafficCars(2, explodePosition)
		for i,car in ipairs(nearestCars) do
		
		
			local pursuer = gameutil.CastObject(car, "CAIPursuerCar", GO_CAR_AI)
			if pursuer ~= nil and pursuer:IsAlive() then
				pursuer:SetDamage(200)
				
				pursuer:SetBodyDamage{
					1,
					1,
					1,
					1,
					1,
					1,
					1,
				}
			
				pursuer:SetVelocity(Vector3D.new(0,1,0) * Vector3D.new(2))
				
				--ParticleEffects.CreateSmoke(pursuer:GetOrigin(), MISSION.smokeObjectDesc)
				--ParticleEffects.CreateSmoke(pursuer:GetOrigin(), MISSION.smokeObjectDesc2)
			
			end
		end
		
		if length(MISSION.playerCar:GetOrigin() - explodePosition) < 7.5 then
			local playerCar = MISSION.playerCar
		
			--playerCar:SetDamage(MISSION.playerCar:GetDamage() + 3)
			
			newBodyDamage = playerCar:GetBodyDamage()
			for i, v in pairs(newBodyDamage) do
				v = v + 0.3
				if v > 1 then
					v = 1
				end
				newBodyDamage[i] = v
			end
			
			playerCar:SetBodyDamage(newBodyDamage)
			
			playerCar:SetOrigin(playerCar:GetOrigin() + Vector3D.new(0,-0.05,0))
			
			sounds:Emit( EmitParams.new("menu.click", playerCar:GetOrigin()) )
		end
		
		world:RemoveObject(MISSION.objects[1])
		table.remove(MISSION.objects,1)
	end, 1.3)
		
	local bombRandomizer = math.random(1, 5) * 0.1
	missionmanager:ScheduleEvent( function() 
		PointSpawn()
	end, 2 + bombRandomizer )
end