
world:SetLevelName("default")

MISSION.MusicScript = "rio_day"
MISSION.Init = function()

	MISSION.Settings.EnableTraffic = false

	-- put globals here
	MISSION.Data = {
		startPos = Vector3D.new(-18, 0.44, -99),
		startAng = Vector3D.new(0,-90,0),
		
		targetPosition = Vector3D.new(82, 1, 20),
	}
	
	local AIState = {	
		path = {},
		pathNextChange = 0,
		stuckTimer = 1,
		stuckActiveTimer = 0,
	}

	local aiCar = gameses:CreateCar("carla",CAR_TYPE_NORMAL)
	aiCar:Set("aiState", AIState)
	
	
	-- when updated
	local AIState = aiCar:Get("aiState")

	-- modify properties of AIState

	
	MISSION.aiCar = aiCar;
	aiCar:SetColorScheme(4)
	aiCar:SetOrigin( MISSION.Data.startPos )
	aiCar:SetAngles( MISSION.Data.startAng )
	aiCar:Spawn()
	aiCar:Set("aiState", AIState)
	
	
	
	--aiCar:SetMaxSpeed(80)
	
	-- player car to be near
	local playerCar = gameses:CreateCar("rollo",CAR_TYPE_NORMAL)
	MISSION.playerCar = playerCar
	playerCar = gameses:CreateCar("rollo",CAR_TYPE_NORMAL)
	playerCar:SetMaxDamage(16)
	playerCar:SetColorScheme(0)
	playerCar:SetOrigin( Vector3D.new(-40, 0.44, -99) )
	playerCar:SetAngles( Vector3D.new(0,-90,0) )

	playerCar:Spawn()
	
	--aiCar:SetPursuitTarget( playerCar )
	--aiCar:BeginPursuit(0.0)
	
	gameses:SetPlayerCar( playerCar )
	
	gameHUD:ShowScreenMessage("Getaway AI", 3.5)
	
	gameHUD:AddMapTargetPoint(MISSION.Data.targetPosition)
	gameHUD:AddTrackingObject(MISSION.aiCar, HUD_DOBJ_IS_TARGET)
	
	-- here we start
	MissionManager:SetRefreshFunc( MISSION.Update )
	
	SetMusicState(1)
end

MISSION.RespawnCar = function()
	local aiCar = MISSION.aiCar

	aiCar:SetOrigin( MISSION.Data.startPos )
	aiCar:SetAngles( MISSION.Data.startAng )
	aiCar:Repair()
end

local lineColorY = Vector4D.new(1,1,0,1)
local lineColorR = Vector4D.new(1,0,0,1)
local lineColorG = Vector4D.new(0,1,0,1)

local SPHERERADIUS = 2.0

--------------------------------------------------------------------------------

local function NeedsPathUpdate(path, carPos)
	
	local lastPos = nil

	for i,v in ipairs(path) do
	
		if lastPos == nil then
			lastPos = v
		else
			local posOnPathFac = lineProjection(lastPos, v, carPos)
			
			local posOnPath = lerp(lastPos, v, posOnPathFac)
			
			--debugoverlay:Sphere3D(posOnPath, SPHERERADIUS, lineColorR, 0)
			
			-- test if we inside any path point
			if posOnPathFac >= -0.25 and posOnPathFac <= 1 and length(posOnPath - carPos) < 60 then
				return false
			end
			
			lastPos = vecCopy(v)
		end
	end
	
	return true
end

local function GetNextPath( car, allowRandom, prevPath)

	local carPos = vecCopy(car:GetOrigin())
	local carForward = car:GetForwardVector()
	local carDirVec = vecCopy(carPos + carForward * Vector3D.new(18.0))

	local path = {}
	
	local bestStraight = nil
	local bestStraightPos = carPos
	local bestLanePos = carPos
	
	local currentRoadInfo = gameutil.GetRoadAtPosition(carPos, 32)

	if currentRoadInfo.numLanes > 0 then
	
		local dirVec = currentRoadInfo.directionXZ
		local perpXZ = normalize(currentRoadInfo.endPos - currentRoadInfo.startPos)

		-- get all roads
		for i in range(0,currentRoadInfo.numLanes) do
			-- get straight
			local testStraightPos = currentRoadInfo.startPos + perpXZ*vec3((i-1) * 4.0)
			
			local straight = gameutil.GetStraightAtPosition(testStraightPos, 32)
			
			local strDirVec = straight.directionXZ
			local carDirectionCos = dot(vec3(strDirVec:x(), 0, strDirVec:y()), carForward)
			
			if carDirectionCos > 0 then
				bestStraight = straight
				bestStraightPos = testStraightPos
			end
		end
		
		local randomLaneIdx = 1
		

		do
			local bestRoadInfo = gameutil.GetRoadAtPosition(bestStraightPos, 32)
			
			if bestRoadInfo.numLanes > 0 then
				perpXZ = normalize(bestRoadInfo.endPos - bestRoadInfo.startPos)
				randomLaneIdx = math.floor(math.random()*100) % (bestRoadInfo.numLanes)
				
				local lanePos = bestRoadInfo.startPos + perpXZ*vec3(randomLaneIdx * 4.0)
				
				table.insert(path, lanePos)
			end
		end
		
		local jn = gameutil.GetJunctionAtPosition(bestStraightPos, 32)
		
		if bestStraight == nil and jn.straight ~= nil then
			bestStraight = jn.straight
		end

		do
			local bestRoadInfo = gameutil.GetRoadAtPosition(bestStraight.endPos, 32)
			
			if bestRoadInfo.numLanes > 0 then
				local lanePos = bestRoadInfo.startPos + perpXZ*vec3(randomLaneIdx * 4.0)
							
				--debugoverlay:Sphere3D(bestRoadInfo.startPos, SPHERERADIUS, lineColorG, 0)
				
				table.insert(path, lanePos + vec3(bestStraight.directionXZ:x() * 9, 0, bestStraight.directionXZ:y() * 9))
			end
		end

		--debugoverlay:Sphere3D(bestStraightPos, SPHERERADIUS, lineColorY, 0)

		if #jn.junction.exits > 0 then
			local randExitId = math.floor(math.random()*100) % (#jn.junction.exits)
			local randExit = jn.junction.exits[randExitId+1]
			--debugoverlay:Line3D(randExit.startPos + vec3_up, randExit.endPos + vec3_up, lineColorG, lineColorG, 0)
			
			--table.insert(path, randExit.startPos)
			table.insert(path, randExit.endPos)
		else
			-- TODO: handle dead ends
		end
		
		-- also process each segment of path with navigation
		local newPath = {}
	
		local lastPos = nil

		for i,v in ipairs(path) do
		
			--table.insert(newPath, v)

			if lastPos == nil then
				lastPos = vecCopy(v)
				table.insert(newPath, v)
			else
				local pathResult = gameutil.FindPath(lastPos, v, 2048, false)

				if #pathResult > 0 then
					for _i,_v in ipairs(pathResult) do
						table.insert(newPath, _v)
					end
				else
					table.insert(newPath, v)
				end
				lastPos = v
			end
		end
		
		return newPath
	end
	
	-- try make very random path
	if allowRandom then
		if prevPath == nil then
			local randomPosVec = carPos + vec3(math.random()*100-50, math.random()*100-50, math.random()*100-50)
			return gameutil.FindPath(carPos, randomPosVec, 2048, true)
		else
			return gameutil.FindPath(carPos, prevPath[#prevPath], 2048, true)
		end
	end
	
	return nil
end

-- main mission update
MISSION.Update = function( delta )

	-- update
	local aiCar = MISSION.aiCar;
	
	local aiState = aiCar:Get("aiState")

	local carSpeed = aiCar:GetSpeedWheels()
	local angVel = math.abs(aiCar:GetAngularVelocity():get_y())
	local lateralVel = math.abs(aiCar:GetLateralSliding())
	
	local carPos = vecCopy(aiCar:GetOrigin())
	local carForward = aiCar:GetForwardVector()
	local carDirVec = vecCopy(carPos + carForward * vec3(clamp(carSpeed, -5, 12)))
	
	aiState.pathNextChange = aiState.pathNextChange - delta
	
	if NeedsPathUpdate(aiState.path, carDirVec) or aiState.pathNextChange < 0 then
	
		local allowRandomPath = aiState.pathNextChange > 0
	
		local newPath = GetNextPath(aiCar, false, aiState.path)
	
		if newPath == nil and allowRandomPath then
			aiState.randomPath = GetNextPath(aiCar, true, aiState.path)
			
			aiState.pathPoint = 1
			aiState.pathNextChange = 5 + math.random()*6
		elseif newPath ~= nil and #newPath > 2 then
			aiState.path = newPath
			aiState.randomPath = nil

			aiState.pathPoint = 1
			aiState.pathNextChange = 2 + math.random()*4
		end
	end
	
	local path = if_then_else(aiState.randomPath ~= nil, aiState.randomPath, aiState.path)
	
	if #path > 0 then
		local prevPoint = nil
		for i,v in ipairs(path) do
			if prevPoint == nil then
				prevPoint = v
			else
				debugoverlay:Line3D(prevPoint + vec3_up, v + vec3_up, lineColorY, lineColorY, 0)
				prevPoint = vecCopy(v)
			end
		end
	end
	
	-- pick point on path
	local targetPos = carDirVec
	
	if #path > 0 then
		targetPos = path[1]

		local pinSegment = nil
		local posOnSegment = nil
		local lastPos = nil
		
		local remainingDistOnPath = 5

		for i,v in ipairs(path) do
		
			if lastPos == nil then
				lastPos = v
			else
				local posOnPathFac = lineProjection(lastPos, v, carDirVec)
			
				-- test if we inside any path point
				if pinSegment == nil then
					local posOnPath = lerp(lastPos, v, posOnPathFac)
				
					if posOnPathFac >= -0.1 and posOnPathFac <= 1 then
						debugoverlay:Sphere3D(posOnPath, SPHERERADIUS, lineColorY, 0)
						--debugoverlay:Sphere3D(carDirVec, SPHERERADIUS, lineColorG, 0)
					
						if length(posOnPath - carPos) < 40 then
							pinSegment = i
						end
					end
				elseif posOnSegment == nil then
					local segLen = length(lastPos - v)
				
					if remainingDistOnPath  - segLen < 0 then
						posOnSegment = lerp(lastPos, v, remainingDistOnPath / segLen)
	
						debugoverlay:Sphere3D(posOnSegment + vec3_up, SPHERERADIUS, lineColorG, 0)
						
						--aiState.pathPoint = i
					else
						remainingDistOnPath = remainingDistOnPath - segLen
					end
				end
				
				lastPos = v
			end
		end
		
		targetPos = posOnSegment or path[1]
		
		if aiState.pathPoint+2 > #path and aiState.randomPath == nil then
			aiState.pathNextChange = 0
		end
	end
	
	debugoverlay:Sphere3D(targetPos, SPHERERADIUS, lineColorR, 0)
	
	
	
	local dirToTarget = targetPos - carPos
	local targetSteerFactor = dot(normalize(dirToTarget), aiCar:GetRightVector()) -- * math.pow(2.0 - lateralVel, 1.5)
	
	if lateralVel > 1 then
		local fac = aiCar:GetAngularVelocity():get_y()*0.15
		targetSteerFactor = targetSteerFactor + math.pow(math.abs(fac), 0.75)*sign(fac)
	end
	
	local accelerator = math.pow(1.0 - math.abs(targetSteerFactor), 0.7)
	
	-- check if target is behind
	if dot(carForward, normalize(dirToTarget)) < 0 then
		accelerator = accelerator - 0.5
		
		if carSpeed > 30 then
			doHandbrake = true
		end
	end
	
	local doHandbrake = false
	if math.abs(targetSteerFactor) > 0.85 and carSpeed > 30 then
		accelerator = accelerator - 0.25;
	end
	
	local doBurnout = carSpeed < 40 and lateralVel < 1 and angVel < 1
	
	
	
	if aiState.stuckActiveTimer <= 0 and math.abs(carSpeed) < 4 then
		aiState.stuckTimer = aiState.stuckTimer - delta
		
		if aiState.stuckTimer < 0 then
			aiState.stuckTimer = 2
			aiState.stuckActiveTimer = 2
		end
	else
		aiState.stuckTimer = 2
	end
	
	if aiState.stuckActiveTimer > 0 then
		accelerator = -1
		targetSteerFactor = -targetSteerFactor
		doBurnout = false
		aiState.stuckActiveTimer = aiState.stuckActiveTimer - delta
	end
	
	aiCar:SetControls(accelerator, math.pow(math.abs(targetSteerFactor), 0.8) * sign(targetSteerFactor), false, doHandbrake, doBurnout)

--[[
	local drivePosOnLine = lineProjection(bestStraight.startPos, bestStraight.endPos, carDirVec)
	local drivePos = lerp(bestStraight.startPos, bestStraight.endPos, drivePosOnLine)
	debugoverlay:Sphere3D(drivePos, SPHERERADIUS, lineColorG, 0)
	
	local dirToTarget = drivePos - carPos

	local targetSteerFactor = dot(normalize(dirToTarget), aiCar:GetRightVector())
	
	local collData = gameutil.TestSphere(SPHERERADIUS, carPos, carDirVec, OBJECTCONTENTS_SOLID_OBJECTS )
	
	aiCar:SetControls(1, targetSteerFactor, false, false, false)
	]]
	

	--[[
	local pathResult = gameutil.FindPath(carPos, MISSION.Data.targetPosition, 4096, false)

	if #pathResult > 0 then
		local prevPoint = aiCar:GetOrigin()
		for k,v in ipairs(pathResult) do
			debugoverlay:Line3D(prevPoint + vec3_up, v + vec3_up, lineColorY, lineColorY, 0)
			prevPoint = vecCopy(v)
		end
	end]]

	return true
end