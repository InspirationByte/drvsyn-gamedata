
-- Setup world data
world:SetLevelName("default")
world:SetEnvironmentName("night_clear")
SetMusicName("miami_night")

function MISSION.Init()
	-- setup mission targets
	MISSION.Data = {
		PlayerStart = {
			position = vec3(109.42,0.43,-38.17),
			angles = vec3(0,-90,0),
			car = "rollo",
			colorId = 1,
			maxDamage = 12,
			lock = false
		},
		
		-- target state data for Phase 1
		Phase1 = {
			position = vec3(119, 0.5, 20),
			radius = 10,	-- meters
			showTime = false,
			startMessage = "#MI01_GO_MESSAGE",
			timedOutMessage = "#TIME_UP_MESSAGE",
			wreckedMessage = "#WRECKED_VEHICLE_MESSAGE",
			-- add your other data...
		},
		-- target state data for phase 2
		Phase2 = {
			position = vec3(152, 0.5, 197),
			radius = 10,	-- meters
			timer = 40,		-- seconds
			startMessage = "#MI01_NOTSAFE_MESSAGE",
			timedOutMessage = "Sorry you out of TIME",
			wreckedMessage = "WASTED",
			-- add your other data...
		}
	}

	gameutil.PrecacheStudioModel("models/characters/ped02.egf",
	{
		-- load additional motion package
		"models/characters/test_motion.mop"
	})

	-- setup player car (REQUIRED)
	MISSION.InitPlayerCar( MISSION.Data.PlayerStart )

	-- setup initial state/phase
	MISSION.Phase1()
	
	local ped1 = gameses:CreatePedestrian("models/characters/ped02.egf", -1)
	ped1:SetOrigin(vec3(109.42,0.43,-60.17))
	ped1:Spawn()
	ped1:SetBodyGroups(1 + 2)		-- 2 to make briefcase
	
	-- two ways of initialize array
	local walkingPeds = {
		--ped1,
		--ped2
	}
	
	table.insert(walkingPeds, ped1)
	
	for i in range(1,4) do 
		MissionManager:ScheduleEvent( function() 
			local ped = gameses:CreatePedestrian("models/characters/ped01.egf", -1)
			ped:SetOrigin(vec3(109.42,0.43,-60.17))
			ped:Spawn()
			ped:SetBodyGroups(1)		-- 2 to make briefcase
			
			table.insert(walkingPeds, ped)
		end, 0.4 * i )
	end
	
	MissionManager:SetPluginRefreshFunc( "pedswalk", function()
	
		local run = true
		
		for i,ped in ipairs(walkingPeds) do 
			if PedWalkToTarget(ped, gameses:GetPlayerCar():GetOrigin(), run) < 1.2 then
				
				-- remove pedestrian from world and table
				ped:Remove()
				table.remove(walkingPeds, i)
			end
			
			-- no more pedestrians to walk?
			if #walkingPeds == 0 then
				MissionManager:SetPluginRefreshFunc( "pedswalk", nil)
			end
		end

	end)
end

-- PHASE1 - Getting to the meeting
function MISSION.Phase1()
	-- setup your stuff
	local targetData = MISSION.Data.Phase1 -- we using Phase1 data
	
	-- setup other cars, stuff, message etc
	-- ...
	MISSION.Settings.EnableCops = false	-- disabled cops

	-- change timer, setup HUD target, message
	MISSION.SetupTarget(targetData)

	-- setup your update function
	-- this will be called each frame of game update
	MissionManager:SetRefreshFunc(function(delta)
		local playerCar = gameses:GetPlayerCar()

		-- do generic updates (cops, timer, damage)
		MISSION.GenericUpdate(targetData, delta)
		
		-- you still can access 'targetData' in this scope		
		-- if player is near target - switch phases
		if MISSION.CheckTargetCompletion(targetData) then
			-- you can process your own condition before really completing target
			MISSION.CompleteTarget(targetData)
				
			MissionManager:ScheduleEvent( function() 
				-- switch to phase 2
				MISSION.Phase2()
			end, 2);
				

		end

		-- process your own stuff and check conditions PHASE 1

		-- ...
		
		return true -- true means timer is enabled
	end)
end

-- PHASE2 - getting away
function MISSION.Phase2()
	-- setup your stuff
	local targetData = MISSION.Data.Phase2 -- we using Phase2 data
	
	-- setup other cars, stuff, messages etc
	-- ...
	MISSION.Settings.EnableCops = true -- now we enable cops
	
	-- change timer, setup HUD target, message
	MISSION.SetupTarget(targetData)
	
	-- setup your update function
	-- this will be called each frame of game update
	MissionManager:SetRefreshFunc(function(delta)

		local playerCar = gameses:GetPlayerCar()

		-- do generic updates (cops, timer, damage)
		MISSION.GenericUpdate(targetData, delta)
		
		-- you still can access 'targetData' in this scope
		-- if player is near target - switch phases
		if MISSION.CheckTargetCompletion(targetData) then
			-- you can process your own condition before really completing target
			MISSION.CompleteTarget(targetData)
			
			-- dun!
			MISSION.CompleteMission() -- 
		end
		
		-- process your own stuff and check conditions for PHASE 2
		--...
		
		return true -- true means timer is enabled
	end)
end

-----------------------------------------------------------------
-- SERVICE STUFF BELOW

-- reset state
function MISSION.ResetState()
	MissionManager:SetRefreshFunc(function(dt) 
		return false -- false means
	end)
end

-- you can setup any player car with that
function MISSION.InitPlayerCar( start )
	local playerCar = gameses:CreateCar(start.car, CAR_TYPE_NORMAL)
	
	-- setup
	playerCar:SetOrigin( start.position )
	playerCar:SetAngles( start.angles )
	playerCar:SetMaxDamage( start.maxDamage )
	playerCar:Lock(start.lock)
	
	-- FIXME: other params?

	playerCar:Spawn()
	playerCar:SetColorScheme( start.colorId )
	
	-- gain control over car
	gameses:SetPlayerCar( playerCar )
end

-- generic mission state updates
function MISSION.GenericUpdate(targetData, delta)
	local playerCar = gameses:GetPlayerCar()

	-- update cops
	UpdateCops( playerCar, delta )
	
	-- check for timeout
	if targetData.timer ~= nil and MissionManager:IsTimedOut() then
		MISSION.SetFailed(targetData.timedOutMessage)
		return false
	end
	
	-- check if player vehicle is wrecked
	if CheckVehicleIsWrecked( playerCar, MISSION.Data, delta ) then
		MISSION.SetFailed(targetData.wreckedMessage)
		return false
	end
end

-- sets up target marker, timer
function MISSION.SetupTarget(targetData)

	if targetData.startMessage ~= nil then
		gameHUD:ShowScreenMessage(targetData.startMessage, 3.5)
	end
	
	-- enable timer only if time is greater
	if targetData.timer ~= nil then
		MissionManager:EnableTimeout( true, targetData.timer ) -- enable, time
	else
		MissionManager:EnableTimeout( false, 0 )
	
		if targetData.showTime ~= nil then
			MissionManager:ShowTime(targetData.showTime)
		end
	end

	-- add target on map and 3D world
	targetData.targetHandle = gameHUD:AddMapTargetPoint(targetData.position)
end

-- completes target
function MISSION.CompleteTarget(targetData)
	-- reset update function (in case if we're going use delays with ScheduleEvent)
	MISSION.ResetState()

	-- remove HUD target
	gameHUD:RemoveTrackingObject(targetData.targetHandle)
	
	-- remove timers
	--MissionManager:EnableTimeout( false, 0 )
	--MissionManager:ShowTime(false)
end

-- checks target radius
function MISSION.CheckTargetCompletion(targetData)
	local playerCar = gameses:GetPlayerCar()

	if length(targetData.position - playerCar:GetOrigin()) < targetData.radius then
		return true
	end
	
	return false
end

-- generic FAILURE
function MISSION.SetFailed(reasonText)
	MISSION.ResetState()

	local playerCar = gameses:GetPlayerCar()

	-- lock car, signal mission failure
	playerCar:Lock(true)
	gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )

	-- show message
	if reasonText ~= nil then
		gameHUD:ShowAlert(reasonText, 3.5, HUD_ALERT_DANGER)
	end
end

-- generic COMPLETION
function MISSION.CompleteMission()
	MISSION.ResetState()

	-- lock car, signal mission completion
	local playerCar = gameses:GetPlayerCar()
	playerCar:Lock(true)
	gameses:SignalMissionStatus( MIS_STATUS_SUCCESS, 4.0 )
	
	-- show messages etc
	gameHUD:ShowAlert("#MENU_GAME_TITLE_MISSION_SUCCESS", 3.5, HUD_ALERT_SUCCESS)
	gameHUD:ShowScreenMessage("#MI01_SUCCESS_MESSAGE", 3.5)
end