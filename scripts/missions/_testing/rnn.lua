module("NeuralNet.lua")

world:SetLevelName("ai_pursuer_test")

MISSION.MusicScript = "vegas_night"
MISSION.Init = function()

	MISSION.Settings.EnableTraffic = false

	-- put globals here
	MISSION.Data = {
		startPos = Vector3D.new(9.91, 0.64, -80.80),
		startAng = Vector3D.new(-179.30, -7.52, 179.98),
		
		targetPosition = Vector3D.new(192.35, 0.64, -141.81),
	}

	MISSION.aiCar = gameses:CreateCar("mustang_f",CAR_TYPE_NORMAL) --gameses:CreatePursuerCar("mustang_f", PURSUER_TYPE_COP)
	MISSION.aiCar:SetColorScheme(4)
	MISSION.aiCar:SetOrigin( MISSION.Data.startPos )
	MISSION.aiCar:SetAngles( MISSION.Data.startAng )
	MISSION.aiCar:Spawn()
	
	MISSION.aiCar:SetMaxSpeed(80)
	
	-- player car to be near
	MISSION.playerCar = gameses:CreateCar("rollo",CAR_TYPE_NORMAL)
	MISSION.playerCar:SetMaxDamage(16)
	MISSION.playerCar:SetColorScheme(0)
	--MISSION.playerCar:SetOrigin( Vector3D.new(8.18, 0.54, -71.78) )
	MISSION.playerCar:SetAngles( Vector3D.new(-179.68, -11.92, 179.93) )
	
	MISSION.playerCar:SetOrigin( Vector3D.new(14.43, 0.54, -102.02) )
	MISSION.playerCar:Spawn()
	
	--MISSION.aiCar:SetPursuitTarget( MISSION.playerCar )
	--MISSION.aiCar:BeginPursuit(0.0)
	
	gameses:SetPlayerCar( MISSION.playerCar )
	
	gameHUD:ShowScreenMessage("Recurrent neural network AI test", 3.5)
	
	gameHUD:AddMapTargetPoint(MISSION.Data.targetPosition)
	gameHUD:AddTrackingObject(MISSION.aiCar, HUD_DOBJ_IS_TARGET)
	
	if NEURALNET == nil then
		NEURALNET = NN.createNet(10, 4, 6, 2, "LeakyReLU", false, 0.1 )
	end
	
	-- here we start
	missionmanager:SetRefreshFunc( MISSION.Update )
	
	SetMusicState(1)
end

MISSION.RespawnCar = function()
	MISSION.aiCar:SetOrigin( MISSION.Data.startPos )
	MISSION.aiCar:SetAngles( MISSION.Data.startAng )
	MISSION.aiCar:Repair()
end

--------------------------------------------------------------------------------

local nnLearnCvar = console.FindOrCreateCvar("nn_train", "0", "", 0)
local nnLearnRate = console.FindOrCreateCvar("nn_rate", "0.2", "", 0)

-- main mission update
MISSION.Update = function( delta )

	local lineColor = Vector4D.new(1,1,0,1)

	-- update
	local aiCar = MISSION.aiCar;

	local carPos = vecCopy(aiCar:GetOrigin())
	local carDirVec = vecCopy(carPos + aiCar:GetForwardVector() * Vector3D.new(18.0))
	
	local SPHERERADIUS = 2.0
	
	local colldata = gameutil.TestSphere(SPHERERADIUS, carPos, carDirVec, OBJECTCONTENTS_SOLID_OBJECTS )
	
	local angVel = aiCar:GetAngularVelocity():get_y() / 10.0
	local lateralVel = aiCar:GetLateralSliding() / 10.0
	
	local targetPos = vecCopy(MISSION.playerCar:GetOrigin())
	local dirToTarget = targetPos - carPos
	
	local targetSteerFactor = dot(normalize(dirToTarget), aiCar:GetRightVector()) * 0.5 + 0.5
	local targetFacingFactor = dot(normalize(dirToTarget), aiCar:GetForwardVector()) * 0.5 + 0.5
	
	local inputs = {
		aiCar:GetSpeed() / 180,										-- speed from car
		math.abs(lateralVel),										-- lateral sliding
		if_then_else(lateralVel < 0, 0, 1),	-- sign of lateral
		math.abs(angVel),														-- angular velocity
		if_then_else(angVel < 0, 0, 1),	-- sign of angular
		dot(aiCar:GetUpVector(), Vector3D.new(0,1,0)) * 0.5 + 0.5,	-- car upright
		0.5, --if_then_else(colldata:GetFract() < 1, dot(colldata:GetNormal(), aiCar:GetRightVector()), 0) * 0.5 + 0.5,	-- left/right normal factor
		1.0, --colldata:GetFract(),
		targetSteerFactor,
		targetFacingFactor
	}
	
	debugoverlay:Sphere3D(colldata:GetPosition(), SPHERERADIUS, lineColor, 0.0)
	
	debugoverlay:Text(lineColor, string.format("spd: %g", inputs[1]))
	debugoverlay:Text(lineColor, string.format("lateral: %g s: %g", inputs[2], inputs[3]))
	debugoverlay:Text(lineColor, string.format("angular: %g s: %g", inputs[4], inputs[5]))
	debugoverlay:Text(lineColor, string.format("upright: %g", inputs[6]))
	debugoverlay:Text(lineColor, string.format("coll left/right: %g", inputs[7]))
	debugoverlay:Text(lineColor, string.format("coll fract: %g", inputs[8]))
	debugoverlay:Text(lineColor, string.format("target steer: %g", inputs[9]))
	debugoverlay:Text(lineColor, string.format("target face: %g", inputs[10]))
	
	if nnLearnCvar:GetInt() > 0 then
		local targets = {inputs[8], inputs[7]}
	
		if nnLearnCvar:GetInt() == 2 then
			--targets[1] = targetFacingFactor
			--if colldata:GetFract() >= 1 then
				targets[1] = targetFacingFactor
				targets[2] = targetSteerFactor
			--end
		elseif nnLearnCvar:GetInt() == 3 then
			targets[1] = aiCar:GetAccelBrake() * 0.5 + 0.5
			targets[2] = aiCar:GetSteering() * 0.5 + 0.5
		end
		
		NN.backwardNet(NEURALNET, nnLearnRate:GetFloat(), inputs, targets)
		
		local out = NN.forwardNet(NEURALNET, inputs)
		
		-- 					accel_brake, steering, fastSteer, handbrake, burnout
		aiCar:SetControls(out[1] * 2.0 - 1.0,out[2] * 2.0 - 1.0,false,false,false)
	else
		local out = NN.forwardNet(NEURALNET, inputs)
		
		-- 					accel_brake, steering, fastSteer, handbrake, burnout
		aiCar:SetControls(out[1] * 2.0 - 1.0,out[2] * 2.0 - 1.0,false,false,false)
	end

	local pathResult = gameutil.FindPath(carPos, MISSION.Data.targetPosition, 4096, false)

	if #pathResult > 0 then
		local prevPoint = aiCar:GetOrigin()
		for k,v in ipairs(pathResult) do
			debugoverlay:Line3D(prevPoint, v, lineColor, lineColor, 0)
			prevPoint = vecCopy(v)
		end
	end

	return true
end