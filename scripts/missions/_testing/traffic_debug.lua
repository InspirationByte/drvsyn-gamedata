--------------------------------------------------------------------------------
-- Intro title
--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 11 Mar 2015
--------------------------------------------------------------------------------

world:SetLevelName("default")
world:SetEnvironmentName("day_clear")

MISSION.MusicScript = "la_day"

local g_car = console.FindCvar("g_car")

MISSION.Init = function()

	-- put globals here
	MISSION.Data = {
		startPos = Vector3D.new(54,10.31,-45),
		startAng = Vector3D.new(0,0,0),
		notOnGroundTime = 10.0
	}
	
	local freerideSpawn = world:FindObjectOnLevel("traffic_debug_spawn")

	MISSION.Data.startPos = if_then_else(freerideSpawn.position ~= nil, freerideSpawn.position, Vector3D.new(0,0,0))
	MISSION.Data.startAng = if_then_else(freerideSpawn.rotation ~= nil, freerideSpawn.rotation, Vector3D.new(0,0,0))

	-- car name		maxdamage	pos ang
	MISSION.playerCar = gameses:CreateCar(g_car:GetString(),CAR_TYPE_NORMAL)
	
	if MISSION.playerCar == nil then
		return
	end
	
	MISSION.playerCar:SetMaxDamage(16)
	MISSION.playerCar:SetColorScheme(1)
	
	MISSION.playerCar:SetOrigin( MISSION.Data.startPos )
	MISSION.playerCar:SetAngles( MISSION.Data.startAng )
	
	MISSION.playerCar:AlignToGround()
	
	MISSION.playerCar:Spawn()
	
	gameses:SetPlayerCar( MISSION.playerCar )
	gameHUD:ShowScreenMessage("#TAKE_RIDE_TITLE", 3.5)
	
	-- here we start
	MissionManager:SetRefreshFunc( MISSION.Update )
end

--------------------------------------------------------------------------------

-- main mission update
MISSION.Update = function( delta )

	
	
	return true
end