--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2015
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- Cop test script
--------------------------------------------------------------------------------

world:SetLevelName("default")
world:SetEnvironmentName("dawn_clear")

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

MISSION.MusicScript = "havana_day"

MISSION.Init = function()

	MISSION.Data = {
		tags = 0,
		landmarks = {
			Vector3D.new(103,0.5,-53),
			Vector3D.new(429,0.5,-53),
			Vector3D.new(657,0.5,-588),
			Vector3D.new(-204,0.5,-472),
			Vector3D.new(-244,0.5,162),
			Vector3D.new(125,3.3,461),
			Vector3D.new(568,0.5,343),
			Vector3D.new(346,1.5,829),
		}
	}
	
	-- adjust cops
	MISSION.Settings.EnableCops = true
	MISSION.Settings.EnableRoadBlock = false
	MISSION.Settings.CopAccelerationScale = 3.0
	MISSION.Settings.CopMaxSpeed = 1000;
	MISSION.Settings.MinCops = 24
	MISSION.Settings.MaxCops = 24
	MISSION.Settings.MaxCopDamage = 100;
	
	
	-- car name		maxdamage	pos ang
	local playerCar = gameses:CreateCar("taxi", CAR_TYPE_NORMAL)
	MISSION.playerCar = playerCar
	
	playerCar:Set("OnCarCollision", MISSION.PlayerCarHit)
	
	local landmarkId = math.floor(math.random()*100) % (#MISSION.Data.landmarks)
	Msg("landmarkId "..landmarkId.."\n")
	
	playerCar:SetOrigin( MISSION.Data.landmarks[landmarkId+1] ) --Vector3D.new(41.44, 0.48, 107.64) )
	playerCar:SetAngles( Vector3D.new(-128.42, -85.25, 128.33) )

	playerCar:SetColorScheme(3)
	playerCar:Spawn()

	gameses:SetPlayerCar( playerCar )
	
	MissionManager:EnableTimeout( true, 120 ) -- enable time display
	
	gameHUD:ShowScreenMessage("Save the taxi driver license from evil spirits!", 3.5)
	
	-- this is temporary to spawn cops
	MISSION.Settings.CopRespawnInterval = 0
	MISSION.Settings.CopWantedRespawnInterval2 = 0
	
	ai:SetAllCarsPursuers(true)
	
	ai:SetCopCar("", PURSUER_TYPE_COP)
	ai:SetCopCar("", PURSUER_TYPE_GANG)
	
	-- here we start
	MissionManager:SetRefreshFunc( function(dt)
	
		-- do first game update frame to spawn cops and adjust params
		ai:MakePursued( playerCar )

		-- continue with standard update routine
		MissionManager:SetRefreshFunc(MISSION.Update)
		
		return false
	end	)
end

--------------------------------------------------------------------------------

function MISSION.PlayerCarHit(self, props)
	local aiCar = gameutil.CastObject(props.hitBy, "CAIPursuerCar", GO_CAR_AI)
	
	if aiCar ~= nil and aiCar:IsPursuer() then
		aiCar:SetDamage(1000.0)
		aiCar:Remove()
		
		MISSION.Data.tags = MISSION.Data.tags + 1
		gameHUD:ShowScreenMessage("Hit x"..MISSION.Data.tags.."!", 1.0)
		
		--MissionManager:ScheduleEvent(function() 
		--	aiCar:Remove()
		--end, 3.0)
	end
end

function MISSION.OnFailed()
	gameses:SetLeadCar( MISSION.playerCar )
	MISSION.playerCar:Lock(true)
end

-- main mission update
function MISSION.Update( delta )

	local playerCar = MISSION.playerCar

	UpdateCops( playerCar, delta )
	ai:MakePursued( playerCar )
	
	if MissionManager:IsTimedOut() then
		gameHUD:ShowAlert("You saved the license", 3.5, HUD_ALERT_SUCCESS)
		ai:RemoveAllCars()
		gameses:SignalMissionStatus( MIS_STATUS_SUCCESS, 4.0 )
		
		MissionManager:SetRefreshFunc( function() 
			return false 
		end ) 
	end

	-- check player vehicle is wrecked
	if CheckVehicleIsWrecked( playerCar, MISSION.Data, delta ) then
		gameHUD:ShowAlert("#WRECKED_VEHICLE_MESSAGE", 3.5, HUD_ALERT_DANGER)
		
		MISSION.OnFailed()
		
		MissionManager:SetRefreshFunc( function() 
			return false 
		end ) 

		gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
		return false
	end
	
	return true
end