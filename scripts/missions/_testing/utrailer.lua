--------------------------------------------------------------------------------
-- Default mission script
--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 11 Mar 2015
--------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

local g_car = console.FindCvar("g_car")

world:SetLevelName("default")

MISSION.Init = function()

	world.OnRegionLoaded = function( self, args )
	--	Msg( string.format(" + Region %d loaded\n", args.regionId) )
	end

	world.OnRegionUnloaded = function( self, args )
	--	Msg( string.format(" - Region %d unloaded\n", args.regionId) )
	end

	-- put globals here
	MISSION.Data = {
		startPos = Vector3D.new(54,0.31,-45),
		startAng = Vector3D.new(0,0,0),
		notOnGroundTime = 10.0
	}

	-- car name		maxdamage	pos ang
	MISSION.playerCar = gameses:CreateCar(g_car:GetString(),CAR_TYPE_NORMAL)
	MISSION.playerCar:SetMaxDamage(16)
	MISSION.playerCar:SetColorScheme(1)
	
	MISSION.playerCar:SetOrigin( MISSION.Data.startPos )
	MISSION.playerCar:SetAngles( MISSION.Data.startAng )
	
	MISSION.playerCar:AlignToGround()
	
	MISSION.playerCar:Spawn()
	gameses:SetPlayerCar( MISSION.playerCar )
	
	local uTrailer = gameses:CreateCar("utrailer",CAR_TYPE_NORMAL)
	uTrailer:SetMaxDamage(4)
	uTrailer:SetOrigin( MISSION.Data.startPos - Vector3D.new(0,0,4) )
	uTrailer:SetAngles( MISSION.Data.startAng )
	uTrailer:AlignToGround()
	uTrailer:Spawn()
	
	-- connect trailer to our car
	MISSION.playerCar:HingeVehicle(1, uTrailer, 0);
	
	gameHUD:ShowScreenMessage("#TAKE_RIDE_TITLE", 3.5)
	
	-- here we start
	MissionManager:SetRefreshFunc( MISSION.Update )
end

--------------------------------------------------------------------------------

MISSION.End = function(status)
	local playerCar = gameses:GetPlayerCar()

	playerCar:Lock(true)

	MissionManager:SetRefreshFunc( function() 
		return false 
	end ) 

	gameses:SignalMissionStatus( status, 4.0 )
end

-- main mission update
MISSION.Update = function( delta )
	local playerCar = gameses:GetPlayerCar()

	UpdateCops( playerCar, delta )

	--[[if MISSION.playerCar:GetHingedVehicle() == nil then
	
		gameHUD:ShowAlert("#FAILED_MESSAGE", 3.5, HUD_ALERT_DANGER)
		gameHUD:ShowScreenMessage("#TRAILER_DAMAGED", 3.5)
	
		MISSION.End(MIS_STATUS_FAILED)
		
		return false
	end]]
	
	if CheckVehicleIsWrecked( playerCar, MISSION.Data, delta ) then
	
		gameHUD:ShowAlert("#WRECKED_VEHICLE_MESSAGE", 3.5, HUD_ALERT_DANGER)
	
		MISSION.End(MIS_STATUS_FAILED)

		return false
	end
	
	return true
end