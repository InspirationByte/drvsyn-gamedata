--------------------------------------------------------------------------------
-- Default mission script
--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 11 Mar 2015
--------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

local g_car = console.FindCvar("g_car")

world:SetLevelName("default")

MISSION.Init = function()

	-- put globals here
	MISSION.Data = {
		startPos = Vector3D.new(54,0.31,-45),
		startAng = Vector3D.new(0,0,0),
		notOnGroundTime = 10.0
	}

	-- car name		maxdamage	pos ang
	local playerCar = gameses:CreateCar(g_car:GetString(),CAR_TYPE_NORMAL)
	playerCar:SetMaxDamage(16)
	playerCar:SetColorScheme(1)
	
	playerCar:SetOrigin( MISSION.Data.startPos )
	playerCar:SetAngles( MISSION.Data.startAng )
	
	playerCar:AlignToGround()
	
	playerCar:Spawn()
	gameses:SetPlayerCar( playerCar )
	
	MISSION.playerCar = playerCar
	
	local crate = objects:CreateObject("dummy")
	crate:SetModel("models/cityobjs/cardbox3.egf")
	crate:SetLocalOrigin( vec3(0,0.45,-1.5) )
	crate:SetLocalAngles( vec3(0,0,0) )
	crate:Spawn()
	crate:SetParent(MISSION.playerCar)

	MISSION.crate = crate;
	MISSION.crate_vel = vec3(0)
	
	gameHUD:ShowScreenMessage("#TAKE_RIDE_TITLE", 3.5)
	
	-- here we start
	MissionManager:SetRefreshFunc( MISSION.Update )
end

--------------------------------------------------------------------------------

MISSION.End = function(status)
	local playerCar = gameses:GetPlayerCar()

	playerCar:Lock(true)

	MissionManager:SetRefreshFunc( function() 
		return false 
	end ) 

	gameses:SignalMissionStatus( status, 4.0 )
end

MISSION.MoveCrate = function(delta)
	local playerCar = gameses:GetPlayerCar()
	local crate = MISSION.crate
	
	local curCratePos = crate:GetLocalOrigin()
	
	local crateLocalVel = playerCar:GetVelocityAtLocalPoint(curCratePos)
	
	local vel = (crateLocalVel - MISSION.crate_vel) * vec3(1,9,1)
	if vel:y() > 0 then
		vel:y(0)
	end
	
	MISSION.crate_vel = crateLocalVel
	
	-- move crate
	local newCratePos = curCratePos - vel * vec3(delta * 2)
	
	-- apply 'gravity'
	newCratePos:y(newCratePos:y() - delta * 9.0)
	
	-- restrain 
	newCratePos = clamp(newCratePos, vec3(-0.25,0.45,-2.0), vec3(0.25, 1.5, -1.25))
	
	crate:SetLocalOrigin(newCratePos)
end

-- main mission update
MISSION.Update = function( delta )
	local playerCar = gameses:GetPlayerCar()

	UpdateCops( playerCar, delta )

	--[[if MISSION.playerCar:GetHingedVehicle() == nil then
	
		gameHUD:ShowAlert("#FAILED_MESSAGE", 3.5, HUD_ALERT_DANGER)
		gameHUD:ShowScreenMessage("#TRAILER_DAMAGED", 3.5)
	
		MISSION.End(MIS_STATUS_FAILED)
		
		return false
	end]]
	
	MISSION.MoveCrate(delta)
	
	if CheckVehicleIsWrecked( playerCar, MISSION.Data, delta ) then
	
		gameHUD:ShowAlert("#WRECKED_VEHICLE_MESSAGE", 3.5, HUD_ALERT_DANGER)
	
		MISSION.End(MIS_STATUS_FAILED)

		return false
	end
	
	return true
end