--------------------------------------------------------------------------------
-- Default mission script
--------------------------------------------------------------------------------
-- By Shurumov Ilya
-- 11 Mar 2015
--------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

local g_car = console.FindCvar("g_car")

world:SetLevelName("ai_test")
world:SetEnvironmentName("day_clear")

MISSION.startPosIdx = 1

function MISSION.Init()

	MISSION.MusicScript = "vegasday"

	-- put globals here
	defaultMissionData = {
		startPos = Vector3D.new(30.14,0.52,82.24),
		startAng = Vector3D.new(0,0,0),
		notOnGroundTime = 10.0
	}
	
	AIStartPos = {
		{Vector3D.new(120.82,0.52,142.26), Vector3D.new(0,90,0)},
		{Vector3D.new(73.95, 0.34, 185.11), Vector3D.new(0, 180, 0)},
		{Vector3D.new(78.25, 0.35, 182.31), Vector3D.new(0,180,0)},
		{Vector3D.new(46.09, 0.34, 134.01), Vector3D.new(0,-90,0)},
	}
	
	local aiPos = AIStartPos[MISSION.startPosIdx]
	
	MISSION.startPosIdx = MISSION.startPosIdx + 1

	if MISSION.startPosIdx > #AIStartPos then
		MISSION.startPosIdx = 1
	end

	MISSION.Settings.EnableTraffic = false
	ai:SetTrafficCarsEnabled(false)

	-- car name		maxdamage	pos ang
	local playerCar = gameses:CreateCar(g_car:GetString(),CAR_TYPE_NORMAL)
	MISSION.playerCar = playerCar

	playerCar:SetMaxDamage(12)
	playerCar:SetColorScheme(1)
	
	playerCar:SetOrigin( defaultMissionData.startPos )
	playerCar:SetAngles( defaultMissionData.startAng )
	
	playerCar:Spawn()
	
	--local uTrailer = gameses:CreateCar("utrailer",CAR_TYPE_NORMAL)
	--uTrailer:SetOrigin( defaultMissionData.startPos - Vector3D.new(0,0,4) )
	--uTrailer:SetAngles( defaultMissionData.startAng )
	--uTrailer:Spawn()
	
	gameses:SetPlayerCar( playerCar )
	
	-- connect trailer to our car
	--playerCar:HingeVehicle(1, uTrailer, 0);
	
	-- our test car
	
	local trafficTest = gameses:CreateCar("carla", CAR_TYPE_TRAFFIC_AI)
	
	MISSION.trafficTest = gameutil.CastObject(trafficTest, "CAITrafficCar", GO_CAR_AI)
	
	MISSION.trafficTest:SetOrigin( aiPos[1] )
	MISSION.trafficTest:SetAngles( aiPos[2] )
	
	MISSION.trafficTest:Spawn()
	MISSION.trafficTest:AlignToGround()
	
	MISSION.trafficTest:InitAI(false)

	gameHUD:ShowScreenMessage("AI traffic test", 3.5)

	-- here we start
	MissionManager:SetRefreshFunc( MISSION.Update )
end

--------------------------------------------------------------------------------

-- main mission update
function MISSION.Update( delta )

	UpdateCops( MISSION.playerCar, delta )
	
	
	return true
end