--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2015
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- Cop test script
--------------------------------------------------------------------------------

world:SetLevelName("default")
world:SetEnvironmentName("duskevening_clear")

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

MISSION.MusicScript = "la_day"
	
MISSION.Init = function()
	
	MISSION.Data = {
		loseTailMessage = false
	}
	
	MISSION.Settings.EnableCops = false
	
	-- car name		maxdamage	pos ang
	local playerCar = gameses:CreateCar("torino", CAR_TYPE_NORMAL)
	
	
	playerCar:SetMaxSpeed(140)
	playerCar:SetTorqueScale(1.1)
	playerCar:SetColorScheme(3)
	playerCar:SetMaxDamage(6.0)

	playerCar:SetOrigin( Vector3D.new(134.96,0.31,124.11) )
	playerCar:SetAngles( Vector3D.new(0,90,0) )

	playerCar:Spawn()

	local opponentCar = gameses:CreateCar("cop_regis", CAR_TYPE_NORMAL)

	opponentCar:SetOrigin( Vector3D.new(174.96,0.41,124.11) )
	opponentCar:SetAngles( Vector3D.new(0,90,0) )

	opponentCar:Spawn()
	opponentCar:AlignToGround()
	
	opponentCar:SetMaxDamage(100.0)
	opponentCar:SetMaxSpeed(190)
	opponentCar:SetTorqueScale(1.8)

	--var pursuerAI = ai:MakeDriverAI( DEFAULT_PURSUER )
	--opponentCar:SetDriverAI(pursuerAI)
	
	--opponentCar:SetPursuitTarget( playerCar )
	--opponentCar:BeginPursuit(0.0)

	MISSION.playerCar = opponentCar
	MISSION.opponentCar = playerCar

	gameses:SetPlayerCar( opponentCar )
	gameses:SetLeadCar( playerCar )

	MissionManager:EnableTimeout( true, 98 ) -- enable, time
	
	gameHUD:ShowScreenMessage("#LOSE_TAIL_MESSAGE", 3.5)
	
	--ai:SetMaxTrafficCars(0)
	
	-- here we start
	MissionManager:SetRefreshFunc( MISSION.Update )
end

--------------------------------------------------------------------------------

-- main mission update
function MISSION.Update( delta )

	local playerCar = MISSION.playerCar
	local opponentCar = MISSION.opponentCar

	UpdateCops( playerCar, delta )
	--[[
	if playerCar:GetPursuedCount() == 0 then
		gameHUD:ShowAlert("#WELL_DONE_MESSAGE", 3.5, HUD_ALERT_SUCCESS)
		
		if world:IsValidObject(opponentCar) then
			opponentCar:Enable(false)
		end

		playerCar:Lock(true)
		
		MissionManager:SetRefreshFunc( function() 
			return false 
		end ) 

		gameses:SignalMissionStatus( MIS_STATUS_SUCCESS, 4.0 )
		return false
	end]]
	
	-- check player vehicle is wrecked
	if CheckVehicleIsWrecked( playerCar, MISSION.Data, delta ) then
		gameHUD:ShowAlert("#WRECKED_VEHICLE_MESSAGE", 3.5, HUD_ALERT_DANGER)
		
		playerCar:Lock(true)
		
		MissionManager:SetRefreshFunc( function() 
			return false 
		end ) 

		gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
		return false
	end

	if MissionManager:IsTimedOut() then
		playerCar:Lock(true)
		
		gameHUD:ShowAlert( "#TIME_UP_MESSAGE", 4.0, HUD_ALERT_DANGER)
		
		MissionManager:SetRefreshFunc( function() 
			return false 
		end ) 
		
		gameses:SignalMissionStatus( MIS_STATUS_FAILED, 4.0 )
		
		return false
	end
	
	return true
end
