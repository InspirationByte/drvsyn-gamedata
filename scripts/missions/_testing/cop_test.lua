--//////////////////////////////////////////////////////////////////////////////////
--// Copyright � Inspiration Byte
--// 2009-2015
--//////////////////////////////////////////////////////////////////////////////////

--------------------------------------------------------------------------------
-- Cop test script
--------------------------------------------------------------------------------

world:SetLevelName("ai_pursuer_test")
world:SetEnvironmentName("duskevening_clear")

----------------------------------------------------------------------------------------------
-- Mission initialization
----------------------------------------------------------------------------------------------

MISSION.startPosIdx = 1

MISSION.Init = function()
	MISSION.MusicScript = "la"
	
	MISSION.Data = {
		loseTailMessage = false,
		startPos = {
			{Vector3D.new(182,0.42,-15), Vector3D.new(0,-120,0)},
			{Vector3D.new(212,0.42,-20), Vector3D.new(0,-120,0)},
			{Vector3D.new(231,0.42,-140), Vector3D.new(0,0,0)},
			{Vector3D.new(238,0.42,-118), Vector3D.new(0,90,0)},
		},
		AIStartPos = {
			{Vector3D.new(8.0,0.42,-69.43), Vector3D.new(0,-90,0)},
			{Vector3D.new(8.0,0.42,-69.43), Vector3D.new(0,-90,0)},
			{Vector3D.new(8.0,0.42,-69.43), Vector3D.new(0,-90,0)},
			{Vector3D.new(231,0.42,-160), Vector3D.new(0,90,0)},
		}
	}
	
	MISSION.Settings.EnableCops = false
	
	-- car name		maxdamage	pos ang
	local playerCar = gameses:CreateCar("torino", CAR_TYPE_NORMAL)
	MISSION.playerCar = playerCar
	
	playerCar:SetMaxSpeed(140)
	playerCar:SetTorqueScale(1.1)
	playerCar:SetColorScheme(5)
	playerCar:SetMaxDamage(6.0)

	local plrPos = MISSION.Data.startPos[MISSION.startPosIdx]
	local aiPos = MISSION.Data.AIStartPos[MISSION.startPosIdx]
	
	playerCar:SetOrigin( plrPos[1] )
	playerCar:SetAngles( plrPos[2] )

	gameHUD:ShowScreenMessage("Pursuer navigation test " .. MISSION.startPosIdx, 3.5)
	
	MISSION.startPosIdx = MISSION.startPosIdx + 1

	if MISSION.startPosIdx > #MISSION.Data.startPos then
		MISSION.startPosIdx = 1
	end

	playerCar:Spawn()
	
	--playerCar:Set("OnCarCollision", MISSION.OnPlayerCollide)

	local opponentCar = gameses:CreateCar("cop_regis", CAR_TYPE_PURSUER_COP_AI)
	opponentCar = gameutil.CastObject(opponentCar, "CAIPursuerCar", GO_CAR_AI)
	
	opponentCar:SetOrigin( aiPos[1] )
	opponentCar:SetAngles( aiPos[2] )

	opponentCar:Spawn()
	opponentCar:AlignToGround()
	
	opponentCar:SetMaxDamage(10000.0)
	opponentCar:SetMaxSpeed(1000)
	opponentCar:SetTorqueScale(1.5)

	opponentCar:SetPursuitTarget( playerCar )
	opponentCar:BeginPursuit(0.5)
	
	MISSION.opponentCar = opponentCar;

	gameses:SetPlayerCar( playerCar )
	--gameses:SetViewCar(opponentCar)

	MissionManager:EnableTimeout( true, 98 ) -- enable, time
	
	
	
	--ai:SetMaxTrafficCars(0)
	
	-- here we start
	MissionManager:SetRefreshFunc( MISSION.Update )
end

--------------------------------------------------------------------------------

function MISSION.OnPlayerCollide(plrcar, data)
	if data.hitBy == MISSION.opponentCar then
		MissionManager:SetRefreshFunc( function() 
			return false 
		end ) 

		gameses:SignalMissionStatus( MIS_STATUS_SUCCESS, 0.0 )
	end
end

-- main mission update
function MISSION.Update( delta )

	local playerCar = MISSION.playerCar
	local opponentCar = MISSION.opponentCar

	playerCar:SetFelony(1.0)

	if world:IsValidObject(opponentCar) then
		opponentCar:SetDamage(0.0)
	end

	UpdateCops( playerCar, delta )
	
	return true
end
