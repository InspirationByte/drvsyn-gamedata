"cone.smash"
{
	wave 		"cars/cone.wav";

	pitch		0.9;
	volume		0.6;
	distance	5.0;
	rolloff		3.0;

	channel		"CHAN_ITEM";
};

"wood.smash"
{
	wave 		"objects/woodfence.wav";

	pitch		0.67;
	volume		0.4;
	distance	5.0;
	rolloff		2.5;

	channel		"CHAN_ITEM";
}

"metal.smash"
{
	wave 		"objects/steelbarrier.wav";

	pitch		0.6;
	volume		0.5;
	distance	5.0;

	channel		"CHAN_ITEM";
}

"pole.smash"
{
	wave 		"objects/steelbarrier.wav";

	pitch		1.0;
	volume		0.5;
	distance	5.0;

	channel		"CHAN_ITEM";
}

"cardbox.smash"
{
	wave 		"objects/cardbox.wav";

	pitch		0.6;
	volume		0.5;
	distance	5.0;

	channel		"CHAN_ITEM";
}
"fence.smash"
{
	rndwave
	{
		wave 		"hitsounds/fence_hit1.wav";
		wave 		"hitsounds/fence_hit2.wav";
		wave 		"hitsounds/fence_hit3.wav";
	}

	pitch		1.0;
	volume		1.0;
	distance	5.0;

	channel		"CHAN_ITEM";
}

"hydrant.blow"
{
	wave 		"objects/hydrantblow.wav";

	pitch		1.0;
	volume		1.0;
	distance	6.0;

	channel		"CHAN_ITEM";
};


//-------------------------------------------------------------------------------------------------

"cardbox.smash_light"
{
	rndwave
	{
		wave 	"hitsounds/box_hit_light1.wav";
		wave 	"hitsounds/box_hit_light2.wav";
		wave 	"hitsounds/box_hit_light3.wav";
		wave 	"hitsounds/box_hit_light4.wav";
	}

	pitch		0.8;
	volume		0.7;
	distance	5.0;

	channel		"CHAN_ITEM";
}

"cardbox.smash_medium"
{
	rndwave
	{
		wave 	"hitsounds/box_hit_medium1.wav";
		wave 	"hitsounds/box_hit_medium2.wav";
		wave 	"hitsounds/box_hit_medium3.wav";
		wave 	"hitsounds/box_hit_medium4.wav";
	}

	pitch		0.8;
	volume		0.7;
	distance	5.0;

	channel		"CHAN_ITEM";
}

"cardbox.smash_hard"
{
	rndwave
	{
		wave 	"hitsounds/box_hit_hard1.wav";
		wave 	"hitsounds/box_hit_hard2.wav";
		wave 	"hitsounds/box_hit_hard3.wav";
	}

	pitch		0.8;
	volume		0.7;
	distance	5.0;

	channel		"CHAN_ITEM";
}


//-------------------------------------------------------------------------------------------------

"cone.smash_light"
{
	rndwave
	{
		wave 	"hitsounds/cone_hit_light1.wav";
		wave 	"hitsounds/cone_hit_light2.wav";
	}

	pitch		0.7;
	volume		0.7;
	distance	5.0;

	channel		"CHAN_ITEM";
}

"cone.smash_medium"
{
	rndwave
	{
		wave 	"hitsounds/cone_hit_medium1.wav";
		wave 	"hitsounds/cone_hit_medium2.wav";
	}

	pitch		0.7;
	volume		0.7;
	distance	5.0;

	channel		"CHAN_ITEM";
}

"cone.smash_hard"
{
	rndwave
	{
		wave 	"hitsounds/cone_hit_hard.wav";
	}

	pitch		0.7;
	volume		0.7;
	distance	5.0;

	channel		"CHAN_ITEM";
}

//-------------------------------------------------------------------------------------------------

"plastic.smash_light"
{
	rndwave
	{
		wave 	"hitsounds/plastic_hit_light1.wav";
		wave 	"hitsounds/plastic_hit_light2.wav";
	}

	pitch		0.5;
	volume		1.0;
	distance	5.0;

	channel		"CHAN_ITEM";
}

"plastic.smash_medium"
{
	rndwave
	{
		wave 	"hitsounds/plastic_hit_medium1.wav";
		wave 	"hitsounds/plastic_hit_medium2.wav";
		wave 	"hitsounds/plastic_hit_medium3.wav";
		wave 	"hitsounds/plastic_hit_medium4.wav";
	}

	pitch		0.5;
	volume		0.7;
	distance	5.0;

	channel		"CHAN_ITEM";
}

"plastic.smash_hard"
{
	rndwave
	{
		wave 	"hitsounds/plastic_hit_hard1.wav";
		wave 	"hitsounds/plastic_hit_hard2.wav";
	}

	pitch		0.5;
	volume		0.7;
	distance	5.0;

	channel		"CHAN_ITEM";
}


//-------------------------------------------------------------------------------------------------

"metal.smash_light"
{
	rndwave
	{
		wave 	"hitsounds/metal_hit_light1.wav";
	}

	pitch		0.5;
	volume		1.0;
	distance	5.0;

	channel		"CHAN_ITEM";
}

"metal.smash_medium"
{
	rndwave
	{
		wave 	"hitsounds/metal_hit_medium1.wav";
	}

	pitch		0.5;
	volume		0.7;
	distance	5.0;

	channel		"CHAN_ITEM";
}

"metal.smash_hard"
{
	rndwave
	{
		wave 	"hitsounds/metal_hit_hard1.wav";
	}

	pitch		0.5;
	volume		0.7;
	distance	5.0;

	channel		"CHAN_ITEM";
}

//------------------------------------------------------------------------

"fence.smash_light"
{
	wave 		"hitsounds/fence_hit1.wav";

	pitch		1.0;
	volume		0.3;
	distance	2.0;

	channel		"CHAN_ITEM";
}

"fence.smash_medium"
{
	wave 		"hitsounds/fence_hit2.wav";

	pitch		1.0;
	volume		0.5;
	distance	3.0;

	channel		"CHAN_ITEM";
}

"fence.smash_hard"
{
	wave 		"hitsounds/fence_hit3.wav";

	pitch		1.0;
	volume		1.0;
	distance	5.0;

	channel		"CHAN_ITEM";
}

"dumpster.hit_light"
{
	wave 		"hitsounds/dumpster_hit2.wav";

	pitch		0.4;
	volume		1.0;
	distance	3.0;

	channel		"CHAN_ITEM";
}

"dumpster.hit_medium"
{
	wave 		"hitsounds/dumpster_hit1.wav";

	pitch		0.9;
	volume		0.9;
	distance	3.0;

	channel		"CHAN_ITEM";
}

"dumpster.hit_hard"
{
	wave 		"hitsounds/dumpster_hit1.wav";

	pitch		0.8;
	volume		1.0;
	distance	5.0;

	channel		"CHAN_ITEM";
}
//------------------------------------------------------------------------

"wood.smash_light"
{
	rndwave
	{
		wave 	"hitsounds/wood_hit_light1.wav";
		wave 	"hitsounds/wood_hit_light2.wav";
	}

	pitch		0.5;
	volume		1.0;
	distance	5.0;

	channel		"CHAN_ITEM";
}

"wood.smash_medium"
{
	rndwave
	{
		wave 	"hitsounds/wood_hit_medium1.wav";
	}

	pitch		0.5;
	volume		0.9;
	distance	5.0;

	channel		"CHAN_ITEM";
}

"wood.smash_hard"
{
	rndwave
	{
		wave 	"hitsounds/wood_hit_medium1.wav";
	}

	pitch		0.5;
	volume		1.0;
	distance	5.0;

	channel		"CHAN_ITEM";
}
